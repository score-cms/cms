# score/CmsBundle #

SAZP CMS

## What is this repository for? ##

* Score CSM
* Version: __2.9.0__

### Parts of Score CMS ###

* [base](https://bitbucket.org/score-cms/base)
* [cms](https://bitbucket.org/score-cms/cms)

## How do I get set up? ##

* Create project 5.4
    ```
     composer create-project symfony/skeleton:"5.4.*" my_project_name
    ```

* Add .htaccess to `public/` dir. - [example](https://gist.github.com/Guibzs/a3e0b3ea4eb00c246cda66994defd8a4 "GitHub")

* Add repositories to `composer.json`

    ``` 
        "repositories": [
            {
              "type": "vcs",
              "url": "git@bitbucket.org:score-cms/base.git"
            },
            {
              "type": "vcs",
              "url": "git@bitbucket.org:score-cms/cms.git"
            }
        ]
    ```

* Install score bundles

    ```
    composer require score/base
    composer require score/cms
    ```

* Add routes to `config/routes/annotations.yaml`

    ```
    score_base:
        resource: "@ScoreBaseBundle/Controller/"
        prefix: /
        type: annotation
    
    score_cms:
        resource: "@ScoreCmsBundle/Controller/"
        prefix: /
        type: annotation
    ```
    
* Update `providers`, `firewalls` and `access_control` in security `config/packages/security.yaml` (first firewall should be admin)

    ```  
    security:       
        providers:
            users_in_memory: { memory: null }
            cms_provider:
                entity:
                    class: 'Score\CmsBundle\Entity\User'        
        firewalls:
            admin:
                lazy: true
                provider: cms_provider
                pattern: ^/admin
                form_login:
                    login_path:  admin_login
                    check_path:  admin_login
                    default_target_path: admin_start
                logout:
                    path: admin_logout
                    target: admin_login
            main:
                lazy: true
                provider: users_in_memory

        access_control:
            - { path: ^/admin/login$, roles: IS_AUTHENTICATED_ANONYMOUSLY }
            - { path: ^/admin/, roles: ROLE_CMS }
    ```

* Change `default_locale` and `fallbacks` in file `config/packages/translation.yml` from `en` to `sk`.

* Add form theme for twig in `config/packages/twig.yml`

    ```
    twig:
        form_themes: ['bootstrap_3_layout.html.twig']
    ```   

* Update content in file `src/DataFixtures/AppFixtures.php` 

    ```
    class AppFixtures extends Fixture implements \Doctrine\Common\DataFixtures\DependentFixtureInterface {
        public function getDependencies() { return ['Score\CmsBundle\DataFixtures\AppFixtures', 'Score\CmsBundle\DataFixtures\UserFixtures'];}
        public function load(ObjectManager $manager) {}
    }
    ```

* In `.env` file  update `DATABASE_URL` and `MAILER_DSN` (You need to use your parameters, this is just example). Set an email parameter in `config/services.yaml` with that e-mail and name. (Most things will work without a mailer too.)

    ```
    # .env
    MAILER_DSN=smtp://user:pass@smtp.example.com:port
    DATABASE_URL="mysql://root:@127.0.0.1:3306/mydbname?serverVersion=5.7"


    # config/services.yaml
    parameters:
        email: ['info@sazp.sk', 'SAŽP']
    ```


* Run

    ```
    php bin/console doctrine:database:create
    php bin/console make:migration
    php bin/console doctrine:migrations:migrate
    ```

* Run (then type [yes])

    ```
    php bin/console doctrine:fixtures:load
    ```

* Now it should work. Go to `/admin/login` sign in as  `admin/admin`

### CMS TEXT - options ###

* In any html block you can use editable text `CMS_TEXT(code)`.
    ```
    ... <div>CMS_TEXT(idsk-intro-title)</div> ...
    ```

* In any twig template you can use filter `cms_text`.
    ```
    ... {{ 'idsk-intro-title' | cms_text }} ...
    ... {{ 'idsk-intro-title' | cms_text | raw }} ...
    ```

### CMS WIDGET ###

* In any html block you can use widget `CMS_WIDGET(widget_code, widget_value_code)`.
    ```
    ... <div>CMS_WIDGET(person, person1)</div> ...
    ```

* In any twig template you can use filter `cms_widget`.
    ```
    ... {{ "person, person1" | cms_widget | raw }} ...
    ```

    #### CREATE FIRST WIDGET ####

* In the widget JSON, define which values will be used. (widget code: `person`)
    ```
    ... 	
        [{  
            "code": "title", "label": "Person name", "type": "text", "min": 5, "max": 25
        },{
            "code": "description", "label": "Person summary", "help": "max 250", "type": "richtext", "min": 7, "max": 250, "rows": 10, "editor": "cke5_table"
        }]
    ...
    ```

* In the widget HTML, define where each of the values should be displayed. (widget code: `person`)
    ```
    ... 	
        <div class="col-md-6 col-lg-5 order-md-1 ">
            <h4><span class="title"><a href="blog.html"> CMS_WIDGET_VALUE(title)</a></span></h4>
            CMS_WIDGET_VALUE(description)
        </div> 
    ...
    ```
* Add a new item to the widget and set its code to `person1`

* Now the editor has access to edit person texts and does not destroy the html structure.


## Advanced options  ##

* If you want a search form, set a global variable for twig in `config/packages/twig.yaml` and proccess the search in `/vyhladavanie` path. Default is just basic and very slow. 

    ```
    twig:
        globals:
            search_form: true                   # shows search input in page header
            hide_homepage_intro_block: true     # homepage skips rendering introblock
            hide_pages_heading: true            # disable to show page name as page heading
            page_container_fluid: true          # page content on full screen width
            wavex_main_menu_from_sections: false
            # color one of [red, brown, yellow, green1, green2, green3, green4, blue1, blue2, purple, pink]
            wavex_template_color: 'green4'
            # editor one of [markdown, cke4, cke5_full, cke5_table,  cke5_simple, cke5_basic]
            cms_editor: 'cke5_full'
            cms_page_form_show_all: true # shows teaser and image
            # urlArticlePrefix: 'clanky'
            # urlEventPrefix: 'udalosti'
            # urlDocumentPrefix: 'dokument'
    ```

* If you want a custom template, set a parameter in `config/services.yaml` with path to custom layout file. Use blocks as in example in  `vendor/score/cms/Resources/views/Public/example_layout.html.twig`. You can also use one of `['default', 'idsk', 'wavex']`.

    ```
    parameters:
        template: 'base.html.twig'
    ```

* If you want to modify any of the score cms templates, you can overwrite it by passing modified file to a special path as in this example.

    ```
    # original file
    vendor\score\cms\src\Resources\views\Public\default\menuTree.html.twig
    # custom modified file
    templates\bundles\ScoreCmsBundle\Public\default\menuTree.html.twig
    ```

* You can reset any parameter in `config/services.yaml`.

    ```
    parameters:
        template: 'default'
        email: 'info.sazp@gmail.com'  # format 'info@sazp.sk' OR ['info@sazp.sk', 'SAŽP']
        mailer_max_attempts: 3 # How many times should it try to send
        block_accordion_allow_open: false # Allow input in accordion form
        block_accordion_allow_summary: true # Allow input in accordion form
        block_accordion_max_items: 8
        block_accordion_hide_advanced: true
        article_public_upload_directory: '/cms/article/icon'
        event_public_upload_directory: '/cms/event/icon'
        gallery_public_upload_directory: '/cms/gallery'
        page_public_upload_directory: '/cms/page/icon' 
        cms_gallery_thumbnail_cut: false
        # Defines the way how to proccess file request ['load', 'redirect', 'open', 'create']
        cms_gallery_image_strategy: 'load'
        ckeditor_public_upload_directory: '/cms/cke_images'
        ckeditor_image_sizes: [200, 400, 800, 1920]
        document_public_upload_directory: '/cms/documents'
        document_max_uploaded_file_size: 5 # 5MB
        document_file_types: [
            'image/png',
            'image/jpeg',
            #'application/postscript',                                                   # .eps, .ps
            'application/pdf',
            'application/msword',                                                       # .doc, .dot
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',  # .docx
            'application/vnd.ms-excel',                                                 # .xls, .xlt, .xla
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',        # .xlsx
            'application/zip'
        ]
        cms_media_directories: 
            'gallery': '/cms_gallery/cf0723543be/preview/540'
            'nature': '/images/nature'
        access_checking: true

        cms_thumbnail_cut: false # default for others (next lines)
        # you can specify explicitly for each cms part
        cms_gallery_thumbnail_cut: false
        cms_article_thumbnail_cut: false
        cms_document_thumbnail_cut: false
        cms_event_thumbnail_cut: false
        cms_page_thumbnail_cut: false
  
        # menu ID : page ID // filters pages tree by parentID for this menu
        cms_menu_page_connection:
            1: 1

    ```

* You can use mailer to send emails in controller.
    ```
    public function emailAction(\Score\CmsBundle\Services\MailerManager $mailerManager) {
        $body = $this->renderView('pathtomailtemplate.html.twig',['name' => 'My name']);
        $mailerManager
            ->setTo('some@email.sk')
            ->setSubject("Upozornenie")
            ->setBody($body)
            ->send();
    }
    ```


* Wavex template `config/packages/twig.yaml`

    ```
    twig:
        globals:
            wavex_main_menu_from_sections: true
            # color one of [red, brown, yellow, green1, green2, green3, green4, blue1, blue2, purple, pink]
            wavex_template_color: 'blue2'
    ```

* Run if needed.

    ```
    php bin/console cache:clear
    php bin/console cache:clear --env=prod
    php bin/console assets:install
    ```



## Versions updates ##

### 2.9.0 ###
* Categories for events. (needs migration).
* Article, event – unique slug. 

### 2.8.0 ###
* Api for datatables + (page - parent and redirect , document - relevants, blocks for parts). Trash bin. Menu add/remove child.
* Document level, year and params. Unique seoId. (needs migration).

### 2.7.0 ###
* Menu is connected to page
* Accordion has type and header (parameter: block_accordion_hide_advanced)
* If you use custom templates, heck all custom templates for menu! (item.node.name -> item.name, ...) check custom templates for page editing (form.menuName).
* For old compatibility (`score-cms` 2.6.* -> 2.7.0) after migration go to `/admin/menu/each-menu-to-page-name` all page menu items will connect with pages and menu names will be added to pages.

### 2.6.0 ###
* page redirect to another page, userId
* metatag date -> datetype input
* edit block with referer
* page image

### 2.5.* ###
* page thumbnail
* parameter thumbnail cut
* page tree to page-edit
 
### 2.5.0 ###
* `score-base` `2.2.*` is needed.
* Blocks for each cms part (page, article, event, document).
* for old compatibility (`score-cms` 2.4.* -> 2.5.0) after migration go to `/admin/page/pageblock-to-blockjoint` old pages will be converted to new structure.
* Access management for each user.
* Metatags for each cms part.
* Added trash bin (basic user cannot remove).
* Added role `ROLE_TOP_MANAGER` has full access.

### 2.4.0 ###
* Documents, ckeditor update, media

### 2.2.2 ###
* Ckeditor 5 + image uploader

### 2.2.1 ###
* Private methods to protected, form custom block

### 2.2.0 ###
* Changes from 1.4 , markdown editor

### 2.1.3 ###
* Fix classes in services and security

### 2.1.0 ###
* Wavex template

### 2.0.0 ###
* symfony 5.4 (from 4.4)

### 1.1.0 ###
* form send mail notification
* gallery loading real images, added subdirs
* cms_text in templates and html block (option)
* templates without calling render-controller

### 1.0.0 ###
* bundles block and page integrated to cms bundle, added article, event, form, gallery, accordion, logs and mailer

### Authors ###

* Marek Hubáček
* Jozef Hort