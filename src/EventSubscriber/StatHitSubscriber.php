<?php

namespace Score\CmsBundle\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpFoundation\Session\Session;

class StatHitSubscriber implements EventSubscriberInterface
{
    protected $statManager;

    public function __construct($statManager)
    {
        $this->statManager = $statManager;
    }

     /**
     * Get the value of statManager
     */ 
    public function getStatManager()
    {
        return $this->statManager;
    }

    /**
     * Set the value of statManager
     *
     * @return  self
     */ 
    public function setStatManager($statManager)
    {
        $this->statManager = $statManager;

        return $this;
    }
    
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER  => 'addHit',
        ];
    }


    public function addHit($event)
    {
       //$uri = $
       $controller = $event->getController();

       if(!is_array($controller))
       {
           return;
       }
       $hitTriggers = [
           ['controller' => \Score\CmsBundle\Controller\Page\PublicController::class,'action' => 'subpageAction'],
           ['controller' => \Score\CmsBundle\Controller\Page\PublicController::class,'action' => 'cookiesAction'], 
           ['controller' => \Score\CmsBundle\Controller\Page\PublicController::class,'action' => 'searchAction'],  
           ['controller' => \Score\CmsBundle\Controller\Article\PublicController::class,'action' => 'listAction'], 
           ['controller' => \Score\CmsBundle\Controller\Article\PublicController::class,'action' => 'detailAction'],
           ['controller' => \Score\CmsBundle\Controller\Event\PublicController::class,'action' => 'listAction'], 
           ['controller' => \Score\CmsBundle\Controller\Event\PublicController::class,'action' => 'detailAction'],
        ];

        
        foreach($hitTriggers as $hitTrigger)
        {
            if(is_a($controller[0],$hitTrigger['controller']) and $controller[1] == $hitTrigger['action'])
            {
                $parameters = $event->getRequest()->attributes->all();
               
                $statData = ['source_type' => $parameters['_route'], 'source_title' => '','source_id' => ''];
                if($controller[1] == 'searchAction')
                {
                    $statData['stat_type'] = 'search';
                    $statData['source_id'] =  $event->getRequest()->query->get('search');
                }
                else
                {
                    $statData['stat_type'] = 'request';
                    if(array_key_exists('slug',$parameters))
                    {
                        $statData['source_id'] = $parameters['slug'];
                    }

                    if(array_key_exists('seoid',$parameters))
                    {
                        $statData['source_id'] = $parameters['seoid'];
                    }
                }
//                $dot = strpos($statData['source_id'], '.');
//                $slash = strpos($statData['source_id'], '/');

//                if ($dot === false and $slash === false) {
                    $this->getStatManager()->addHit($statData);
//                }
            }
        }
    }

   
}