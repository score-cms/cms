<?php

namespace Score\CmsBundle\EventListener;

use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\Persistence\ManagerRegistry;

use Score\CmsBundle\Entity\ChangesLog;
use DateTimeInterface;
use Score\CmsBundle\Entity\Stat;
use Score\CmsBundle\Entity\User;

// use Score\CmsBundle\Entity\Block\Block;
// use Score\CmsBundle\Entity\Group;
// use Score\CmsBundle\Entity\Menu;
// use Score\CmsBundle\Entity\Page\Page;
// use Score\CmsBundle\Entity\Article\Article;
// use Score\CmsBundle\Entity\Event\Event;
// use Score\CmsBundle\Entity\Form\Webform;
// use Score\CmsBundle\Entity\Form\WebformField;
// use Score\CmsBundle\Entity\Gallery\Gallery;
// use Score\CmsBundle\Entity\Gallery\GalleryImage;

class ChangesListener {
    protected $tokenStorage;
    protected ManagerRegistry $registry;
    protected $uniqId = '';
    protected $logs = [];

    public function __construct($tokenStorage, ManagerRegistry $registry) {
        $this->tokenStorage = $tokenStorage;
        $this->registry = $registry;
        $this->uniqId = uniqid();
    }

    public function postPersist(LifecycleEventArgs $args) {
        $this->processEvents('persist', $args);
    }

    public function preUpdate(LifecycleEventArgs $args) {
        $this->processEvents('update', $args);
    }

    public function preRemove(LifecycleEventArgs $args) {
        $this->processEvents('remove', $args);
    }

    // zaznamenava iba zmeny v hodnotach, nezaznamena odstranenie po pridani bez update-u, nezaznamena zmeny v prepajaniach entit - zmeny blokov...
    public function postFlush(PostFlushEventArgs $args) {
        if (empty($this->logs))
            return;

        $emNormal = $this->registry->getManagerForClass(ChangesLog::class);

        foreach ($this->logs as $log) {
            $emNormal->persist($log);
        }

        $this->logs = [];
        $emNormal->flush();
    }

    protected function processEvents($event, $args) {
        $className = get_class($args->getObject());
        if (!in_array($className, [ChangesLog::class, Stat::class]))
            $this->logEvent($className, $event, $args);
    }


    protected function logEvent($className, $event, $args) {
        $entity = $args->getObject();
        if (!$this->tokenStorage->getToken()) { // FIxtures - no User
            $user = new User();
            $user->setId(0);
            $user->setEmail('fixtures@cms.sk');
        } else if (is_string($this->tokenStorage->getToken()->getUser())) {
            $user = new User();
            $user->setId(0);
            $user->setEmail('no-user@cms.sk');
        } else {
            $user = $this->tokenStorage->getToken()->getUser();
        }
        if ($event === 'update') {
            $changes = $args->getEntityChangeSet();

            foreach ($changes as $fieldName => $values) {
                $log = new ChangesLog();

                $oldV = $this->getStringValue($values[0], $log);
                $newV = $this->getStringValue($values[1], $log);

                $log
                    ->setUserId($user->getId())
                    ->setUserEmail($user->getEmail())
                    ->setEvent($event)
                    ->setIdentifier($this->uniqId)
                    ->setEntity($className)
                    ->setEntityId($entity->getId())
                    ->setField($fieldName)
                    ->setOldValue($oldV)
                    ->setNewValue($newV);
                $this->logs[] = $log;
            }
        } else {
            $log = new ChangesLog();
            $log
                ->setUserId($user->getId())
                ->setUserEmail($user->getEmail())
                ->setEvent($event)
                ->setIdentifier($this->uniqId)
                ->setEntity($className)
                ->setEntityId($entity->getId());
            $this->logs[] = $log;
        }
    }

    protected function getStringValue($v, $log) {
        $r = "";
        if (is_object($v)) {
            $log->setCanRevert(false);
            if ($v instanceof DateTimeInterface) {
                $r = $v->format('c');
            } else {
                $r = strval($v->getId());
            }
        } else {
            $r = strval($v);
            //$log->setCanRevert( $log->getCanRevert() !== false && substr( $r, 0, 1 ) !== "{" );
            $log->setCanRevert($log->getCanRevert() !== false);
        }
        return $r;
    }

}
