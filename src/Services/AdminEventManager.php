<?php

namespace Score\CmsBundle\Services;

use Score\BaseBundle\Services\BaseAdminManager as BaseAdminManager;
use Score\CmsBundle\Entity\Multisite\SiteItemEvent;

class AdminEventManager extends BaseAdminManager {

    protected $multisiteManager;
    
    function __construct($em, $repository,$multisiteManager)
    {
        $this->db_provider = $em;
        $this->repositiory = $this->db_provider->getManager()->getRepository($repository);
        $this->multisiteManager = $multisiteManager;
    }
   

    public function handleMultisite($event)
    {
        //remove old
        $repository = $this->db_provider->getManager()->getRepository(SiteItemEvent::class);
        $qd = $repository->createQueryBuilder('t')->delete();
        $qd->where('t.event = :itemId');
        $qd->setParameter('itemId', $event->getId());
        $query = $qd->getQuery();
        $query->getResult();
        
        if(null != $event->getMultisite())
        {
            $multisites = [];
            foreach($event->getMultisite() as $site)
            {
                $siteItemEvent = new SiteItemEvent();
                $siteItemEvent->setSite($site);
                $siteItemEvent->setEvent($event);
                $multisites[] = $siteItemEvent; 
            }

            $event->setSiteItemEvent($multisites);
        }
        else
        {
            $event->setSiteItemEvent([]);
        }
    }
   
   
    public function getUploadDir($params, $imageManager = null)
    {

    if ($imageManager) {
        $sub = '/' . date('Y-m') . '/' . date('Y-m-d');
        $baseUploadDir = $params->get("event_upload_directory") . $sub;
        $publicUploadDir = $params->get('event_public_upload_directory') . $sub;
        $imageManager->createPath($baseUploadDir);
        return [
            'absolute_path' => $baseUploadDir,
            'public_path' => $publicUploadDir
        ];
    }
    // predpokladam ze nasledujuci kod je zbytocny

    // $baseDir = $params->get('kernel.project_dir').'/public/';
    // $baseUploadDir = $params->get('event_upload_directory');
    // $publicUploadDir = $params->get('event_public_upload_directory').'/'.date('Y-m').'/'.date('Y-m-d');

    // if(!file_exists( $baseDir.'event'))
    // {
    //     mkdir($baseDir.'event');
    // }

    // if(!file_exists( $baseDir.'event/icon'))
    // {
    //     mkdir($baseDir.'event/icon');
    // }


    // if(!file_exists( $baseUploadDir.'/'.date('Y-m')))
    // {
    //     mkdir($baseUploadDir.'/'.date('Y-m'));
    // }
    // if(!file_exists( $baseUploadDir.'/'.date('Y-m').'/'.date('Y-m-d')))
    // {
    //     mkdir($baseUploadDir.'/'.date('Y-m').'/'.date('Y-m-d'));
    // }

    // return ['absolute_path' => $baseUploadDir.'/'.date('Y-m').'/'.date('Y-m-d'),'public_path' => $publicUploadDir];
   }

   

}
