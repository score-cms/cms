<?php

namespace Score\CmsBundle\Services;


use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Expr\Comparison;
use Score\BaseBundle\Entity\BaseEntity;
use Score\CmsBundle\Entity\Access\Access;
use Score\CmsBundle\Entity\Access\AccessArticle;
use Score\CmsBundle\Entity\Access\AccessDocument;
use Score\CmsBundle\Entity\Access\AccessEvent;
use Score\CmsBundle\Entity\Access\AccessPage;
use Score\CmsBundle\Entity\Article\Article;
use Score\CmsBundle\Entity\Document\Document;
use Score\CmsBundle\Entity\Event\Event;
use Score\CmsBundle\Entity\Page\Page;
use Score\CmsBundle\Entity\User;

class AccessManager {

//    protected $em;
//    protected $pageManager;

//    function __construct($doctrine, $pageManager) {
//        $this->em = $doctrine->getManager();
//        $this->pageManager = $pageManager;
//    }

    public static $pageRoles = [];


    protected function getConnectionAccess(User $user, BaseEntity $entity, $createNew = false) {
        $accesses = $entity->getAccesses()->matching((new Criteria())->where(new Comparison('user', Comparison::EQ, $user)));
        $access = $accesses->first();
        if ($access) {
            return $access;
        }

        if (!$createNew) {
            return null;
        }

        if (is_a($entity, Page::class)) {
            $access = new AccessPage();
            $entity->addAccess($access);
        } elseif (is_a($entity, Article::class)) {
            $access = new AccessArticle();
            $entity->addAccess($access);
        } elseif (is_a($entity, Event::class)) {
            $access = new AccessEvent();
            $entity->addAccess($access);
        } elseif (is_a($entity, Document::class)) {
            $access = new AccessDocument();
            $entity->addAccess($access);
        }
        $user->addAccess($access);
        return $access;
    }

    // na to aby to prebehlo staci persist entity alebo usera
    public function addAccessFor(User $user, $entity, $params = []) {

        $access = $this->getConnectionAccess($user, $entity, true);
        foreach ($params as $key => $param) {
            $access->setParamByKey($key, $param);
        }
        return $access;
    }

    // na to aby to prebehlo staci persist entity alebo usera
    public function removeAccessFor(User $user, $entity) {

        $access = $this->getConnectionAccess($user, $entity);
        if ($access) {
            $entity->removeAccess($access);
            $user->removeAccess($access);
        }
        return $access;
    }

    // na to aby to prebehlo staci persist page alebo usera
    public function updatePageAccess(User $user, Page $page, $hasAccess, $isHeritable, $params = []) {

        if ($hasAccess) { // pridanie pristupu
            $this->addAccessFor($user, $page, $params)
                ->setIsHeritable($isHeritable);
            if ($isHeritable) { // pridanie pristupu vsetkym podstrankam rovnako
                foreach ($page->getAllChildren() as $child) {
                    $this->addAccessFor($user, $child, $params)
                        ->setIsHeritable($isHeritable);
                }
            }
        } else { // odstranenie pristupu
            $this->removeAccessFor($user, $page);
            if ($isHeritable) { // odstranenie pristupu aj vsetkym podstrankam
                foreach ($page->getAllChildren() as $child) {
                    $this->removeAccessFor($user, $child);
                }
            }
        }
        $access = $this->getConnectionAccess($user, $page);
        return $access;
    }

    public function updateEntityAccess(User $user, BaseEntity $entity, $hasAccess, $params = []) {

        if ($hasAccess) { // pridanie pristupu
            $this->addAccessFor($user, $entity, $params);
        } else { // odstranenie pristupu
            $this->removeAccessFor($user, $entity);
        }
        $access = $this->getConnectionAccess($user, $entity);
        return $access;
    }

    // na to aby to prebehlo staci persist page
    public function inheritPageAccess(Page $page) {
        $parentPage = $page->getParentPage();
        if ($parentPage) {
            foreach ($parentPage->getAccesses() as $parentAccess) {
                if ($parentAccess->getIsHeritable()) {
                    $access = new AccessPage();
                    $access->setIsHeritable($parentAccess->getIsHeritable())
                        ->setRole($parentAccess->getRole())
                        ->setParams($parentAccess->getParams())
                        ->setUser($parentAccess->getUser());
                    $page->addAccess($access);
                }
            }
        }
        return $page;
    }

    public function hasUserAccessTo(User $user, $entity, $fullAccess = false) {

        if (in_array("ROLE_ADMIN", $user->getRoles())) {
            return true;
        }
        if (in_array("ROLE_TOP_MANAGER", $user->getRoles())) {
            return true;
        }
        if (!$this->hasUserEntityRole($user, $entity)) {
            return false;
        }

        $access = $this->getConnectionAccess($user, $entity);
        if ($access) {
            if ($fullAccess) {
                return $access->getParamByKey("full_access");
            }
            return true;
        }

        return false;
    }

    public function canUserCreateSubPage(User $user, Page $entity) {
        return $this->canUser($user, $entity, "create");
    }

    public function canUserDeletePage(User $user, Page $entity) {
        return $this->canUser($user, $entity, "delete");
    }

    public function canUserEditAccessToPage(User $user, Page $entity) {
        return $this->canUser($user, $entity, "full_access");
    }

    public function canUserEditAccessToEntity(User $user, BaseEntity $entity) {
        return $this->canUser($user, $entity, "full_access");
    }

    public function canUserDeleteEntity(User $user, BaseEntity $entity) {
        return $this->canUser($user, $entity, "delete");
    }

    protected function canUser(User $user, $entity, $action) {
        if (in_array("ROLE_ADMIN", $user->getRoles()) || in_array("ROLE_TOP_MANAGER", $user->getRoles())) {
            return true;
        }
        if (!$this->hasUserEntityRole($user, $entity)) {
            return false;
        }
        $access = $this->getConnectionAccess($user, $entity);
        if ($access) {
            if ($action === "create") {
                return $access->getParamByKey("can_create", false);
            } elseif ($action === "delete") {
                return $access->getParamByKey("can_delete", false);
            } elseif ($action === "full_access") {
                return $access->getParamByKey("full_access", false);
            }
        }
        return false;
    }

    protected function hasUserEntityRole(User $user, BaseEntity $entity) {
        return (
            (is_a($entity, Page::class) && in_array("ROLE_PAGE_MANAGER", $user->getRoles()))
            || (is_a($entity, Article::class) && in_array("ROLE_ARTICLE_MANAGER", $user->getRoles()))
            || (is_a($entity, Event::class) && in_array("ROLE_EVENT_MANAGER", $user->getRoles()))
            || (is_a($entity, Document::class) && in_array("ROLE_DOCUMENT_MANAGER", $user->getRoles()))
        );
    }
}

