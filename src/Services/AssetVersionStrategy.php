<?php
namespace Score\CmsBundle\Services;

use Symfony\Component\Asset\VersionStrategy\VersionStrategyInterface;

class AssetVersionStrategy implements VersionStrategyInterface
{



    public function getVersion($path)
    {
        return date('YmdHis');
        /*
        if (!is_array($this->hashes)) {
            $this->hashes = $this->loadManifest();
        }

        return isset($this->hashes[$path]) ? $this->hashes[$path] : '';
         *
         */
    }

    public function applyVersion($path)
    {

        $version = $this->getVersion($path);

        if ('' === $version) {
            return $path;
        }

        return $path.'?ver='.$version;

    }


}
