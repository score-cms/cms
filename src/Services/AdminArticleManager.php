<?php

namespace Score\CmsBundle\Services;

use Score\BaseBundle\Services\BaseAdminManager as BaseAdminManager;
use Score\CmsBundle\Entity\Multisite\SiteItemArticle;

class AdminArticleManager extends BaseAdminManager {

    protected $multisiteManager;
    
    function __construct($em, $repository,$multisiteManager)
    {
        $this->db_provider = $em;
        $this->repositiory = $this->db_provider->getManager()->getRepository($repository);
        $this->multisiteManager = $multisiteManager;
    }
   

    public function handleMultisite($article)
    {
        //remove old
        $repository = $this->db_provider->getManager()->getRepository(SiteItemArticle::class);
        $qd = $repository->createQueryBuilder('t')->delete();
        $qd->where('t.article = :itemId');
        $qd->setParameter('itemId', $article->getId());
        $query = $qd->getQuery();
        $query->getResult();
        
        if(null != $article->getMultisite())
        {
            $multisites = [];
            foreach($article->getMultisite() as $site)
            {
                $siteItemArticle = new SiteItemArticle();
                $siteItemArticle->setSite($site);
                $siteItemArticle->setArticle($article);
                $multisites[] = $siteItemArticle; 
            }

            $article->setSiteItemArticle($multisites);
        }
        else
        {
            $article->setSiteItemArticle([]);
        }
    }
   
    public function getUploadDir($params, $imageManager = null)
    {

    if ($imageManager) {
        $sub = '/' . date('Y-m') . '/' . date('Y-m-d');
        $baseUploadDir = $params->get("article_upload_directory") . $sub;
        $publicUploadDir = $params->get('article_public_upload_directory') . $sub;
        $imageManager->createPath($baseUploadDir);


        if(!file_exists($baseUploadDir))
        {
            $baseDir = $params->get('kernel.project_dir').'/public/';
            if(!file_exists( $baseDir.'cms'))
            {
                 mkdir($baseDir.'cms');
            }

            if(!file_exists( $baseDir.'cms/article'))
            {
                 mkdir($baseDir.'cms/article');
            }

            if(!file_exists( $params->get("article_upload_directory")))
            {
               mkdir($params->get("article_upload_directory"));
            }

            if(!file_exists( $params->get("article_upload_directory").'/'.date('Y-m')))
            {
                 mkdir($params->get("article_upload_directory").'/'.date('Y-m'));
            }

            if(!file_exists( $baseUploadDir.'/'.date('Y-m').'/'.date('Y-m-d')))
            {
                 mkdir($params->get("article_upload_directory").'/'.date('Y-m').'/'.date('Y-m-d'));
            }
        }

        return [
            'absolute_path' => $baseUploadDir,
            'public_path' => $publicUploadDir
        ];

        
    }
    // predpokladam ze nasledujuci kod je zbytocny

    // $baseDir = $params->get('kernel.project_dir').'/public/';
    // $baseUploadDir = $params->get('article_upload_directory');
    // $publicUploadDir = $params->get('article_public_upload_directory').'/'.date('Y-m').'/'.date('Y-m-d');

    // if(!file_exists( $baseDir.'article'))
    // {
    //     mkdir($baseDir.'article');
    // }

    // if(!file_exists( $baseDir.'article/icon'))
    // {
    //     mkdir($baseDir.'article/icon');
    // }

    // if(!file_exists( $baseUploadDir.'/'.date('Y-m')))
    // {
    //     mkdir($baseUploadDir.'/'.date('Y-m'));
    // }
    // if(!file_exists( $baseUploadDir.'/'.date('Y-m').'/'.date('Y-m-d')))
    // {
    //     mkdir($baseUploadDir.'/'.date('Y-m').'/'.date('Y-m-d'));
    // }

    // return ['absolute_path' => $baseUploadDir.'/'.date('Y-m').'/'.date('Y-m-d'),'public_path' => $publicUploadDir];
   }
}
