<?php

namespace Score\CmsBundle\Services;

use Score\BaseBundle\Entity\BaseEntity;
use Score\CmsBundle\Entity\User;

class MetatagManager {

    public function updateMetatagsFor(BaseEntity $entity, User $user) {

//        update user for new metatags
        foreach ($entity->getMetatags() as $metatag) {
            if (!$metatag->getUserId()) {
                $metatag->setUserId($user->getId());
            }
        }

//        find visible metatags for this entity and check for duplicity
        $visibleMetatags = $entity->getMetatags()->filter(function ($m) {
            return $m->getVisibility();
        });

        $isDuplicity = false;
        $arr = [];
        foreach ($visibleMetatags as $m) {
            $key = $m->getMetatagPattern()->getId();
            if (array_key_exists($key, $arr)) {
                $arr[$key][] = $m;
                $isDuplicity = true;
            } else {
                $arr[$key] = [$m];
            }
        }

//        only last one from visibles of each pattern stays visible
        foreach ($arr as $ms) {
            foreach ($ms as $k => $m) {
                $m->setVisibility($k === count($ms) - 1);
            }
        }

        return $isDuplicity;
    }

}

