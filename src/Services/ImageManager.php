<?php

namespace Score\CmsBundle\Services;

use Score\BaseBundle\Services\ImageManager as BaseImageManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageManager extends BaseImageManager
{

    public function isImage(UploadedFile $uploadedFile) {
        //overenie ci ide o obrazok
        $arr  = explode("/", $uploadedFile->getClientMimeType());
        return ($arr && $arr[0] === "image");
    }

    public function createImageFrom($origFile, $size, $ext)
    {
        $image = parent::resize($origFile, null, $size, null, true);
        return $image->show($ext);
    }

    public function readFileParams($origFile)
    {
        $params = getimagesize($origFile);
        $params["x"] = $params[0];
        $params["y"] = $params[1];
        $params["ratio"] =  $this->getBaseRatio($params[0], $params[1]);
        $params["ratioOrig"] =  $this->getBaseRatio($params[0], $params[1], false);
        return $params;
    }

    public function scaleTo($origFile, $newFile, $width = 100, $height = null)
    {
        return parent::scaleTo($origFile, $newFile, $width, $height);
    }

    public function resize($origFile, $newFile, $maxWidth = null, $maxHeight = null, $fitIn = false)
    {
        //$newFile = substr_replace($origFile, '-temp.', strrpos($origFile, '.'), 1);
        return parent::resize($origFile, $newFile, $maxWidth, null, true);
  
    }

    public function removeAll($path) {
        if (is_dir($path)) {
            foreach(glob($path . '/*') as $file) {
                if (is_dir($file)) {
                    $this->removeAll($file);
                } else {
                    unlink($file);
                }
            }
            rmdir($path);
        } else {
            unlink($path);
        }
    }
}
