<?php

namespace Score\CmsBundle\Services;
use Score\BaseBundle\Services\AdjacencyArrayTreeManager;

class PageTreeManager extends AdjacencyArrayTreeManager {

    protected $pageParentChoices;

    public function getPageParentChoices()
    {
        return $this->pageParentChoices;
    }

    public function setPageParentChoices($pageParentChoices)
    {
        $this->pageParentChoices = $pageParentChoices;
    }

    /**
     *
     * @param type $id
     * @param type $parent_record
     * @return type
     */
    public function buildPageParentChoices($id,$options = array('depth' => 100))
    {
        if($this->hasChildren($id))
        {
            $childs = $this->getChildren($id);


            if(count($childs) > 0)
            {
                foreach($childs as $child)
                {
                    $this->pageParentChoices[$child->getId()]= $child;

                    if($options['depth'] > $child->getLvl())
                    {
                        if($this->hasChildren($child->getId()))
                        {
                             $this->buildPageParentChoices($child->getId());
                        }
                    }
                }
            }
        }
         return $this->pageParentChoices;
    }

}
