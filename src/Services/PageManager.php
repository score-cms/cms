<?php

namespace Score\CmsBundle\Services;

use Score\BaseBundle\Services\BaseManager as BaseManager;
use Score\CmsBundle\Entity\Menu;

class PageManager extends BaseManager {

    public function getPagesByParent($parent)
    {
        return $this->getRepository()->findBy(array('parentPage' => $parent));
    }

    public function getPageById($id)
    {
        return $this->getRepository()->find($id);
    }
    public function getPageBySeoId($seo_id)
    {
        return $this->getRepository()->findOneBy(array('seoId' => $seo_id));
    }

//    public function getMainMenu()
//    {
//         $repository = $this->getDbProvider()->getRepository(Menu::class);
//
//
//        return $repository->findBy(
//                        array('parentId' => 1),
//                        array('sortOrder' => 'ASC')
//        );
//    }

//    public function getBlockjoints($page)
//    {
//        $blocks =  $this->getRepository()->findBlockJoints($page);
//
//        $groups = array('news' => array());
//        foreach($blocks as $block)
//        {
//            $groups[$block->getType()][] = $block;
//        }
//
//
//        return $groups;
//
//    }
}
