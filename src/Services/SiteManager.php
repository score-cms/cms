<?php

namespace Score\CmsBundle\Services;

use Score\BaseBundle\Services\BaseAdminManager;

class SiteManager extends BaseAdminManager {

    function __construct($em, $repository)
    {
        $this->db_provider = $em;
        $this->repositiory = $this->db_provider->getManager()->getRepository($repository);
    }

}

