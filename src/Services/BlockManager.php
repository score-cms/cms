<?php


namespace Score\CmsBundle\Services;

use Doctrine\Common\Collections\ArrayCollection;
use Score\BaseBundle\Services\BaseManager;
use Score\CmsBundle\Entity\Article\Article;
use Score\CmsBundle\Entity\Block\Block;
use Score\CmsBundle\Entity\Block\BlockJoint;
use Score\CmsBundle\Entity\Block\BlockJointArticle;
use Score\CmsBundle\Entity\Block\BlockJointDocument;
use Score\CmsBundle\Entity\Block\BlockJointEvent;
use Score\CmsBundle\Entity\Document\Document;
use Score\CmsBundle\Entity\Event\Event;


class BlockManager extends BaseManager {
    public function getGroups() {
        $groups = [
            'static' => 'score.block.type.static',
            //'menu' => 'Menu',
            //'news' => 'score.block.type.news',
            'html' => 'score.block.type.html',
        ];
        return $groups;
    }

    public function getBlockById($id) {
        return $this->getRepository()->find($id);
    }

    public function reorderBlocks($sortList) {
        $repo = $this->getRepository();
        $em = $this->getDbProvider()->getManager();
        foreach ($sortList as $sort => $id) {
            $sort++;
            $block = $repo->findOneBy(array('id' => $id));
            $block->setSortOrder($sort);
            $em->persist($block);
        }
        $em->flush();

    }


    public function updateBlockJoints($entity, $blocks, $joints) {

        $em = $this->getDbProvider()->getManager();

        $tempCollection = new ArrayCollection();
        if ($joints) {
            foreach ($joints as $index => $jointId) {
                $joint;
                if ($jointId) {
                    $joint = $em->getRepository(BlockJoint::class)->find($jointId);
                } else {
                    if (is_a($entity, Article::class))
                        $joint = new BlockJointArticle();
                    if (is_a($entity, Document::class))
                        $joint = new BlockJointDocument();
                    if (is_a($entity, Event::class))
                        $joint = new BlockJointEvent();
                    $blockId = $blocks[$index];
                    $block = $em->getRepository(Block::class)->find($blockId);
                    if ($block) {
                        $block->addBlockJoint($joint);
                        $entity->addBlockJoint($joint);
                    }
                }
                $joint->setPlace("center")
                    ->setSortOrder($index);
                $tempCollection->add($joint);

            }
        }

        foreach ($entity->getBlockJoints() as $joint) {
            if (!$tempCollection->contains($joint)) {
                $entity->removeBlockJoint($joint);
            }
        }

        return $entity;
    }

}


