<?php

namespace Score\CmsBundle\Services;

use Score\BaseBundle\Services\BaseAdminManager;

class OptionManager extends BaseAdminManager {

    function __construct($em, $repository)
    {
        $this->db_provider = $em;
        $this->repositiory = $this->db_provider->getManager()->getRepository($repository);
    }


    public function getAllSettings()
    {
        $pageSettings =  $this->getRepository()->findAll();
        $settings = [];
        foreach($pageSettings as $pageSetting)
        {
           if($pageSetting->getVisibility() == true)
           {
            $settings[$pageSetting->getCode()] = $pageSetting->getValue();
           }
           
           
        }

       return  $settings;

    }

    
    /*
    public function getMultisiteChoices()
    {
        $repo = $this->getRepository();
        $multisiteOption = $repo->findOneBy(['code' => 'multisite']);
        if($multisiteOption && $multisiteOption->getValue() == 'yes')
        {
            $repo = $this->db_provider->getManager()->getRepository('ScoreCmsBundle:Multisite\Site');
            $items = $repo->findAll();
            return $items;
        }
    }

    public function isMultisiteEnabled()
    {
        $repo = $this->getRepository();
        $multisiteOption = $repo->findOneBy(['code' => 'multisite']);
        if($multisiteOption && $multisiteOption->getValue() == 'yes')
        {
            return true;
        }
        return false;
    }
    */

 
}

