<?php

namespace Score\CmsBundle\Services;

use Score\CmsBundle\Entity\Event\Event;
use Score\BaseBundle\Services\BaseAdminManager;
use Score\CmsBundle\Entity\Article\Article;
use Score\CmsBundle\Entity\Page\Page;
use Score\CmsBundle\Entity\Stat;

class StatManager extends BaseAdminManager {

    public function __construct($em, $repository) {
        $this->db_provider = $em;
        $this->repositiory = $this->db_provider->getManager()->getRepository($repository);
    }

    public function addHit($data) {

        $sourceId = $data['source_id'] ?? null;
        $sourceType = $data['source_type'] ?? null;
        $statType = $data['stat_type'] ?? null;
        $sourceTitle = $data['source_title'] ?? null;

        $stat = new Stat();
        $stat->setCreatedAt(new \DateTime());
        $url = $_SERVER['REQUEST_URI'];
        if (strlen($url) > 970) {
            $url = substr($url, 0, 950) . '...' . strlen($url) . '...' . substr($url, -5);
        }
        $stat->setSourceUrl($url);
        $stat->setSourceTitle($sourceTitle);
        $stat->setBrowser($_SERVER['HTTP_USER_AGENT']);
        $stat->setSourceType($sourceType);
        $stat->setStatType($statType);
        $stat->setSourceId($sourceId);
        if (array_key_exists('HTTP_REFERER', $_SERVER)) {
            $urlRef = $_SERVER['HTTP_REFERER'];
            if (strlen($urlRef) > 970) {
                $urlRef = substr($urlRef, 0, 950) . '...' . strlen($urlRef) . '...' . substr($urlRef, -5);
            }
            $stat->setReferer($urlRef);
        }
        if (array_key_exists('PHPSESSID', $_COOKIE)) {
            $stat->setSessionId($_COOKIE['PHPSESSID']);
        }


        $em = $this->getDbProvider()->getManager();
        $em->persist($stat);
        $em->flush();
    }

    public function getTopViewedContent() {
        $em = $this->getDbProvider()->getManager();
        $query = $em->createQuery("SELECT s.sourceUrl, s.sourceId, s.sourceType from Score\CmsBundle\Entity\Stat s where s.statType ='request' and ( s.sourceType = 'score_public_articles_detail' or s.sourceType = 'score_public_events_detail' ) group by  s.sourceUrl, s.sourceId, s.sourceType order by  count(s.id) desc");
        $query->setMaxResults(5);
        $items = $query->getResult();

        foreach ($items as $key => $item) {
            $items[$key]['sourceTitle'] = '???';
            if ($item['sourceType'] == 'score_public_articles_detail') {
                $article = $this->getDbProvider()->getRepository(Article::class)->findOneBy(array('slug' => $item['sourceId']));
                $items[$key]['sourceTitle'] = $article ? $article->getName() : "";
            }

            if ($item['sourceType'] == 'score_public_events_detail') {
                $event = $this->getDbProvider()->getRepository(Event::class)->findOneBy(array('slug' => $item['sourceId']));
                $items[$key]['sourceTitle'] = $event ? $event->getName() : "";
            }
        }


        return $items;
    }

    public function getTopSearchContent() {
        $em = $this->getDbProvider()->getManager();
        $query = $em->createQuery("SELECT s.sourceUrl, s.sourceId from Score\CmsBundle\Entity\Stat s where s.statType = 'search'  group by s.sourceUrl, s.sourceId order by  count(s.id) desc");
        $query->setMaxResults(5);
        $items = $query->getResult();
        return $items;
    }

}

