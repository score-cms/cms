<?php

namespace Score\CmsBundle\Services;

use Score\BaseBundle\Services\BaseManager as BaseManager;
use Score\BaseBundle\Services\SeoUrl;
use Score\CmsBundle\Entity\Block\BlockJointDocument;
use Score\CmsBundle\Entity\Document\Document;
use Score\CmsBundle\Entity\Document\DocumentCategory;
use Score\CmsBundle\Entity\Document\DocumentDescription;
use Score\CmsBundle\Entity\Document\DocumentFile;
use Score\CmsBundle\Entity\Document\DocumentLevel;
use Score\CmsBundle\Entity\Metatag\MetatagDocument;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class DocumentManager extends BaseManager {

    protected $imageManager;
    protected $seoUrl;
    protected $rootDir;
    protected $publicDir;
    protected $maxFileSize;
    protected $allowedTypes;
    protected $thumbnailCut;

    function __construct($em, $repository, ImageManager $imageManager, SeoUrl $seoUrl, $rootDir, $publicDir, $maxFileSize, $allowedTypes, $thumbnailCut) {
        $this->db_provider = $em;
        $this->repositiory = $this->db_provider->getManager()->getRepository($repository);
        $this->imageManager = $imageManager;
        $this->seoUrl = $seoUrl;
        $this->rootDir = $rootDir;
        $this->publicDir = $publicDir;
        $this->maxFileSize = $maxFileSize;
        $this->allowedTypes = $allowedTypes;
        $this->thumbnailCut = $thumbnailCut;
    }

    // public function getDocumentsByParent($parent)
    // {
    //     return $this->getRepository()->findBy(['parentPage' => $parent]);
    // }

    public function getDocumentById($id) {
        return $this->getRepository()->find($id);
    }

    public function getDocumentBySeoId($seo_id) {
        return $this->getRepository()->findOneBy(['seoId' => $seo_id]);
    }

    public function updateDocumentDescriptions($document, $oldDescriptionsArr) {
        $em = $this->db_provider->getManager();

        foreach ($oldDescriptionsArr as $dd) {
            if (!$document->getDocumentDescriptions()->contains($dd)) {
                $dd->setDocument(null);
                $dd->setDocumentDomain(null);
                $em->remove($dd);
            }
        }

        foreach ($document->getDocumentDescriptions() as $dd) {
            $dd->setDocument($document);
            $em->persist($dd);
        }

        return $document;
    }

    public function updateDocumentFiles(Document $document, $oldFilesArr, $uploadedFilesArr) {
        $em = $this->db_provider->getManager();

        $errors = [];
        foreach ($oldFilesArr as $file) {
            if (!$document->getDocumentFiles()->contains($file)) {
                $this->removeFile($file);
                $file->setDocument(null);
                $em->remove($file);
            }
        }

        if ($document->getDocumentFiles()) {
            foreach ($document->getDocumentFiles() as $k => $f) {
                if (!$f->getId() && (array_key_exists($k, $uploadedFilesArr) && $uploadedFilesArr[$k]['file'])) { // novy a ma aj nahraty subor
                    if ($this->uploadFile($f, $uploadedFilesArr[$k]['file'])) {// ak je nahraty subor OK tak to aj ulozi
                        $f->setDocument($document);
                        $em->persist($f);
                    } else {// ak je nahraty subor ale nieje OK (format, velkost)
                        $document->removeDocumentFile($f);
                        $errors[] = 'score.alert.document.notUploadedFile';
                    }
                } elseif (!$f->getId() && (!$f->getUrl() || !$f->getInfo())) { // novy a nema nahraty subor ani zadanu url alebo info
                    $document->removeDocumentFile($f);
                    $errors[] = 'score.alert.document.notUploadedFile';
                } else { // len aktualizacia
                    $f->setDocument($document);
                    $em->persist($f);
                }
            }
        }
        return $errors;
    }

    protected function removeFile(DocumentFile $documentFile) {
        if (!$documentFile->getIsExternal()) {
            $url = $this->rootDir . $documentFile->getUrl();
            if (file_exists($url)) {
                unlink($url);
            }
        }
        return $documentFile;
    }

    protected function uploadFile(DocumentFile $documentFile, $uploadedFile) {
        $maxSize = $this->maxFileSize * 1024 * 1024;

        if ($uploadedFile instanceof UploadedFile && ($uploadedFile->getSize() < $maxSize) && in_array($uploadedFile->getMimeType(), $this->allowedTypes)) {

            $root = $this->rootDir;
            $dir = $this->publicDir . '/' . date('Y');

            if (!file_exists($root . $dir)) {
                $this->imageManager->createPath($root . $dir);
            }

            //name and unique ID at the end
            $origName = $this->seoUrl->createSlug($uploadedFile->getClientOriginalName());
            $parts = explode(".", $origName);
            $extension = array_pop($parts);
            $nameString = implode(".", $parts);
            $name = $nameString . uniqid("_") . "." . $extension;

            if (file_exists($root . $dir . $name)) {
                throw new \Exception('The file upload failed (This file is already uploaded).');
                //return ["error" => ["message" => "The file upload failed (This file is already uploaded)."]];
            }

            $documentFile->setIsExternal(false)
                ->setFilename($uploadedFile->getClientOriginalName())
                ->setType($uploadedFile->getMimeType())
//                ->setExtension($uploadedFile->guessExtension()) // ai -> pdf, cdr -> bin
                ->setExtension($extension)
                ->setSize($uploadedFile->getSize())
                ->setUrl($dir . '/' . $name);

            $uploadedFile->move($root . $dir, $name);
            return $documentFile;

        }
        return null;
    }


    public function handleIconUpload($document, $icon) {
        if (
            $icon instanceof UploadedFile
            && $this->imageManager->isImage($icon)
        ) {

            $root = $this->rootDir;
            $dir = $this->publicDir . '/icons';

            $fileName = $document->getCode() . '.' . $icon->guessExtension();
            $fileThumb100Name = $document->getCode() . '_thumb_100x100.' . $icon->guessExtension();
            $fileThumb640Name = $document->getCode() . '_thumb_640x480.' . $icon->guessExtension();

            if (!file_exists($root . $dir)) {
                $this->imageManager->createPath($root . $dir);
            }

            try {

                $icon->move($root . $dir, $fileName);
                if ($this->thumbnailCut === true) {

                    $this->imageManager->scaleTo(
                        $root . $dir . '/' . $fileName,
                        $root . $dir . '/' . $fileThumb100Name,
                        100, 100);
                    $this->imageManager->scaleTo(
                        $root . $dir . '/' . $fileName,
                        $root . $dir . '/' . $fileThumb640Name,
                        640, 480);
                } else {
                    $this->imageManager->thumbOver(
                        $root . $dir . '/' . $fileName,
                        $root . $dir . '/' . $fileThumb100Name,
                        100, 100);
                    $this->imageManager->thumbOver(
                        $root . $dir . '/' . $fileName,
                        $root . $dir . '/' . $fileThumb640Name,
                        640, 480);
                }
            } catch (FileException $e) {
                echo $e;

            }
            return $dir . '/' . $fileName;
        }
        return "";
    }

    public function updateSearch(Document $document) {
        $text = $document->getName() . ' ' .
            $document->getFullName() . ' ' .
            $document->getTeaser() . ' ' .
            $document->getDescription() . ' ' .
            ($document->getActualFile() ? $document->getActualFile()->getFilename() : '') . ' ' .
            $document->getYear() . ' ' .
            implode(" ", $document->getDocumentLevels()->map(fn(DocumentLevel $l) => $l->getName() . ' ')->toArray()) . ' ' .
            implode(" ", $document->getDocumentCategories()->map(fn(DocumentCategory $dc) => $dc->getName() . ' ')->toArray()) . ' ' .
            implode(" ", $document->getDocumentDescriptions()
                ->filter(fn(DocumentDescription $dd) => $dd->getPublic())
                ->map(fn(DocumentDescription $dd) => $dd->getDocumentDomain()->getName() . ' ' . $dd->getDescription() . ' ')
                ->toArray()
            ) . ' ' .
            implode(" ", $document->getBlockJoints()
                ->filter(fn(BlockJointDocument $bjd) => $bjd->getBlock()->getVisibility())
                ->map(fn(BlockJointDocument $bjd) => $bjd->getBlock()->getContent() . ' ')
                ->toArray()
            ) . ' ';
            implode(" ", $document->getMetatags()->map(fn(MetatagDocument $md) => $md->getValue() . ' ')->toArray()) . ' ';
        $search = strip_tags(mb_strtolower($this->seoUrl->removeDiacritic($text)));
        $document->setSearch($search);
    }

    public function updateSeoId(Document $document) {
        $document->setSeoId($this->seoUrl->generateSeoString($document->getSeoId()));
    }


}
