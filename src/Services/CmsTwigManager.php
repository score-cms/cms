<?php

namespace Score\CmsBundle\Services;

use Score\BaseBundle\Services\AdjacencyArrayTreeManager;
use Score\CmsBundle\Entity\Block\Accordion;
use Score\CmsBundle\Entity\Document\Document;
use Score\CmsBundle\Entity\Menu;
use Score\CmsBundle\Entity\Option;
use Score\CmsBundle\Entity\Widget\Widget;

class CmsTwigManager {
    protected $em;
    protected $optionTexts = [];
//    protected $adjacencyArrayTreeManager;, AdjacencyArrayTreeManager $adjacencyArrayTreeManager

    public function __construct($em) {
        $this->em = $em;
//        $this->adjacencyArrayTreeManager = $adjacencyArrayTreeManager;
        $this->optionTexts = $this->initOptionTexts();
    }

    protected function initOptionTexts() {
        $qb = $this->em->createQueryBuilder();
        $options = $qb->select("o")
            ->from(Option::class, "o")
            ->where($qb->expr()->isNotNull("o.code"))
            ->andWhere('o.visibility = :p')->setParameter('p', true)
            ->getQuery()->getResult();

        //$options = $this->em->getRepository(Option::class)->findBy(["visibility"=>true]);

        $texts = [];
        if ($options) {
            foreach ($options as $option) {
                $texts[$option->getCode()] = $option->getValue();
            }
        }
        return $texts;
    }

    public function replaceAllOptionText($text) {
        $arr = explode("CMS_TEXT(", $text);
        $result = array_shift($arr);

        if ($arr) {
            foreach ($arr as $a) {
                $ex = explode(")", $a, 2);
                $result .= $this->replaceOptionText($ex[0]);
                $result .= $ex[1];
                //array_shift($arr);
            }
        }
        return $result;
    }

    public function replaceOptionText($text) {
        return array_key_exists($text, $this->optionTexts) ? $this->optionTexts[$text] : "-";
    }

    public function getAccordion($id) {
        return $this->em->getRepository(Accordion::class)->find($id);
    }

    public function replaceWidget($params) {
        $p = explode(",", $params, 2);
        $p = array_map('trim', $p);

        $widget = $this->em->getRepository(Widget::class)->findOneByCode($p[0]);

        if (!$widget) {
            return ("no widget");
        }

        $fw = $widget->getWidgetValues()->filter(function ($element) use ($p) {
            return $element->getCode() === $p[1];
        });

        $value = $fw->first();

        if (!$value || !$value->getDataArr()) {
            return ("–");
        }

        $data = $value->getDataArr();
        $template = $widget->getTemplate();

        return $this->parseWidgetTemplate($template, $data);
    }

    protected function parseWidgetTemplate($template, $data) {
        $arr = explode("CMS_WIDGET_VALUE(", $template);
        $result = [];
        $result[] = array_shift($arr);

        if ($arr) {
            foreach ($arr as $a) {
                $ex = explode(")", $a, 2);
                $code = trim($ex[0]);
                $result[] = array_key_exists($code, $data) ? $data[$code] : "*";
                $result[] = $ex[1];
            }
        }
        return implode("", $result);
    }

    public function replaceAllWidgets($text) {
        $arr = explode("CMS_WIDGET(", $text);
        $result = array_shift($arr);

        if ($arr) {
            foreach ($arr as $a) {
                $ex = explode(")", $a, 2);
                $result .= $this->replaceWidget($ex[0]);
                $result .= $ex[1];
            }
        }
        return $result;
    }

    public function getMenuTree($id) {

        if ($id === 'root') {
            $roots = $this->em->getRepository(Menu::class)->findBy(['rootId' => null], ['createdAt' => 'ASC', 'id' => 'ASC']);
            if ($roots && count($roots)) {
                return $roots[0]->getChildren();
            } else {
                return [];
            }
        }
        $menu = $this->em->getRepository(Menu::class)->find($id);
        if (!$menu)
            return [];

        return $menu->getChildren();
    }

    public function updateDocumentLinks($html) {
        if (strpos($html, ' data-cms="document-link"')) {
            $regex = '/(<a([^<>]+)? data-cms\=\"document-link\"([^<>]+)?>([^<>]+)?<\/a>)+/';
            return preg_replace_callback($regex, [$this, 'parseLink'], $html, 120);
        }
        return $html;
    }

    protected function parseLink($link) {
        preg_match("/(?<=code=\")([^\"]+)?/", $link[0], $code);
        $identifier = is_array($code) && array_key_exists(0, $code) ? $code[0] : "0";
        $document = $this->em->getRepository(Document::class)->findOneByCode($identifier);

        if ($document && $document->getPublic()) {
            preg_match("/(?<=domain=\")([^\"]+)?/", $link[0], $domain);
            preg_match("/(?<=action=\")([^\"]+)?/", $link[0], $action);

            $text = $url = $download = "";
            if ($action[0] === "download" || !$document->getHasDetail()) { // download document
                $url = "/dokument/f/" . $document->getSeoId() . $document->getExtension();
                $text = $document->getName() . $document->getInfo();
                $download = "download=\"" . $document->getSeoId() . $document->getExtension() . "\" ";
            } else { // show detail
                $url = "/dokument/" . $document->getSeoId();
                $url .= $domain[0] ? ("#" . $domain[0]) : "";
                $text = $document->getName();
            }

            $result = "<a href=\"" . $url . "\" data-document-code=\"" . $code[0] . "\" data-document-domain=\"" . $domain[0] . "\" data-document-action=\"" . $action[0] . "\" " . $download . "data-cms=\"document-link\">" . $text . "</a>";
            return $result;
        }
        return ""; // Returns empty string if document doesnt exist... // return $link[0]; // can return original link string
    }
}