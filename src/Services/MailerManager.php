<?php

namespace Score\CmsBundle\Services;

use DateTimeInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;


use Symfony\Component\Mailer\MailerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\Address;

use Score\CmsBundle\Entity\MailBoard;
use Score\BaseBundle\Services\MailerManager as BaseMailerManager;

class MailerManager extends BaseMailerManager
{

    // protected $mailer;
    // protected $logger;
    // protected $subject;
    // protected $from = 'info@cms.sk';
    // protected $to;
    // protected $body;
    protected $em;
    protected $email;
    protected $maxAttempts;
    protected $firstTryAt;


    public function __construct($mailer, $logger, $em, $maxAttempts, $email)
    {
        $this->mailer = $mailer;
        $this->logger = $logger;
        $this->em = $em;
        $this->maxAttempts = $maxAttempts;
        $this->email = $email;
    }

    protected function resetValues()
    {
        $this->setSubject(null)
            ->setBody(null)
            ->setTo(null)
            ->setFrom(null)
            ->setFirstTryAt(null);
    }


    public function send($data = [])
    {
        
        if (array_key_exists('subject', $data))
            $this->setSubject($data['subject']);
        if (array_key_exists('to', $data))
            $this->setTo($data['to']);
        if (array_key_exists('from', $data))
            $this->setFrom($data['from']);
            if (array_key_exists('body', $data))
            $this->setBody($data['body']);
        if (array_key_exists('body', $data))
            $this->setBody($data['body']);
        if (array_key_exists('firstTryAt', $data))
            $this->setFirstTryAt($data['firstTryAt']);

        if ($this->getTo() && $this->getSubject() && $this->getBody()) {
            $this->saveToBoard();
            $this->checkAllMails();
        }

        $this->resetValues();
    }

    protected function tryToSendMail($mailBoard = null)
    {
        if ($mailBoard) {
            $mailBoard->addAttemptNow();

            $e = $this->email;
            if (is_array($e)){
                $from = new Address($e[0], array_key_exists(1, $e) ? $e[1] : "");
            } else {
                $from = new Address($e);
            }

            $email = (new Email())
            ->from($from)
            ->to($mailBoard->getMailTo())
            //->cc('cc@example.com')
            //->bcc('bcc@example.com')
            //->replyTo('fabien@example.com')
            //->priority(Email::PRIORITY_HIGH)
            ->subject( $mailBoard->getSubject())
            // ->text('Sending emails is fun again ss!')
            ->html( $mailBoard->getBody());

            try {
                $this->mailer->send($email);
                $mailBoard->setIsSentNow();
                
            } catch (TransportExceptionInterface $e) {
                // some error prevented the email sending; display an
                // error message or try to resend the message
            }

            $this->em->persist($mailBoard);
            $this->em->flush();
        }
    }


    protected function checkAllMails()
    {
        $mails = $this->em->getRepository(MailBoard::class)->getMailsToSend($this->maxAttempts);
        foreach ($mails as $mailBoard) {
            $this->tryToSendMail($mailBoard);
        }
    }


    protected function saveToBoard()
    {
        $mailBoard = new MailBoard();
        $mailBoard
            //->setMailFrom($this->getFrom())
            ->setMailFrom('default')
            ->setMailTo($this->getTo())
            ->setSubject($this->getSubject())
            ->setBody($this->getBody())
            ->setAttempts(0)
            ->setIsSent(false);
        
        if ($this->getFirstTryAt() && ($this->getFirstTryAt() instanceof DateTimeInterface )) {
            $mailBoard->setFirstTry($this->getFirstTryAt());
        }

        $this->em->persist($mailBoard);
        $this->em->flush();
    }

    public function getFirstTryAt()
    {
        return $this->firstTryAt;
    }

    public function setFirstTryAt(?DateTimeInterface $firstTryAt)
    {
        $this->firstTryAt = $firstTryAt;
        return $this;
    }
}
