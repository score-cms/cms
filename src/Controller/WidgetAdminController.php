<?php

namespace Score\CmsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Score\BaseBundle\Controller\AdminController;
use Score\CmsBundle\Entity\Widget\Widget;
use Score\CmsBundle\Entity\Widget\WidgetValue;
use Score\CmsBundle\Form\WidgetType;
use Score\CmsBundle\Form\WidgetValueBasicType;
use Score\CmsBundle\Form\WidgetValueType;

/**
 * @Route("/admin/widget", name="score-cms-widget-")
 */
class WidgetAdminController extends AdminController {
    protected function canIAccess($onlyAdmin = false) {
        if ($onlyAdmin) {
            return $this->getUser()->getIsAdmin();
        }
        $allowed = ["ROLE_WIDGET_MANAGER", "ROLE_ADMIN"];
        return (count(array_intersect($allowed, $this->getUser()->getRoles())) >= 1);
    }

    protected function redirectHome() {
        $this->addFlash('danger', 'score.alert.user.lowRole');
        return $this->redirectToRoute('admin_start');
    }

    /**
     * @Route("/code/{widget_code}/{widget_data_code}", name="code", methods={"GET"})
     */
    public function widgetCodeAction(Request $request, $widget_code, $widget_data_code): Response {

        $em = $this->getDoctrine()->getManager();
        $widget = $em->getRepository(Widget::class)->findOneByCode($widget_code);

        if ($widget === null) {
            $this->addFlash('widget_code_danger', "Widget [$widget_code] zatial neexistuje.");
            return $this->redirect($request->headers->get('referer'));
        }

        foreach ($widget->getWidgetValues() as $v) {
            if ($v->getCode() === $widget_data_code) {
                return $this->redirectToRoute('score-cms-widget-update-data', ['widgetId' => $widget->getId(), 'id' => $v->getId()]);
            }
        }

        return $this->redirectToRoute('score-cms-widget-list');
    }

    /**
     * @Route("/list", name="list", methods={"GET"})
     */
    public function widgetListAction(Request $request): Response {

        if (!$this->canIAccess())
            return $this->redirectHome();

        $em = $this->getDoctrine()->getManager();
        $widgets = $em->getRepository(Widget::class)->findAll();

        return $this->render('@ScoreCms/Widget/list.html.twig', [
            'widgets' => $widgets,
        ]);
    }


    /**
     * @Route("/new", name="new", methods={"GET", "POST"})
     */
    public function newAction(Request $request, $id = 0): Response {
        if (!$this->canIAccess(true))
            return $this->redirectHome();

        return $this->editAction($request, $id);
    }


    /**
     * @Route("/edit/{id}", name="edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, $id): Response {
        if (!$this->canIAccess(true))
            return $this->redirectHome();

        $em = $this->getDoctrine()->getManager();
        $widget = $em->getRepository(Widget::class)->find($id);

        if (!$widget) {
            $widget = new Widget();
        }

        $form = $this->createForm(WidgetType::class, $widget);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if (!$widget->getPattern() || ($widget->getPattern() && $widget->getPatternArr())) {
                $em->persist($widget);
                $em->flush();
                $this->addFlash('widget_edit_success', 'Údaje boli zmenené');
                return $this->redirectToRoute('score-cms-widget-edit', ['id' => $widget->getId()]);
            }
            $this->addFlash('widget_edit_danger', 'JSON štruktura je chybná! Zmeny neboli uložené. Opravte chybu a uložte znovu.');
        }

        return $this->render('@ScoreCms/Widget/edit.html.twig', [
            'entity' => $widget,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete", methods={"GET"})
     */
    public function deleteAction(Request $request, $id): Response {
        if (!$this->canIAccess(null, true))
            return $this->redirectHome();

        $em = $this->getDoctrine()->getManager();
        $widget = $em->getRepository(Widget::class)->find($id);

        if ($widget) {
            foreach ($widget->getWidgetValues() as $value) {
                $widget->removeWidgetValue($value);
                $em->remove($value);
            }
            $em->persist($widget);
            $em->flush();
            $em->remove($widget);
            $em->flush();
            $this->addFlash("admin-success", "score.alert.widget.deleted");
        }

        return $this->redirectToRoute('score-cms-widget-list');
    }

    /**
     * ====================   WIDGET VALUE   ==========================
     */

    /**
     * @Route("/value-list/{id}", name="value-list", methods={"GET"})
     */
    public function valuesListAction(Request $request, $id): Response {

        $em = $this->getDoctrine()->getManager();
        $widget = $em->getRepository(Widget::class)->find($id);

        if (!$this->canIAccess())
            return $this->redirectToRoute('score_cms_option_groups');

        return $this->render('@ScoreCms/Widget/values_list.html.twig', [
            'values' => $widget->getWidgetValues(),
            'widgetId' => $id,
        ]);
    }

    /**
     * @Route("/value-new/{widgetId}", name="value-new", methods={"GET", "POST"})
     * @Route("/value-edit/{widgetId}/{id}", name="value-edit", methods={"GET", "POST"})
     */
    public function editValueAction(Request $request, $widgetId, $id = 0): Response {
        if (!$this->canIAccess($id === 0)) //pridava len admin!
            return $this->redirectHome();

        $em = $this->getDoctrine()->getManager();
        $widget = $em->getRepository(Widget::class)->find($widgetId);
        $value = $em->getRepository(WidgetValue::class)->find($id);

        if (!$widget) {
            $this->addFlash('danger', 'Chyba.');
            return $this->redirectToRoute('admin_start');
        }
        if (!$value) {
            $value = new WidgetValue();
            $widget->addWidgetValue($value);
        }


        $form = $this->createForm($this->getUser()->getIsAdmin() ? WidgetValueType::class : WidgetValueBasicType::class, $value);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if (!$value->getData() || ($value->getData() && $value->getDataArr())) {
                if (!$value->getDataArr()) {
                    //vytvori zakladny json s prazdnymi hodnotami ale pole 'data' potom uz obsahuje potrebne kluce
                    $value->setDataArr(
                        array_merge(...array_map(function ($n) {
                            return ([$n['code'] => ""]);
                        }, $widget->getPatternArr()))
                    );
                }
                $em->persist($value);
                $em->flush();
                $this->addFlash('widget_edit_success', 'Údaje boli zmenené');
                return $this->redirectToRoute('score-cms-widget-value-edit', ['widgetId' => $widgetId, 'id' => $value->getId()]);
            }
            $this->addFlash('widget_edit_danger', 'JSON štruktura je chybná! Vymažte pole údaje ak chcete štrukturu vygenerovat automaticky. Zmeny neboli uložené.');
        }

        return $this->render('@ScoreCms/Widget/edit_value.html.twig', [
            'entity' => $value,
            'form' => $form->createView(),
            'widget' => $widget,
        ]);
    }


    /**
     * @Route("/update-data/{widgetId}/{id}", name="update-data", methods={"GET", "POST"})
     */
    public function updateDataValueAction(Request $request, $widgetId, $id = 0): Response {
        if (!$this->canIAccess())
            return $this->redirectHome();

        $em = $this->getDoctrine()->getManager();
        $widget = $em->getRepository(Widget::class)->find($widgetId);
        $value = $em->getRepository(WidgetValue::class)->find($id);

        if (!$widget || !$value) {
            $this->addFlash('danger', 'Chyba.');
            return $this->redirectToRoute('admin_start');
        }


        if ($request->get("widget_submited")) {
            $formData = $request->get("widget");
            if ($this->validateData($widget->getPatternArr(), $value->getDataArr(), $formData)) {
                $newData = $this->parseToSaveData($widget->getPatternArr(), $value->getDataArr(), $formData);
                $value->setDataArr($newData);
                $em->persist($value);
                $em->flush();
                $this->addFlash('widget_edit_success', 'Údaje boli zmenené');
                return $this->redirectToRoute('score-cms-widget-update-data', ['widgetId' => $widgetId, 'id' => $value->getId()]);
            }
            $this->addFlash('widget_edit_danger', 'Údaje neboli zmenené');
            $value->setDataArr($formData);
        }

        return $this->render('@ScoreCms/Widget/update_data.html.twig', [
            'value' => $value,
            'widget' => $widget,
        ]);
    }

    protected function validateData($pattern, $oldData, $formData) {

        $valid = true;
        foreach ($pattern as $patt) {
            $code = $patt['code'];
            if (in_array($patt['type'], ["text", "textarea", "richtext"])) {
                if (array_key_exists("min", $patt) && $patt['min']) {
                    if (strlen(strip_tags($formData[$code])) < $patt['min']) {
                        $valid = false;
                        $this->addFlash('danger', $patt['label'] . ": min. " . $patt['min'] . " znakov. (Použitých: " . strlen(strip_tags($formData[$code])) . ".)");
                    }
                }
                if (array_key_exists("max", $patt) && $patt['max']) {
                    if (strlen(strip_tags($formData[$code])) > $patt['max']) {
                        $valid = false;
                        $this->addFlash('danger', $patt['label'] . ": max. " . $patt['max'] . " znakov. (Použitých: " . strlen(strip_tags($formData[$code])) . ".)");
                    }
                }
            } elseif ($patt['type'] === "image") {
                if (preg_match('/\"(\s+)?\"/', $formData[$code])) { // vyhladavanie len bielych znakov v uvodzovkach alebo prazdne uvodzovky
                    $valid = false;
                    $this->addFlash('danger', $patt['label'] . ": musí mať vyplnený alt aj url.");
                }

            } elseif ($patt['type'] === "choice") {
                $values = array_column($patt['choices'], "value");
                if (!in_array($formData[$code], $values)) {
                    $valid = false;
                    $this->addFlash('danger', $patt['label'] . ": vyberte možnosť.");
                }
            }
        }

        return $valid;
    }


    protected function parseToSaveData($pattern, $oldData, $formData) {
        $result = [];
        foreach ($pattern as $patt) {
            $code = $patt['code'];
            $result[$code] = $formData[$code];
        }
        return $result;
    }

    /**
     * @Route("/value-delete/{widgetId}/{id}", name="value-delete", methods={"GET"})
     */
    public function valueDeleteAction(Request $request, $widgetId, $id): Response {
        if (!$this->canIAccess(null, true))
            return $this->redirectHome();

        $em = $this->getDoctrine()->getManager();
        $value = $em->getRepository(WidgetValue::class)->find($id);

        if ($value) {
            $em->remove($value);
            $em->flush();
            $this->addFlash("admin-success", "score.alert.widget.value.deleted");
        }

        return $this->redirectToRoute('score-cms-widget-value-list', ['id' => $widgetId]);
    }
}
