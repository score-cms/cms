<?php

namespace Score\CmsBundle\Controller;


use Doctrine\Persistence\ManagerRegistry;
use Score\CmsBundle\Entity\Article\Article;
use Score\CmsBundle\Entity\Block\AccordionItem;
use Score\CmsBundle\Entity\Block\Block;
use Score\CmsBundle\Entity\Document\Document;
use Score\CmsBundle\Entity\Document\DocumentDescription;
use Score\CmsBundle\Entity\Event\Event;
use Score\CmsBundle\Entity\Option;
use Score\CmsBundle\Entity\Page\Page;
use Score\CmsBundle\Entity\Widget\Widget;
use Score\CmsBundle\Entity\Widget\WidgetValue;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Score\BaseBundle\Services\SeoUrl;
use Score\CmsBundle\Services\ImageManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;

class MediaController extends AbstractController {
    protected function canIAccess() {
        $allowed = ["ROLE_ADMIN", "ROLE_IMAGES_MANAGER"];
        return (count(array_intersect($allowed, $this->getUser()->getRoles())) >= 1);
    }

    protected function redirectHome() {
        $this->addFlash('danger', 'score.alert.user.lowRole');
        return $this->redirectToRoute('admin_start');
    }

    /**
     * @Route("/admin/api/cke-image-upload", name="score-cke-image-upload")
     */
    public function uploadAction(Request $request, ImageManager $imageManager, SeoUrl $seoUrl) {
        $dir = $this->getParameter('ckeditor_public_upload_directory') . "/";
        $sizes = $this->getParameter('ckeditor_image_sizes');

        $file = $request->files->get('upload');
        $urls = $this->addFile($imageManager, $seoUrl, $file, $dir, $sizes);

        if ($urls) {
            $result = ["urls" => $urls];
            return new JsonResponse($result);
        }

        return new JsonResponse(["error" => ["message" => "The image upload failed."]]);
    }

    /**
     * @Route("/admin/media/manage-images", name="score-manage-images")
     */
    public function manageImagesAction(Request $request, ImageManager $imageManager, SeoUrl $seoUrl) {
        if (!$this->canIAccess())
            return $this->redirectHome();
        
        if ($request->get('foldername')) {
            $dir = $this->getParameter('ckeditor_public_upload_directory') . "/";
            $subdir = "subs/" . $seoUrl->createSlug($request->get('foldername')) . "/";
            $sizes = $this->getParameter('ckeditor_image_sizes');
            $root = $this->getParameter('kernel.project_dir') . '/public';
            if (!file_exists($root . $dir . $subdir)) {
                mkdir($root . $dir . $subdir);
            }

            foreach ($request->files->get('files') as $file) {
                $this->addFile($imageManager, $seoUrl, $file, $dir . $subdir, $sizes);
            }
            $this->addFlash('success', 'score.alert.media.folderAdded');
            return $this->redirectToRoute('score-manage-images');
        }

        $result = $this->imagesAction($request, true);
        return $this->render('@ScoreCms/Media/list.html.twig', [
            "dir" => $result["dir"],
            "sizes" => $result["sizes"],
            "filenames" => $result["filenames"],
            "customs" => $result["customs"],
            "subs" => $result["subs"],
        ]);
    }

    /**
     * @Route("/admin/media/delete-folder", name="score-media-delete-folder")
     */
    public function deleteFolderAction(Request $request, ImageManager $imageManager) {
        if (!$this->canIAccess())
            return $this->redirectHome();

        $folder = $request->get('folder');
        if ($folder) {
            $root = $this->getParameter('kernel.project_dir') . '/public';
            $uploadDir = $this->getParameter('ckeditor_public_upload_directory') . "/";
            $sizes = $this->getParameter('ckeditor_image_sizes');
            if (empty(glob($root . $uploadDir . "subs/" . $folder . "/" . $sizes[0] . "/*"))) {
                $imageManager->removeAll($root . $uploadDir . "subs/" . $folder . "/");
                $this->addFlash('success', 'score.alert.media.folderDeleted');
            } else {
                $this->addFlash('danger', 'score.alert.media.folderNotEmpty');
            }
        }
        return $this->redirectToRoute('score-manage-images');
    }

    /**
     * @Route("/admin/media/delete-image", name="score-media-delete")
     */
    public function deleteImagesAction(Request $request, ManagerRegistry $doctrine) {
        if (!$this->canIAccess())
            return $this->redirectHome();

        $em = $doctrine->getManager();
        $string = $request->get('filename');
        $dir = $request->get('dir');
        $custom = $request->get('custom');

        $data = [];
        $conditions = [
            "pages" => [Page::class, 'e.content LIKE :string'],
            "articles" => [Article::class, 'e.teaser LIKE :string OR e.body LIKE :string'],
            "events" => [Event::class, 'e.teaser LIKE :string OR e.content LIKE :string'],
            "documents" => [Document::class, 'e.description LIKE :string'],
            "documentDescriptions" => [DocumentDescription::class, 'e.description LIKE :string'],
            "blocks" => [Block::class, 'e.content LIKE :string'],
            "accordionItems" => [AccordionItem::class, 'e.content LIKE :string'],
            "options" => [Option::class, 'e.value LIKE :string'],
            "widgetTemplate" => [Widget::class, 'e.template LIKE :string'],
            //"widgetValue" => [WidgetValue::class, 'e.data LIKE :string'],
        ];
        foreach ($conditions as $name => $condition) {
            $data[$name] = $em->getRepository($condition[0])->createQueryBuilder('e')
                ->where($condition[1])->setParameter('string', '%' . "/" . $string . '%')->getQuery()->getResult();
        }
        $data["widgetValue"] = $em->getRepository(WidgetValue::class)->createQueryBuilder('e')
            ->where("e.data LIKE :string ESCAPE '!'")->setParameter('string', '%' . str_replace(["_"], ["!_"], trim(json_encode("/" . $string), '"')) . '%')->getQuery()->getResult();


        $links = $this->getMessageLinks($data);

        if (count($links)) {
            $this->addFlash("danger", "Obrázok \"" . $string . "\"  sa nepodarilo vymazať pretože je na webe použitý. Obrázok bude možné vymazať len ak nebude nikde použitý. <br> " . implode(" | ", $links));
        } else {
            $root = $this->getParameter('kernel.project_dir') . '/public';
            if (!$custom) {
                $sizes = $this->getParameter('ckeditor_image_sizes');
                $result = [];
                foreach (array_merge($sizes, ["orig"]) as $size) {
                    $url = $root . $dir . $size . "/" . $string;
                    $result[] = file_exists($url) && unlink($url);
                }
                if (in_array(true, $result)) {
                    $this->addFlash("success", "Obrázok (" . $string . ") bol vymazaný");
                } else {
                    $this->addFlash("warning", "Chyba! <br> Obrázok (" . $string . ")");
                }
            } else {
                $url = $root . $dir . $string;
                if (file_exists($url) && unlink($url)) {
                    $this->addFlash("success", "Obrázok (" . $string . ") bol vymazaný");
                } else {
                    $this->addFlash("warning", "Chyba! <br> Obrázok (" . $string . ")");
                }
            }
        }

        return $this->redirectToRoute('score-manage-images');
    }


    /**
     * @Route("/admin/api/cke-images", name="score-cke-images")
     */
    public function imagesAction(Request $request, $noResponse = false) {


        $root = $this->getParameter('kernel.project_dir') . '/public';
        $uploadDir = $this->getParameter('ckeditor_public_upload_directory') . "/";
        $sizes = $this->getParameter('ckeditor_image_sizes');

        $directory = $root . $uploadDir . "orig/";

        $result = [
            "dir" => $uploadDir,
            "sizes" => $sizes,
            "filenames" => $this->loadFiles($directory),
            "subs" => [],
            "customs" => []
        ];

        foreach (glob($root . $uploadDir . "subs/*") as $dir) {
            $result['subs'][] = [
                "sizes" => $sizes,
                "folder" => str_replace($root . $uploadDir . "subs/", "", $dir),
                "name" => ucfirst(str_replace("-", " ", str_replace($root . $uploadDir . "subs/", "", $dir))),
                "dir" => str_replace($root, "", $dir) . "/",
                "filenames" => $this->loadFiles($dir . "/orig/"),
            ];
        }

        $customDirs = $this->getParameter('cms_media_directories');
        if ($customDirs && count($customDirs)) {
            foreach ($customDirs as $key => $dir) {
                $result['customs'][] = [
                    "name" => $key,
                    "dir" => $dir . "/",
                    "filenames" => $this->loadFiles($root . $dir . "/"),
                ];
            }
        }

        if ($noResponse) {
            return $result;
        }

        return new JsonResponse($result);
        //return new JsonResponse(["error" => ["message" => "The image upload failed (max 2.7MB)."]]);
    }

    protected function loadFiles($dir) {

        $images = glob($dir . "*.*");
        $result = [];

        foreach ($images as $image) {
            $result[] = str_replace($dir, "", $image);
        }

        return $result;
    }

    protected function getMessageLinks($data) {
        $links = [];
        $linkFormat = "<a href=\"%s\">%s</a>";
        foreach ($data as $name => $entities) {
            foreach ($entities as $entity) {
                switch ($name) {
                    case "pages":
                        $links[] = sprintf($linkFormat, $this->generateUrl("score_cms_page_edit", [
                            "id" => $entity->getId()
                        ]), "Stránka: " . $entity->getName());
                        break;
                    case "articles":
                        $links[] = sprintf($linkFormat, $this->generateUrl("score_cms_article_edit", [
                            "id" => $entity->getId()
                        ]), "Článok: " . $entity->getName());
                        break;
                    case "events":
                        $links[] = sprintf($linkFormat, $this->generateUrl("score_cms_event_edit", [
                            "id" => $entity->getId()
                        ]), "Udalosť: " . $entity->getName());
                        break;
                    case "documents":
                        $links[] = sprintf($linkFormat, $this->generateUrl("score-cms-document-edit", [
                            "id" => $entity->getId()
                        ]), "Dokument: " . $entity->getName());
                        break;
                    case "documentDescriptions":
                        $links[] = sprintf($linkFormat, $this->generateUrl("score-cms-document-edit", [
                            "id" => $entity->getDocument()->getId()
                        ]), "Oblasť: " . $entity->getDocumentDomain()->getName() . " pre dokument: " . $entity->getDocument()->getName());
                        break;
                    case "blocks":
                        $links[] = sprintf($linkFormat, $this->generateUrl("score-block-to-edit", [
                            "id" => $entity->getId()
                        ]), "Blok: " . $entity->getName());
                        break;
                    case "accordionItems":
                        $links[] = sprintf($linkFormat, $this->generateUrl("score-accordion-edit", [
                            "id" => $entity->getAccordion()->getId()
                        ]), "Akordeón: " . $entity->getAccordion()->getName());
                        break;
                    case "options":
                        $links[] = sprintf($linkFormat, $this->generateUrl("score_cms_option_code", [
                            "code" => $entity->getCode()
                        ]), "Text: " . $entity->getName());
                        break;
                    case "widgetTemplate":
                        $links[] = sprintf($linkFormat, $this->generateUrl("score-cms-widget-edit", [
                            "id" => $entity->getId()
                        ]), "Šablóna pre widgety: " . $entity->getName());
                        break;
                    case "widgetValue":
                        $links[] = sprintf($linkFormat, $this->generateUrl("score-cms-widget-update-data", [
                                "id" => $entity->getId(),
                                "widgetId" => $entity->getWidget()->getId()
                            ]), "Widget: " . $entity->getName() . " (" . $entity->getWidget()->getName()) . ")";
                }
            }
        }
        return $links;
    }

    protected function addFile(ImageManager $imageManager, $seoUrl, $file, $dir, $sizes) {
        $maxSize = 2.7 * 1000 * 1000;
        //$allowedTypes = ['image/png', 'image/jpeg', 'image/gif'];
        $allowedTypes = ['image/png', 'image/jpeg'];

        if (
            $file instanceof UploadedFile && ($file->getSize() < $maxSize)
            && in_array($file->getMimeType(), $allowedTypes)
        ) {
            $root = $this->getParameter('kernel.project_dir') . '/public';
            $result = [];

            //name and unique ID at the end
            $origName = $seoUrl->createSlug($file->getClientOriginalName());
            $parts = explode(".", $origName);
            $extension = implode(".", array_slice($parts, -1));
            $nameString = implode(".", array_slice($parts, 0, count($parts) - 1));
            $name = $nameString . uniqid("_") . "." . $extension;

            $file->move($root . $dir . "orig", $name);
            $origPath = $root . $dir . "orig/" . $name;
            $result["default"] = $dir . max($sizes) . "/" . $name;
            foreach ($sizes as $s) {
                $imageManager->createPath($root . $dir . $s);
                $newPath = $root . $dir . $s . "/" . $name;
                $imageManager->resize($origPath, $newPath, $s, null, true);
                $result[$s] = $dir . $s . "/" . $name;
            }
            return $result;
        }
        return null;
    }
}
