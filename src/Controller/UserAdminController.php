<?php

namespace Score\CmsBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Score\CmsBundle\Entity\Group;
use Score\CmsBundle\Entity\User;
use Score\CmsBundle\Entity\Users;
use Score\CmsBundle\Form\UserPassType;
use Score\CmsBundle\Form\UsersType;
use Score\CmsBundle\Form\UserType;
//use Score\CmsBundle\Services\PassManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
//use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Score\BaseBundle\Services\Generator;
use Score\CmsBundle\Services\MailerManager;


class UserAdminController extends AbstractController
{

    protected function canIAccess($lvl = null)
    {
        if ($lvl === 'all') {
            return in_array('ROLE_CMS', $this->getUser()->getRoles());
        }
        $allowed = ["ROLE_USER_MANAGER", "ROLE_ADMIN"];
        return (count(array_intersect($allowed, $this->getUser()->getRoles())) >= 1);
    }

    protected function defaultRedirect()
    {
        $this->addFlash("warning", "score.alert.user.lowRole");
        return $this->redirectToRoute('admin_start');
    }

    // protected function updateuser(User $user, $em)
    // {
    //     if ($user->getNewroles2()) {
    //         $user->removeAllGroups();
    //         $groupRepo = $em->getRepository(Group::class);
    //         foreach ($user->getNewroles2() as $r) {
    //             $user->addGroup($groupRepo->find($r));
    //         }
    //     }
    //     $em->persist($user);
    //     $em->flush();
    //     return $user;
    // }

    /**
     * Lists all user entities.
     *
     * @Route("/admin/users", name="admin_users", methods={"GET"})
     *
     */
    public function indexAction()
    {
        if (!$this->canIAccess())
            return $this->defaultRedirect();


        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository(User::class)->findBy(["isActive" => true], ["id" => 'DESC']);

        return $this->render('@ScoreCms/User/index.html.twig', [
            'users' => $users,
        ]);
    }

    /**
     * Creates a new user entity.
     *
     * @Route("/admin/users/new", name="admin_user_new", methods={"GET", "POST"})
     */
    public function newAction(UserPasswordHasherInterface $passwordEncoder, Request $request)
    {
        if (!$this->canIAccess())
            return $this->defaultRedirect();

        $em = $this->getDoctrine()->getManager();
        $user = new User();
        $groups = $em->getRepository(Group::class)->findBy(["visibility" => true], ["id" => 'DESC']);

        $form = $this->createForm(UserType::class, $user, ["groups" => $groups]);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $plainPass = "pass";
            $pass = $passwordEncoder->hashPassword($user, $plainPass);
            $user->setPassword($pass);
            $adminGroup = $em->getRepository(Group::class)->findOneBy(["role" => "ROLE_ADMIN"]);
            if ($adminGroup)
                $user->removeGroup($adminGroup);
            $em->persist($user);
            $em->flush();

            $this->addFlash("admin-success", "score.alert.user.created");
            return $this->redirectToRoute('admin_user_edit', ['id' => $user->getId()]);
        }

        return $this->render('@ScoreCms/User/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
            'create' => true
        ]);
    }

    /**
     * Displays a form to edit an existing user entity.
     *
     * @Route("/admin/users/edit/{id}", name="admin_user_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, $id)
    {
        if (!$this->canIAccess())
            return $this->defaultRedirect();

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->find($id);
        $groups = $em->getRepository(Group::class)->findBy(["visibility" => true], ["id" => 'DESC']);

        $wasAdmin = $user->getIsAdmin();

        $form = $this->createForm(UserType::class, $user, ["groups" => $groups]);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $adminGroup = $em->getRepository(Group::class)->findOneBy(["role" => "ROLE_ADMIN"]);
            if ($this->getUser() === $user || !$this->getUser()->getIsAdmin()) {
                if ($wasAdmin && $adminGroup) {
                    $user->addGroup($adminGroup);
                } elseif (!$wasAdmin && $adminGroup) {
                    $user->removeGroup($adminGroup);
                }
            }
            $em->persist($user);
            $em->flush();
            $this->addFlash("admin-success", "score.alert.user.edited");
            return $this->redirectToRoute('admin_user_edit', ['id' => $user->getId()]);
        }

        return $this->render('@ScoreCms/User/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     *
     * @Route("/admin/users/all", name="admin_users_all", methods={"GET", "POST"})
     *
     */
    public function editAllAction(Request $request)
    {
        if (!$this->canIAccess())
            return $this->defaultRedirect();


        $em = $this->getDoctrine()->getManager();
        //$groups = $em->getRepository(Group::class)->findBy(["visibility" => true], ["id" => 'DESC']);
        $groups = $em->getRepository(Group::class)->findBy([], ["id" => 'ASC']);
        $users = $em->getRepository(User::class)->findBy([], ["id" => 'DESC']);
        $usersCollection = new Users($users);
        $wereAdmins = array_map(function ($n) {
            return ($n->getIsAdmin());
        }, $users);

        $form = $this->createForm(UsersType::class, $usersCollection, ["groups" => $groups]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $adminGroup = $em->getRepository(Group::class)->findOneBy(["role" => "ROLE_ADMIN"]);
            foreach ($users as $key => $user) {
                if ($this->getUser() === $user || !$this->getUser()->getIsAdmin()) {
                    if ($wereAdmins[$key] && $adminGroup) {
                        $user->addGroup($adminGroup);
                    } elseif (!$wereAdmins[$key] && $adminGroup) {
                        $user->removeGroup($adminGroup);
                    }
                }
                $em->persist($user);
            }
            $em->flush();
            $this->addFlash("admin-success", "score.alert.user.all.edited");
            return $this->redirectToRoute('admin_users_all');
        }

        return $this->render('@ScoreCms/User/edit_all.html.twig', [
            'users' => $users,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a user entity.
     *
     * @Route("/admin/users/{id}", name="user_show", methods={"GET"})
     *
     */
    public function showAction($id = null)
    {
        if (!$this->canIAccess())
            return $this->defaultRedirect();

        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        return $this->render('@ScoreCms/User/show.html.twig', [
            'user' => $user,
        ]);
    }


    /**
     * Deletes a user entity.
     *
     * @Route("/admin/users/delete/{id}", name="admin_user_delete", methods={"GET"})
     *
     */
    public function deleteAction(Request $request, $id)
    {
        if (!$this->canIAccess())
            return $this->defaultRedirect();

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->find($id);

        if ($user && !$user->getIsAdmin()) {
            // foreach ($user->getGroups() as $g) {
            //     $g->removeUser($user);
            //     $em->persist($g);
            // }
            $em->remove($user);
            $em->flush();
        } else {
            $this->addFlash("admin-danger", "score.alert.user.notDeleted");
        }
        return $this->redirectToRoute('admin_users');
    }

    /**
     *
     * @Route("/admin/my-pass-change", name="admin_my_pass_change", methods={"GET", "POST"})
     *
     */
    public function changeMyPasswordAction(UserPasswordHasherInterface $passwordEncoder, Request $request)
    {
        return $this->changePasswordAction($passwordEncoder, $request, null, true);
    }


    /**
     *
     * @Route("/admin/users/pass/{id}", name="admin_user_pass", methods={"GET", "POST"})
     *
     */
    public function changePasswordAction(UserPasswordHasherInterface $passwordEncoder, Request $request, $id, $selfEditing = false)
    {
        if (!$this->canIAccess($selfEditing === false ? null : 'all'))
            return $this->defaultRedirect();

        $em = $this->getDoctrine()->getManager();
        $user = $selfEditing === false 
            ? $em->getRepository(User::class)->find($id)
            : $this->getUser() ;

        if ($user) {
            $form = $this->createForm(UserPassType::class, $user);
            $form->handleRequest($request);


            if ($form->isSubmitted() && $form->isValid()) {
                if (
                    strlen($request->get('user_pass')['newpass1']) > 6
                    && $request->get('user_pass')['newpass1'] === $request->get('user_pass')['newpass2']
                    //                    && ($this->canIEditUsers() || $user->getPassword() === $passwordEncoder->hashPassword($user, $request->get('user_pass')['oldpass']))
                ) {
                    $plainPass = $request->get('user_pass')['newpass1'];
                    $pass = $passwordEncoder->hashPassword($user, $plainPass);
                    $user->setPassword($pass);
                    $em->persist($user);
                    $em->flush();
                    $this->addFlash("admin-success", "score.alert.user.passwordChanged");
                } else {
                    $this->addFlash("admin-danger", "score.alert.user.passwordNotChanged");
                }
                return $selfEditing === false 
                    ? $this->redirectToRoute('admin_users')
                    : $this->redirectToRoute('admin_start');
            }
        }

        return $this->render('@ScoreCms/User/change_pass.html.twig', [
            'form' => $form->createView(),
            'user' => $user,
        ]);
    }

    /**
     *
     * @Route("/admin/users/pass-email/{id}", name="admin_user_pass_email", methods={"GET"})
     *
     */
    public function changePasswordEmailAction( MailerManager $mailerManager, Generator $generator, UserPasswordHasherInterface $passwordEncoder, Request $request, $id)
    {
        if (!$this->canIAccess())
            return $this->defaultRedirect();

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->find($id);

        if ($user) {
            $newPlainPass = $generator->generate(8);
            $pass = $passwordEncoder->hashPassword($user, $newPlainPass);
            $user->setPassword($pass);
            $em->persist($user);
            
            $body = $this->renderView(
                '@ScoreCms/Mail/user_pass_change.html.twig',
                ['newPlainPass' => $newPlainPass]
            );

            $mailerManager
                ->setTo($user->getEmail())
                ->setSubject("Upozornenie na zmenu prihlasovacích údajov.")
                ->setBody($body)
                ->send();

            $em->flush();
            $this->addFlash("admin-success", "score.alert.user.passwordChangedEmail");
            //return new Response($body);
        } else {
            $this->addFlash("admin-danger", "score.alert.user.passwordNotChangedEmail");
        }
        return $this->redirectToRoute('admin_users');


    }
}
