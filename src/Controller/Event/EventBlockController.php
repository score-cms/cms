<?php

namespace  Score\CmsBundle\Controller\Event;

use Score\CmsBundle\Entity\Event\Event;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class EventBlockController extends AbstractController
{

    public function topAction(EntityManagerInterface $entityManager): Response
    {
        $repo = $entityManager->getRepository(Event::class);
        $topEvents = $repo->findTopEvents();
        $bigEvent = array_shift($topEvents);

        return $this->render('@ScoreCms/Public/default/event_block_top.html.twig', [
            'events' => $topEvents,
            'bigEvent' => $bigEvent
        ]);
    }

    public function topByCategoryAction(EntityManagerInterface $entityManager, $category = null, $size = "small", $count = 4, $header = null): Response
    {
        $repo = $entityManager->getRepository(Event::class);
        $topArticles = $repo->findTopEventsByCategory($category, $count);
        $bigArticle = array_shift($topArticles);

        return $this->render('@ScoreCms/Public/default/article_block_top.html.twig', [
            'articles' => $topArticles,
            'bigArticle' => $bigArticle,
            'header' => $header,
            'size' => $size
        ]);
    }

}
