<?php

namespace Score\CmsBundle\Controller\Event;

use Doctrine\Common\Collections\ArrayCollection;
use Score\CmsBundle\Entity\Block\Block;
use Score\CmsBundle\Entity\Event\AllCategories;
use Score\CmsBundle\Entity\Event\Event as Event;
use Score\CmsBundle\Entity\Event\EventCategory;
use Score\CmsBundle\Entity\User;
use Score\CmsBundle\Form\Event\AllCategoriesType;
use Score\CmsBundle\Form\Event\EventType;
use Doctrine\ORM\EntityManagerInterface;
use Score\CmsBundle\Repository\EventDatagrid;
use Score\CmsBundle\Services\AccessManager;
use Score\CmsBundle\Services\BlockManager;
use Score\CmsBundle\Services\MetatagManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Score\BaseBundle\Services\SeoUrl;
use Score\CmsBundle\Services\ImageManager;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Score\CmsBundle\Services\AdminEventManager;
use Score\CmsBundle\Services\OptionManager;

/**
 * @Route("/admin/event")
 */
class EventAdminController extends \Score\BaseBundle\Controller\AdminController {

    protected function canIAccess($topManager = false) {
        $allowed = ["ROLE_EVENT_MANAGER", "ROLE_TOP_MANAGER", "ROLE_ADMIN"];
        if ($topManager) {
            $allowed = ["ROLE_TOP_MANAGER", "ROLE_ADMIN"];
        }
        return (count(array_intersect($allowed, $this->getUser()->getRoles())) >= 1);
    }

    protected function redirectHome() {
        $this->addFlash('danger', 'score.alert.user.lowRole');
        return $this->redirectToRoute('admin_start');
    }

    /**
     * Data for datatable
     *
     * @Route("/admin/event/datatable", name="event_admin_datatable",methods={"GET"})
     */
    public function datatableDataAction(SessionInterface $session, Request $request, SeoUrl $seoUrl) {

        if (!$this->canIAccess()) {
            return $this->redirectHome();
        }

        $em = $this->getDoctrine()->getManager();
        $datagrid = new EventDatagrid($em, Event::class, $this->container->get('router'));
        $datagrid->setPagerLength(10);
        $datagrid->setPagerStart($request->get('start'));
        $datagrid->setOrderParams($request->get('order'));
        $searchTerm = $seoUrl->removeDiacritic($request->get('search')['value']);
        $datagrid->setSearchCriteria($searchTerm);

        $data = $datagrid->getRecordsAsArray();

        $recordsTotal = $datagrid->getRecordsTotal();
        $recordsFiltered = $datagrid->getRecordsFiltered();

        $datagridData = array(
            'draw' => $this->getDatagridDraw($session),
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => $data,
        );
        return $this->json($datagridData);
    }


    // forwarded in Score\CmsBundle\Controller\ApiController;
    public function dataListAction(AccessManager $accessManager, EntityManagerInterface $entityManager): Response {
        if (!$this->canIAccess()) {
            return $this->redirectHome();
        }

        $checkAccess = $this->getParameter("access_checking") && !$this->canIAccess(true);
        $list = $entityManager->createQuery('
        SELECT e.id AS value, e.name, e.published, e.organizer, e.slug, e.date_from AS from, e.date_to AS to
        FROM ' . Event::class . ' e
        LEFT JOIN e.accesses acc 
        LEFT JOIN acc.user u 
        WHERE (e.isDeleted != :true OR e.isDeleted IS NULL)
            AND ' . ($checkAccess ? 'u.id = :userId' : ':userId IS NOT NULL') . '
        ORDER BY  e.date_to DESC, e.date_from DESC, e.name ASC
        ')
            ->setParameters([
                "true" => true,
                "userId" => $this->getUser()->getId(),
            ])
            ->getResult();

        $list = array_reduce($list, function ($carry, $item) {
            $item = array_change_key_case($item, CASE_LOWER);
            if (!isset($carry[$item['value']])) {
                $carry[$item['value']] = [
                    "name" => $item['name'],
                    "value" => $item['value'],
                    "slug" => $item['slug'],
                    "link" => '<a href="' . $this->generateUrl("score_cms_event_edit", ["id" => $item['value']]) . '">' . $item['name'] . '</a>',
                    "published" => $item['published'] === 1 ? '<i class="fa fa-check"></i>' : '<i class="fa fa-times"></i>',
                    "organizer" => $item['organizer'],
                    "publishedFromTo" => $item['from'] && $item['to']
                        ? ($item['from']->format('j.n.Y') . " – " . $item['to']->format('j.n.Y'))
                        : ($item['from']
                            ? ("od " . $item['from']->format('j.n.Y'))
                            : ($item['to']
                                ? ("do " . $item['to']->format('j.n.Y'))
                                : ""
                            )
                        ),
                    "action" => $this->canIAccess(true)
                        ? '<a class="to-trash-event" href="' . $this->generateUrl("score_cms_event_to_trash", ["id" => $item['value']]) . '">Presunúť do koša</a>' // tie v kosi sa tu nenacitavaju a ani ich tu nechceme
                        : "",
                ];
            }
            return $carry;
        }, []);

        return $this->json([
            "status" => "ok",
            "data" => array_values($list)
        ]);
    }

    protected function updateSearch($event, $seoUrl) {
        $text = $event->getName() . ' ' . $event->getTeaser() . ' ' . $event->getContent() . ' ' . $event->getContactPerson();
        $search = strip_tags(mb_strtolower($seoUrl->removeDiacritic($text)));
        $event->setSearch($search);
    }

    /**
     * @Route("/", name="score_cms_event2", methods={"GET"})
     */
    public function index(): Response {
        if (!$this->canIAccess()) {
            return $this->redirectHome();
        }

        return $this->render('@ScoreCms/Event/index.html.twig', [

        ]);
    }

    /**
     * @Route("/list", name="score_cms_event", methods={"GET"})
     */
    public function listAction(AccessManager $accessManager): Response {
        if (!$this->canIAccess()) {
            return $this->redirectHome();
        }

//        $em = $this->getDoctrine()->getManager();
//
//        $list = $em->getRepository(Event::class)->findBy([], ["createdAt" => "DESC"]);
//
//        if (!$this->canIAccess(true)) { //odfiltrovat tie z kosa
//            $list = array_filter($list, function ($v) {
//                return !$v->getIsDeleted();
//            });
//        }
//        $accesableList = [];
//        if ($this->getParameter("access_checking")) { // ak ma zobrazit len tie ktore uzivatel moze editovat
//            foreach ($list as $event) {
//                if ($accessManager->hasUserAccessTo($this->getUser(), $event)) {
//                    $accesableList[] = $event;
//                }
//            }
//        } else {
//            $accesableList = $list;
//        }


        return $this->render('@ScoreCms/Event/list.html.twig', [
//            'list' => $accesableList,
            'list' => [],
            'canDelete' => $this->canIAccess(true),
            'showAddEventBtn' => $this->canIAccess()
        ]);
    }

    protected function buildForm($event, $optionManager) {
        $multisite = [];
        foreach ($event->getSiteItemEvent() as $item) {
            $multisite[] = $item->getSite();
        }
        $event->setMultisite($multisite);
        $form = $this->createForm(EventType::class, $event, []);
        return $form;
    }

    /**
     * @Route("/new", name="score_cms_event_new", methods={"GET", "POST"})
     */
    public function new(
        Request                $request,
        EntityManagerInterface $entityManager,
        BlockManager           $blockManager,
        SeoUrl                 $seoUrl,
        OptionManager          $optionManager,
        AdminEventManager      $adminManager,
        ImageManager           $imageManager,
        AccessManager          $accessManager,
        MetatagManager         $metatagManager
    ): Response {
        if (!$this->canIAccess()) {
            return $this->redirectHome();
        }

        $event = new Event();
        $form = $this->buildForm($event, $optionManager);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user = $this->getUser();
            //generate slug
//            $slug = $seoUrl->generateSeoString($event->getName());
//            $event->setSlug($slug);

            $event->setSlug($seoUrl->generateSeoString($event->getSlug()));

            $event->setCreatedBy($user->getId());
            //$event->setEditedAt(new \Datetime());

            $icon = $event->getIcon();
            if (is_a($icon, '\Symfony\Component\HttpFoundation\File\UploadedFile')) {
                $fileName = $this->handleIconUpload($event, $icon, $adminManager, $imageManager, $params);
                $event->setIcon($fileName);
            } elseif ($icon) {
                $event->setIcon("");
            }
            $adminManager->handleMultisite($event);

            $blocks = $request->get('block_ids');
            $joints = $request->get('joint_ids');
            $blockManager->updateBlockJoints($event, $blocks, $joints);

            $accessManager->updateEntityAccess($user, $event, true, ["full_access" => true, "can_delete" => true]);

            if ($metatagManager->updateMetatagsFor($event, $this->getUser())) {
                $this->addFlash('edit_warning', 'score.alert.metatag.duplicity.warning');
            }
            //search
            $this->updateSearch($event, $seoUrl);
            $entityManager->persist($event);
            $entityManager->flush();
            $this->addFlash('event_create_success', 'Udalosť bola uložená');
            return $this->redirectToRoute('score_cms_event_edit', ['id' => $event->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('@ScoreCms/Event/new.html.twig', [
            'event' => $event,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}/edit", name="score_cms_event_edit", methods={"GET", "POST"})
     */
    public function edit(
        Request               $request,
        Event                 $event,
        AdminEventManager     $adminManager,
        ImageManager          $imageManager,
        BlockManager          $blockManager,
        OptionManager         $optionManager,
        ContainerBagInterface $params,
        SeoUrl                $seoUrl,
        AccessManager         $accessManager,
        MetatagManager        $metatagManager
    ): Response {
        if (!$this->canIAccess()) {
            return $this->redirectHome();
        }

        $em = $this->getDoctrine()->getManager();
        $origIcon = $event->getIcon();
        $form = $this->buildForm($event, $optionManager);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //$event->setEditedAt(new \Datetime());
            $icon = $event->getIcon();
            if (is_a($icon, UploadedFile::class)) {
                $fileName = $this->handleIconUpload($event, $icon, $adminManager, $imageManager, $params);
                $event->setIcon($fileName);
            } else {
                $event->setIcon($origIcon);
            }

            if ($request->request->get('event')['removeIcon'] === "true") {
                $event->setIcon("");
            }

            $event->setSlug($seoUrl->generateSeoString($event->getSlug()));

            $adminManager->handleMultisite($event);

            $blocks = $request->get('block_ids');
            $joints = $request->get('joint_ids');
            $blockManager->updateBlockJoints($event, $blocks, $joints);

            if ($metatagManager->updateMetatagsFor($event, $this->getUser())) {
                $this->addFlash('edit_warning', 'score.alert.metatag.duplicity.warning');
            }

            //search
            $this->updateSearch($event, $seoUrl);
            $em->persist($event);
            $em->flush();
            $this->addFlash('event_edit_success', 'Údaje boli zmenené');
            return $this->redirectToRoute('score_cms_event_edit', array('id' => $event->getId()));
        }

        return $this->render('@ScoreCms/Event/edit.html.twig', [
            'event' => $event,
            'form' => $form->createView(),
            'activeSection' => $request->get('s') ?: "content",
            'showAccessTab' => $accessManager->canUserEditAccessToEntity($this->getUser(), $event),
            'canDelete' => $accessManager->canUserDeleteEntity($this->getUser(), $event),
            'users' => $em->getRepository(User::class)->getListForAccess("event"),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="score_cms_event_delete")
     */
    public function delete(Request $request, Event $event, EntityManagerInterface $entityManager): Response {
        if (!$this->canIAccess(true)) {
            return $this->redirectHome();
        }

        foreach ($event->getEventCategories() as $eventCategory) {
            $event->removeEventCategory($eventCategory);
        }

        $entityManager->remove($event);
        $entityManager->flush();
        $this->addFlash('score_cms_event_success', 'Udalosť bola odstránená');
        return $this->redirectToRoute('score_cms_event', [], Response::HTTP_SEE_OTHER);
    }

    protected function handleIconUpload($event, $icon, $adminManager, $imageManager, $params) {
        $slug = $event->getSlug();
        if (
            $icon instanceof UploadedFile
            && $imageManager->isImage($icon)
        ) {
            $fileId = date('Y_m_d_h_i_s');
            $fileName = $event->getSlug() . '_' . $fileId . '.' . $icon->guessExtension();
            $fileThumb100Name = $event->getSlug() . '_' . $fileId . '_thumb_100x100.' . $icon->guessExtension();
            $fileThumb640Name = $event->getSlug() . '_' . $fileId . '_thumb_640x480.' . $icon->guessExtension();
            $uploadFolder = $adminManager->getUploadDir($params, $imageManager);
            $dir = $uploadFolder['absolute_path'];
            try {
                $icon->move($dir, $fileName);
                if ($this->getParameter("cms_event_thumbnail_cut") === true) {
                    $imageManager->scaleTo($dir . '/' . $fileName, $dir . '/' . $fileThumb100Name, 100, 100);
                    $imageManager->scaleTo($dir . '/' . $fileName, $dir . '/' . $fileThumb640Name, 600, 480);
                } else {
                    $imageManager->thumbOver($dir . '/' . $fileName, $dir . '/' . $fileThumb100Name, 100, 100);
                    $imageManager->thumbOver($dir . '/' . $fileName, $dir . '/' . $fileThumb640Name, 600, 480);
                }
            } catch (FileException $e) {
                echo $e;
            }
            return $uploadFolder['public_path'] . '/' . $fileName;
        }
        return "";
    }


    /**
     * @Route("/categories", name="score_cms_event_categories")
     */
    public function categoriesAction(Request $request) {
        if (!$this->canIAccess()) {
            return $this->redirectHome();
        }

        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository(EventCategory::class)->findBy([], ["createdAt" => "ASC"]);
        $allCategories = new AllCategories($categories);

        $form = $this->createForm(AllCategoriesType::class, $allCategories);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            foreach ($allCategories->getCategories() as $d) {
                $em->persist($d);
            }
            foreach ($categories as $d) {
                if (!$allCategories->getCategories()->contains($d)) {
                    $em->remove($d);
                }
            }
            $em->flush();
            $this->addFlash('success', 'score.alert.event.category.changed');
            return $this->redirectToRoute('score_cms_event_categories');
        }

        return $this->render('@ScoreCms/Event/all_categories_list_edit.html.twig', [
            "all" => $allCategories,
            "form" => $form->createView(),
            "part" => "categories"
        ]);
    }

    /**
     * @Route("/create-block/{part}/{seoName}", name="score_cms_event_create_block")
     */
    public function createBlockAction(Request $request, $part, $seoName) {
        if (!$this->canIAccess(true)) {
            return $this->redirectHome();
        }

        $em = $this->getDoctrine()->getManager();
        $block = new Block();
        $block->setType("controllerContent")
            ->setModule("Score\CmsBundle\Controller\Event\EventBlockController")
            ->setVisibility(true);

        if ($part === "categories") {
            $entity = $em->getRepository(EventCategory::class)->findOneBy(["seoName" => $seoName, "visibility" => true]);
            $block->setName("Výpis udalostí – " . $entity->getName() . "")
                ->setAction("topByCategoryAction")
                ->setParamsArr([
                    "category" => $entity->getSeoName(),
                    "size" => "big",
                    "count" => 4,
                    "header" => "Udalosti"
                ]);
        }
        $em->persist($block);
        $em->flush();

        return $this->redirectToRoute('score-block-edit-expert', ["id" => $block->getId()]);
    }


    /**
     * @Route("/to-trash/{id}", name="score_cms_event_to_trash")
     */
    public function toTrashAction(Request $request, AccessManager $accessManager, $id) {
        if (!$this->canIAccess()) {
            return $this->redirectHome();
        }

        $em = $this->getDoctrine()->getManager();
        $event = $em->getRepository(Event::class)->find($request->get('id'));
        if ($accessManager->canUserDeleteEntity($this->getUser(), $event)) {
            $event->setIsDeleted(true);
            $em->persist($event);
            $em->flush();
            $this->addFlash("admin-success", "score.alert.event.movedToTrash");
        }
        return $this->redirectToRoute('score_cms_event');
    }

    /**
     * @Route("/from-trash/{id}", name="score_cms_event_from_trash")
     */
    public function fromTrashAction(Request $request, $id) {
        if (!$this->canIAccess(true)) {
            return $this->redirectHome();
        }

        $em = $this->getDoctrine()->getManager();
        $event = $em->getRepository(Event::class)->find($request->get('id'));

        $event->setIsDeleted(false);
        $em->persist($event);
        $em->flush();

        $this->addFlash("admin-success", "score.alert.event.movedFromTrash");
        return $this->redirectToRoute('score_cms_event');
    }


    /**
     * @Route("/access/edit2", name="score_cms_event_edit_access")
     */
    public function editAccessAction(Request $request, AccessManager $accessManager) {
        if (!$this->canIAccess()) {
            return $this->redirectHome();
        }

        $em = $this->getDoctrine()->getManager();
        $event = $em->getRepository(Event::class)->find($request->get("event_id"));
        $user = $em->getRepository(User::class)->find($request->get("user_id"));
        if ($event && $user && $accessManager->canUserEditAccessToEntity($this->getUser(), $event)) {
            $params = [
                "full_access" => boolval($request->get("full_access")),
                "can_create" => boolval($request->get("can_create")),
                "can_delete" => boolval($request->get("can_delete")),
                "is_heritable" => boolval($request->get("is_heritable"))
            ];
            $hasAssess = boolval($request->get("has_access"));
            $accessManager->updateEntityAccess($user, $event, $hasAssess, $params);
            $em->persist($user); // vsetky zmeny v pravach vsetkych eventov sa vzdy tykaju jedneho usera preto staci len jeho persist
            $em->flush();

            $this->addFlash('score_cms_event_edit_success', 'Zmeny boli uložené');
            return $this->redirectToRoute('score_cms_event_edit', ['id' => $event->getId(), 's' => 'access']);
        }
        return $this->redirectToRoute('score_cms_event_edit', ['id' => $event->getId(), 's' => 'access']);
    }


}
