<?php

namespace Score\CmsBundle\Controller\Event;

use Score\BaseBundle\Services\Pager;
use Score\CmsBundle\Entity\Event\Event;
use Doctrine\ORM\EntityManagerInterface;
use Score\CmsBundle\Services\StatManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


/**
 * @Route("/udalosti")
 */
class PublicController extends AbstractController {


    /**
     * @Route("/", name="score_public_event_list", methods={"GET"})
     */
    public function listAction(Request $request, EntityManagerInterface $entityManager, StatManager $statManager, Pager $pager): Response {
        $repo = $entityManager->getRepository(Event::class);
        $page = $request->get('p', 1);
        $pageSize = $request->get('size', 6);
        $events = $repo->findEventsByPage(array('page' => $page, 'pageSize' => $pageSize), ['visible' => true]);
        $eventList = array_chunk($events['list'], 3);
        $pager->setUrl($request->getUri());
        $pager->setItemsPerPage($pageSize);
        $pager->setCount($events['total']);
        $pager->setPage($page);
        $pager->getPages();

        return $this->render('@ScoreCms/Public/default/event_list.html.twig', [
            'eventList' => $eventList,
            'pager' => $pager,
        ]);
    }

    /**
     * @Route("/{slug}", name="score_public_events_detail", methods={"GET"})
     */
    public function detailAction(EntityManagerInterface $entityManager, StatManager $statManager, $slug = null): Response {
        $repo = $entityManager->getRepository(Event::class);
        $event = $repo->findOneBy([
            'slug' => $slug,
            'published' => 1,
//            'isDeleted' => false
        ]);
        $responseCode = 200;
        if (!$event || $event->getIsDeleted()) {
            $event = new Event();
            $event->setName('404');
            $event->setContent('Táto udalosť sa tu nenachádza');
            $responseCode = 404;
        }

        $topEvents = $repo->findSidebarEvents($event);
        return new Response($this->renderView('@ScoreCms/Public/default/event_detail.html.twig', [
            'event' => $event,
            'events' => $topEvents,
        ]), $responseCode);
    }


}
