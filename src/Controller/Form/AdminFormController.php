<?php

namespace Score\CmsBundle\Controller\Form;


use Score\BaseBundle\Services\Generator;
use Score\CmsBundle\Entity\Block\Block;
use Score\CmsBundle\Entity\Block\BlockJointPage;
use Score\CmsBundle\Entity\Form\Webform;
use Score\CmsBundle\Entity\Form\WebformField;
use Score\CmsBundle\Entity\Page\Page;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Contracts\Translation\TranslatorInterface;
use Score\CmsBundle\Controller\Form\PublicController;
use Score\CmsBundle\Entity\Form\WebformValue;

class AdminFormController extends PublicController {
    protected function canIAccess() {
        $allowed = ["ROLE_FORM_MANAGER", "ROLE_ADMIN"];
        return (count(array_intersect($allowed, $this->getUser()->getRoles())) >= 1);
    }

    protected function redirectHome() {
        $this->addFlash('danger', 'score.alert.user.lowRole');
        return $this->redirectToRoute('admin_start');
    }


    /**
     * @Route("/admin/forms", name="score_cms_forms")
     * @Route("/admin/form/list", name="score_cms_form_list")
     */
    public function formListAction() {
        if (!$this->canIAccess())
            return $this->redirectHome();


        $em = $this->getDoctrine()->getManager();
        $forms = $em->getRepository(Webform::class)->findBy([], ["editedAt" => "DESC"]);

//        $users = [];
//        foreach ($forms as $form) {
//            $user = $em->getRepository(User::class)->find($form->getCreatorId());
//            $users[] = [
//                "name" => $user->getUsername(),
//                "email" => $user->getEmail()
//            ];
//        }
        return $this->render('@ScoreCms/Form/index.html.twig', [
            "forms" => $forms,
//            "users" => $users
        ]);
    }


    /**
     * @Route("/admin/form/example/{id}", name="score_cms_form_example")
     */
    public function exampleAction(Request $request, $id = null) {
        if (!$this->canIAccess())
            return $this->redirectHome();

        $page = new Page();
        $page->setTitle("Ukážka");
        $blockJoint = new BlockJointPage();
        $block = new Block();
        $block->setParamsArr(["formId" => $id]);
        $block->setType("form");
        $blockJoint->setBlock($block);
        $blockJoint->setPlace("center");
        $page->addBlockJoint($blockJoint);
        return $this->render('@ScoreCms/Public/default/subpage.html.twig', [
            'page' => $page,
        ]);
    }


    /**
     * @Route("/admin/form/new", name="score_cms_form_new")
     * @Route("/admin/form/edit/{id}/{ref}", name="score_cms_form_edit")
     */
    public function formEditAction(Request $request, $id = null, $ref = null) {
        if (!$this->canIAccess())
            return $this->redirectHome();

        return $this->render('@ScoreCms/Form/edit.html.twig', [
            "formId" => $id,
            "referer" => $ref
        ]);
    }

    /**
     * @Route("/admin/form/entries/{id}/{entryId}", name="score_cms_form_data")
     */
    public function formDetailAction(Request $request, $id, $entryId = null) {
        if (!$this->canIAccess())
            return $this->redirectHome();

        return $this->render('@ScoreCms/Form/entries.html.twig', [
            "formId" => $id,
            "entryId" => $entryId
        ]);
    }


    /**
     * @Route("/admin/form/delete/{id}", name="score_cms_form_delete")
     */
    public function formDeleteAction(Request $request, $id = null) {
        if (!$this->canIAccess())
            return $this->redirectHome();

        $em = $this->getDoctrine()->getManager();
        $form = $em->getRepository(Webform::class)->findOneByIdentifier($id);

        if (!$form) {
            $this->addFlash("admin-danger", "score.alert.form.unableDelete");
            return $this->redirectToRoute('score_cms_form_list');
        }

        $blocks = $em->getRepository(Block::class)->findBy(["type" => "form"]);
        foreach ($blocks as $block) {
            if ($block->getParamsArr()["formId"] === $form->getIdentifier()) {
                if ($block->getBlockJoints()->count() > 0) {
                    $names = [];
                    foreach ($block->getBlockJoints() as $blockJoint) {
                        $names[] = $blockJoint->getEntity()->getName();
                    }
                    $this->addFlash('block_danger', 'Unable to delete, still used in: ' . implode(", ", $names) . '.');
                    return $this->redirectToRoute('score_cms_form_list');
                } else {
                    $em->remove($block);
                }
            }
        }

        foreach ($form->getFields() as $f) {
            $form->removeField($f);
            $em->persist($form);
            $em->remove($f);
        }
        foreach ($form->getValues() as $v) {
            $form->removeValue($v);
            $em->persist($form);
            $em->remove($v);
        }
        $em->remove($form);
        $em->flush();

        $this->addFlash("admin-success", "score.alert.form.deleted");
        return $this->redirectToRoute('score_cms_form_list');
    }

    /**
     * @Route("/admin/api/form-save", name="score_cms_api_form-save")
     */
    public function apiSaveFormAction(Request $request, TranslatorInterface $translator, Generator $generator) {
        if (!$this->canIAccess())
            return $this->redirectHome();

        header('Access-Control-Allow-Origin: *');

        $em = $this->getDoctrine()->getManager();
        $form = $em->getRepository(Webform::class)->findOneByIdentifier($request->get("formId"));

        // New form with new block
        if (!$form) {
            $form = new Webform();
            $form->setIdentifier($generator->generate(16))
                ->setCreatorId($this->getUser() ? $this->getUser()->getId() : null)
                ->setName($request->get("formName"));
            $em->persist($form);
            $block = new Block();
            $block->setName($form->getName())
                ->setType("form")
                ->setParamsArr(["formId" => $form->getIdentifier()]);
            $em->persist($block);
            $em->flush();
        }

        //Update BlockName if Changed
        if ($form->getName() !== $request->get("formName")) {
            //$blocks = $em->getRepository(Block::class)->findBy(["type" => "form", "name" => $form->getName()]);
            $blocks = $em->getRepository(Block::class)->findBy(["type" => "form"]);
            foreach ($blocks as $block) {
                if ($block->getParamsArr()["formId"] === $form->getIdentifier()) {
                    $block->setName($request->get("formName"));
                    $em->persist($block);
                }
            }
        }

        $form->setName($request->get("formName"))
            ->setDescription($request->get("formDescription"))
            ->setSuccessMessage($request->get("successMessage"))
            ->setIsActive($request->get("isActive") === 'true')
            ->setSendMail($request->get("sendMail") === 'true')
            ->setMailListArr(json_decode($request->get("mailList"), true))
            ->setParamsArr(json_decode($request->get("params"), true))
            ->setVisibility($request->get("visibility") === 'true');
        if (!$form->getCreatorId())
            $form->setCreatorId($this->getUser() ? $this->getUser()->getId() : null);


        $items = json_decode($request->get("items"), true);

        $fields = $form->getFields();

        // ziskanie klucov pre overenie ci treba vymazavat
        $itemsKeys = array_column($items, 'identifier');

        //vymazanie tych poli ktore boli odstranene
        foreach ($fields as $f) {
            if (!in_array($f->getIdentifier(), $itemsKeys)) {
                $form->removeField($f);
                $em->persist($form);
                $em->remove($f);
            }
        }
        $em->flush();

        // update a pridanie novych poli do formulara
        foreach ($items as $item) {
            $identifier = $item["identifier"];
            $field = new WebformField();
            foreach ($form->getFields() as $f) {
                if ($identifier === $f->getIdentifier()) {
                    $field = $f;
                }
            };
            $form->addField($field);
            $field->setIdentifier($item["identifier"])
                ->setTitle($item["title"])
                ->setHelpText($item["helpText"])
                ->setType($item["type"])
                ->setSortOrder($item["sortOrder"])
                ->setRequired($item["required"])
                ->setVisibility(true);
            if (array_key_exists("choices", $item))
                $field->setChoicesArr($item["choices"]);
            if (array_key_exists("rows", $item))
                $field->setRows($item["rows"]);
            if (array_key_exists("length", $item))
                $field->setLength($item["length"]);
            if (array_key_exists("min", $item))
                $field->setMin($item["min"]);
            if (array_key_exists("max", $item))
                $field->setMax($item["max"]);

            $em->persist($field);
        }

        $em->persist($form);
        $em->flush();

        return $this->loadFormAction($request, $translator, $form->getIdentifier());
    }

    /**
     * @Route("/admin/api/form-load-data/{id}/{entryId}", name="score_cms_api_form-load-data")
     */
    public function apiLoadFormDataAction(Request $request, TranslatorInterface $translator, $id = null, $entryId = null) {
        if (!$this->canIAccess())
            return $this->redirectHome();

        header('Access-Control-Allow-Origin: *');

        $em = $this->getDoctrine()->getManager();
        $form = $em->getRepository(Webform::class)->findOneByIdentifier($id);
        if (!$form) {
            return new JsonResponse([
                "status" => "error",
                "message" => $translator->trans("score.cms.form.error.loadData")
            ]);
        }

        $values = [];
        if (!$entryId) {
            $values = $form->getValues();
        } else {
            $values = $em->getRepository(WebformValue::class)->findBy(["identifier" => $entryId]);
            $values = $values ? $values : [];
        }

        $entries = [];
        foreach ($values as $value) {
            $i = $value->getIdentifier();
            if (!isset($entries[$i]))
                $entries[$i] = [
                    "identifier" => $i,
                    "isUser" => $value->getUserId() !== null,
                    "date" => $value->getCreatedAt()->format("Y.m.d - H:i"),
                    "values" => []
                ];
            $entries[$i]["values"][] = [
                "identifier" => $i,
                "fieldIdentifier" => $value->getFieldIdentifier(),
                "value" => $value->getValueArr(),
            ];
        }
        usort($entries, function ($a, $b) {
            return $b['date'] <=> $a['date'];
        });
        return new JsonResponse([
            'status' => "ok",
            "entries" => $entries
        ]);
    }
}
