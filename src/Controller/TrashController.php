<?php

namespace Score\CmsBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Score\CmsBundle\Entity\Article\Article;
use Score\CmsBundle\Entity\Block\Block;
use Score\CmsBundle\Entity\Document\Document;
use Score\CmsBundle\Entity\Event\Event;
use Score\CmsBundle\Services\AccessManager;
use Score\CmsBundle\Entity\Page\Page;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/admin/trash-bin", name="admin-trash-")
 */
class TrashController extends AbstractController {
    protected function canIAccess() {
        $allowed = ["ROLE_TOP_MANAGER", "ROLE_ADMIN"];
        return (count(array_intersect($allowed, $this->getUser()->getRoles())) >= 1);
    }

    /**
     * @Route("/filtered", name="filtered", methods={"GET"})
     */
    public function trashMyListAction(AccessManager $accessManager, EntityManagerInterface $entityManager): Response {

        $checkAccess = true;
        return $this->render('@ScoreCms/Default/trash.html.twig', [
            'pages' => $this->getPages($entityManager, $checkAccess),
            'documents' => $this->getDocuments($entityManager, $checkAccess),
            'articles' => $this->getArticles($entityManager, $checkAccess),
            'events' => $this->getEvents($entityManager, $checkAccess),
            'blocks' => $this->getBlocks($entityManager, $checkAccess),
        ]);
    }

    /**
     * @Route("/", name="all", methods={"GET"})
     */
    public function trashAllListAction(AccessManager $accessManager, EntityManagerInterface $entityManager): Response {
        if (!$this->canIAccess()) {
            return $this->redirectToRoute("admin-trash-filtered");
        }
        $checkAccess = false;
        return $this->render('@ScoreCms/Default/trash.html.twig', [
            'pages' => $this->getPages($entityManager, $checkAccess),
            'documents' => $this->getDocuments($entityManager, $checkAccess),
            'articles' => $this->getArticles($entityManager, $checkAccess),
            'events' => $this->getEvents($entityManager, $checkAccess),
            'blocks' => $this->getBlocks($entityManager, $checkAccess),
        ]);
    }


    private function getPages(EntityManagerInterface $em, $checkAccess): ?array {
        $builder = $em->createQueryBuilder();
        $builder->select("p")
            ->from(Page::class, "p")
            ->andWhere('p.isDeleted = :true')
            ->setParameter("true", true);
        if ($checkAccess) {
            $builder->leftJoin("p.accesses", "acc")
                ->leftJoin("acc.user", "u")
                ->andWhere("u = :user")
                ->setParameter("user", $this->getUser());
        }
        $builder->add('orderBy', 'p.editedAt DESC');
        return $builder->getQuery()->getResult();
    }

    private function getDocuments(EntityManagerInterface $em, $checkAccess): ?array {
        $builder = $em->createQueryBuilder();
        $builder->select("d")
            ->from(Document::class, "d")
            ->andWhere('d.isDeleted = :true')
            ->setParameter("true", true);
        if ($checkAccess) {
            $builder->leftJoin("d.accesses", "acc")
                ->leftJoin("acc.user", "u")
                ->andWhere("u = :user")
                ->setParameter("user", $this->getUser());
        }
        $builder->add('orderBy', 'd.editedAt DESC');
        return $builder->getQuery()->getResult();
    }

    private function getArticles(EntityManagerInterface $em, $checkAccess): ?array {
        $builder = $em->createQueryBuilder();
        $builder->select("a")
            ->from(Article::class, "a")
            ->andWhere('a.isDeleted = :true')
            ->setParameter("true", true);
        if ($checkAccess) {
            $builder->leftJoin("a.accesses", "acc")
                ->leftJoin("acc.user", "u")
                ->andWhere("u = :user")
                ->setParameter("user", $this->getUser());
        }
        $builder->add('orderBy', 'a.editedAt DESC');
        return $builder->getQuery()->getResult();
    }

    private function getEvents(EntityManagerInterface $em, $checkAccess): ?array {
        $builder = $em->createQueryBuilder();
        $builder->select("e")
            ->from(Event::class, "e")
            ->andWhere('e.isDeleted = :true')
            ->setParameter("true", true);
        if ($checkAccess) {
            $builder->leftJoin("e.accesses", "acc")
                ->leftJoin("acc.user", "u")
                ->andWhere("u = :user")
                ->setParameter("user", $this->getUser());
        }
        $builder->add('orderBy', 'e.editedAt DESC');
        return $builder->getQuery()->getResult();
    }

    private function getBlocks(EntityManagerInterface $em, $checkAccess): ?array {
        if ($checkAccess)
            return [];
        $builder = $em->createQueryBuilder();
        $builder->select("b")
            ->from(Block::class, "b")
            ->andWhere('b.isDeleted = :true')
            ->setParameter("true", true)
            ->add('orderBy', 'b.editedAt DESC');
        return $builder->getQuery()->getResult();
    }

}
