<?php

namespace Score\CmsBundle\Controller\Gallery;

use Score\CmsBundle\Entity\Gallery\Gallery;
use Score\CmsBundle\Entity\Gallery\GalleryImage;
use Score\CmsBundle\Services\ImageManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Contracts\Translation\TranslatorInterface;

class PublicController extends AbstractController
{

    /**
     * @Route("/api/gallery-load/{id}", name="cms_api_gallery_load")
     */
    public function loadAction(Request $request, TranslatorInterface $translator, $id)
    {
        header('Access-Control-Allow-Origin: *');

        // TODO then disable for localhost!
        $isAdmin = $request->headers->get('referer') && str_contains($request->headers->get('referer'), '/admin/gallery/'); // editovanie, example

        $em = $this->getDoctrine()->getManager();
        //$gallery = $em->getRepository(Gallery::class)->find($id);
        $gallery = $em->getRepository(Gallery::class)->findOneByIdentifier($id);

        // ak neexistuje alebo nieje verejny (cez zapor)
        if (!($gallery && ($gallery->getVisibility() || $isAdmin))) {
            return new JsonResponse([
                "status" => "error",
                "message" => $translator->trans("score.cms.gallery.error.loadData")
            ]);
        }

        $data = [
            //"ref" => $request->headers->get('referer'),
            "galleryId" => $gallery->getIdentifier(),
            "identifier" => $gallery->getIdentifier(),
            "name" => $gallery->getName(),
            "description" => $gallery->getDescription(),
            "isActive" => $gallery->getIsActive(),
            "endMessage" => $gallery->getEndMessage(),
            "visibility" => $gallery->getVisibility(),
            "type" => $gallery->getType(),
            "params" => $gallery->getParamsArr(),
            "galleryDir" => $this->getParameter('cms_gallery_dir'), //. '/' . $gallery->getIdentifier(),
            "imageStrategy" => $this->getParameter('cms_gallery_image_strategy'),
            "imageSubDirs" => $this->getParameter('cms_gallery_subdirs'),
            "images" => []
        ];

        foreach ($gallery->getImages() as $image) {
            if (($image->getVisibility() && $image->getIsActive()) || $isAdmin)
                $data["images"][] = [
                    "galleryId" => $gallery->getIdentifier(),
                    "identifier" => $image->getIdentifier(),
                    "name" => $image->getName(),
                    "description" => $image->getDescription(),
                    "sortOrder" => $image->getSortOrder(),
                    "isActive" => $image->getIsActive(),
                    "size" => $image->getSize(),
                    "extension" => $image->getExtension(),
                    "filename" => $image->getOriginalFilename(),
                    "x" => $image->getParamsArr()["x"],
                    "y" => $image->getParamsArr()["y"],
                    "ratio" => $image->getParamsArr()["ratioOrig"],
                ];
        }

        usort($data["images"], function ($a, $b) {
            return $a['sortOrder'] <=> $b['sortOrder'];
        });

        return new JsonResponse([
            'status' => "ok",
            "data" => $data
        ]);
    }



    /**
     * @Route("/file/g/{galleryId}/{imageId}-{size}.{ext}", name="cms-gallery-load-file")
     */
    public function fileAction(Request $request, ImageManager $imageManager, $galleryId, $imageId, $size, $ext)
    {

        //header('Access-Control-Allow-Origin: *');
        $previews = $this->getParameter('cms_gallery_subdirs')["preview"];
        $closest = null;
        foreach ($previews  as $s)
            if (!$closest && $s >= $size)
                $closest = "preview/$s";
        if (!$closest)
            $closest = "preview/" . $previews[count($previews) - 1];

        $strategy = $this->getParameter('cms_gallery_image_strategy');

        // basic simulation of loading time delay (3sec pri 1920)
        //sleep(3*$size/1920);

        $em = $this->getDoctrine()->getManager();
        $image = $em->getRepository(GalleryImage::class)->findOneByIdentifier($imageId);


        if ($image) {
            if ($strategy === 'redirect') { // redirect
                return $this->redirect($this->getParameter('cms_gallery_dir') . "/$galleryId/$closest/$imageId.$ext");
            } elseif ($strategy === 'open') { // open
                $filePath = $this->getParameter('cms_gallery_upload_dir') . "/$galleryId/$closest/$imageId.$ext";
                return new BinaryFileResponse($filePath);
            } else { // create
                //$closest = "preview/".$previews[count($previews) - 1];
                $filePath = $this->getParameter('cms_gallery_upload_dir') . "/$galleryId/$closest/$imageId.$ext";

                // Metoda vrati hotovu odpoved vykresli obrazok kod dalej nepokracuje (alebo aj pokracuje?)
                $file = $imageManager->createImageFrom($filePath, $size, $ext);

                $response = new Response();
                $response->headers->set('Content-Type', in_array($ext, ['png', 'gif']) ? "image/$ext" : "image/jpeg");
                $response->setLastModified($image->getCreatedAt());
                $response->setContent($file);
                $response->setPublic();
                return $response;
            }
        } else {
            return $this->redirectToRoute('default_subpage', ['seoid' => "no-file/g/$galleryId/$imageId-$size.$ext"]);
        }
    }
}
                // $response = new Response();
                // $fileName = $imageId . '-' . $size . '.' . $ext;
                // $disposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_INLINE, $fileName);
                // $response->headers->set('Content-Disposition', $disposition);
                // $response->headers->set('Content-Type', in_array($ext, ['png', 'gif']) ? "image/$ext" : "image/jpeg");
                // $image = $this->getDoctrine()->getManager()->getRepository(GalleryImage::class)->findOneBy(["identifier" => $imageId]);
                // $closest = "preview/".$previews[count($previews) - 1];
                // $response->setContent($file);
                // return $response;
