<?php

namespace Score\CmsBundle\Controller\Gallery;

use Score\BaseBundle\Services\Generator;
use Score\CmsBundle\Entity\Block\BlockJointPage;
use Score\CmsBundle\Services\ImageManager;
use Score\CmsBundle\Entity\Gallery\Gallery;
use Score\CmsBundle\Entity\Gallery\GalleryImage;
use Score\CmsBundle\Entity\Block\Block;
use Score\CmsBundle\Entity\Page\Page;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Contracts\Translation\TranslatorInterface;
use Score\CmsBundle\Controller\Gallery\PublicController;

class AdminGalleryController extends PublicController {

    protected function canIAccess() {
        $allowed = ["ROLE_GALLERY_MANAGER", "ROLE_ADMIN"];
        return (count(array_intersect($allowed, $this->getUser()->getRoles())) >= 1);
    }

    protected function redirectHome() {
        $this->addFlash('danger', 'score.alert.user.lowRole');
        return $this->redirectToRoute('admin_start');
    }

    /**
     * @Route("/admin/gallery", name="score_cms_galleries")
     * @Route("/admin/gallery/list", name="score_cms_gallery_list")
     */
    public function listAction() {
        if (!$this->canIAccess())
            return $this->redirectHome();

        $em = $this->getDoctrine()->getManager();
        $galleries = $em->getRepository(Gallery::class)->findBy([], ["editedAt" => "DESC"]);

        return $this->render('@ScoreCms/Gallery/index.html.twig', [
            "galleries" => $galleries,
        ]);
    }


    /**
     * @Route("/admin/gallery/example/{id}", name="score_cms_gallery_example")
     */
    public function exampleAction(Request $request, $id = null) {
        if (!$this->canIAccess())
            return $this->redirectHome();

        $page = new Page();
        $page->setTitle("Ukážka Galérie");
        $blockJoint = new BlockJointPage();
        $block = new Block();
        $block->setParamsArr(["galleryId" => $id]);
        $block->setType("gallery");
        $blockJoint->setBlock($block);
        $blockJoint->setPlace("center");
        $page->addBlockJoint($blockJoint);
        return $this->render('@ScoreCms/Public/default/subpage.html.twig', [
            'page' => $page,
        ]);
    }


    /**
     * @Route("/admin/gallery/new", name="score_cms_gallery_new")
     * @Route("/admin/gallery/edit/{id}/{ref}", name="score_cms_gallery_edit")
     */
    public function editAction(Request $request, $id = null, $ref = null) {
        if (!$this->canIAccess())
            return $this->redirectHome();

        return $this->render('@ScoreCms/Gallery/edit.html.twig', [
            "galleryId" => $id,
            "referer" => $ref
        ]);
    }


    /**
     * @Route("/admin/gallery/delete/{id}", name="score_cms_gallery_delete")
     */
    public function deleteAction(Request $request, $id = null) {
        if (!$this->canIAccess())
            return $this->redirectHome();

        $em = $this->getDoctrine()->getManager();
        $gallery = $em->getRepository(Gallery::class)->findOneByIdentifier($id);

        if (!$gallery) {
            $this->addFlash("admin-danger", "score.alert.gallery.unableDelete");
            return $this->redirectToRoute('score_cms_gallery_list');
        }

        $blocks = $em->getRepository(Block::class)->findBy(["type" => "gallery"]);
        foreach ($blocks as $block) {
            if ($block->getParamsArr()["galleryId"] === $gallery->getIdentifier()) {
                if ($block->getBlockJoints()->count() > 0) {
                    $names = [];
                    foreach ($block->getBlockJoints() as $blockJoint) {
                        $names[] = $blockJoint->getEntity()->getName();
                    }
                    $this->addFlash('block_danger', 'Unable to delete, still used in: ' . implode(", ", $names) . '.');
                    return $this->redirectToRoute('score_cms_gallery_list');
                } else {
                    $em->remove($block);
                }
            }
        }
        $em->persist($gallery);

        foreach ($gallery->getImages() as $img) {
            $em->remove($img);
        }

        // Remove all folders
        $rootDir = $this->getParameter('cms_gallery_upload_dir') . '/' . $gallery->getIdentifier();

        $this->removeAll($rootDir);

        $em->remove($gallery);
        $em->flush();

        $this->addFlash("admin-success", "score.alert.gallery.deleted");
        return $this->redirectToRoute('score_cms_gallery_list');
    }


    /**
     * @Route("/admin/api/gallery-save", name="score_cms_api_gallery-save")
     */
    public function apiSaveGalleryAction(Request $request, TranslatorInterface $translator, Generator $generator, ImageManager $imageManager) {
        if (!$this->canIAccess())
            return $this->redirectHome();

        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, X-Requested-With");
        $em = $this->getDoctrine()->getManager();
        $gallery = $em->getRepository(Gallery::class)->findOneByIdentifier($request->get("galleryId"));

        // New gallery with new block
        if (!$gallery) {
            $gallery = new Gallery();
            $gallery->setIdentifier($generator->generate(11))
                ->setCreatorId($this->getUser() ? $this->getUser()->getId() : 0)
                ->setType($request->get("type"))
                ->setRatio($request->get("ratio"))
                ->setName($request->get("name"));
            $block = new Block();
            $block->setName($gallery->getName())
                ->setType("gallery")
                ->setParamsArr(["galleryId" => $gallery->getIdentifier()]);
            //->setParamsArr(["galleryId" => $gallery->getId()]);
            $em->persist($block);
            $this->createSubDirs($gallery->getIdentifier(), $imageManager);
        }

        //Update BlockName if Changed
        if ($gallery->getName() !== $request->get("name")) {
            //$blocks = $em->getRepository(Block::class)->findBy(["type" => "gallery", "name" => $gallery->getName()]);
            $blocks = $em->getRepository(Block::class)->findBy(["type" => "gallery"]);
            foreach ($blocks as $block) {
                if ($block->getParamsArr()["galleryId"] === $gallery->getIdentifier()) {
                    $block->setName($request->get("name"));
                    $em->persist($block);
                }
            }
        }

        $gallery->setName($request->get("name"))
            ->setDescription($request->get("description"))
            ->setEndMessage($request->get("endMessage"))
            ->setType($request->get("type"))
            ->setRatio($request->get("ratio"))
            ->setParamsArr(json_decode($request->get("params"), true))
            ->setIsActive($request->get("isActive") === 'true')
            ->setVisibility($request->get("visibility") === 'true');
        if (!$gallery->getCreatorId())
            $gallery->setCreatorId($this->getUser() ? $this->getUser()->getId() : 0);
        $images = json_decode($request->get("images"), true);
        // ziskanie klucov pre overenie ci treba vymazavat
        $imagesKeys = array_column($images, 'identifier');


        foreach ($gallery->getImages() as $img) {
            //vymazanie tych obrazkov ktore boli odstranene
            if (!in_array($img->getIdentifier(), $imagesKeys)) {
                if ($this->removeImageFiles($img)) {
                    $gallery->removeImage($img);
                    $em->persist($gallery);
                    $em->remove($img);
                } else {
                    $img->setIsActive(false)
                        ->setVisibility(false);
                }
            } else {
                // update 
                $image = $images[array_search($img->getIdentifier(), $imagesKeys)];
                $img->setName($image["name"])
                    ->setDescription($image["description"])
                    ->setSortOrder($image["sortOrder"])
                    ->setIsActive($image["isActive"] === true)
                    ->setVisibility(strlen($image["name"]) >= 1);
                $em->persist($img);
            }
        }
        $em->persist($gallery);
        $em->flush();

        return $this->loadAction($request, $translator, $gallery->getIdentifier());
    }

    /**
     * @Route("/admin/api/gallery/upload-images", name="score_cms_api_gallery_upload_images")
     */
    public function apiAddImagesAction(Request $request, TranslatorInterface $translator, Generator $generator, ImageManager $imageManager) {
        if (!$this->canIAccess())
            return $this->redirectHome();

        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, X-Requested-With");

        //die("ok");

        $id = $request->get("galleryId");
        $em = $this->getDoctrine()->getManager();
        $gallery = $em->getRepository(Gallery::class)->findOneByIdentifier($id);
        if ($gallery) {
            $maxSize = 2.7 * 1000 * 1000;
            //$allowedTypes = ['image/png', 'image/jpeg', 'image/gif'];
            $allowedTypes = ['image/png', 'image/jpeg'];
            $file = $request->files->get('file');

            if (
                $file instanceof UploadedFile && ($file->getSize() < $maxSize)
                && in_array($file->getMimeType(), $allowedTypes)
            ) {

                $image = new GalleryImage();
                $image
                    ->setSize($file->getSize())
                    ->setSortOrder($gallery->getImages()->count())
                    ->setIsActive(true)
                    ->setIdentifier($generator->generate(16))
                    ->setOriginalFilename($file->getClientOriginalName())
                    ->setExtension($file->getClientOriginalExtension())
                    ->setVisibility(false);


                $gallery->addImage($image);
                $em->persist($gallery);
                $em->persist($image);

                $this->createSubDirs($gallery->getIdentifier(), $imageManager);
                $result = $this->createImageFiles($image, $file, $imageManager);

                $em->flush();
                return new JsonResponse($result);
            }
        }
        return new JsonResponse([
            "status" => "error",
            "message" => $translator->trans("score.cms.gallery.error.addImage"),
        ]);
    }


    protected function createImageFiles(GalleryImage $img, UploadedFile $file, ImageManager $imageManager) {

        $identifier = $img->getGallery()->getIdentifier();
        $rootDir = $this->getParameter('cms_gallery_upload_dir') . '/' . $identifier;
        $fileName = $img->getIdentifier() . '.' . $file->getClientOriginalExtension();
        $file->move($rootDir . '/orig', $fileName);
        $origPath = $rootDir . '/orig/' . $fileName;
        $img->setParamsArr($imageManager->readFileParams($origPath))
            //->setMetaArr(exif_read_data($origPath, "FILE, COMPUTED, ANY_TAG, IFD0, THUMBNAIL, COMMENT, EXIF", true, true));
            ->setMetaArr([]);

        $cut = $this->getParameter("cms_gallery_thumbnail_cut") === true;
        foreach ($this->getParameter('cms_gallery_subdirs') as $type => $widths) {
            foreach ($widths as $width) {
                $newPath = $rootDir . '/' . $type . '/' . $width . '/' . $fileName;
                if ($type === "thumbnail") {
                    if ($cut) {
                        $imageManager->scaleTo($origPath, $newPath, $width);
                    } else {
                        $imageManager->thumbOver($origPath, $newPath, $width);
                    }
                } else {
                    $imageManager->resize($origPath, $newPath, $width, null, true);
                }
            }
        }

        $result = [
            "status" => "ok",
            "galleryId" => $identifier,
            "identifier" => $img->getIdentifier(),
            "extension" => $img->getExtension(),
            "src" => $this->getParameter('cms_gallery_dir') . '/' . $identifier . '/thumbnail/200/' . $fileName,
        ];

        return $result;
    }


    protected function removeImageFiles(GalleryImage $img) {
        $res = true;
        $rootDir = $this->getParameter('cms_gallery_upload_dir') . '/' . $img->getGallery()->getIdentifier();
        $fileName = $img->getIdentifier() . '.' . $img->getExtension();

        foreach ($this->getParameter('cms_gallery_subdirs') as $type => $widths) {
            foreach ($widths as $width) {
                $path = $rootDir . '/' . $type . '/' . $width . '/' . $fileName;
                if (!file_exists($path) || unlink($path)) {
                    $res = $res === true;
                } else {
                    $res = false;
                }
            }
        }
        $path = $rootDir . '/orig/' . $fileName;
        if (!file_exists($path) || unlink($path)) {
            $res = $res === true;
        } else {
            $res = false;
        }

        return $res;
    }

    protected function createSubDirs($galleryId, $imageManager) {
        $dir = $this->getParameter('cms_gallery_upload_dir');

        if (is_dir($dir . "/" . $galleryId))
            return true;

        foreach ($this->getParameter('cms_gallery_subdirs') as $type => $widths) {
            foreach ($widths as $width) {
                $imageManager->createPath($dir . '/' . $galleryId . '/' . $type . '/' . $width);
            }
        }
        return true;
    }

    protected function removeAll($path) {
        if (is_dir($path)) {
            foreach (glob($path . '/*') as $file) {
                if (is_dir($file)) {
                    $this->removeAll($file);
                } else {
                    unlink($file);
                }
            }
            rmdir($path);
        } else {
            unlink($path);
        }
    }

}
