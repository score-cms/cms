<?php

namespace Score\CmsBundle\Controller;


use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Score\BaseBundle\Services\SeoUrl;
use Score\CmsBundle\Services\ImageManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends AbstractController
{

    /**
     * @Route("/admin/start", name="admin_start")
     */
    public function dashboardAction()
    {
        return $this->render('@ScoreCms/Default/start.html.twig', array(

        ));

        //return $this->redirect($this->generateUrl('user'));
        //return $this->render('ScoreCmsBundle:Default:dashboard.html.twig');
    }


}
