<?php

namespace Score\CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController {

    /**
     * @Route("/admin/login", name="admin_login")
     */
    public function loginAction(AuthenticationUtils $authenticationUtils, $public = false) {
        // get the login error if there is one
        //$authenticationUtils = $this->get('security.authentication_utils');
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

//        var_dump($error);
        if (!$error && $this->getUser()) {
            return $this->redirectToRoute('admin_start');
        }

        return $this->render('@ScoreCms/Security/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
            'targetPath' => $public ? "/user/show" : "/admin/start",
        ]);
    }

    /**
     * @Route("/admin/logout", name="admin_logout")
     */
    public function logoutAction() {
    }
}
