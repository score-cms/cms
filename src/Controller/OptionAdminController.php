<?php

namespace  Score\CmsBundle\Controller;
use Score\BaseBundle\Services\SeoUrl;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Score\CmsBundle\Entity\Option;
use Score\CmsBundle\Entity\OptionGroup;
use Score\CmsBundle\Form\Option\OptionFullType;
use Score\CmsBundle\Form\Option\OptionGroupType;
use Score\CmsBundle\Form\Option\OptionType;

/**
 * @Route("/admin/option")
 */
class OptionAdminController extends \Score\BaseBundle\Controller\AdminController
{
    protected function canIAccess(OptionGroup $group = null, $onlyAdmin = false)
    {
        if ($onlyAdmin) {
            return $this->getUser()->getIsAdmin();
        }
        if ($group && $group->getCheckUsers()) {
            return $this->getUser()->getIsAdmin() || $group->getUsers()->contains($this->getUser());
        }
        $allowed = ["ROLE_OPTION_MANAGER", "ROLE_ADMIN"];
        return (count(array_intersect($allowed, $this->getUser()->getRoles())) >= 1);
    }

    protected function redirectHome() {
        $this->addFlash('danger', 'score.alert.user.lowRole');
        return $this->redirectToRoute('admin_start');
    }

    /**
     * @Route("/code/{code}", name="score_cms_option_code", methods={"GET"})
     */
    public function optionCodeAction(Request $request, $code): Response
    {

        $em = $this->getDoctrine()->getManager();
        $option = $em->getRepository(Option::class)->findOneByCode($code);

        if ($option === null) {
            $this->addFlash('option_edit_danger', "Text [$code] zatial neexistuje.");
            return $this->redirect($request->headers->get('referer'));
            //return $this->redirectToRoute('score_cms_option_groups');
        }

        $groups = $em->getRepository(OptionGroup::class)->findAll();

        foreach ($groups as $g) {
            if ($g->getOptions()->contains($option)) {
                return $this->redirectToRoute('score_cms_option_edit', ['groupId' => $g->getId(), 'id' => $option->getId()]);
            }
        }
        
        return $this->redirectToRoute('score_cms_option_groups');

    }

    /**
     * @Route("/list/{id}", name="score_cms_option_list", methods={"GET"})
     */
    public function optionListAction(Request $request, $id): Response
    {

        $em = $this->getDoctrine()->getManager();
        $group = $em->getRepository(OptionGroup::class)->find($id);

        if (!$this->canIAccess($group))
            return $this->redirectToRoute('score_cms_option_groups');

        return $this->render('@ScoreCms/Option/list.html.twig', [
            'options' => $group ? $group->getOptions() : [],
            'groupId' => $id,
        ]);
    }

    
    /**
     * @Route("/new/{groupId}", name="score_cms_option_new", methods={"GET", "POST"})
     * @Route("/edit-full/{groupId}/{id}", name="score_cms_option_edit_full", methods={"GET", "POST"})
     */
    public function editFullAction(Request $request, $groupId, $id = 0): Response
    {
        if (!$this->canIAccess(null, true))
            return $this->redirectHome();

        return $this->editAction($request, $groupId, $id, true);
    }      


    /**
    * @Route("/edit/{groupId}/{id}", name="score_cms_option_edit", methods={"GET", "POST"})
    */
   public function editAction(Request $request, $groupId, $id, $full = false): Response
   {
        $em = $this->getDoctrine()->getManager();
        $group = $em->getRepository(OptionGroup::class)->find($groupId);
        $option = $em->getRepository(Option::class)->find($id);
       
        if (!$this->canIAccess($group))
            return $this->redirectHome();

        if (!$group) {
            $this->addFlash('danger', 'Chyba.');
            return $this->redirectToRoute('admin_start');
        }
        if (!$option) {
            $option = new Option();
            $group->addOption($option);
        }


        $form = $this->createForm($full ? OptionFullType::class : OptionType::class, $option);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($option);
            $em->flush();
            $this->addFlash('option_edit_success', 'Údaje boli zmenené');
            return $this->redirectToRoute('score_cms_option_edit', ['groupId' => $groupId, 'id' => $option->getId()]);
       }

       return $this->render('@ScoreCms/Option/edit.html.twig', [
            'entity' => $option,
            'form' => $form->createView(),
            'group' => $group,
       ]);
   }

    /**
     * @Route("/delete/{groupId}/{id}", name="score_cms_option_delete", methods={"GET"})
     */
    public function deleteAction(Request $request, $groupId, $id): Response
    {
        if (!$this->canIAccess(null, true))
            return $this->redirectHome();

        $em = $this->getDoctrine()->getManager();
        $option = $em->getRepository(Option::class)->find($id);
        $group = $em->getRepository(OptionGroup::class)->find($groupId);

        if ($option && $group) {    
            $group->removeOption($option);
            $em->persist($group);
            $em->remove($option);
            $em->flush();
            $this->addFlash("admin-success", "score.alert.option.deleted");
        }

        return $this->redirectToRoute('score_cms_option_list', ['id' => $groupId]);
    }       
    
    /**
     * ====================   OPTION  GROUP   ==========================
     */
    

    /**
     * @Route("/groups-list", name="score_cms_option_groups", methods={"GET"})
     */
    public function groupListAction(): Response
    {
        if (!$this->canIAccess())
            return $this->redirectHome();

        $em = $this->getDoctrine()->getManager();
        $groups = $em->getRepository(OptionGroup::class)->findBy([],['editedAt'=> 'ASC']);

        return $this->render('@ScoreCms/Option/groups_list.html.twig',[
            'groups' => $groups
        ]);
    }
    
    /**
     * @Route("/group-new", name="score_cms_option_group_new", methods={"GET", "POST"})
     * @Route("/group-edit/{id}", name="score_cms_option_group_edit", methods={"GET", "POST"})
     */
    public function groupEditAction(Request $request, $id = 0): Response
    {
        if (!$this->canIAccess(null, true))
            return $this->redirectHome();

        $em = $this->getDoctrine()->getManager();
        $group = $em->getRepository(OptionGroup::class)->find($id);
       
        if (!$group) {
            $group = new OptionGroup();
        }

        $form = $this->createForm(OptionGroupType::class, $group);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($group);
            $em->flush();
            $this->addFlash('option_edit_success', 'Údaje boli zmenené');
            return $this->redirectToRoute('score_cms_option_group_edit', array('id' => $group->getId()));
       }

       return $this->render('@ScoreCms/Option/edit.html.twig', [
           'entity' => $group,
           'form' => $form->createView(),
       ]);
    }
    
    /**
     * @Route("/group-delete/{id}", name="score_cms_option_group_delete", methods={"GET"})
     */
    public function groupDeleteAction(Request $request, $id): Response
    {
        if (!$this->canIAccess(null, true))
            return $this->redirectHome();

        $em = $this->getDoctrine()->getManager();
        $group = $em->getRepository(OptionGroup::class)->find($id);

        if ($group) {
            foreach($group->getOptions() as $option) {
                $group->removeOption($option);
                $em->remove($option);
            }           
            $em->persist($group);
            $em->flush();
            $em->remove($group);
            $em->flush();
            $this->addFlash("admin-success", "score.alert.option.group.deleted");
        }

        return $this->redirectToRoute('score_cms_option_groups');
    }
}
