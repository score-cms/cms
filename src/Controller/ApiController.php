<?php

namespace Score\CmsBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Score\BaseBundle\Services\AdjacencyArrayTreeManager;
use Score\CmsBundle\Controller\Article\ArticleAdminController;
use Score\CmsBundle\Controller\Document\DocumentAdminController;
use Score\CmsBundle\Controller\Event\EventAdminController;
use Score\CmsBundle\Entity\Access\Access;
use Score\CmsBundle\Entity\Block\BlockJointPage;
use Score\CmsBundle\Entity\Document\Document;
use Score\CmsBundle\Entity\Metatag\MetatagPage;
use Score\CmsBundle\Entity\Page\PageBlock;
use Score\CmsBundle\Entity\User;
use Score\CmsBundle\Services\AccessManager;
use Score\CmsBundle\Services\BlockManager;
use Score\CmsBundle\Form\Page\PageForm;
use Score\CmsBundle\Entity\Page\Page;
use Score\CmsBundle\Entity\Block\Block;
use Score\CmsBundle\Services\ImageManager;
use Score\CmsBundle\Services\MetatagManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Score\BaseBundle\Controller\AdminController as BaseAdminController;
use Score\CmsBundle\Services\AdminPageManager;
use Score\CmsBundle\Services\PageManager;
use Score\CmsBundle\Repository\PageDatagrid;
use Score\BaseBundle\Services\SeoUrl;
use Score\CmsBundle\Controller\Page\ApiController as PageApiController;


/**
 * @Route("/admin/score/api", name="admin-score-api-")
 */
class ApiController extends AbstractController {
//    protected function canIAccess($part = "", $topManager = false) {
//        $allowed = ["ROLE_TOP_MANAGER", "ROLE_ADMIN"];
//        if (!$topManager) {
//            if ($part === "page") array_push($allowed, "ROLE_PAGE_MANAGER");
//            if ($part === "article") array_push($allowed, "ROLE_ARTICLE_MANAGER");
//            if ($part === "event") array_push($allowed, "ROLE_EVENT_MANAGER");
//            if ($part === "document") array_push($allowed, "ROLE_DOCUMENT_MANAGER");
//        }
//        return (count(array_intersect($allowed, $this->getUser()->getRoles())) >= 1);
//    }


// =====================================  PAGE START ===========================================================

    /**
     * @Route("/page/data-list", name="page-list", methods={"GET"})
     */
    public function pageDataListAction(AccessManager $accessManager, EntityManagerInterface $entityManager, AdminPageManager $pageManager): Response {

        return $this->forward(PageApiController::class . "::dataListAction", [
            'accessManager' => $accessManager,
            'entityManager' => $entityManager,
            'pageManager' => $pageManager,
        ]);
    }

    /**
     * @Route("/page/availables", name="page-availables")
     */
    public function parentListAction(Request $request, AdminPageManager $pageManager) {
        return $this->forward(PageApiController::class . "::parentListAction", [
            'request' => $request,
            'pageManager' => $pageManager,
        ]);
    }

// =====================================  PAGE END ===========================================================
// =====================================  BLOCK START ===========================================================

    /**
     * @Route("/block/availables", name="block-availables")
     */
    public function pageBlocksDatatablesAction(Request $request, EntityManagerInterface $entityManager) {
        $blocks = $entityManager->getRepository(Block::class)->loadAvailableBlocksList();
        $blocks = array_map(function ($v) {
            $v = array_change_key_case($v, CASE_LOWER);
            return [
                "name" => $v["name"],
                "value" => $v["value"],
                "type" => $v["type"],
                "action" => '<a href="#" data-id="' . $v["value"] . '" data-name="' . $v["name"] . '">Pridať blok</a>'
            ];
        }, $blocks);

        return $this->json([
            "status" => "ok",
            "data" => array_values($blocks)
        ]);
    }

// =====================================  BLOCK END ===========================================================
// =====================================  ARTICLE START ===========================================================

    /**
     * @Route("/article/data-list", name="article-list", methods={"GET"})
     */
    public function articleDataListAction(AccessManager $accessManager, EntityManagerInterface $entityManager): Response {

        return $this->forward(ArticleAdminController::class . "::dataListAction", [
            'accessManager' => $accessManager,
            'entityManager' => $entityManager
        ]);
    }

// =====================================  ARTICLE END ===========================================================
// =====================================  DOCUMENT START ===========================================================

    /**
     * @Route("/document/data-list", name="document-list", methods={"GET"})
     */
    public function documentDataListAction(AccessManager $accessManager, EntityManagerInterface $entityManager): Response {

        return $this->forward(DocumentAdminController::class . "::dataListAction", [
            'accessManager' => $accessManager,
            'entityManager' => $entityManager
        ]);
    }

    /**
     * @Route("/document/availables", name="document-availables")
     */
    public function documentAvailablesAction(EntityManagerInterface $entityManager) {

        $list = $entityManager->createQuery('
        SELECT d.id AS value, d.name
        FROM ' . Document::class . ' d
        LEFT JOIN d.documentFiles df
        WHERE d.public = :true 
            AND (d.isDeleted != :true OR d.isDeleted IS NULL)
            AND df.isCurrent = :true
        ORDER BY d.name ASC
        ')
            ->setParameter("true", true)
            ->getResult();

        $list = array_reduce($list, function ($carry, $item) {
            $item = array_change_key_case($item, CASE_LOWER);
            if (!isset($carry[$item['value']])) {
                $carry[$item['value']] = [
                    "name" => $item['name'],
                    "value" => $item['value'],
                ];
            }
            return $carry;
        }, []);

        return $this->json([
            "status" => "ok",
            "data" => array_values($list)
        ]);
    }

    /**
     * @Route("/document/cke-plugin-data", name="document-cke", methods={"GET"})
     */
    public function documentCkeDataAction(EntityManagerInterface $entityManager): Response {

        return $this->forward(DocumentAdminController::class . "::apiListForCkeAction", [
            'entityManager' => $entityManager
        ]);
    }

// =====================================  DOCUMENT END ===========================================================
// =====================================  EVENT START ===========================================================

    /**
     * @Route("/event/data-list", name="event-list", methods={"GET"})
     */
    public function eventDataListAction(AccessManager $accessManager, EntityManagerInterface $entityManager): Response {

        return $this->forward(EventAdminController::class . "::dataListAction", [
            'accessManager' => $accessManager,
            'entityManager' => $entityManager
        ]);
    }

// =====================================  EVENT END ===========================================================


}
