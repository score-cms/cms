<?php

namespace Score\CmsBundle\Controller\Page;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Score\BaseBundle\Services\AdjacencyArrayTreeManager;
use Score\CmsBundle\Entity\Access\Access;
use Score\CmsBundle\Entity\Block\BlockJointPage;
use Score\CmsBundle\Entity\Metatag\MetatagPage;
use Score\CmsBundle\Entity\Page\PageBlock;
use Score\CmsBundle\Entity\User;
use Score\CmsBundle\Services\AccessManager;
use Score\CmsBundle\Services\BlockManager;
use Score\CmsBundle\Form\Page\PageForm;
use Score\CmsBundle\Entity\Page\Page;
use Score\CmsBundle\Entity\Block\Block;
use Score\CmsBundle\Services\ImageManager;
use Score\CmsBundle\Services\MetatagManager;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Score\BaseBundle\Controller\AdminController as BaseAdminController;
use Score\CmsBundle\Services\AdminPageManager;
use Score\CmsBundle\Services\PageManager;
use Score\CmsBundle\Repository\PageDatagrid;
use Score\BaseBundle\Services\SeoUrl;


class AdminController extends BaseAdminController {
    protected function canIAccess($topManager = false) {
        $allowed = ["ROLE_PAGE_MANAGER", "ROLE_TOP_MANAGER", "ROLE_ADMIN"];
        if ($topManager) {
            $allowed = ["ROLE_TOP_MANAGER", "ROLE_ADMIN"];
        }
        return (count(array_intersect($allowed, $this->getUser()->getRoles())) >= 1);
    }


    /**
     * Data for datatable
     *
     * @Route("/admin/page/datatable", name="page_admin_datatable", methods={"GET"})
     *
     */
    public function datatableDataAction(SessionInterface $session, Request $request) {

        if (!$this->canIAccess())
            return $this->redirectToRoute('admin_start');

        // $em = $this->getDoctrine()->getManager();
        // $datagrid = new PageDatagrid($em, Page::class, $this->container->get('router'));
        // $datagrid->setPagerLength(10);
        // $datagrid->setPagerStart($request->get('start'));
        // $datagrid->setOrderParams($request->get('order'));

        // $data = $datagrid->getRecordsAsArray();

        // $recordsTotal = $datagrid->getRecordsTotal();
        // $recordsFiltered = $datagrid->getRecordsFiltered();

        $datagridData = array(
            // 'draw' => $this->getDatagridDraw($session),
            // 'recordsTotal' => $recordsTotal,
            // 'recordsFiltered' => $recordsFiltered,
            // 'data' => $data,
        );
        return $this->json($datagridData);
    }


    public function getRootPage() {
        $em = $this->getDoctrine()->getManager();
        $pageRepo = $em->getRepository(Page::class);
        $page = $pageRepo->findOneBy(['name' => 'root']);
        //$pageManager = $this->get('score.admin.page.manager');
        //$page = $pageManager->getPageById(1);
        return $page;
    }

    /**
     * @Route("/admin/page/list", name="score_cms_pages")
     */
    public function indexAction(AdminPageManager $pageManager, EntityManagerInterface $entityManager, AccessManager $accessManager) {
        if (!$this->canIAccess())
            return $this->redirectToRoute('admin_start');

//        $list = $pageManager->getPageParentChoices($this->getRootPage()->getId());
//        if (!$this->canIAccess(true)) { //odfiltrovat tie z kosa
//            $list = array_filter($list, function ($v) {
//                return !$v->getIsDeleted();
//            });
//        }

        $checkAccess = $this->getParameter("access_checking") && !$this->canIAccess(true);

//        $accesableList = [];
//        if ($checkAccess) { // ak ma zobrazit len tie ktore uzivatel moze editovat
//            foreach ($list as $page) {
//                if ($accessManager->hasUserAccessTo($this->getUser(), $page)) {
//                    $accesableList[] = $page;
//                }
//            }
//        } else {
//            $accesableList = $list;
//        }


//        $tree = $pageManager->getPagesTree($accessManager, $this->getUser(), $this->getRootPage()->getChildren(), [], $checkAccess);
        $tree = $this->getPagesTree($entityManager, $pageManager);

        $ignoreAccess = !$this->getParameter("access_checking");

        return $this->render('@ScoreCms/Page/Admin/index.html.twig', [
            'list' => [], // $accesableList,
            'canDelete' => $this->canIAccess(true),
            'showAddPageBtn' => $ignoreAccess || $this->canIAccess(true),
            'tree' => $tree
        ]);
    }

    /**
     * @Route("/admin/page/subpages", name="score_cms_page_subpages")
     */
    public function subpagesAction() {
        if (!$this->canIAccess())
            return $this->redirectToRoute('admin_start');


        $pageManager = $this->get('score.admin.page.manager');
        $parent = $pageManager->getPageById($this->get('request')->get('parent_id'));
        $list = $pageManager->getPagesByParent($parent);

        $data = array();
        $result = array();
        foreach ($list as $page) {
            $entityData = $pageManager->convertEntityToArray($page);
            $entityData['link'] = $this->generateUrl('score_cms_page_subpages', array('parent_id' => $page->getId()));
            $entityData['edit_link'] = $this->generateUrl('score_cms_page_edit', array('id' => $page->getId()));
            $entityData['delete_link'] = $this->generateUrl('score_cms_page_to_trash', array('id' => $page->getId()));
            $data[] = $entityData;
        }

        // create a JSON-response with a 200 status code

        $result['subpages'] = $data;
        $result['add_link'] = $this->generateUrl('score_cms_page_create', array('parent_id' => $this->get('request')->get('parent_id')));


        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    protected function buildForm($page) {
        $multisite = [];
        foreach ($page->getSiteItemPage() as $item) {
            $multisite[] = $item->getSite();
        }
        $page->setMultisite($multisite);
        $form = $this->createForm(PageForm::class, $page, []);
        return $form;
    }

    /**
     * @Route("/admin/page/sub/{parentId}", name="score_cms_page_sub")
     */
    public function createSubAction(
        AdminPageManager $pageManager,
        Request          $request,
        SeoUrl           $seoUrl,
        AccessManager    $accessManager,
        MetatagManager   $metatagManager,
                         $parentId
    ) {
        if (!$this->canIAccess()) {
            $this->addFlash('page_access_danger', 'score.page.roleError');
            return $this->redirectToRoute('admin_start');
        }
        $em = $this->getDoctrine()->getManager();
        $parentPage = $em->getRepository(Page::class)->find($parentId);
        if (!$accessManager->canUserCreateSubPage($this->getUser(), $parentPage)) {
            $this->addFlash('page_access_danger', 'score.page.roleError');
            return $this->redirectToRoute('admin_start');
        }
        $page = new Page();
        $page->setParentPage($parentPage);
        $page->setStatus($parentPage->getStatus())
            ->setLayout($parentPage->getLayout());
        return $this->createAction($pageManager, $request, $seoUrl, $accessManager, $metatagManager, $page);
    }

    /**
     * @Route("/admin/page/create", name="score_cms_page_create")
     */
    public function createAction(
        AdminPageManager $pageManager,
        Request          $request,
        SeoUrl           $seoUrl,
        AccessManager    $accessManager,
        MetatagManager   $metatagManager,
                         $page = null
    ) {
        if (!$page) {
            if (!$this->canIAccess(true)) {
                $this->addFlash('page_access_danger', 'score.page.roleError');
                return $this->redirectToRoute('admin_start');
            }
            $page = new Page();
            $page->setStatus("public")
                ->setLayout("menu-center")
                ->setParentPage($this->getRootPage());
        }

        $page->setUserId($this->getUser()->getId());

        $form = $this->buildForm($page);
        $em = $this->getDoctrine()->getManager();

//        $parentPages = $pageManager->getPageParentChoices();
//        $parentPages = [$page->getParentPage()]; // $pageManager->getPageParentChoices();
        $parentPages = $page->getParentPage() && $page->getParentPage()->getName() !== "root" ? [$page->getParentPage()] : []; // $pageManager->getPageParentChoices();

        $form->handleRequest($request);
        $activeSection = (null == $request->get('s')) ? 'page' : $request->get('s');

        $ignoreAccess = !$this->getParameter("access_checking");
        $canEditParent = $ignoreAccess || $this->canIAccess(true);


        if ($form->isSubmitted() && $form->isValid()) {

            // PARENT
            if ($canEditParent && ($request->get('parent_page') !== $page->getParentPage()->getId())) {
                $parent = null;
                if ($request->get('parent_page')) {
                    $parent = $em->getRepository(Page::class)->find($request->get('parent_page'));
                }
                if (!$parent) {
                    $parent = $this->getRootPage($pageManager);
                }
                $page->setParentPage($parent);
                $page->setLvl($parent->getLvl() + 1);
                foreach ($page->getAllChildren() as $child) { // aktualizacia levelu pre vsetky podstranky
                    $child->setLvl($child->getParentPage()->getLvl() + 1);
                    $em->persist($child);
                }
            }
            $page->setLvl($page->getParentPage()->getLvl() + 1); // aj ak sa tu parent nemeni a je default tak lvl musi byt


            // IMAGE
            $icon = $page->getIcon();
            if (is_a($icon, UploadedFile::class)) {
                $fileName = $this->handleIconUpload($page, $icon, null, $imageManager);
                $page->setIcon($fileName);
            } else {
                $page->setIcon("");
            }

            $page->setSeoId($this->updateSeoId($seoUrl, $page->getSeoId()));


            $page->setLvl($page->getParentPage()->getLvl() + 1);
            $page->setSortOrder($page->getParentPage()->getChildren()->count());
            $accessManager->inheritPageAccess($page);
            $em->persist($page);
            $pageManager->handleMultisite($page);
            //create default block
            $block = $em->getRepository(Block::class)->findOneBy(['type' => 'pageContent']);
            $blockJoint = new BlockJointPage();
            $block->addblockJoint($blockJoint);
            $page->addBlockJoint($blockJoint);
            $blockJoint->setPlace('center');
            $blockJoint->setSortOrder(0);

            if ($metatagManager->updateMetatagsFor($page, $this->getUser())) {
                $this->addFlash('edit_warning', 'score.alert.metatag.duplicity.warning');
            }
            //search
            $this->updateSearch($page, $seoUrl);
            $em->persist($page);
            $em->flush();

            $this->addFlash('score_cms_page_edit_success', 'Stránka bola vytvorená.');
            return $this->redirectToRoute('score_cms_page_edit', ['id' => $page->getId()]);

//            return $this->redirectToRoute('score_cms_pages');
        }


        return $this->render('@ScoreCms/Page/Admin/create.html.twig', array(
            'form' => $form->createView(),
            'activeSection' => $activeSection,
            'parentPages' => $parentPages,
            'page' => $page,
            'canEditParent' => $canEditParent,
            'tree' => []
        ));
    }

    /**
     * @Route("/admin/page/edit", name="score_cms_page_edit")
     */
    public function editAction(
        Request                   $request,
        AdminPageManager          $pageManager,
        ImageManager              $imageManager,
        AdjacencyArrayTreeManager $treeManager,
        SeoUrl                    $seoUrl,
        AccessManager             $accessManager,
        MetatagManager            $metatagManager
    ) {
        if (!$this->canIAccess()) {
            $this->addFlash('page_access_danger', 'score.page.roleError');
            return $this->redirectToRoute('admin_start');
        }
        $em = $this->getDoctrine()->getManager();
        $pageRepo = $em->getRepository(Page::class);

        $treeManager->setRepository($pageRepo);

        $page = $pageManager->getPageById($request->get('id'));
        $checkAccess = $this->getParameter("access_checking") && !$this->canIAccess(true);

        if ($page->getName() === "root" && !in_array("ROLE_ADMIN", $this->getUser()->getRoles())) {
            return $this->redirectToRoute('admin_start');
        }

        if ($checkAccess) {
            if (!$accessManager->hasUserAccessTo($this->getUser(), $page)) {
                $this->addFlash('page_access_danger', 'score.page.roleError');
                return $this->redirectToRoute('admin_start');
            }
        }


        $origIcon = $page->getIcon();
        $form = $this->buildForm($page);

//        $parentPages = $pageManager->getPageParentChoices();
        $parentPages = $page->getParentPage() && $page->getParentPage()->getName() !== "root" ? [$page->getParentPage()] : []; // $pageManager->getPageParentChoices();

        $ignoreAccess = !$this->getParameter("access_checking");
        $canEditParent = $ignoreAccess || $this->canIAccess(true);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            // PARENT
            if ($canEditParent && ($request->get('parent_page') !== $page->getParentPage()->getId())) {
                $parent = null;
                if ($request->get('parent_page')) {
//                    $parent = $parentPages[$request->get('parent_page')];
                    $parent = $em->getRepository(Page::class)->find($request->get('parent_page'));
                }
                if (!$parent) {
                    $parent = $this->getRootPage($pageManager);
                }
                $page->setParentPage($parent);
                $page->setLvl($parent->getLvl() + 1);
                foreach ($page->getAllChildren() as $child) { // aktualizacia levelu pre vsetky podstranky
                    $child->setLvl($child->getParentPage()->getLvl() + 1);
                    $em->persist($child);
                }
            }

            // ICON
            $icon = $page->getIcon();
            if (is_a($icon, UploadedFile::class)) {
                $fileName = $this->handleIconUpload($page, $icon, $origIcon, $imageManager);
                $page->setIcon($fileName);
            } else {
                $page->setIcon($origIcon);
            }

            if ($request->get('page_form')['removeIcon'] === "true") {
                $this->handleIconUpload($page, null, $origIcon, $imageManager); //nech vymaze subory stareho obrazku
                $page->setIcon("");
            }

            if ($page->getRedirectToPage() === $page) {
                $this->addFlash('edit_warning', 'score.alert.page.selfRedirect');
                $page->setRedirectToPage(null);
            }

            $page->setSeoId($this->updateSeoId($seoUrl, $page->getSeoId()));

            // MULTISITE
            $pageManager->handleMultisite($page);

            $em = $this->getDoctrine()->getManager();

            if ($metatagManager->updateMetatagsFor($page, $this->getUser())) {
                $this->addFlash('edit_warning', 'score.alert.metatag.duplicity.warning');
            }

            //search
            $this->updateSearch($page, $seoUrl);
            $em->persist($page);
            $em->flush();

            $this->addFlash('score_cms_page_edit_success', 'Zmeny boli uložené');
            return $this->redirectToRoute('score_cms_page_edit', ['id' => $page->getId()]);
        }

        $activeSection = $request->get('s') ?: 'page';

        //$logs = $em->getRepository(ChangesLog::class)->findBy(["entity" => Page::class, "entityId" => $page->getId(), "field" => "content"], ["editedAt" => "DESC"], 15);
        $users = $em->getRepository(User::class)->getListForAccess("page");


//        $subpages = $pageManager->getPagesByParent($page);

//        $parents = $page->getAllParents(true);
//        $ids = $parents->map(function ($p) {
//            return $p->getId();
//        })->toArray();
//        $ids[] = $page->getId();
//
//        $tree = $pageManager->getPagesTree($accessManager, $this->getUser(), $parents->last() ? $parents->last()->getChildren() : [], $ids, $checkAccess);

        $tree = $this->getPagesTree($em, $pageManager, $page);

        return $this->render('@ScoreCms/Page/Admin/edit.html.twig', array(
            'form' => $form->createView(),
//            'availableBlocks' => [],
//            'availableBlocks' => $em->getRepository(Block::class)->loadBlocksList(),
            'availableBlocks' => $page->getBlockJoints()->map(function (BlockJointPage $bj) {
                return $bj->getBlock();
//            })->filter(function (Block $b) {
//                return $b->getType() !== "pageContent";
            })->toArray(),
            'page' => $page,
            //'blockJoints' => $blockJoints,
            'activeSection' => $activeSection,
            'parentPages' => $parentPages,
//            'subpages' => $subpages,
//            'logs' => $logs
            'users' => $users,
            'canEditParent' => $canEditParent,
            'showAccessTab' => $ignoreAccess || $accessManager->canUserEditAccessToPage($this->getUser(), $page),
            'showAddSubpageBtn' => $ignoreAccess || $accessManager->canUserCreateSubPage($this->getUser(), $page),
            'showDeleteBtn' => $ignoreAccess || $accessManager->canUserDeletePage($this->getUser(), $page),
            'tree' => $tree
        ));
    }

    public function updateSeoId(SeoUrl $seoManager, ?string $seo = "") {
        $result = [];
        $arr = explode("/", $seo);
        foreach ($arr as $item) {
            $result[] = $seoManager->generateSeoString($item);
        }

        return implode("/", $result);
    }

    /**
     * @Route("/admin/page-preview/{id}", name="subpage_preview")
     */
    public function subpagePreviewAction(Request $request, PageManager $pageManager) {
        if (!$this->canIAccess())
            return $this->redirectToRoute('admin_start');

        //$this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');
        $page = $pageManager->getPageById($request->get('id'));
        return $this->render('@ScoreCms/Public/default/subpage.html.twig', array(
            'page' => $page,
        ));
    }

    /**
     * @Route("/admin/page/reorder/{id}", name="score_cms_page_subpages_reorder")
     */
    public function reorderPagesAction(PageManager $pageManager, BlockManager $blockManager, Request $request, SeoUrl $seoUrl, $id) {
        if (!$this->canIAccess())
            return $this->redirectToRoute('admin_start');

        $em = $this->getDoctrine()->getManager();
        $page = $pageManager->getPageById($id);
        $sorted = $request->get('page_subpages');
        if ($page) {
            if ($sorted) {
                foreach ($page->getChildren() as $child) {
                    $i = array_search($child->getId(), $sorted);
                    $child->setSortOrder($i);
                    $em->persist($child);
                }

                $em->flush();
                $this->addFlash('score_cms_page_edit_success', 'Zmeny boli uložené');
            }
            return $this->redirectToRoute('score_cms_page_edit', ['id' => $page->getId(), 's' => "sub"]);
        }
        return $this->redirectToRoute('score_cms_pages');
    }


    /**
     * @Route("/admin/page/block/edit", name="score_cms_page_edit_blocks")
     */
    public function editBlocksAction(PageManager $pageManager, BlockManager $blockManager, Request $request, SeoUrl $seoUrl) {
        if (!$this->canIAccess())
            return $this->redirectToRoute('admin_start');

        $blocks = $request->get('page_blocks');
        $joints = $request->get('page_joints');

        $page = $pageManager->getPageById($request->get('id'));
        $em = $this->getDoctrine()->getManager();

        $tempCollection = new ArrayCollection();
        foreach ($joints as $place => $ids) {
            foreach ($ids as $index => $jointId) {
                $joint;
                if ($jointId) {
                    $joint = $em->getRepository(BlockJointPage::class)->find($jointId);
                } else {
                    $joint = new BlockJointPage();
                    $blockId = $blocks[$place][$index];
                    $block = $em->getRepository(Block::class)->find($blockId);
                    $block->addBlockJoint($joint);
                    $page->addBlockJoint($joint);
                }
                $joint->setPlace($place)
                    ->setSortOrder($index);
                $tempCollection->add($joint);
            }
        }

        foreach ($page->getBlockJoints() as $joint) {
            if (!$tempCollection->contains($joint)) {
                $page->removeBlockJoint($joint);
            }
        }
        $em->persist($page);
        $em->flush();

        $this->updateSearch($page, $seoUrl);
        $em->persist($page);
        $em->flush();
        $this->addFlash('score_cms_page_edit_success', 'Zmeny boli uložené');
        return $this->redirectToRoute('score_cms_page_edit', array('id' => $page->getId(), 's' => 'block'));
    }

    /**
     * @Route("/admin/page/block/inherit/{id}/{jointId}", name="score_cms_page_inherit_block")
     */
    public function inheritBlockAction(PageManager $pageManager, BlockManager $blockManager, Request $request, SeoUrl $seoUrl, $id, $jointId) {
        if (!$this->canIAccess())
            return $this->redirectToRoute('admin_start');

        $em = $this->getDoctrine()->getManager();
        $page = $em->getRepository(Page::class)->find($id);
        $blockJoint = $em->getRepository(BlockJointPage::class)->find($jointId);

        $count = 0;
        foreach ($page->getAllChildren() as $child) {
            $exists = false;
            foreach ($child->getBlockJoints() as $joint) {
                if ($joint->getPlace() === $blockJoint->getPlace() && $joint->getBlock() === $blockJoint->getBlock()) {
                    $exists = true;
                }
            }
            if (!$exists) {
                $joint = new BlockJointPage();
                $joint->setPlace($blockJoint->getPlace())
                    ->setSortOrder($child->getBlockJoints()->count());
                $blockJoint->getBlock()->addBlockJoint($joint);
                $child->addBlockJoint($joint);
                $this->updateSearch($child, $seoUrl);
                $em->persist($child);
                $count++;
            }
        }

        $em->flush();
        $message = 'Žiadne podstránky neboli zmenene.';
        if ($count > 4)
            $message = "Bolo upravenych $count podstránok.";
        elseif ($count > 1)
            $message = "Boli upravené $count podstránky.";
        elseif ($count === 1)
            $message = "Bola upravená 1 podstránka.";


        $this->addFlash('score_cms_page_edit_success', $message);
        return $this->redirectToRoute('score_cms_page_edit', ['id' => $page->getId(), 's' => 'block']);
    }

    /**
     * @Route("/admin/page/metatag/inherit/{id}/{metatagId}", name="score_cms_page_inherit_metatag")
     */
    public function inheritMetatagAction(PageManager $pageManager, MetatagManager $metatagManager, Request $request, SeoUrl $seoUrl, $id, $metatagId) {
        if (!$this->canIAccess())
            return $this->redirectToRoute('admin_start');

        return $this->redirectToRoute('score_cms_page_edit', ['id' => $id]);

        $em = $this->getDoctrine()->getManager();
        $page = $em->getRepository(Page::class)->find($id);
        $pageMetatag = $em->getRepository(MetatagPage::class)->find($metatagId);

//        var_dump($pageMetatag);
//        die;

        $count = 0;
        foreach ($page->getAllChildren() as $child) {
            $metatag = new MetatagPage();
            $metatag->setMetatagPattern($pageMetatag->getMetatagPattern())
                ->setValue($pageMetatag->getValue());
            $child->addMetatag($metatag);
            $metatagManager->updateMetatagsFor($child, $this->getUser());

            $em->persist($child);
            $count++;
        }

        $em->flush();

        $message = 'Žiadne podstránky neboli zmenene.';
        if ($count > 4)
            $message = "Bolo upravenych $count podstránok.";
        elseif ($count > 1)
            $message = "Boli upravené $count podstránky.";
        elseif ($count === 1)
            $message = "Bola upravená 1 podstránka.";


        $this->addFlash('score_cms_page_edit_success', $message);
        return $this->redirectToRoute('score_cms_page_edit', ['id' => $page->getId(), 's' => 'block']);
    }


    /**
     * @Route("/admin/page/to-trash/{id}", name="score_cms_page_to_trash")
     */
    public function toTrashAction(Request $request, PageManager $pageManager, $id) {
        if (!$this->canIAccess())
            return $this->redirectToRoute('admin_start');

        $em = $this->getDoctrine()->getManager();
        $page = $pageManager->getPageById($request->get('id'));

//        if ($page->getChildren()->count()) {
//            $this->addFlash("admin-danger", "score.alert.page.errorHasChildren");
//            return $this->redirectToRoute('score_cms_pages');
//        }

        $page->setIsDeleted(true);
        $em->persist($page);
        $em->flush();

        $this->addFlash("admin-success", "score.alert.page.movedToTrash");
        return $this->redirectToRoute('score_cms_pages');
    }

    /**
     * @Route("/admin/page/from-trash/{id}", name="score_cms_page_from_trash")
     */
    public function fromTrashAction(Request $request, PageManager $pageManager, $id) {
        if (!$this->canIAccess(true))
            return $this->redirectToRoute('admin_start');

        $em = $this->getDoctrine()->getManager();
        $page = $pageManager->getPageById($request->get('id'));

        $page->setIsDeleted(false);
        $em->persist($page);
        $em->flush();

        $this->addFlash("admin-success", "score.alert.page.movedFromTrash");
        return $this->redirectToRoute('score_cms_pages');
    }


    /**
     * @Route("/admin/page/delete/{id}", name="score_cms_page_delete")
     */
    public function deleteAction(Request $request, PageManager $pageManager, AdjacencyArrayTreeManager $treeManager, $id) {
        if (!$this->canIAccess())
            return $this->redirectToRoute('admin_start');

        $em = $this->getDoctrine()->getManager();
        $page = $pageManager->getPageById($request->get('id'));

        if ($page->getChildren()->count()) {
            $this->addFlash("admin-danger", "score.alert.page.errorHasChildren");
            return $this->redirectToRoute('score_cms_pages');
        }

        foreach ($page->getRedirectedPages() as $p) {
            $p->setRedirectToPage(null);
        }

        $em->remove($page);
        $em->flush();

        $this->addFlash("admin-success", "score.alert.page.deleted");
        return $this->redirectToRoute('score_cms_pages');
    }

    /**
     * @Route("/admin/page/access/edit", name="score_cms_page_edit_access")
     */
    public function editAccessAction(Request $request, AccessManager $accessManager) {
        if (!$this->canIAccess())
            return $this->redirectToRoute('admin_start');

        $em = $this->getDoctrine()->getManager();
        $page = $em->getRepository(Page::class)->find($request->get("page_id"));
        $user = $em->getRepository(User::class)->find($request->get("user_id"));

        if ($page && $user) {

            $params = [
                "full_access" => boolval($request->get("full_access")),
                "can_create" => boolval($request->get("can_create")),
                "can_delete" => boolval($request->get("can_delete"))
            ];
            $hasAssess = boolval($request->get("has_access"));
            $isHeritable = boolval($request->get("is_heritable"));

            $accessManager->updatePageAccess($user, $page, $hasAssess, $isHeritable, $params);
            $em->persist($user); // vsetky zmeny v pravach vsetkych stranok sa vzdy tykaju jedneho usera preto staci len jeho persist
            $em->flush();

            $this->addFlash('score_cms_page_edit_success', 'Zmeny boli uložené');
            return $this->redirectToRoute('score_cms_page_edit', ['id' => $page->getId(), 's' => 'access']);

        }

        return $this->redirectToRoute('score_cms_page_edit', ['id' => $page->getId(), 's' => 'access']);
    }

    protected function updateSearch($page, $seoUrl) {
        $text = $page->getName() . ' ' . $page->getTitle() . ' ' . $page->getContent() . ' ' . $page->getSeoId();
        foreach ($page->getBlockJoints() as $pb) {
            $text .= $pb->getBlock()->getContent();
            // TODO dorobit aj Texty a Widgety v html blokoch(aj opacne?)
        }
        $search = strip_tags(mb_strtolower($seoUrl->removeDiacritic($text)));
        $page->setSearch($search);
    }

    /**
     * @Route("/admin/page/pageblock-to-blockjoint", name="score_cms_page_pageblock_to_blockjoint")
     */
    public function refactoreBlocksAction() {
        if (!$this->canIAccess(true))
            return $this->redirectToRoute('admin_start');

        $em = $this->getDoctrine()->getManager();
        $pbs = $em->getRepository(PageBlock::class)->findAll();
        foreach ($pbs as $pb) {
            $joint = new BlockJointPage();
            $joint
                ->setPage($pb->getPage())
                ->setBlock($pb->getBlock())
                ->setPlace($pb->getPlace())
                ->setSortOrder($pb->getSortOrder());
            $em->persist($joint);
            $em->remove($pb);
        }
        $em->flush();
        $this->addFlash('score_cms_page_edit_success', 'Zmeny boli uložené – pageBlock zmeneny na blockJointPage.');
        return $this->redirectToRoute('score_cms_pages');
//        return new Response("<html><body><p>asdf</p></body></html>");

    }

    protected function handleIconUpload($page, $icon, $oldIcon, ImageManager $imageManager) {
        $fileId = date('yzHis');
        $slug = str_replace("/", "_", $page->getSeoId()) . "_" . $fileId;

        if ($oldIcon) {
            // REMOVE OLD ICONS
            $parts = explode('.', $oldIcon);
            $extension = end($parts);
            $root = $this->getParameter('kernel.project_dir') . '/public';
            $files = [
                $root . $oldIcon,
                $root . str_replace('.' . $extension, '', $oldIcon) . '_thumb_640x480.' . $extension,
                $root . str_replace('.' . $extension, '', $oldIcon) . '_thumb_100x100.' . $extension,
            ];
            foreach ($files as $file) {
                file_exists($file) && unlink($file);
            }
        }

        if (
            $icon instanceof UploadedFile
            && $imageManager->isImage($icon)
        ) {
            $fileName = $slug . '.' . $icon->guessExtension();
            $fileThumb100Name = $slug . '_thumb_100x100.' . $icon->guessExtension();
            $fileThumb640Name = $slug . '_thumb_640x480.' . $icon->guessExtension();
            $dir = $this->getParameter("kernel.project_dir") . "/public" . $this->getParameter("page_public_upload_directory");
            if (!file_exists($dir)) {
                $imageManager->createPath($dir);
            }

            try {
                $icon->move($dir, $fileName);
                if ($this->getParameter("cms_page_thumbnail_cut") === true) {
                    $imageManager->scaleTo($dir . '/' . $fileName, $dir . '/' . $fileThumb100Name, 100, 100);
                    $imageManager->scaleTo($dir . '/' . $fileName, $dir . '/' . $fileThumb640Name, 600, 480);
                } else {
                    $imageManager->thumbOver($dir . '/' . $fileName, $dir . '/' . $fileThumb100Name, 100, 100);
                    $imageManager->thumbOver($dir . '/' . $fileName, $dir . '/' . $fileThumb640Name, 600, 480);
                }
            } catch (FileException $e) {
                //echo $e;
            }
            return $this->getParameter("page_public_upload_directory") . '/' . $fileName;
        }
        return "";

    }

    private function getPagesTree(EntityManagerInterface $entityManager, AdminPageManager $pageManager, ?Page $page = null) {

        $id = $page ? $page->getId() : 0;

        $checkAccess = $this->getParameter("access_checking") && !$this->canIAccess(true);

        $conn = $entityManager->getConnection();
        $sql = '
            SELECT  p.id, p.name, p.parent_id AS parentid, p.lvl, p.seo_id, p.redirect_to_id AS redirect, p.status, p.isdeleted,
            CASE WHEN ' . ($checkAccess ? 'ac.user_id = :userId' : ':userId IS NOT NULL') . ' THEN 1 ELSE 0 END AS canaccess,
            (SELECT COUNT(*) FROM cms_page p3 WHERE p3.parent_id = p.id) AS count
            FROM cms_page p
            LEFT JOIN cms_access ac ON ac.entity_id = p.id AND ac.access_entity = \'page\' AND ac.user_id = :userId
            WHERE p.parent_id IN (
                SELECT p2.id
                FROM cms_page p2
                START WITH p2.id = :pageId
                CONNECT BY PRIOR  p2.parent_id = p2.id
            ) OR  p.lvl < 2
            ORDER BY  p.lvl,  p.sort_order ASC
        ';

        $list = $conn->fetchAllAssociative($sql, [
            "userId" => $this->getUser()->getId(),
            "pageId" => $id,
        ]);

        $list = array_reduce($list, function ($carry, $item) {
            $item = array_change_key_case($item, CASE_LOWER);
            if (!in_array($item['id'], array_column($carry[$item['parentid']] ?? [], 'id')))
                $carry[$item['parentid']][] = $item;
            return $carry;
        }, []);

        $root = $list[$list[""][0]["id"] ?? 0] ?? [];

        return $pageManager->buildTree($root, $list);
    }

}
