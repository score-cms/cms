<?php

namespace Score\CmsBundle\Controller\Page;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Score\BaseBundle\Services\AdjacencyArrayTreeManager;
use Score\CmsBundle\Entity\Access\Access;
use Score\CmsBundle\Entity\Block\BlockJointPage;
use Score\CmsBundle\Entity\Metatag\MetatagPage;
use Score\CmsBundle\Entity\Page\PageBlock;
use Score\CmsBundle\Entity\User;
use Score\CmsBundle\Services\AccessManager;
use Score\CmsBundle\Services\BlockManager;
use Score\CmsBundle\Form\Page\PageForm;
use Score\CmsBundle\Entity\Page\Page;
use Score\CmsBundle\Entity\Block\Block;
use Score\CmsBundle\Services\ImageManager;
use Score\CmsBundle\Services\MetatagManager;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Score\BaseBundle\Controller\AdminController as BaseAdminController;
use Score\CmsBundle\Services\AdminPageManager;
use Score\CmsBundle\Services\PageManager;
use Score\CmsBundle\Repository\PageDatagrid;
use Score\BaseBundle\Services\SeoUrl;


class ApiController extends BaseAdminController {
    protected function canIAccess($topManager = false) {
        $allowed = ["ROLE_PAGE_MANAGER", "ROLE_TOP_MANAGER", "ROLE_ADMIN"];
        if ($topManager) {
            $allowed = ["ROLE_TOP_MANAGER", "ROLE_ADMIN"];
        }
        return (count(array_intersect($allowed, $this->getUser()->getRoles())) >= 1);
    }


    // forwarded in Score\CmsBundle\Controller\ApiController;
    public function dataListAction(AccessManager $accessManager, EntityManagerInterface $entityManager, AdminPageManager $pageManager): Response {
        if (!$this->canIAccess()) {
            $this->addFlash('page_access_danger', 'score.page.roleError');
            return $this->redirectToRoute('admin_start');
        }

        $checkAccess = $this->getParameter("access_checking") && !$this->canIAccess(true);
        //$checkAccess = true;

        $list = $entityManager->createQuery('
            SELECT p.id, p.name, p.lvl, p.seoId, p.parentId
            FROM ' . Page::class . ' p
            LEFT JOIN p.accesses acc 
            LEFT JOIN acc.user u 
            WHERE p.name != :root AND 
        ' . ($checkAccess ? 'u.id = :userId' : ':userId IS NOT NULL') . '
            AND (p.isDeleted != 1 OR p.isDeleted IS NULL)
            ORDER BY  p.lvl,  p.sortOrder ASC
        ')
            ->setParameters([
                "root" => "root",
                "userId" => $this->getUser()->getId(),
            ])
            ->getResult();

        $ids = array_column($list, "id");

        $listReduced = array_reduce($list, function ($carry, $item) {
            $item = array_change_key_case($item, CASE_LOWER);
            $item["name"] = str_repeat(" -", max($item["lvl"] - 1, 0)) . " " . $item["name"];
            if (!in_array($item['id'], array_column($carry[$item['parentid']] ?? [], 'id')))
                $carry[$item['parentid']][] = $item;
            return $carry;
        }, []);

        $keys = array_filter(array_keys($listReduced), function ($key) use ($ids) {
            return !in_array($key, $ids);
        });

        $root = array_merge(...array_filter($listReduced, function ($item, $key) use ($keys) {
            return in_array($key, $keys);
        }, ARRAY_FILTER_USE_BOTH));

        $list = $pageManager->buildTree($root, $listReduced, false);

        $list = array_map(function ($item) {
            return [
                "name" => $item["name"],
                "link" => '<a href="' . $this->generateUrl("score_cms_page_edit", ["id" => $item["id"]]) . '">' . $item["name"] . '</a>',
                "lvl" => $item["lvl"],
                "value" => $item["id"],
                "seoId" => "/" . $item["seoid"],
                "action" => $this->canIAccess(true)
                    ? '<a class="to-trash-page" href="' . $this->generateUrl("score_cms_page_to_trash", ["id" => $item["id"]]) . '">Presunúť do koša</a>' // tie v kosi sa tu nenacitavaju a ani ich tu nechceme
                    : "",
            ];
        }, $list);

        return $this->json([
            "status" => "ok",
            "data" => array_values($list),
        ]);
    }


    // forwarded in Score\CmsBundle\Controller\ApiController;
    public function parentListAction(Request $request, AdminPageManager $pageManager, EntityManagerInterface $entityManager) {
        if (!$this->canIAccess()) {
            $this->addFlash('page_access_danger', 'score.page.roleError');
            return $this->redirectToRoute('admin_start');
        }

        $list = $entityManager->createQuery('
            SELECT p.id, p.name, p.lvl, pp.id as parentId,
            CASE WHEN p.status = :public AND (p.isDeleted != :true OR p.isDeleted IS NULL) THEN 1 ELSE 0 END AS valid
            FROM ' . Page::class . ' p
            LEFT JOIN p.parentPage pp
            ORDER BY p.lvl,  p.sortOrder ASC
        ')
            ->setParameters(["public" => "public", "true" => true])
            ->getResult();

        $list = array_reduce($list, function ($carry, $item) {
            $item = array_change_key_case($item, CASE_LOWER);
            $carry[$item['parentid']][] = [
                "id" => $item["id"],
                "name" => str_repeat(" -", max($item["lvl"] - 1, 0)) . " " . $item["name"],
                "valid" => $item["valid"] === "1",
            ];
            return $carry;
        }, []);


        $root = $list[$list[""][0]["id"] ?? 0] ?? null;
//        if (!$root) $root = $list[$list[""][0]["ID"] ?? 0] ?? null;

        if ($root) {
            $list = $pageManager->buildTree($root, $list, false);
        } else {
            $list = [];
        }

        return $this->json([
            "status" => "ok",
            "data" => array_values(array_map(fn($v) => ["name" => $v["name"], "value" => $v["id"]], array_filter($list, fn($item) => $item["valid"]))),
        ]);

    }


}
