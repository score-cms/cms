<?php

namespace Score\CmsBundle\Controller\Page;

use DateTime;
use Score\CmsBundle\Entity\Menu;
use Score\CmsBundle\Entity\Stat;
use Score\CmsBundle\Services\PageManager;
use Score\CmsBundle\Services\StatManager;
use Score\CmsBundle\Services\OptionManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Score\BaseBundle\Services\AdjacencyArrayTreeManager;
use Score\BaseBundle\Services\SeoUrl;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PublicController extends AbstractController
{
    /**
     * @Route("/", name="default_homepage")
     * @Route("/{seoid}", name="default_subpage", requirements={"seoid"="^(?!login$|logout$|registration$|api/|user/|clanky/|udalosti/|dokumenty/|dokument/|admin/|cookies$|vyhladavanie|_profiler/).+"}, condition="!request.isXmlHttpRequest()")
     */
    public function subpageAction(PageManager $pageManager, $seoid = null, $cmsTemplate = true)
    {
        $responseCode = 200;
        if ($seoid) {
            if ($seoid === "homepage") {
                return $this->redirectToRoute('default_homepage');
            }
            $page = $pageManager->getPageBySeoId($seoid);
            if (!$page || $page->getIsDeleted() || $page->getStatus() === "hidden") {
                $page = $pageManager->getPageBySeoId('error');
                $responseCode = 404;
            }
            if ($page->getRedirectToPage()) {
                return $this->redirectToRoute('default_subpage', [
                    "seoid" => $page->getRedirectToPage()->getSeoId()
                ]);
            }
        } else {
            $page = $pageManager->getPageBySeoId('homepage');
        }

        if ($cmsTemplate === false) {
            return ["page" => $page, "code" => $responseCode];
        }

        return new Response($this->renderView('@ScoreCms/Public/default/subpage.html.twig', [
            'page' => $page,
        ]), $responseCode);
    }

    /**
     * @Route("/cookies", name="cookies")
     */
    public function cookiesAction(){
        return $this->render('@ScoreCms/Public/idsk/cookies.html.twig');
    }

     /**
     * @Route("/vyhladavanie", name="search")
     */
    public function searchAction(Request $request, SeoUrl $seoUrlManager, $cmsTemplate = true){

        $slug = $request->get('search');
        $result = [];

        if ($slug && $seoUrlManager->createSlug($slug)) {
            
            //$seoSlug = $seoUrlManager->createSlug($slug);
            $seoSlug = $seoUrlManager->removeDiacritic($slug);

            $con = $this->getDoctrine()->getManager()->getConnection();
            $rawData = [];
            // //$rawData['page'] = $con->fetchAllAssociative('SELECT p.title as name , p.content , p.seo_id as slug, p.icon as icon, p.edited_at as edit FROM cms_page p');
            // $rawData['page'] = $con->fetchAllAssociative('SELECT p.title as name , p.content , p.seo_id as slug, p.icon as icon, p.edited_at as edit FROM cms_page p WHERE p.seo_id NOT IN (?, ?, ?)', ["as", "error", "homepage"]);
            // $rawData['article'] = $con->fetchAllAssociative('SELECT a.name as name, a.teaser, a.body, a.author, a.slug as slug, a.icon as icon, a.edited_at as edit FROM cms_article a');
            // $rawData['event'] = $con->fetchAllAssociative('SELECT e.name as name, e.content, e.teaser, e.organizer, e.contact_person, e.slug as slug, e.icon as icon, e.edited_at as edit FROM cms_event e');

            $rawData['page'] = $con->fetchAllAssociative('SELECT p.title as name , p.seo_id as slug, p.p_search as search, p.icon as icon, p.edited_at as edit FROM cms_page p WHERE p.seo_id NOT IN (?, ?, ?)', ["as", "error", "homepage"]);
            $rawData['article'] = $con->fetchAllAssociative('SELECT a.name as name, a.slug as slug, a.search as search, a.icon as icon, a.edited_at as edit FROM cms_article a');
            $rawData['event'] = $con->fetchAllAssociative('SELECT e.name as name, e.slug as slug, e.e_search as search, e.icon as icon, e.edited_at as edit FROM cms_event e');

            foreach ($rawData as $type => $rows) {
                foreach ($rows as $row) {
                    if ($row['SEARCH']  && strpos($row['SEARCH'], $seoSlug) !== false) {
                        $result[] = [
                            "title" => $row['NAME'],
                            "slug" => $row['SLUG'],
                            "icon" => $row['ICON'],
                            "edited"=> $row['EDIT'],
                            "type" => $type
                        ];
                    }
                }
            }

            usort($result, function ($item1, $item2) {
                return $item2['edited'] <=> $item1['edited'];
            });
        }

        if ($cmsTemplate === false) {
            return $result;
        }

        return $this->render('@ScoreCms/Public/default/search.html.twig', [
            "items" => $result,
            "search_slug" => $slug
        ]);
    }
}
