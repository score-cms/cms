<?php

namespace Score\CmsBundle\Controller;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
//use Symfony\Component\HttpFoundation\Response;
use Score\CmsBundle\Entity\ChangesLog;

/**
 * @Route("/admin/logs", name="score-cms-logs")
 */
class LogsController extends AbstractController
{
    protected function canIAccess()
    {
        $allowed = ["ROLE_ADMIN"];
        return (count(array_intersect($allowed, $this->getUser()->getRoles())) >= 1);
    }

    /**
     * @Route("/list", name="-list")
     */
    public function listAction()
    {
        if (!$this->canIAccess())
            return $this->redirectToRoute('admin_start');

        $em = $this->getDoctrine()->getManager();
        $logs = $em->getRepository(ChangesLog::class)->getDistinctNames();

        return $this->render('@ScoreCms/Log/list.html.twig', [
            'logs' => $logs,
        ]);
    }


    /**
     * @Route("/list/e/{slug}", name="-list-entity", requirements={"slug"=".+"}, methods={"GET"})
     */
    public function listEntityAction(Request $request, $slug, $all = false)
    {
        if (!$this->canIAccess())
            return $this->redirectToRoute('admin_start');

        $className = str_replace("-", "\\", $slug);
        $em = $this->getDoctrine()->getManager();
        $logs = $em->getRepository(ChangesLog::class)->getOneEntityList($className, $all);

        return $this->render('@ScoreCms/Log/entity.html.twig', [
            'logs' => $logs,
        ]);
    }

    /**
     * @Route("/list/e-full/{slug}", name="-list-entity-full", requirements={"slug"=".+"}, methods={"GET"})
     */
    public function listEntityFullAction(Request $request, $slug)
    {
        if (!$this->canIAccess())
            return $this->redirectToRoute('admin_start');

        return $this->listEntityAction($request, $slug, true);
    }

    /**
     * @Route("/revert/{id}/{slug}", name="-revert", requirements={"slug"=".+"}, methods={"GET"})
     */
    public function revertAction(Request $request, ManagerRegistry $registry, $id, $slug)
    {
        if (!$this->canIAccess())
            return $this->redirectToRoute('admin_start');

        $className = str_replace("-", "\\", $slug);
//        $em = $this->getDoctrine()->getManager();
        $em = $registry->getManagerForClass(ChangesLog::class);
        $log = $em->getRepository(ChangesLog::class)->find($id);

        if ($log && $log->getField() && $log->getEntity() === $className) {
            $em2 = $registry->getManagerForClass($className);
            $entity = $em2->getRepository($log->getEntity())->find($log->getEntityId());
            $methodName = "set".ucfirst($log->getField());
            $entity->{$methodName}($log->getOldValue());
            $em2->persist($entity);
            $em2->flush();
            $this->addFlash('score_cms_success', 'Zmena bola vrátená do stavu pred zmenou.');
        } else {
            $this->addFlash('score_cms_danger', 'Chyba. Zmena sa nepodarila vrátiť.');
        }
        return $this->redirectToRoute('score-cms-logs-list-entity', ["slug" => $slug]);
    }


    /**
     * @Route("/dump/{id}/{slug}", name="-dump", requirements={"slug"=".+"}, methods={"GET"})
     */
    public function dumpAction(Request $request, $id, $slug)
    {
        if (!$this->canIAccess())
            return $this->redirectToRoute('admin_start');

        //$className = str_replace("-", "\\", $slug);
        //$em = $this->getDoctrine()->getManager();
        //$log = $em->getRepository(ChangesLog::class)->find($id);
        //
        // if (false && $log && $log->getEntity() === $className) {
        //     $entity = $em->getRepository($log->getEntity())->find($log->getEntityId());
        //     return $this->render('@ScoreCms/Log/dump.html.twig', [
        //         'log' => $log,
        //         'entity' => $entity,
        //     ]);
        // } 
        // $this->addFlash('score_cms_danger', 'Chyba.');
        return $this->redirectToRoute('score-cms-logs-list-entity', ["slug" => $slug]);
    }


}
