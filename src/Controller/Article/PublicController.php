<?php

namespace Score\CmsBundle\Controller\Article;

use Score\CmsBundle\Entity\Article\Article;
use Score\BaseBundle\Services\Pager;
use Doctrine\ORM\EntityManagerInterface;
use Score\CmsBundle\Services\StatManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


/**
 * @Route("/clanky")
 */
class PublicController extends AbstractController {

    /**
     * @Route("/", name="score_public_articles_list", methods={"GET"})
     */
    public function listAction(Request $request, EntityManagerInterface $entityManager, Pager $pager): Response {
        $repo = $entityManager->getRepository(Article::class);
        $page = $request->get('p', 1);
        $pageSize = $request->get('size', 6);
        $articles = $repo->findArticlesByPage(['page' => $page, 'pageSize' => $pageSize], ['visible' => true]);
        $articleList = array_chunk($articles['list'], 3);

        $pager->setUrl($request->getUri());
        $pager->setItemsPerPage($pageSize);
        $pager->setCount($articles['total']);
        $pager->setPage($page);
        $pager->getPages();


        return $this->render('@ScoreCms/Public/default/article_list.html.twig', [
            'articleList' => $articleList,
            'pager' => $pager,
        ]);
    }

    /**
     * @Route("/{slug}", name="score_public_articles_detail", methods={"GET"})
     */
    public function detailAction(EntityManagerInterface $entityManager, $slug = null): Response {
        $repo = $entityManager->getRepository(Article::class);
        $article = $repo->findOneBy([
            'slug' => $slug,
            'published' => 1,
//            'isDeleted' => false
        ]);
        $responseCode = 200;
        if (!$article || $article->getIsDeleted()) {
            $article = new Article();
            $article->setName('404');
            $article->setBody('Tento článok sa tu nenachádza');
            $responseCode = 404;
        }
        $topArticles = $repo->findSidebarArticles($article);
        return new Response($this->renderView('@ScoreCms/Public/default/article_detail.html.twig', [
            'article' => $article,
            'articles' => $topArticles,
        ]), $responseCode);
    }


}
