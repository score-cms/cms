<?php

namespace  Score\CmsBundle\Controller\Article;

use Doctrine\ORM\EntityManagerInterface;
use Score\CmsBundle\Entity\Article\Article;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class ArticleBlockController extends AbstractController
{
    public function topAction(EntityManagerInterface $entityManager): Response
    {
        $repo = $entityManager->getRepository(Article::class);
        $topArticles = $repo->findTopArticles();
        $bigArticle = array_shift($topArticles);

        return $this->render('@ScoreCms/Public/default/article_block_top.html.twig', [
            'articles' => $topArticles,
            'bigArticle' => $bigArticle
        ]);
    }

    public function topByCategoryAction(EntityManagerInterface $entityManager, $category = null, $size = "small", $count = 4, $header = null): Response
    {
        $repo = $entityManager->getRepository(Article::class);
        $topArticles = $repo->findTopArticlesByCategory($category, $count);
        $bigArticle = array_shift($topArticles);

        return $this->render('@ScoreCms/Public/default/article_block_top.html.twig', [
            'articles' => $topArticles,
            'bigArticle' => $bigArticle,
            'header' => $header,
            'size' => $size
        ]);
    }


}
