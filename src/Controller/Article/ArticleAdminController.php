<?php

namespace Score\CmsBundle\Controller\Article;

use Doctrine\Common\Collections\ArrayCollection;
use Score\BaseBundle\Services\AdjacencyArrayTreeManager;
use Score\CmsBundle\Entity\Article\AllCategories;
use Score\CmsBundle\Entity\Article\Article;
use Score\BaseBundle\Services\SeoUrl;
use Score\CmsBundle\Entity\Article\ArticleCategory;
use Score\CmsBundle\Entity\Block\Block;
use Score\CmsBundle\Entity\User;
use Score\CmsBundle\Form\Article\AllCategoriesType;
use Score\CmsBundle\Form\Article\ArticleType;
use Doctrine\ORM\EntityManagerInterface;
use Score\CmsBundle\Services\ImageManager;
use Score\CmsBundle\Event\ArticleEditEvent;
use Score\CmsBundle\Services\AccessManager;
use Score\CmsBundle\Services\BlockManager;
use Score\CmsBundle\Services\MetatagManager;
use Score\CmsBundle\Services\OptionManager;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Score\CmsBundle\Repository\ArticleDatagrid;
use Symfony\Component\Routing\Annotation\Route;
use Score\CmsBundle\Services\AdminArticleManager;
use Score\CmsBundle\Entity\Multisite\SiteItemArticle;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

/**
 * @Route("/admin/article")
 */
class ArticleAdminController extends \Score\BaseBundle\Controller\AdminController {
    protected function canIAccess($topManager = false) {
        $allowed = ["ROLE_ARTICLE_MANAGER", "ROLE_TOP_MANAGER", "ROLE_ADMIN"];
        if ($topManager) {
            $allowed = ["ROLE_TOP_MANAGER", "ROLE_ADMIN"];
        }
        return (count(array_intersect($allowed, $this->getUser()->getRoles())) >= 1);
    }

    protected function redirectHome() {
        $this->addFlash('danger', 'score.alert.user.lowRole');
        return $this->redirectToRoute('admin_start');
    }

    /**
     * Data for datatable
     *
     * @Route("/admin/article/datatable", name="article_admin_datatable", methods={"GET"})
     */
    public function datatableDataAction(SessionInterface $session, Request $request, SeoUrl $seoUrl, TranslatorInterface $translator) {
        if (!$this->canIAccess()) {
            return $this->redirectHome();
        }


        $em = $this->getDoctrine()->getManager();
        $datagrid = new  ArticleDatagrid($em, Article::class, $this->container->get('router'), $translator);
        $datagrid->setPagerLength(10);
        $datagrid->setPagerStart($request->get('start'));
        $datagrid->setOrderParams($request->get('order'));
        $searchTerm = $seoUrl->removeDiacritic($request->get('search')['value']);
        $datagrid->setSearchCriteria($searchTerm);

        $data = $datagrid->getRecordsAsArray();

        $recordsTotal = $datagrid->getRecordsTotal();
        $recordsFiltered = $datagrid->getRecordsFiltered();

        $datagridData = array(
            'draw' => $this->getDatagridDraw($session),
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => $data,
        );
        return $this->json($datagridData);
    }

    // forwarded in Score\CmsBundle\Controller\ApiController;
    public function dataListAction(AccessManager $accessManager, EntityManagerInterface $entityManager): Response {
        if (!$this->canIAccess()) {
            return $this->redirectHome();
        }

        $limit = $this->getParameter("cms_articles_list_count");
        $limit = $limit === "all" ? 0 : intval($limit);

        $checkAccess = $this->getParameter("access_checking") && !$this->canIAccess(true);

        $conn = $entityManager->getConnection();
        $sql = '
             SELECT b.*
             FROM (
                SELECT a.id AS value, a.name, a.published, a.author, ac.a_name AS category, a.slug, a.published_from AS datefrom, a.published_to AS dateto,
                    DENSE_RANK() OVER (ORDER BY a.published_from DESC, a.name ASC, a.id DESC) as rn
                FROM cms_article a
                LEFT JOIN cms_conn_article_category ccac ON ccac.article_id = a.id 
                LEFT JOIN cms_article_category ac ON ac.id = ccac.category_id
                LEFT JOIN cms_access acc ON acc.entity_id = a.id AND acc.access_entity = \'article\'
                WHERE a.isdeleted != 1
                    AND ' . ($checkAccess ? 'acc.user_id = :userId' : ':userId IS NOT NULL') . '
                 ORDER BY  a.published_from DESC, a.name ASC, a.id DESC
                 ) b
             WHERE :limit = 0 OR b.rn <= :limit
        ';

        $list = $conn->fetchAllAssociative($sql, [
            "userId" => $this->getUser()->getId(),
            "limit" => $limit,
        ]);

        $list = array_reduce($list, function ($carry, $item) {
            $item = array_change_key_case($item, CASE_LOWER);
            if (!isset($carry[$item['value']])) {
                $from = $item['datefrom'] ? (new \DateTime($item['datefrom']))->format('j.n. Y') : null;
                $to = $item['dateto'] ? (new \DateTime($item['dateto']))->format('j.n. Y') : null;
                $carry[$item['value']] = [
                    "name" => $item['name'],
                    "link" => '<a href="' . $this->generateUrl("score_cms_article_edit", ["id" => $item['value']]) . '">' . $item['name'] . '</a>',
                    "value" => $item['value'],
                    "published" => $item['published'] === "1" ? '<i class="fa fa-check"></i>' : '<i class="fa fa-times"></i>',
                    "publishedFromTo" => $from && $to
                        ? ($from . " – " . $to)
                        : ($from
                            ? ("od " . $from)
                            : ($to
                                ? ("do " . $to)
                                : ""
                            )
                        ),
                    "autor" => $item['author'],
                    "slug" => $item['slug'],
                    "categories" => [],
                    "action" => $this->canIAccess(true)
                        ? '<a class="to-trash-article" href="' . $this->generateUrl("score_cms_article_to_trash", ["id" => $item['value']]) . '">Presunúť do koša</a>' // tie v kosi sa tu nenacitavaju a ani ich tu nechceme
                        : "",
                ];
            }
            if ($item['category'] && !in_array($item['category'], $carry[$item['value']]['categories'])) {
                $carry[$item['value']]['categories'][] = $item['category'];
            }
            return $carry;
        }, []);

        return $this->json([
            "status" => "ok",
            "data" => array_values($list)
        ]);

    }

    /**
     * @Route("/", name="score_cms_articles2", methods={"GET"})
     */
    public function indexAction(): Response {
        if (!$this->canIAccess()) {
            return $this->redirectHome();
        }
        // return $this->redirectToRoute('score_cms_articles');

        return $this->render('@ScoreCms/Article/index.html.twig', [

        ]);
    }

    /**
     * @Route("/list", name="score_cms_articles", methods={"GET"})
     */
    public function listAction(): Response {
        if (!$this->canIAccess()) {
            return $this->redirectHome();
        }

        // $em = $this->getDoctrine()->getManager();

        // $list = $em->getRepository(Article::class)->findBy([], ["createdAt" => "DESC"]);

        // if (!$this->canIAccess(true)) { //odfiltrovat tie z kosa
        //     $list = array_filter($list, function ($v) {
        //         return !$v->getIsDeleted();
        //     });
        // }
        // $accesableList = [];
        // if ($this->getParameter("access_checking")) { // ak ma zobrazit len tie ktore uzivatel moze editovat
        //     foreach ($list as $article) {
        //         if ($accessManager->hasUserAccessTo($this->getUser(), $article)) {
        //             $accesableList[] = $article;
        //         }
        //     }
        // } else {
        //     $accesableList = $list;
        // }


        return $this->render('@ScoreCms/Article/list.html.twig', [
            'list' => [],// $accesableList,
            'canDelete' => $this->canIAccess(true),
            //'showAddArticleBtn' => $this->canIAccess()
        ]);
    }

    protected function handleIconUpload($article, $icon, $adminManager, ImageManager $imageManager, $params) {
        $slug = $article->getSlug();
        if (
            $icon instanceof UploadedFile
            && $imageManager->isImage($icon)
        ) {
            $fileId = date('Y_m_d_h_i_s');
            $fileName = $article->getSlug() . '_' . $fileId . '.' . $icon->guessExtension();
            $fileThumb100Name = $article->getSlug() . '_' . $fileId . '_thumb_100x100.' . $icon->guessExtension();
            $fileThumb640Name = $article->getSlug() . '_' . $fileId . '_thumb_640x480.' . $icon->guessExtension();
            $uploadFolder = $adminManager->getUploadDir($params, $imageManager);
            $dir = $uploadFolder['absolute_path'];
            try {
                $icon->move($dir, $fileName);
                if ($this->getParameter("cms_article_thumbnail_cut") === true) {
                    $imageManager->scaleTo($dir . '/' . $fileName, $dir . '/' . $fileThumb100Name, 100, 100);
                    $imageManager->scaleTo($dir . '/' . $fileName, $dir . '/' . $fileThumb640Name, 600, 480);
                } else {
                    $imageManager->thumbOver($dir . '/' . $fileName, $dir . '/' . $fileThumb100Name, 100, 100);
                    $imageManager->thumbOver($dir . '/' . $fileName, $dir . '/' . $fileThumb640Name, 600, 480);
                }
            } catch (FileException $e) {
                echo $e;
            }
            return $uploadFolder['public_path'] . '/' . $fileName;
        }
        return "";

    }

    protected function updateSearch($article, $seoUrl) {
        $text = $article->getName() . ' ' . $article->getTeaser() . ' ' . $article->getBody() . ' ' . $article->getAuthor();
        $search = strip_tags(mb_strtolower($seoUrl->removeDiacritic($text)));
        $article->setSearch($search);
    }

    protected function buildForm($article, $optionManager) {
        $multisite = [];
        foreach ($article->getSiteItemArticle() as $item) {
            $multisite[] = $item->getSite();
        }
        $article->setMultisite($multisite);
        $form = $this->createForm(ArticleType::class, $article, []);
        return $form;
    }

    /**
     * @Route("/new", name="score_cms_article_new", methods={"GET", "POST"})
     */
    public function newAction(
        Request                $request,
        AdminArticleManager    $adminManager,
        BlockManager           $blockManager,
        SeoUrl                 $seoUrl,
        ImageManager           $imageManager,
        OptionManager          $optionManager,
        ContainerBagInterface  $params,
        AccessManager          $accessManager,
        MetatagManager         $metatagManager,
        EntityManagerInterface $em
    ) {
        if (!$this->canIAccess()) {
            return $this->redirectHome();
        }

        $article = new Article();
        $article->setPublishedFrom(new \Datetime())
//            ->setAuthor($this->getUser()->getUsername())
//            ->setAuthor('SAŽP')
            ->setPriority(20)
            ->setPublished(2);
        $form = $this->buildForm($article, $optionManager);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();
            //generate slug
            $this->handleUniqueSeo($em, $seoUrl, $article);
            $article->setCreatedBy($user->getId());
            $icon = $article->getIcon();
            $fileName = $this->handleIconUpload($article, $icon, $adminManager, $imageManager, $params);
            $adminManager->handleMultisite($article);
            $article->setIcon($fileName);

            $blocks = $request->get('block_ids');
            $joints = $request->get('joint_ids');
            $blockManager->updateBlockJoints($article, $blocks, $joints);

            $accessManager->updateEntityAccess($user, $article, true, ["full_access" => true, "can_delete" => true]);

            if ($metatagManager->updateMetatagsFor($article, $this->getUser())) {
                $this->addFlash('edit_warning', 'score.alert.metatag.duplicity.warning');
            }

            //search
            $this->updateSearch($article, $seoUrl);

            $em->persist($article);
            $em->flush();
            $this->addFlash('article_create_success', 'Článok bol uložený');
            return $this->redirectToRoute('score_cms_article_edit', ['id' => $article->getId()]);
        }

        return $this->render('@ScoreCms/Article/new.html.twig', [
            'article' => $article,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="score_cms_article_edit", methods={"GET", "POST"})
     */
    public function edit(
        Request                  $request,
        Article                  $article,
        EntityManagerInterface   $em,
        AdminArticleManager      $adminManager,
        BlockManager             $blockManager,
        ImageManager             $imageManager,
        ContainerBagInterface    $params,
        SeoUrl                   $seoUrl,
        OptionManager            $optionManager,
        EventDispatcherInterface $dispatcher,
        AccessManager            $accessManager,
        MetatagManager           $metatagManager
    ): Response {

        if (!$this->canIAccess()) {
            return $this->redirectHome();
        }

        if (!$accessManager->hasUserAccessTo($this->getUser(), $article)) {
            $this->addFlash('danger', 'score.alert.user.lowRole');
            return $this->redirectToRoute('score_cms_articles');
        }

        $origIcon = $article->getIcon();
        $form = $this->buildForm($article, $optionManager);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $icon = $article->getIcon();
            if (is_a($icon, UploadedFile::class)) {
                $fileName = $this->handleIconUpload($article, $icon, $adminManager, $imageManager, $params);
                $article->setIcon($fileName);
            } else {
                $article->setIcon($origIcon);
            }

            if ($request->request->get('article')['removeIcon'] === "true") {
                $article->setIcon("");
            }

            $adminManager->handleMultisite($article);

            $blocks = $request->get('block_ids');
            $joints = $request->get('joint_ids');
            $blockManager->updateBlockJoints($article, $blocks, $joints);

            if ($metatagManager->updateMetatagsFor($article, $this->getUser())) {
                $this->addFlash('edit_warning', 'score.alert.metatag.duplicity.warning');
            }
            $this->handleUniqueSeo($em, $seoUrl, $article);

            $this->updateSearch($article, $seoUrl);
            $em->persist($article);
            $em->flush();

            $this->addFlash('article_edit_success', 'Údaje boli zmenené');
            return $this->redirectToRoute('score_cms_article_edit', array('id' => $article->getId()));
        }

        return $this->render('@ScoreCms/Article/edit.html.twig', [
            'article' => $article,
            'form' => $form->createView(),
            'activeSection' => $request->get('s') ?: "content",
            'showAccessTab' => $accessManager->canUserEditAccessToEntity($this->getUser(), $article),
            'canDelete' => $accessManager->canUserDeleteEntity($this->getUser(), $article),
            'users' => $em->getRepository(User::class)->getListForAccess("article"),
        ]);
    }

    private function handleUniqueSeo(
        EntityManagerInterface $manager,
        SeoUrl                 $seoUrl,
        Article                $article
    ): void {
        $articleRepository = $manager->getRepository(Article::class);
        $slug = $seoUrl->generateSeoString($article->getSlug());
        $article->setSlug($slug);
        $i = 2;
        while (!in_array($articleRepository->findOneBy(['slug' => $slug]), [null, $article])) {
            $slug = $article->getSlug() . '-' . $i;
            $i++;
        }
        if ($slug !== $article->getSlug()) {
            $this->addFlash('warning', 'Článok s rovnakou URL adresou už existuje. Bola pridaná číslica na koniec URL adresy.');
        }
        $article->setSlug($slug);
    }

    /**
     * @Route("/delete/{id}", name="score_cms_article_delete")
     */
    public function delete(Request $request, Article $article, EntityManagerInterface $entityManager): Response {
        if (!$this->canIAccess(true)) {
            return $this->redirectHome();
        }

        foreach ($article->getArticleCategories() as $articleCategory) {
            $article->removeArticleCategory($articleCategory);
        }

        // TODO remove images
        $entityManager->remove($article);
        $entityManager->flush();
        $this->addFlash('score_cms_article_success', 'Článok bol odstránený');
        return $this->redirectToRoute('score_cms_articles', [], Response::HTTP_SEE_OTHER);
    }


    /**
     * @Route("/categories", name="score_cms_article_categories")
     */
    public function categoriesAction(Request $request) {
        if (!$this->canIAccess()) {
            return $this->redirectHome();
        }

        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository(ArticleCategory::class)->findBy([], ["createdAt" => "ASC"]);
        $allCategories = new AllCategories($categories);

        $form = $this->createForm(AllCategoriesType::class, $allCategories);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            foreach ($allCategories->getCategories() as $d) {
                $em->persist($d);
            }
            foreach ($categories as $d) {
                if (!$allCategories->getCategories()->contains($d)) {
                    $em->remove($d);
                }
            }
            $em->flush();
            $this->addFlash('success', 'score.alert.article.category.changed');
            return $this->redirectToRoute('score_cms_article_categories');
        }

        return $this->render('@ScoreCms/Article/all_categories_list_edit.html.twig', [
            "all" => $allCategories,
            "form" => $form->createView(),
            "part" => "categories"
        ]);
    }

    /**
     * @Route("/create-block/{part}/{seoName}", name="score_cms_article_create_block")
     */
    public function createBlockAction(Request $request, $part, $seoName) {
        if (!$this->canIAccess(true)) {
            return $this->redirectHome();
        }

        $em = $this->getDoctrine()->getManager();
        $block = new Block();
        $block->setType("controllerContent")
            ->setModule("Score\CmsBundle\Controller\Article\ArticleBlockController")
            ->setVisibility(true);

        if ($part === "categories") {
            $entity = $em->getRepository(ArticleCategory::class)->findOneBy(["seoName" => $seoName, "visibility" => true]);
            $block->setName("Výpis článkov – " . $entity->getName() . "")
                ->setAction("topByCategoryAction")
                ->setParamsArr([
                    "category" => $entity->getSeoName(),
                    "size" => "big",
                    "count" => 4,
                    "header" => "Aktuality"
                ]);
        }
        $em->persist($block);
        $em->flush();

        return $this->redirectToRoute('score-block-edit-expert', ["id" => $block->getId()]);
    }


    /**
     * @Route("/to-trash/{id}", name="score_cms_article_to_trash")
     */
    public function toTrashAction(Request $request, AccessManager $accessManager, $id) {
        if (!$this->canIAccess()) {
            return $this->redirectHome();
        }

        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Article::class)->find($request->get('id'));

        if ($accessManager->canUserDeleteEntity($this->getUser(), $article)) {
            $article->setIsDeleted(true);
            $em->persist($article);
            $em->flush();
            $this->addFlash("admin-success", "score.alert.article.movedToTrash");
        }
        return $this->redirectToRoute('score_cms_articles');
    }

    /**
     * @Route("/from-trash/{id}", name="score_cms_article_from_trash")
     */
    public function fromTrashAction(Request $request, $id) {
        if (!$this->canIAccess(true)) {
            return $this->redirectHome();
        }

        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Article::class)->find($request->get('id'));

        $article->setIsDeleted(false);
        $em->persist($article);
        $em->flush();

        $this->addFlash("admin-success", "score.alert.article.movedFromTrash");
        return $this->redirectToRoute('score_cms_articles');
    }


    /**
     * @Route("/access/edit2", name="score_cms_article_edit_access")
     */
    public function editAccessAction(Request $request, AccessManager $accessManager) {
        if (!$this->canIAccess()) {
            return $this->redirectHome();
        }

        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Article::class)->find($request->get("article_id"));
        $user = $em->getRepository(User::class)->find($request->get("user_id"));
        if ($article && $user && $accessManager->canUserEditAccessToEntity($this->getUser(), $article)) {
            $params = [
                "full_access" => boolval($request->get("full_access")),
                "can_create" => boolval($request->get("can_create")),
                "can_delete" => boolval($request->get("can_delete")),
                "is_heritable" => boolval($request->get("is_heritable"))
            ];
            $hasAssess = boolval($request->get("has_access"));
            $accessManager->updateEntityAccess($user, $article, $hasAssess, $params);
            $em->persist($user); // vsetky zmeny v pravach vsetkych clankov sa vzdy tykaju jedneho usera preto staci len jeho persist
            $em->flush();

            $this->addFlash('score_cms_article_edit_success', 'Zmeny boli uložené');
            return $this->redirectToRoute('score_cms_article_edit', ['id' => $article->getId(), 's' => 'access']);
        }
        return $this->redirectToRoute('score_cms_article_edit', ['id' => $article->getId(), 's' => 'access']);
    }

}
