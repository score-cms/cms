<?php

namespace Score\CmsBundle\Controller;

use Score\CmsBundle\Entity\Metatag\AllMetatagPatterns;
use Score\CmsBundle\Entity\Metatag\MetatagPattern;
use Score\CmsBundle\Form\Metatag\AllMetatagPatternsType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class MetatagAdminController extends AbstractController {

    protected function canIAccess() {
        return $this->getUser()->getIsAdmin();
    }


    /**
     * @Route("/admin/metratag/patterns", name="score_cms_metatag_patterns")
     */
    public function indexAction(Request $request) {


        if (!$this->canIAccess()) {
            $this->addFlash('danger', 'score.alert.user.lowRole');
            return $this->redirectToRoute('admin_start');
        }

        $em = $this->getDoctrine()->getManager();
        $metaqtagPattterns = $em->getRepository(MetatagPattern::class)->findBy([], ["createdAt" => "ASC"]);
        $AllMetaqtagPattterns = new AllMetatagPatterns($metaqtagPattterns);

        $form = $this->createForm(AllMetatagPatternsType::class, $AllMetaqtagPattterns);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            foreach ($AllMetaqtagPattterns->getMetatagPatterns() as $pattern) {
                $em->persist($pattern);
            }
            foreach ($metaqtagPattterns as $pattern) {
                if (!$AllMetaqtagPattterns->getMetatagPatterns()->contains($pattern)) {
                    $em->remove($pattern);
                }
            }
            $em->flush();
            $this->addFlash('success', 'score.alert.metatag.pattern.changed');
            return $this->redirectToRoute('score_cms_metatag_patterns');
        }

        return $this->render('@ScoreCms/Metatag/all_patterns_edit.html.twig', [
            "all" => $AllMetaqtagPattterns,
            "form" => $form->createView(),
        ]);
    }


}
