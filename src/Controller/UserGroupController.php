<?php

namespace Score\CmsBundle\Controller;

use Score\CmsBundle\Entity\Group;
use Score\CmsBundle\Form\GroupType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Score\BaseBundle\Controller\AdminController;
use Score\CmsBundle\Entity\User;
use Score\CmsBundle\Services\AdminGroupManager;
use Score\CmsBundle\Repository\GroupDatagrid;

class UserGroupController extends AdminController
{
    protected function canIAccess()
    {
        $allowed = ["ROLE_USER___GROUP_MANAGER", "ROLE_ADMIN"];
        return (count(array_intersect($allowed, $this->getUser()->getRoles())) >= 1);
    }


    /**
     * Data for datatable
     *
     * @Route("/admin/group/datatable", name="admin_group_datatable", methods={"GET"})
     */
    public function datatableDataAction(SessionInterface $session, Request $request)
    {
        if (!$this->canIAccess())
            return $this->redirectToRoute('admin_start');


        $em = $this->getDoctrine()->getManager();
        $datagrid = new GroupDatagrid($em, Group::class, $this->container->get('router'));
        $datagrid->setPagerLength(10);
        $datagrid->setPagerStart($request->get('start'));
        $datagrid->setOrderParams($request->get('order'));

        $data = $datagrid->getRecordsAsArray();

        $recordsTotal = $datagrid->getRecordsTotal();
        $recordsFiltered = $datagrid->getRecordsFiltered();

        $datagridData = array(
            'draw' => $this->getDatagridDraw($session),
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => $data,
        );
        return $this->json($datagridData);
    }


    /**
     * Lists all group entities.
     *
     * @Route("/admin/groups", name="admin_groups", methods={"GET"})
     */
    public function indexAction()
    {
        if (!$this->canIAccess())
            return $this->redirectToRoute('admin_start');

        $em = $this->getDoctrine()->getManager();
        $groups = $em->getRepository(Group::class)->findBy([], ["id" => 'ASC']);

        return $this->render('@ScoreCms/Group/index.html.twig', [
            "groups" => $groups
        ]);


    }

    /**
     * Creates a new group entity.
     *
     * @Route("/admin/group/new", name="admin_group_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        if (!$this->canIAccess())
            return $this->redirectToRoute('admin_start');

        $group = new Group();
        $form = $this->createForm(GroupType::class, $group);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($group);
            $em->flush();
            $this->addFlash("admin-success", "score.alert.group.created");

            return $this->redirectToRoute('admin_group_edit', array('id' => $group->getId()));
        }

        return $this->render('@ScoreCms/Group/edit.html.twig', array(
            'group' => $group,
            'form' => $form->createView(),
        ));


    }


    /**
     * Displays a form to edit an existing group entity.
     *
     * @Route("/admin/group/edit/{id}", name="admin_group_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, $id)
    {
        if (!$this->canIAccess())
            return $this->redirectToRoute('admin_start');

        $group = $this->getDoctrine()->getManager()->getRepository(Group::class)->find($id);

        $editForm = $this->createForm(GroupType::class, $group);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("admin-success", "score.alert.group.edited");

            return $this->redirectToRoute('admin_group_edit', array('id' => $group->getId()));
        }

        return $this->render('@ScoreCms/Group/edit.html.twig', array(
            'group' => $group,
            'form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a group entity.
     *
     * @Route("/admin/group/delete/{id}", name="admin_group_delete", methods={"GET"})
     */
    public function deleteAction(Request $request, $id)
    {
        if (!$this->canIAccess())
            return $this->redirectToRoute('admin_start');

        $em = $this->getDoctrine()->getManager();
        $group = $em->getRepository(Group::class)->find($id);
        $users = $em->getRepository(User::class)->findAll();


        if ($group) {
            foreach ($users as $u) {
                $u->removeGroup($group);
                $em->persist($u);
            }
            $em->remove($group);
            //$em->persist($group);
            $em->flush();
            $this->addFlash("admin-success", "score.alert.group.deleted");
        }

        return $this->redirectToRoute('admin_groups');
    }


}
