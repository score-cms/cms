<?php

namespace Score\CmsBundle\Controller\Block;

use Score\CmsBundle\Entity\Block\Accordion;
use Score\CmsBundle\Entity\Block\Block;
use Score\CmsBundle\Form\Block\AccordionType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Accordion controller.
 * @Route("/admin/accordion", name="score-accordion", methods={"GET", "POST"})
 */
class AccordionAdminController extends AbstractController {
    protected function canIAccess() {
        $allowed = ["ROLE_ACCORDION_MANAGER", "ROLE_ADMIN"];
        return (count(array_intersect($allowed, $this->getUser()->getRoles())) >= 1);
    }

    protected function redirectHome() {
        $this->addFlash('danger', 'score.alert.user.lowRole');
        return $this->redirectToRoute('admin_start');
    }


    /**
     * Lists all Accordion entities.
     * @Route("/list", name="-list", methods={"GET"})
     */
    public function listAction() {
        if (!$this->canIAccess())
            return $this->redirectHome();

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository(Accordion::class)->findBy(["visibility" => true], ["editedAt" => "DESC"]);
        return $this->render('@ScoreCms/Block/Accordion/list.html.twig', [
            'accordions' => $entities,
            'showExpert' => false
        ]);

    }

    /**
     * Creates a new Accordion entity.
     * @Route("/new", name="-new", methods={"GET", "POST"})
     * @Route("/edit/{id}/{ref}", name="-edit", methods={"GET", "POST"})
     */
    public function createAction(Request $request, $id = null, $ref = null) {
        if (!$this->canIAccess())
            return $this->redirectHome();

        $em = $this->getDoctrine()->getManager();
        $block = null;

        if ($id !== null) {
            $entity = $em->getRepository(Accordion::class)->find($id);
            if (!$entity) throw $this->createNotFoundException('Unable to find accordion entity.');
            $blocks = $em->getRepository(Block::class)->findBy(["type" => "accordion"]);
            foreach ($blocks as $b) {
                if ($b->getParamsArr()["accordionId"] === $entity->getId()) {
                    $block = $b;
                }
            }
            if (!$block) throw $this->createNotFoundException('Unable to find accordion block.');
        } else {
            $entity = new Accordion();
            $block = new Block();
            $block->setType("accordion");
        }

        $options = [
            "allow_open" => $this->getParameter('block_accordion_allow_open'),
            "allow_summary" => $this->getParameter('block_accordion_allow_summary'),
        ];

        $form = $this->createForm(AccordionType::class, $entity, $options);
        $form->handleRequest($request);

        $maxItems = $this->getParameter('block_accordion_max_items');

        if ($entity->getItems()->count() > $maxItems) {
            $this->addFlash('accordion_danger', 'Bol prekročený maximálny počet ' . $maxItems . ' položiek na jeden akordeón. Akordeón nebol uložený, opravte ho a skuste uložiť znovu.');
        } elseif ($form->isSubmitted()) {
            if ($form->isValid()) {
                if (!$entity->getId())
                    $entity->setUserId($this->getUser()->getId());
                $em->persist($entity);
                $em->flush();
                if ($entity->getName() !== $block->getName()) {
                    $block->setName($entity->getName())
                        ->setParamsArr(["accordionId" => $entity->getId()]);
                    $em->persist($block);
                    $em->flush();
                }
                $this->addFlash('accordion_success', 'score.alert.accordion.updated');
                return $this->redirectToRoute('score-accordion-edit', ['id' => $entity->getId()]);
            } else {
                $this->addFlash('accordion_danger', 'score.alert.accordion.validationError');
            }
        }
        // elseif ($form->isSubmitted()) {           
        //     $this->addFlash('accordion_danger', 'Akordeón nebol uložený, Nesprávne dáta opravte a skuste uložiť znovu.');
        // } 


        return $this->render('@ScoreCms/Block/Accordion/edit.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
            'maxItems' => $maxItems,
            "referer" => $ref,
            "hideAdvanced" => $this->getParameter('block_accordion_hide_advanced'),
        ));
    }


    /**
     * Deletes a Accordion entity.
     * @Route("/delete/{id}", name="-delete", methods={"GET"})
     */
    public function deleteAction(Request $request, $id) {
        if (!$this->canIAccess())
            return $this->redirectHome();

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository(Accordion::class)->find($id);
        if (!$entity) throw $this->createNotFoundException('Unable to find accordion entity.');
        $blocks = $em->getRepository(Block::class)->findBy(["type" => "accordion"]);
        foreach ($blocks as $block) {
            if ($block->getParamsArr()["accordionId"] === $entity->getId()) {
                if ($block->getBlockJoints()->count() > 0) {
                    $names = [];
                    foreach ($block->getBlockJoints() as $blockJoint) {
                        $names[] = $blockJoint->getEntity()->getName();
                    }
                    $this->addFlash('block_danger', 'Unable to delete, still used in: ' . implode(", ", $names) . '.');
                    return $this->redirectToRoute('score-accordion-list');
                } else {
                    $em->remove($block);
                }
            }
        }
        if (!$block) throw $this->createNotFoundException('Unable to find accordion block.');


        $em->remove($entity);
        $em->remove($block);
        $em->flush();

        $this->addFlash('accordion_success', 'score.alert.accordion.deleted');
        return $this->redirectToRoute('score-accordion-list');
    }


}
