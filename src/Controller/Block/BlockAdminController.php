<?php

namespace Score\CmsBundle\Controller\Block;

use Symfony\Component\Form\Form;
use Score\CmsBundle\Entity\Block\Block;
use Score\CmsBundle\Form\Block\BlockType;

//use Score\CmsBundle\Entity\ChangesLog;
use Score\CmsBundle\Form\Block\FullBlockType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Score\CmsBundle\Services\BlockManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Block controller.
 * @Route("/admin/block", name="score-block", methods={"GET", "POST"})
 */
class BlockAdminController extends AbstractController {
    protected function canIAccess($level = 0) {
        if ($level >= 1)
            return (in_array("ROLE_ADMIN", $this->getUser()->getRoles()));

        $allowed = ["ROLE_BLOCK_MANAGER", "ROLE_ADMIN"];
        return (count(array_intersect($allowed, $this->getUser()->getRoles())) >= 1);
    }

    protected function redirectHome() {
        $this->addFlash('danger', 'score.alert.user.lowRole');
        return $this->redirectToRoute('admin_start');
    }

    /**
     * Lists all Block entities.
     * @Route("/list", name="-list", methods={"GET"})
     */
    public function listAction() {
        if (!$this->canIAccess())
            return $this->redirectHome();

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository(Block::class)->loadStaticBlocksList();
        return $this->render('@ScoreCms/Block/Admin/list.html.twig', [
            'blocks' => $entities,
            'showExpert' => false
        ]);

    }

    /**
     * Lists all Block entities.
     * @Route("/list-full", name="-list-full", methods={"GET"})
     */
    public function listFullAction() {
        if (!$this->canIAccess(1))
            return $this->redirectToRoute('score-block-list');

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository(Block::class)->loadBlocksList();

        return $this->render('@ScoreCms/Block/Admin/list.html.twig', [
            'blocks' => $entities,
            'showExpert' => true
        ]);

    }

    /**
     * @Route("/redirect-to-edit/{id}/{ref}", name="-to-edit", methods={"GET"})
     */
    public function redirectToEditAction(Request $request, $id, $ref = null) {
        $em = $this->getDoctrine()->getManager();
        $block = $em->getRepository(Block::class)->find($id);
        //return $this->redirect($this->generateUrl('score-block-edit', array('id' => $block->getId())));

        if ($block->getType() === "static")
            return $this->redirectToRoute('score-block-edit', ["id" => $id, "ref" => $ref]);
        elseif ($block->getType() === "menu")
            return $this->redirectToRoute('score_cms_menu', ["menuId" => $block->getParamsArr()['menuId'], "ref" => $ref]);
        elseif ($block->getType() === "accordion")
            return $this->redirectToRoute('score-accordion-edit', ["id" => $block->getParamsArr()['accordionId'], "ref" => $ref]);
        elseif ($block->getType() === "form")
            return $this->redirectToRoute('score_cms_form_edit', ["id" => $block->getParamsArr()['formId'], "ref" => $ref]);
        elseif ($block->getType() === "gallery")
            return $this->redirectToRoute('score_cms_gallery_edit', ["id" => $block->getParamsArr()['galleryId'], "ref" => $ref]);
//        elseif ($block->getType() === "html" || $block->getType() === "controllerContent")
        elseif ($this->canIAccess(1))
            return $this->redirectToRoute('score-block-edit-expert', ["id" => $id, "ref" => $ref]);

        $this->addFlash('block_danger', 'chyba');
        return $this->redirect($request->headers->get('referer'));

    }

    /**
     * Creates a new Block entity.
     * @Route("/new", name="-new", methods={"GET", "POST"})
     */
    public function createAction(BlockManager $manager, Request $request) {
        if (!$this->canIAccess())
            return $this->redirectHome();

        $groupEnum = $manager->getGroups();

        $entity = new Block();
        $form = $this->createForm(BlockType::class, $entity, ['groups' => array_flip($groupEnum)]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $entity->setSortOrder(time());
            $em->persist($entity);
            $em->flush();

            $this->addFlash('block_success', 'score.alert.block.created');
            return $this->redirect($this->generateUrl('score-block-edit', array('id' => $entity->getId())));
        }

        return $this->render('@ScoreCms/Block/Admin/edit.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
            'logs' => []
        ));
    }


    /**
     * Displays a form to edit an existing Block entity.
     * @Route("/edit/{id}/{ref}", name="-edit", methods={"GET", "POST"})
     */
    public function editAction(BlockManager $manager, Request $request, $id, $ref = null) {
        if (!$this->canIAccess())
            return $this->redirectHome();

        $em = $this->getDoctrine()->getManager();

        $block = $em->getRepository(Block::class)->find($id);

        if (!$block) {
            throw $this->createNotFoundException('Unable to find Block entity.');
        }
        $groupEnum = $manager->getGroups();

        $editForm = $this->createForm(BlockType::class, $block, ['groups' => array_flip($groupEnum)]);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            //return new Response('<html>ok</html>');

            $this->addFlash('block_success', 'score.alert.block.updated');
            return $this->redirectToRoute('score-block-edit', array('id' => $block->getId()));
        }

//        $logs = $em->getRepository(ChangesLog::class)->findBy(["entity" => Block::class, "entityId" => $id, "field" => "content"], ["editedAt" => "DESC"],15);

        return $this->render('@ScoreCms/Block/Admin/edit.html.twig', [
            'entity' => $block,
            'form' => $editForm->createView(),
//            'logs' => $logs
            "referer" => $ref
        ]);
    }

    /**
     * @Route("/to-trash/{id}", name="-to-trash")
     */
    public function toTrashAction(Request $request, $id) {
        if (!$this->canIAccess())
            return $this->redirectHome();

        $em = $this->getDoctrine()->getManager();
        $block = $em->getRepository(Block::class)->find($request->get('id'));

        $block->setIsDeleted(true);
        $em->persist($block);
        $em->flush();
        $this->addFlash("admin-success", "score.alert.block.movedToTrash");

        return $this->redirectToRoute('score-block-list');
    }

    /**
     * @Route("/from-trash/{id}", name="-from-trash")
     */
    public function fromTrashAction(Request $request, $id) {
        if (!$this->canIAccess(1))
            return $this->redirectHome();

        $em = $this->getDoctrine()->getManager();
        $block = $em->getRepository(Block::class)->find($request->get('id'));

        $block->setIsDeleted(false);
        $em->persist($block);
        $em->flush();

        $this->addFlash("admin-success", "score.alert.block.movedFromTrash");
        return $this->redirectToRoute('score-block-list-full');
    }


    /**
     * Deletes a Block entity.
     * @Route("/delete/{id}", name="-delete", methods={"GET"})
     */
    public function deleteAction(Request $request, $id) {
        if (!$this->canIAccess(1))
            return $this->redirectHome();

        $em = $this->getDoctrine()->getManager();
        $block = $em->getRepository(Block::class)->find($id);

        if (!$block) {
            throw $this->createNotFoundException('Unable to find Block entity.');
        }

        if ($block->getBlockJoints()->count() > 0) {
            $names = [];
            foreach ($block->getBlockJoints() as $blockJoint) {
                $names[] = $blockJoint->getEntity()->getName();
            }
            $this->addFlash('block_danger', 'Unable to delete, this block is used in: ' . implode(", ", $names) . '.');
            return $this->redirectToRoute('score-block-list-full');
        }

        $em->remove($block);
        $em->flush();

        $this->addFlash('block_success', 'score.alert.block.deleted');
        return $this->redirect($this->generateUrl('score-block-list-full'));
    }

    /**
     * @Route("/edit-expert/{id}/{ref}", name="-edit-expert", methods={"GET", "POST"})
     */
    public function expertEditAction(Request $request, $id, $ref = null) {
        if (!$this->canIAccess(1))
            return $this->redirectHome();

        $em = $this->getDoctrine()->getManager();

        $block = $em->getRepository(Block::class)->find($id);

        if (!$block) {
            throw $this->createNotFoundException('Unable to find Block entity.');
        }

        $editForm = $this->createForm(FullBlockType::class, $block);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            //return new Response('<html>ok</html>');

            $this->addFlash('block_success', 'score.alert.block.updated');
            return $this->redirectToRoute('score-block-edit-expert', array('id' => $block->getId()));
        }

//        $logs = $em->getRepository(ChangesLog::class)->findBy(["entity" => Block::class, "entityId" => $id, "field" => "content"], ["editedAt" => "DESC"],15);

        return $this->render('@ScoreCms/Block/Admin/edit_expert.html.twig', [
            'entity' => $block,
            'form' => $editForm->createView(),
//            'logs' => $logs
            "referer" => $ref
        ]);
    }

}
