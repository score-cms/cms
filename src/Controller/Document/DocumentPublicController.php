<?php

namespace Score\CmsBundle\Controller\Document;

use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Persistence\ManagerRegistry;
use Score\CmsBundle\Entity\Document\Document;
use Score\CmsBundle\Entity\Document\DocumentCategory;
use Score\CmsBundle\Entity\Document\DocumentDomain;

class DocumentPublicController extends AbstractController {

    public function documentsByDomainAction($domain = null, $size = "small", $asArray = false) {
        //public function documentsByCategoryAction(Request $request, ManagerRegistry $doctrine, $domain): Response {

        $em = $this->getDoctrine()->getManager();
        $domainEntity = $em->getRepository(DocumentDomain::class)->findOneBySeoName($domain);
        $documents = [];
        if ($domainEntity) {
            foreach ($domainEntity->getDocumentDescriptions() as $dd) {
                if (!$dd->getDocument()->getIsDeleted() && $dd->getDocument()->getPublic() && $dd->getDocument()->getVisibility()){
                    $documents[] = $dd->getDocument();
                }
            }
        }

        if ($asArray) {
            return $documents;
        }

        return $this->render("@ScoreCms/Public/default/documents_block.html.twig", [
            'category' => "",
            'documents' => $documents,
            'domain' => $domainEntity,
            'size' => $size
        ]);
    }

    public function documentsByCategoryAction($category = null, $size = "small", $asArray = false) {
        //public function documentsByCategoryAction(Request $request, ManagerRegistry $doctrine, $category): Response {

        $em = $this->getDoctrine()->getManager();
        $categoryEntity = $em->getRepository(DocumentCategory::class)->findOneBy(["seoName" => $category, 'visibility' => true]);
        $documents = [];
        if ($categoryEntity) {
            $documents = $categoryEntity->getDocuments()->filter(function($d){return !$d->getIsDeleted() &&  $d->getPublic() && $d->getVisibility();});
        }

        if ($asArray) {
            return $documents;
        }

        return $this->render("@ScoreCms/Public/default/documents_block.html.twig", [
            'category' => $categoryEntity,
            'documents' => $documents,
            'domain' => "",
            'size' => $size
        ]);
    }

    /**
     * @Route("/dokumenty", name="documents")
     * @Route("/dokumenty/{filter}", name="documents-filtered")
     */
    public function listAction(Request $request, ManagerRegistry $doctrine, $filter = null): Response {
        $em = $doctrine->getManager();
        $documents = [];
        if ($filter) { // filter kategorii ak nenajde ide na oblasti
            $documents = $this->documentsByCategoryAction($filter, null, true);
            if (!count($documents)) {
                $documents = $this->documentsByDomainAction($filter, null, true);
            }
        } else {
            $documents = $em->getRepository(Document::class)->findBy(['isDeleted' => false, 'public' => true]);
        }

        return $this->render('@ScoreCms/Public/default/documents.html.twig', [
            'documents' => $documents,
        ]);
    }

    /**
     * @Route("/dokument/{seoId}", name="document")
     */
    public function documentAction(Request $request, ManagerRegistry $doctrine, $seoId): Response {
        $em = $doctrine->getManager();
        $document = $em->getRepository(Document::class)->findOneBySeoId($seoId);
        if (!$document || $document->getIsDeleted() || !$document->getPublic()) {
            return $this->redirectToRoute('documents');
        }
        if (!$document->getHasDetail() && $document->getActualFile()) {
            return $this->redirectToRoute('document-download-ext', [
                'seoId' => $document->getSeoid(),
                'ext' => $document->getExtension()
            ]);
        }

        return $this->render('@ScoreCms/Public/default/document.html.twig', [
            'document' => $document,
        ]);
    }

    /**
     * @Route("/dokument/f/{seoId}.{ext}", name="document-download-ext")
     * @Route("/dokument/f/{seoId}", name="document-download")
     */
    public function documentDownloadAction(Request $request, ManagerRegistry $doctrine, $seoId, $ext = null): Response {
        $em = $doctrine->getManager();
        $document = $em->getRepository(Document::class)->findOneBySeoId($seoId);
        if (!$document || $document->getIsDeleted() || !$document->getPublic()) {
            return $this->redirectToRoute('documents');
        }
        $file = $document->getActualFile();
        if (!$file) {
            return $this->redirectToRoute('documents');
        }

        if ($file->getIsExternal()) {
            return $this->redirect($file->getUrl());
        }

        $root = $this->getParameter('kernel.project_dir') . '/public';
        $response = new BinaryFileResponse($root . $file->getUrl());

        return $response;
    }

    /**
     * @Route("/document-info/{seoId}.{ext}", name="document-info-ext")
     * @Route("/document-info/{seoId}", name="document-info")
     */
    public function documentInfoAction(Request $request, ManagerRegistry $doctrine, $seoId, $ext = null): Response {
        $em = $doctrine->getManager();
        $document = $em->getRepository(Document::class)->findOneBySeoId($seoId);
        if (!$document || $document->getIsDeleted() || !$document->getPublic()) {
            return $this->redirectToRoute('documents');
        }
        return new JsonResponse([
            "name" => $document->getName(),
            "info" => $document->getInfo(),
            "link" => $this->generateUrl('document-download-ext', [
                'seoId' => $document->getSeoId(),
                'ext' => $document->getExtension(),
            ])
        ]);
    }



}