<?php

namespace Score\CmsBundle\Controller\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Score\BaseBundle\Services\SeoUrl;
use Score\CmsBundle\Entity\Article\Article;
use Score\CmsBundle\Entity\Block\Block;
use Score\CmsBundle\Entity\Document\AllLevels;
use Score\CmsBundle\Entity\Document\DocumentDescription;
use Score\CmsBundle\Entity\Document\DocumentDomain;
use Score\BaseBundle\Services\Generator;
use Score\CmsBundle\Entity\Document\AllCategories;
use Score\CmsBundle\Entity\Document\AllDomains;
use Score\CmsBundle\Entity\Document\DocumentLevel;
use Score\CmsBundle\Entity\Event\Event;
use Score\CmsBundle\Entity\Page\Page;
use Score\CmsBundle\Entity\User;
use Score\CmsBundle\Form\Document\AllLevelsType;
use Score\CmsBundle\Services\AccessManager;
use Score\CmsBundle\Services\BlockManager;
use Score\CmsBundle\Services\CmsTwigManager;
use Score\CmsBundle\Services\MetatagManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Contracts\Translation\TranslatorInterface;
use Score\CmsBundle\Entity\Document\Document;
use Score\CmsBundle\Entity\Document\DocumentCategory;
use Score\CmsBundle\Form\Document\AllCategoriesType;
use Score\CmsBundle\Form\Document\AllDomainsType;
use Score\CmsBundle\Form\Document\DocumentType;
use Score\CmsBundle\Services\DocumentManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin/document", name="score-cms-document-")
 */
class DocumentAdminController extends AbstractController {

    protected function canIAccess($topManager = false) {
        $allowed = ["ROLE_DOCUMENT_MANAGER", "ROLE_TOP_MANAGER", "ROLE_ADMIN"];
        if ($topManager) {
            $allowed = ["ROLE_TOP_MANAGER", "ROLE_ADMIN"];
        }
        return (count(array_intersect($allowed, $this->getUser()->getRoles())) >= 1);
    }

    protected function redirectHome() {
        $this->addFlash('danger', 'score.alert.user.lowRole');
        return $this->redirectToRoute('admin_start');
    }

    // forwarded in Score\CmsBundle\Controller\ApiController;
    public function dataListAction(AccessManager $accessManager, EntityManagerInterface $entityManager) {

        if (!$this->canIAccess()) {
            return $this->redirectHome();
        }
        $checkAccess = $this->getParameter("access_checking_document") && !$this->canIAccess(true);
        $list = $entityManager->createQuery('
        SELECT d.id AS value, d.name, dc.name AS category, d.hasDetail, d.public
        FROM ' . Document::class . ' d
        LEFT JOIN d.documentCategories dc
        LEFT JOIN d.accesses acc 
        LEFT JOIN acc.user u 
        WHERE (d.isDeleted != :true OR d.isDeleted IS NULL)
        AND ' . ($checkAccess ? 'u.id = :userId' : ':userId IS NOT NULL') . '
            AND (dc.visibility = :true OR dc IS NULL)
        ORDER BY d.name ASC, dc.name ASC
        ')
            ->setParameters([
                "true"=> true,
                "userId" => $this->getUser()->getId(),
            ])
            ->getResult();


        // group by document, caterories and domains to array
        $list = array_reduce($list, function ($carry, $item) {
            $item = array_change_key_case($item, CASE_LOWER);
            if (!isset($carry[$item['value']])) {
                $carry[$item['value']] = [
                    "value" => $item['value'],
                    "name" => '<a href="' . $this->generateUrl("score-cms-document-edit", ["id" => $item['value']]) . '">' . $item['name'] . '</a>',
                    "public" => $item['public'] ? '<i class="fa fa-check"></i>' : '<i class="fa fa-times"></i>',
                    "hasDetail" => $item['hasdetail'] ? '<i class="fa fa-check"></i>' : '<i class="fa fa-times"></i>',
                    "action" => $this->canIAccess(true)
                        ? '<a class="to-trash-document" href="' . $this->generateUrl("score-cms-document-to-trash", ["id" => $item['value']]) . '">Presunúť do koša</a>' // tie v kosi sa tu nenacitavaju a ani ich tu nechceme
                        : "",
                    "categories" => [],
                ];
            }

            if ($item['category'] && !in_array($item['category'], $carry[$item['value']]['categories'])) {
                $carry[$item['value']]['categories'][] = $item['category'];
            }

            return $carry;
        }, []);

        return $this->json([
            "status" => "ok",
            "data" => array_values($list)
        ]);

    }

    /**
     * @Route("/", name="")
     * @Route("/list", name="list")
     */
    public function listAction(AccessManager $accessManager) {

        if (!$this->canIAccess()) {
            return $this->redirectHome();
        }

//        $em = $this->getDoctrine()->getManager();
//
//        $list = $em->getRepository(Document::class)->findBy([], ["createdAt" => "DESC"]);
//
//        if (!$this->canIAccess(true)) { //odfiltrovat tie z kosa
//            $list = array_filter($list, function ($v) {
//                return !$v->getIsDeleted();
//            });
//        }
//        $accesableList = [];
//        if ($this->getParameter("access_checking_document")) { // ak ma zobrazit len tie ktore uzivatel moze editovat
//            foreach ($list as $article) {
//                if ($accessManager->hasUserAccessTo($this->getUser(), $article)) {
//                    $accesableList[] = $article;
//                }
//            }
//        } else {
//            $accesableList = $list;
//        }


        return $this->render('@ScoreCms/Document/list.html.twig', [
            'list' => [], // $accesableList,
            'canDelete' => $this->canIAccess(true),
        ]);
    }

    /**
     * @Route("/create-block/{part}/{seoName}", name="create-block")
     */
    public function createBlockAction(Request $request, $part, $seoName) {
        if (!$this->canIAccess(true)) {
            return $this->redirectHome();
        }

        $em = $this->getDoctrine()->getManager();
        $block = new Block();
        $block->setType("controllerContent")
            ->setModule("Score\CmsBundle\Controller\Document\DocumentPublicController")
            ->setVisibility(true);

        if ($part === "categories") {
            $entity = $em->getRepository(DocumentCategory::class)->findOneBy(["seoName" => $seoName, "visibility" => true]);
            $block->setName("Výpis dokumentov – " . $entity->getName() . " big")
                ->setAction("documentsByCategoryAction")
                ->setParamsArr([
                    "category" => $entity->getSeoName(),
                    "size" => "big"
                ]);
        } else {
            $entity = $em->getRepository(DocumentDomain::class)->findOneBy(["seoName" => $seoName, "visibility" => true]);
            $block->setName("Výpis dokumentov za oblasť – " . $entity->getName() . " big")
                ->setAction("documentsByDomainAction")
                ->setParamsArr([
                    "domain" => $entity->getSeoName(),
                    "size" => "big"
                ]);
        }
        $em->persist($block);
        $em->flush();

        return $this->redirectToRoute('score-block-edit-expert', ["id" => $block->getId()]);
    }

    /**
     * @Route("/new", name="new")
     */
    public function newAction(
        Request         $request,
        DocumentManager $documentManager,
        Generator       $generator,
        BlockManager    $blockManager,
        AccessManager   $accessManager,
        MetatagManager  $metatagManager
    ) {
        return $this->editAction($request, $documentManager, $generator, $blockManager, $accessManager, $metatagManager, null);
    }

    /**
     * @Route("/edit/{id}", name="edit")
     */
    public function editAction(
        Request         $request,
        DocumentManager $documentManager,
        Generator       $generator,
        BlockManager    $blockManager,
        AccessManager   $accessManager,
        MetatagManager  $metatagManager,
                        $id
    ) {
        if (!$this->canIAccess()) {
            return $this->redirectHome();
        }

        $document = null;
        $em = $this->getDoctrine()->getManager();

        if ($id) {
            $document = $em->getRepository(Document::class)->find($id);
        } else {
            $document = new Document();
            $document->setCode($generator->generate(16));
            $accessManager->updateEntityAccess($this->getUser(), $document, true, ["full_access" => true, "can_delete" => true]);
        }

        if (!$document) {
            $this->addFlash('danger', 'score.alert.document.noDocument');
            return $this->redirectToRoute('score-cms-document-list');
        }

        $checkAccess = $this->getParameter("access_checking_document") && !$this->canIAccess(true);
        if ($checkAccess && !$accessManager->hasUserAccessTo($this->getUser(), $document)) { // TODO
            $this->addFlash('danger', 'score.alert.user.lowRole');
            return $this->redirectToRoute('score-cms-document-list');
        }

        $oldDescriptionsArr = $document->getDocumentDescriptions()->toArray();
        $oldFilesArr = $document->getDocumentFiles()->toArray();

        $form = $this->createForm(DocumentType::class, $document);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $documentManager->updateSeoId($document);
            $documentManager->updateDocumentDescriptions($document, $oldDescriptionsArr);

            $files = [];
            if ($request->files->get('document') && array_key_exists('documentFiles', $request->files->get('document'))) {
                $files = $request->files->get('document')['documentFiles'];
            }
            $errorsf = $documentManager->updateDocumentFiles($document, $oldFilesArr, $files);
            foreach ($errorsf as $error) {
                $this->addFlash('warning', $error);
            }

            if ($request->get('document')['removeIcon'] === "true") {
                $document->setIcon("");
            }

            if ($request->files->get('document') && array_key_exists('iconFile', $request->files->get('document'))) {
                $icon = $request->files->get('document')['iconFile'];
                if ($icon instanceof UploadedFile) {
                    $fileName = $documentManager->handleIconUpload($document, $icon);
                    $document->setIcon($fileName);
                }
            }

            $blocks = $request->get('block_ids');
            $joints = $request->get('joint_ids');
            $blockManager->updateBlockJoints($document, $blocks, $joints);

            if ($metatagManager->updateMetatagsFor($document, $this->getUser())) {
                $this->addFlash('edit_warning', 'score.alert.metatag.duplicity.warning');
            }

            $documentManager->updateSearch($document);
            $em->persist($document);
            $em->flush();
            $this->addFlash('success', 'score.alert.document.changed');
            return $this->redirectToRoute('score-cms-document-edit', ['id' => $document->getId()]);
        }

        return $this->render('@ScoreCms/Document/edit.html.twig', [
            "form" => $form->createView(),
            "document" => $document,
            'activeSection' => $request->get('s') ?: "content",
            'showAccessTab' => $id && $accessManager->canUserEditAccessToEntity($this->getUser(), $document),
            'canDelete' => $id && $accessManager->canUserDeleteEntity($this->getUser(), $document),
            'users' => $em->getRepository(User::class)->getListForAccess("document"),
        ]);

    }

    /**
     * @Route("/domains", name="domains")
     */
    public function domainsAction(Request $request) {
        if (!$this->canIAccess(true)) {
            return $this->redirectHome();
        }

        $em = $this->getDoctrine()->getManager();
        $domains = $em->getRepository(DocumentDomain::class)->findBy([], ["createdAt" => "ASC"]);
        $allDomains = new AllDomains($domains);

        $form = $this->createForm(AllDomainsType::class, $allDomains);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            foreach ($allDomains->getDomains() as $d) {
                $em->persist($d);
            }
            foreach ($domains as $d) {
                if (!$allDomains->getDomains()->contains($d)) {
                    $em->remove($d);
                }
            }
            $em->flush();
            $this->addFlash('success', 'score.alert.document.domain.changed');
            return $this->redirectToRoute('score-cms-document-domains');
        }

        return $this->render('@ScoreCms/Document/all_category_list_edit.html.twig', [
            "all" => $allDomains,
            "form" => $form->createView(),
            "part" => "domains"
        ]);
    }


    /**
     * @Route("/categories", name="categories")
     */
    public function categoriesAction(Request $request) {
        if (!$this->canIAccess(true)) {
            return $this->redirectHome();
        }

        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository(DocumentCategory::class)->findBy([], ["createdAt" => "ASC"]);
        $allCategories = new AllCategories($categories);

        $form = $this->createForm(AllCategoriesType::class, $allCategories);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            foreach ($allCategories->getCategories() as $d) {
                $em->persist($d);
            }
            foreach ($categories as $d) {
                if (!$allCategories->getCategories()->contains($d)) {
                    $em->remove($d);
                }
            }
            $em->flush();
            $this->addFlash('success', 'score.alert.document.category.changed');
            return $this->redirectToRoute('score-cms-document-categories');
        }

        return $this->render('@ScoreCms/Document/all_category_list_edit.html.twig', [
            "all" => $allCategories,
            "form" => $form->createView(),
            "part" => "categories"
        ]);
    }


    /**
     * @Route("/levels", name="levels")
     */
    public function levelsAction(Request $request) {
        if (!$this->canIAccess(true)) {
            return $this->redirectHome();
        }

        $em = $this->getDoctrine()->getManager();
        $levels = $em->getRepository(DocumentLevel::class)->findBy([], ["createdAt" => "ASC"]);
        $allLevels = new AllLevels($levels);

        $form = $this->createForm(AllLevelsType::class, $allLevels);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            foreach ($allLevels->getLevels() as $d) {
                $em->persist($d);
            }
            foreach ($levels as $d) {
                if (!$allLevels->getLevels()->contains($d)) {
                    $em->remove($d);
                }
            }
            $em->flush();
            $this->addFlash('success', 'score.alert.document.level.changed');
            return $this->redirectToRoute('score-cms-document-levels');
        }

        return $this->render('@ScoreCms/Document/all_category_list_edit.html.twig', [
            "all" => $allLevels,
            "form" => $form->createView(),
            "part" => "levels"
        ]);
    }


    /**
     * @Route("/delete/{id}", name="delete")
     */
    public function deleteAction(Request $request, DocumentManager $documentManager, ManagerRegistry $doctrine, $id) {
        if (!$this->canIAccess(true)) {
            return $this->redirectHome();
        }

        $em = $doctrine->getManager();
        $document = $em->getRepository(Document::class)->find($id);
        if (!$document) {
            $this->addFlash('danger', 'score.alert.document.notDeleted');
            return $this->redirectToRoute('score-cms-document-list');
        }

        $root = $this->getParameter('kernel.project_dir') . '/public';
        //$dir = $this->getParameter('document_public_upload_directory');
        if ($document->getIcon()) {
            $icon = $document->iconThumb(100, 100);
            foreach ([
                         $root . $icon,
                         $root . str_replace('_thumb_100x100', "_thumb_640x480", $icon),
                         $root . str_replace('_thumb_100x100', "", $icon),
                     ] as $file) {
                file_exists($file) && unlink($file);
            }
        }

        foreach ($document->getDocumentCategories() as $category) {
            $document->removeDocumentCategory($category);
            $em->persist($category);
        }

        foreach ($document->getDocumentDescriptions() as $description) {
            $document->removeDocumentDescription($description);
            $em->remove($description);
        }

        foreach ($document->getDocumentLevels() as $level) {
            $document->removeDocumentLevel($level);
            $em->persist($level);
        }

        foreach ($document->getMyRelevants() as $d) {
            $d->removeMyRelevant($document);
            $em->persist($d);
        }

        foreach ($document->getDocumentFiles() as $documentFile) {
            if (!$documentFile->getIsExternal()) {
                file_exists($root . $documentFile->getUrl()) && unlink($root . $documentFile->getUrl());
            }
            $document->removeDocumentFile($documentFile);
            $em->remove($documentFile);
        }

        $em->remove($document);
        $em->flush();

        $this->addFlash('success', 'score.alert.document.deleted');
        return $this->redirectToRoute('score-cms-document-list');
    }

    /**
     * @Route("/api-document-check-name", name="api-check-name")
     */
    public function apiCheckNameAction(Request $request, ManagerRegistry $doctrine, SeoUrl $seoUrl): Response {
        header('Access-Control-Allow-Origin: *');
        if (!$this->canIAccess()) {
            return $this->redirectHome();
        }

        // TODO this method doesnt work well
        $code = $request->get('code');
        $fullName = $request->get('fullname');

        $em = $doctrine->getManager();
//        $documents = $em->getRepository(Document::class)->findAll();
        $documents = $em->getRepository(Document::class)->createQueryBuilder('d')
            ->select('d.id, d.name, d.fullName, d.code, d.isDeleted')
            ->getQuery()
            ->getResult();

        $docsArr = [];
        $words = $this->getWordsArray($seoUrl, $fullName);

        foreach ($documents as $d) {
            $c = 0;
            $ws2 = $this->getWordsArray($seoUrl, $d['fullName']);
            foreach ($words as $w) {
                $c += in_array($w, $ws2) ? 1 : 0;
            }
            if ($c && $d['code'] !== $code) {
                $docsArr[] = [
                    "count" => $c,
                    "name" => $d['name'],
                    "fullname" => $d['isDeleted'] ? "Z koša: " . $d['fullName'] : $d['fullName'],
//                    "seo" => $d['seoId'],
                    //"info" => $d['info'],
                    "url" => $this->generateUrl('score-cms-document-edit', ["id" => $d['id']])
                ];
            }
        }

        usort($docsArr, function ($item1, $item2) {
            return $item2['count'] <=> $item1['count'];
        });

        $result = [
            "string" => $fullName,
            "status" => "ok",
            "documents" => array_slice(array_values($docsArr), 0, 10),
        ];
        return new JsonResponse($result);

    }

    public function getWordsArray(SeoUrl $seoUrl, ?string $string): array {
        return $string ? preg_split('/(\/| )+/', $seoUrl->removeDiacritic(strtolower($string))) : []; // rozdeluje lomitkom a medzerou (nevracia prazdne stringy)
//        return $string ? explode(" ", $seoUrl->removeDiacritic(strtolower($string))) : []; // rozdeluje len medzerou
    }

    /**
     * @Route("/create-list", name="create-list") // initial categories and domains
     */
    public function createListAction() {
        if (!$this->canIAccess(true)) {
            return $this->redirectHome();
        }

        $em = $this->getDoctrine()->getManager();
        $dom = [
            'posudzovanie-vplyvov-na-zp' => 'Posudzovanie vplyvov na ŽP',
            'integrovana-prevencia-a-kontrola-znecistovania' => 'Integrovaná prevencia a kontrola znečisťovania',
            'prevencia-zavaznych-priemyselnych-havarii' => 'Prevencia závažných priemyselných havárií',
            'environmentalne-skody' => 'Environmentálne škody',
            'environmentalne-zataze' => 'Environmentálne záťaže',
            'klimaticka-zmena' => 'Klimatická zmena',
            'odpady' => 'Odpady',
            'hluk' => 'Hluk',
            'polnohospodarstvo' => 'Poľnohospodárstvo ',
            'lesne-hospodarstvo' => 'Lesné hospodárstvo',
            'priemysel' => 'Priemysel',
            'energetika' => 'Energetika',
            'doprava' => 'Doprava',
            'cestovny-ruch' => 'Cestovný ruch',
            'zdravie-a-zp' => 'Zdravie a ŽP',
            'ochrana-prirody-biota' => 'Ochrana prírody (biota)',
            'horninove-prostredie' => 'Horninové prostredie',
            'ovzdusie' => 'Ovzdušie',
            'poda' => 'Pôda',
            'voda' => 'Voda',
            'zivotne-prostredie' => 'Životné prostredie',
            'jadrova-bezpecnost' => 'Jadrová bezpečnosť',
        ];
        $cat = [
            'zakon' => 'Zákon',
            'vyhlaska' => 'Vyhláška',
            'nariadenie' => 'Nariadenie',
            'smernica' => 'Smernica',
            'usmernenie' => 'Usmernenie',
            'strategia' => 'Stratégia',
            'program' => 'Program',
            'plan' => 'Plán',
            'koncepcia' => 'Koncepcia',
            'medzinarodny-dohovor' => 'Medzinárodný dohovor',
            'metodicky-pokyn' => 'Metodický pokyn',
            //        'ine' => 'Iné',
        ];

        $levels = [
            'narodna' => 'Národná',
            'medzinarodna' => 'Medzinárodná',
        ];

        foreach ($cat as $k => $n) {
            $e = new DocumentCategory();
            $e->setName($n)->setSeoName($k)->setPublic(true)->setVisibility(true);
            $em->persist($e);
        }

        foreach ($dom as $k => $n) {
            $e = new DocumentDomain();
            $e->setName($n)->setSeoName($k)->setPublic(true)->setVisibility(true);
            $em->persist($e);
        }

        foreach ($levels as $k => $n) {
            $e = new DocumentLevel();
            $e->setName($n)->setSeoName($k)->setPublic(true)->setVisibility(false);
            $em->persist($e);
        }
        //$em->flush();

        return $this->redirectToRoute('score-cms-document-list');

    }


    /**
     * @Route("/to-trash/{id}", name="to-trash")
     */
    public function toTrashAction(Request $request, AccessManager $accessManager, $id) {
        if (!$this->canIAccess()) {
            return $this->redirectHome();
        }

        $em = $this->getDoctrine()->getManager();
        $document = $em->getRepository(Document::class)->find($request->get('id'));
        if ($accessManager->canUserDeleteEntity($this->getUser(), $document)) {
            $document->setIsDeleted(true);
            $em->persist($document);
            $em->flush();
            $this->addFlash("admin-success", "score.alert.document.movedToTrash");
        }
        return $this->redirectToRoute('score-cms-document-list');
    }

    /**
     * @Route("/from-trash/{id}", name="from-trash")
     */
    public function fromTrashAction(Request $request, $id) {
        if (!$this->canIAccess(true)) {
            return $this->redirectHome();
        }

        $em = $this->getDoctrine()->getManager();
        $document = $em->getRepository(Document::class)->find($request->get('id'));

        $document->setIsDeleted(false);
        $em->persist($document);
        $em->flush();

        $this->addFlash("admin-success", "score.alert.document.movedFromTrash");
        return $this->redirectToRoute('score-cms-document-list');
    }


    /**
     * @Route("/access/edit2", name="edit-access")
     */
    public function editAccessAction(Request $request, AccessManager $accessManager) {
        if (!$this->canIAccess()) {
            return $this->redirectHome();
        }

        $em = $this->getDoctrine()->getManager();
        $document = $em->getRepository(Document::class)->find($request->get("document_id"));
        $user = $em->getRepository(User::class)->find($request->get("user_id"));
        $checkAccess = $this->getParameter("access_checking_document") && !$this->canIAccess(true);
        if ($document && $user && $accessManager->canUserEditAccessToEntity($this->getUser(), $document)) {
            $params = [
                "full_access" => boolval($request->get("full_access")),
                "can_create" => boolval($request->get("can_create")),
                "can_delete" => boolval($request->get("can_delete")),
                "is_heritable" => boolval($request->get("is_heritable"))
            ];
            $hasAssess = boolval($request->get("has_access"));
            $accessManager->updateEntityAccess($user, $document, $hasAssess, $params);
            $em->persist($user); // vsetky zmeny v pravach vsetkych documentov sa vzdy tykaju jedneho usera preto staci len jeho persist
            $em->flush();

            $this->addFlash('score-cms-document-edit-success', 'Zmeny boli uložené');
            return $this->redirectToRoute('score-cms-document-edit', ['id' => $document->getId(), 's' => 'access']);
        }
        return $this->redirectToRoute('score-cms-document-edit', ['id' => $document->getId(), 's' => 'access']);
    }

    /**
     * @Route("/update-document-links-everywhere", name="update-links")
     */
    public function updateLinksAction(Request $request, CmsTwigManager $twigManager) {
        if (!$this->canIAccess(true)) {
            return $this->redirectHome();
        }

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository(Page::class)->findAll();
        foreach ($entities as $e) {
            $e->setContent($twigManager->updateDocumentLinks($e->getContent()));
            $em->persist($e);
        }

        $entities = $em->getRepository(Article::class)->findAll();
        foreach ($entities as $e) {
            $e->setBody($twigManager->updateDocumentLinks($e->getBody()));
            $em->persist($e);
        }

        $entities = $em->getRepository(Event::class)->findAll();
        foreach ($entities as $e) {
            $e->setContent($twigManager->updateDocumentLinks($e->getContent()));
            $em->persist($e);
        }

        $entities = $em->getRepository(Document::class)->findAll();
        foreach ($entities as $e) {
            $e->setDescription($twigManager->updateDocumentLinks($e->getDescription()));
            $em->persist($e);
        }

        $entities = $em->getRepository(Block::class)->findAll();
        foreach ($entities as $e) {
            if ($e->getType() === "static") {
                $e->setContent($twigManager->updateDocumentLinks($e->getContent()));
                $em->persist($e);
            }
        }

        $em->flush();

        $this->addFlash('score-cms-document-edit-success', 'Zmeny boli uložené');
        return $this->redirectToRoute('score-cms-document-list');
    }


    // forwarded in Score\CmsBundle\Controller\ApiController;
    public function apiListForCkeAction(EntityManagerInterface $entityManager): Response {

        $list = $entityManager->createQuery('
        SELECT d.id AS value, d.name, d.seoId AS seo, d.code, ddd.name AS domain, dc.name AS category, df.extension AS ext, df.info AS info
        FROM ' . Document::class . ' d
        LEFT JOIN d.documentDescriptions dd
        LEFT JOIN dd.documentDomain ddd
        LEFT JOIN d.documentCategories dc
        LEFT JOIN d.documentFiles df
        WHERE d.public = :true 
            AND (d.isDeleted != :true OR d.isDeleted IS NULL)
            AND (ddd.visibility = :true OR dd IS NULL) 
            AND (dd.visibility = :true OR dd IS NULL) 
            AND (dc.visibility = :true OR dc IS NULL)
            AND df.isCurrent = :true
        ORDER BY d.name ASC, ddd.name ASC, dc.name ASC
        ')
            ->setParameter("true", true)
            ->getResult();


        // group by document, caterories and domains to array
        $list = array_reduce($list, function ($carry, $item) {
            $item = array_change_key_case($item, CASE_LOWER);
            if (!isset($carry[$item['value']])) {
                $carry[$item['value']] = [
                    "name" => $item['name'],
                    "value" => $item['value'],
                    "seo" => $item['seo'],
                    "code" => $item['code'],
                    "info" => $item['info'] ? " (" . $item['info'] . ")" : "",
                    "ext" => $item['ext'] ? "." . $item['ext'] : "",
                    "domains" => [],
                    "categories" => [],
                ];
            }
            if ($item['domain'] && !in_array($item['domain'], $carry[$item['value']]['domains'])) {
                $carry[$item['value']]['domains'][] = $item['domain'];
            }

            if ($item['category'] && !in_array($item['category'], $carry[$item['value']]['categories'])) {
                $carry[$item['value']]['categories'][] = $item['category'];
            }

            return $carry;
        }, []);


        $domains = $entityManager->createQueryBuilder()
            ->select("d.name as name, d.seoName as seo")
            ->from(DocumentDomain::class, "d")
            ->andWhere('d.visibility = :true')->setParameter("true", true)
            ->add('orderBy', 'd.name ASC')
            ->getQuery()->getResult();

        $categories = $entityManager->createQueryBuilder()
            ->select("d.name as name, d.seoName as seo")
            ->from(DocumentCategory::class, "d")
            ->andWhere('d.visibility = :true')->setParameter("true", true)
            ->add('orderBy', 'd.name ASC')
            ->getQuery()->getResult();

        $result = [
            "status" => "ok",
            "documents" => array_values($list),
            "domains" => $domains,
            "categories" => $categories,
            "docsCount" => count($list),
            "urlPrefixDocumentShow" => "/dokument/",
            "urlPrefixDocumentDownload" => "/dokument/f/",
        ];

        return new JsonResponse($result);
    }


}
