<?php

namespace Score\CmsBundle\Controller;

use Doctrine\Common\Collections\Criteria;
use Score\CmsBundle\Entity\Menu;
use Score\CmsBundle\Form\MenuForm;
use Score\CmsBundle\Entity\Page\Page;
use Score\CmsBundle\Entity\Block\Block;
use Score\CmsBundle\Entity\Multisite\Site;
use Score\CmsBundle\Services\OptionManager;
use Symfony\Component\HttpFoundation\Request;
use Score\CmsBundle\Services\AdminPageManager;
use Symfony\Component\Routing\Annotation\Route;
use Score\CmsBundle\Entity\Multisite\SiteItemMenu;
use Score\BaseBundle\Services\AdjacencyArrayTreeManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MenuController extends AbstractController {
    protected function canIAccess($admin = false) {
        $allowed = ["ROLE_MENU_MANAGER", "ROLE_ADMIN"];
        if ($admin) {
            $allowed = ["ROLE_ADMIN"];
        }
        return (count(array_intersect($allowed, $this->getUser()->getRoles())) >= 1);
    }


    public function getRootPage() {
        return $this->getDoctrine()->getManager()->getRepository(Page::class)->findOneBy(['name' => 'root']);
    }

    /**
     * @Route("/admin/menu/merge", name="score_cms_menu_merge")
     */
    public function testAction() {
        $em = $this->getDoctrine()->getManager();

        // admin funkcia na spajanie menu
        $newParentId = 2348;
        $oldMenuId = 1445;

        $menu = $em->getRepository(Menu::class)->find($newParentId);
        $items1 = $em->getRepository(Menu::class)->findBy(['rootId' => $oldMenuId]);
        foreach ($items1 as $item) {
            $item->setRootId($menu->getId());
            if ($item->getParentId() === $oldMenuId) {
                $item->setParentId($menu->getId());
            }
            $item->setLvl($item->getLvl() + 1);
            $em->persist($item);
        }
//        $em->flush();
        die("odkomentujFlush");

    }


    /**
     * @Route("/admin/menu/edit", name="score_cms_menu")
     */
    public function indexAction(AdjacencyArrayTreeManager $treeManager, Request $request, AdminPageManager $pageManager, OptionManager $optionManager) {
        if (!$this->canIAccess()) {
            $this->addFlash('danger', 'score.alert.user.lowRole');
            return $this->redirectToRoute('admin_start');
        }

        $menuId = $request->get('menuId');
        $ref = $request->get('ref');
        $repository = $this->getDoctrine()->getRepository(Menu::class);
        $menuList = $repository->findBy(['rootId' => null], ['createdAt' => 'ASC', 'id' => 'ASC']);

        $pageList = $pageManager->getPageParentChoices($this->getRootPage()->getId());
        $pageList = array_filter($pageList, function ($v) { //odfiltruje tie z kosa
            return !$v->getIsDeleted();
        });

        $currentMenuId = intval($menuId);

        if ($menuId === null) {
            $currentMenuId = $menuList[0]->getId();
        }

        $items = $repository->find($currentMenuId)->getChildren();

        $menu = $repository->find($currentMenuId);
        $existMenuItems = [];
        foreach ($menu->getSiteItemMenu() as $siteItem) {
            $existMenuItems[] = $siteItem->getSite()->getId();
        }

        $multisiteSites = $this->getDoctrine()->getRepository(Site::class)->findAll();
        $multisiteChoices = [];
        $multisiteEnabled = false;
        foreach ($multisiteSites as $multisiteSite) {
            $multisiteChoices[$multisiteSite->getId()] = $multisiteSite->getName();
        }
        if (empty($multisiteChoices)) {
            $multisiteEnabled = true;
        }

        $tree = [];
        if ($this->getParameter("cms_menu_page_connection")) {
            foreach ($this->getParameter("cms_menu_page_connection") as $mid => $pid) {
                if ($mid === $currentMenuId) {
//                    $root = $this->getDoctrine()->getManager()->getRepository(Page::class)->findOneBy(["name" => "root"]);
                    $root = $this->getDoctrine()->getManager()->getRepository(Page::class)->find($pid);
                    $tree = $pageManager->getPagesTree(null, null, $root->getChildren(), [], false, true);
                }
            }
        }


        return $this->render('@ScoreCms/Menu/index.html.twig', [
            'list' => $menuList,
            'pageList' => $pageList,
            'items' => $items,
            'currentMenuId' => $currentMenuId,
            'multisiteChoices' => $multisiteChoices,
            'existMenuItems' => $existMenuItems,
            'multisiteEnabled' => $multisiteEnabled,
            'menu' => $menu,
            'tree' => $tree,
            "referer" => $ref
        ]);
    }

    /**
     * @Route("/admin/menu/create", name="score_cms_menu_create")
     */
    public function createAction(Request $request) {
        if (!$this->canIAccess()) {
            $this->addFlash('danger', 'score.alert.user.lowRole');
            return $this->redirectToRoute('admin_start');
        }

        $em = $this->getDoctrine()->getManager();
        $menu = new Menu();
        $menu->setName($request->get('name'));
        $menu->setType("block-menu");
        $em->persist($menu);
        $em->flush();

        //vytvor blok
        $block = new Block();
        $block->setName($menu->getName());
        $block->setLang('sk');
        $block->setType('menu');
        $block->setVisibility(true);
        $block->setParamsArr(['menuId' => $menu->getId()]);

        $em->persist($block);
        $em->flush();

        $this->addFlash('score_cms_menu_create_success', 'Menu bolo vytvorené');
        return $this->redirectToRoute('score_cms_menu', ["menuId" => $menu->getId()]);

    }

    /**
     * @Route("/admin/menu/save", name="score_cms_menu_save")
     */
    public function saveAction(Request $request) {
        if (!$this->canIAccess()) {
            $this->addFlash('danger', 'score.alert.user.lowRole');
            return $this->redirectToRoute('admin_start');
        }

        $menuId = $request->get('root_id');
        $items = $request->get('items');
        $repository = $this->getDoctrine()->getRepository(Menu::class);
        $menuR = $repository->find($menuId);

        $em = $this->getDoctrine()->getManager();
        $siteRepository = $em->getRepository(Site::class);
        $siteItemRepository = $em->getRepository(SiteItemMenu::class);
        $qd = $siteItemRepository->createQueryBuilder('t')->delete();
        $qd->where('t.menu = :itemId');
        $qd->setParameter('itemId', $menuId);
        $query = $qd->getQuery();
        $query->getResult();


        if (null != $request->get('multisite')) {
            $multisites = explode(',', $request->get('multisite'));

            foreach ($multisites as $siteId) {
                $site = $siteRepository->find($siteId);
                $siteItemMenu = new SiteItemMenu();
                $siteItemMenu->setSite($site);
                $siteItemMenu->setMenu($menuR);
                $em->persist($siteItemMenu);
                $em->flush();
            }

        }

        // all menu items before submit - unused will be deleted
        $oldCollection = $menuR->getAllChildren()->matching((Criteria::create())->orderBy(['lvl' => 'DESC']));

        $em = $this->getDoctrine()->getManager();
        $ids = $this->saveMenuItems($em, $items, $menuR);

        foreach ($oldCollection as $child) {
            if ($child->getId() && !in_array($child->getId(), $ids)) {
                $em->remove($child);
            }
        }
        $em->flush();

        return $this->json([
            'status' => 'success',
            'ids' => $ids
        ]);

    }

    protected function saveMenuItems($em, $items, Menu $parent) {

        $ids = [];
        foreach ($items as $order => $item) {
            $menu = null;
            if (array_key_exists('id', $item) && intval($item['id']) >= 1) {
                $menu = $em->getRepository(Menu::class)->find($item['id']);
            } else {
                $menu = new Menu();
            }

            $ids[] = $menu->getId();
            $menu->setParent($parent);
            $menu->setName($item['name']);
            $menu->setLink($item['url']);
            $menu->setSortOrder(($order + 1));
            $menu->setRootId($parent->getRootId() ?? $parent->getId());
            $menu->setLvl($parent->getLvl() ? ($parent->getLvl() + 1) : 1);
            $menu->setLinkType($item['type']);
            if ($item['type'] === 'page') {
                $menu->setLinkTypeData(json_encode([
                    'id' => $item['typeId'],
                    'seoId' => $item['url']
                ]));
                $page = $em->getRepository(Page::class)->find($item['typeId']);
                $page->addMenu($menu);
            }
            $em->persist($menu);

            if (array_key_exists('children', $item)) {
                $chids = $this->saveMenuItems($em, $item['children'], $menu);
                $ids = array_merge($ids, $chids);
            }
        }
        return $ids;
    }

    /**
     * @Route("/admin/menu/delete", name="score_cms_menu_delete")
     */
    public function deleteAction(Request $request) {
        if (!$this->canIAccess(true)) {
            $this->addFlash('danger', 'score.alert.user.lowRole');
            return $this->redirectToRoute('admin_start');
        }

        $menuId = $request->get('menuId');

        if ($menuId) {
            $em = $this->getDoctrine()->getManager();
            $repository = $em->getRepository(Menu::class);
            $items = $repository->findBy(['rootId' => $menuId]);
            $items[] = $repository->find($menuId);

            foreach ($items as $item) {
                $em->remove($item);
            }
            $blocks = $em->getRepository(Block::class)->findBy(["type" => "menu"]);
            foreach ($blocks as $block) {
                if ($block->getParamsArr()["menuId"] == $menuId) {
                    if ($block->getBlockJoints()->count() > 0) {
                        $names = [];
                        foreach ($block->getBlockJoints() as $blockJoint) {
                            $names[] = $blockJoint->getEntity()->getName();
                        }
                        $this->addFlash('block_danger', 'Unable to delete, still used in: ' . implode(", ", $names) . '.');
                        return $this->redirectToRoute('score_cms_menu', ["menuId" => $menuId]);
                    } else {
                        $em->remove($block);
                    }
                }
            }
            $em->flush();

            $this->addFlash('success', 'Menu vymazané.');
        } else {
            $this->addFlash('danger', 'Chyba.');
        }

        return $this->redirectToRoute('score_cms_menu');

    }


    /**
     * @Route("/admin/menu/each-menu-to-page-name", name="score_cms_menu_each_menu_to_page_name")
     */
    public function eachMenuToPageNameAction(Request $request) {
        if (!$this->canIAccess(true)) {
            $this->addFlash('danger', 'score.alert.user.lowRole');
            return $this->redirectToRoute('admin_start');
        }

        $em = $this->getDoctrine()->getManager();
        $menuRepo = $em->getRepository(Menu::class);
        $pageRepo = $em->getRepository(Page::class);
        $menusRoot = $menuRepo->findBy(['rootId' => null]);
        $i1 = $i2 = $i3 = 0;

        foreach ($menuRepo->findAll() as $item) { // nemuselo by to tu byt keby parentid a parent mali v db rovnaky stlpec ale tak to nefunguje
            if ($item->getParentId()) {
                $parent = $menuRepo->find($item->getParentId());
                if ($parent) {
                    $item->setParent($parent);
                    $em->persist($item);
                }
            }
        }
        $em->flush();

        foreach ($menusRoot as $menu) {
            $menu->setParent(null);
            $em->persist($menu);

            foreach ($menu->getAllChildren() as $child) {
                if ($child->getLinkType() === 'page') { // ak je polozka menu typu page
                    $pageId = $child->getLinkTypeId();
                    if ($pageId && !$child->getPage()) { // ak polozka menu nema priradenu page ale ma jej ID po starom
                        $page = $pageRepo->find($pageId);
                        if ($page) { // ak page aj realne este existuje
                            if (!$page->getMenuName()) { // ak page nema zadany nazov do menu, tak ho vyplni z položky v menu
                                $page->setMenuName($child->getName());
                                $i2++;
                            }
                            $page->addMenu($child);
                            $em->persist($child);
                            $em->persist($page);
                            $i1++;
                        }
                    }
                }
            }
        }

        foreach ($pageRepo->findAll() as $page) {
            if (!$page->getMenuName()) { // ak page nema zadany nazov do menu, tak ho vyplni z názvu
                $page->setMenuName($page->getName());
                $em->persist($page);
                $i3++;
            }
        }

        $em->flush();

        $this->addFlash('success', "Všetky položky v menu a aj všetky stránky boli skontrolované a prekonvertované. (Stránok do menu: [$i1], Menu názvy stránok z menu: [$i2], Menu názvy stránok z názvov stránok: [$i3].)");

        return $this->redirectToRoute('score_cms_menu');

    }


}
