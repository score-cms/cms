<?php 
namespace Score\CmsBundle\Event;

use Score\CmsBundle\Entity\Article\Article;
use Symfony\Contracts\EventDispatcher\Event;


class ArticleEditEvent extends Event
{
    public const NAME = 'article.admin.edit';

    protected $article;

    public function __construct(Article $article)
    {
        $this->article = $article;
    }

    public function getArticle(): Article
    {
        return $this->article;
    }
}