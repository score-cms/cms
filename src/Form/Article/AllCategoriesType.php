<?php

namespace Score\CmsBundle\Form\Article;


use Score\CmsBundle\Entity\Article\AllCategories;
use Score\CmsBundle\Entity\Article\Article;
use Score\CmsBundle\Entity\Article\ArticleCategory;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AllCategoriesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $builder->add('categories', CollectionType::class, [
            'entry_type' => ArticleCategoryType::class,
            'entry_options' => [
                'label' => false, 
                'attr' => [
                    //"style" => "display:flex; align-items:center;",
                    "class" => "item-wrapper"
                    ]
            ],
            'prototype_data' => new ArticleCategory(),
            'allow_delete' => true,
            'allow_add' => true,
            'label' => false,
            //'attr' => ["style" => "display:flex;"]
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AllCategories::class,
        ]);
    }
}