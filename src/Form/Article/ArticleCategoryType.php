<?php

namespace Score\CmsBundle\Form\Article;

use Score\CmsBundle\Entity\Article\ArticleCategory;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleCategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', null, [
                'label_format' => 'score.cms.document.edit.%name%',
                'row_attr' => ['style' => "min-width:50%;"]
            ])
            // ->add('public', null, [
            //     'label_format' => 'score.cms.document.edit.%name%'
            // ])
            ->add('seoName', null, [
                'label_format' => 'score.cms.document.edit.%name%'
            ])
            ->add('visibility', null, [
                'label_format' => 'score.cms.document.edit.%name%'
            ])
            // ->add('createdAt', null, [
            //     'label_format' => 'score.cms.document.edit.%name%'
            // ])
            // ->add('editedAt', null, [
            //     'label_format' => 'score.cms.document.edit.%name%'
            // ])
            // ->add('lastCheckAt', null, [
            //     'label_format' => 'score.cms.document.edit.%name%'
            // ])
            // ->add('nextCheckAt', null, [
            //     'label_format' => 'score.cms.document.edit.%name%'
            // ])
            // ->add('documents', null, [
            //     'label_format' => 'score.cms.document.edit.%name%'
            // ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ArticleCategory::class,
        ]);
    }
}
