<?php

namespace Score\CmsBundle\Form\Article;

use Doctrine\ORM\EntityRepository;
use Score\CmsBundle\Entity\Article\Article;
use Score\CmsBundle\Entity\Block\Block;
use Score\CmsBundle\Entity\Metatag\MetatagArticle;
use Score\CmsBundle\Entity\Multisite\Site;
use Score\CmsBundle\Form\Metatag\MetatagArticleType;
use Score\CmsBundle\Form\Metatag\MetatagType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ArticleType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, $options) {
        $builder
            ->add('name', null, [
                'label' => 'score.article.form.name'
            ])
            ->add('slug', null, [
                'label' => 'score.article.form.slug'
            ])
            ->add('icon', FileType::class, [
                'data_class' => null,
                'required' => false,
                'help' => 'score.cms.document.edit.iconHelp',
                'label' => 'score.article.form.icon'
            ])
            ->add('removeIcon', HiddenType::class, [
                'mapped' => false,
                "data" => "false"
            ])
            ->add('teaser', TextareaType::class, [
                'label' => 'score.article.form.teaser',
                'required' => false
            ])
            ->add('body', TextareaType::class, [
                'label' => 'score.article.form.body',
                'required' => false
            ])
            ->add('published', ChoiceType::class, [
                'choices' => [
                    'score.article.form.empty' => '',
                    'score.article.form.published.yes' => '1',
                    'score.article.form.published.no' => '2'
                ],
                'label' => 'score.article.form.published'
            ])
            ->add('priority', ChoiceType::class, [
                'choices' => [
                    'score.article.form.empty' => '',
                    'score.article.form.priority.top' => '10',
                    'score.article.form.priority.default' => '20'
                ],
                'label' => 'score.article.form.priority'
            ])
//            ->add('category', ChoiceType::class, [
//                'choices' => [
//                    'score.article.form.empty' => '',
//                    'score.article.form.category.standard' => 'standard',
//                    'score.article.form.category.flash' => 'flash'
//                ],
//                'label' => 'score.article.form.category'
//            ])
            ->add('author', null, [
                'label' => 'score.article.form.author'
            ])
            ->add('published_from', DateType::class, [
                "widget" => 'single_text',
                'label' => 'score.article.form.published_from',
                'required' => false
            ])
            ->add('published_to', DateType::class, [
                "widget" => 'single_text',
                'label' => 'score.article.form.published_to',
                'required' => false
            ])
            ->add('articleCategories', null, [
                'label_format' => 'score.article.form.%name%',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->where('t.visibility = :v')
                        ->setParameter('v', true)
                        ->orderBy('t.seoName', 'ASC');
                },
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('multisite', EntityType::class, [
                'class' => Site::class,
                'multiple' => true,
                'expanded' => true,
                'choice_label' => 'name',
                'label' => 'score.article.form.multisite',
                'required' => false
            ])
            ->add('articleBlocks', EntityType::class, [
                'placeholder' => '',
                'label' => 'score.page.edit.block.add',
                'class' => Block::class,
                'choices' => [],
//                'query_builder' => function (EntityRepository $er) {
//                    return $er->getBlocksListToForm();
//                },
//                'choice_label' => function ($block) {
////                    return $block->getName() . " – " . $block->getType();
//                    return $block->getName();
//                },
                'mapped' => false,
                'required' => false,
            ])
            ->add('metatags', CollectionType::class, [
                'entry_type' => MetatagArticleType::class,
                'entry_options' => [
                    'label' => false,
                    'attr' => [
                        "class" => "item-wrapper"
                    ]
                ],
                'prototype_data' => new MetatagArticle(),
                'allow_delete' => true,
                'allow_add' => true,
                'label' => false,
                'by_reference' => false,
            ]);


    }

    public function configureOptions(OptionsResolver $resolver): void {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
        //$resolver->setRequired('multisiteChoices');
    }
}
