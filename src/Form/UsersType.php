<?php

namespace Score\CmsBundle\Form;

use Doctrine\Common\Collections\ArrayCollection;
use Score\CmsBundle\Entity\Group;
use Score\CmsBundle\Entity\User;
use Score\CmsBundle\Entity\Users;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class UsersType extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('users', CollectionType::class, [
                'entry_type' => UserType::class,
                'entry_options' => [
                    'groups' => $options['groups'],
                    'label' => false
                ],
            ])
          ;
    }


    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Users::class,
            'groups' => [],
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'score_cmsbundle_users';
    }
}
