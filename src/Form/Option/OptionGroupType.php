<?php

namespace Score\CmsBundle\Form\Option;

use Score\CmsBundle\Entity\OptionGroup;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;


class OptionGroupType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'label_format' => 'score.cms.option.edit.%name%'
            ])
            ->add('checkUsers', null, [
                'label_format' => 'score.cms.option.edit.%name%'
            ])
            ->add('users', null, [
                'label_format' => 'score.cms.option.edit.%name%',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                    ->join('u.groups', 'g')
                    ->where('g.role = :r')
                    ->setParameter('r', "ROLE_OPTION_MANAGER");
               },
                'row_attr' => [
                    'class' => 'dropdown'
                ],
                'multiple' => true,
                'expanded' => true,
                'attr' => [
                    'class' => 'dropdown-menu',
                    'aria-labelledby' => 'some_same_id',
                    'style' => 'padding:1em;'
                ],
                'label_attr' => [
                    'class' => 'btn btn-info dropdown-toggle',
                    'type' => "button",
                    'data-toggle' => "dropdown",
                    'aria-haspopup' => "true",
                    'aria-expanded' => "false",
                    'id' => 'some_same_id',
                ],
            ])
            ->add('visibility', null, [
                'label_format' => 'score.cms.option.edit.%name%'
        ]);      

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => OptionGroup::class
        ));
    }

    


}
