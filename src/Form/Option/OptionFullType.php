<?php

namespace Score\CmsBundle\Form\Option;

use Score\CmsBundle\Entity\Option;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class OptionFullType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code', null, [
                'label_format' => 'score.cms.option.edit.%name%'
            ])
            ->add('name', null, [
                'label_format' => 'score.cms.option.edit.%name%'
            ])
            ->add('value', null, [
                'label_format' => 'score.cms.option.edit.%name%'
                ])
            ->add('visibility', null, [
                'label_format' => 'score.cms.option.edit.%name%'
            ])
            ->add('params', TextType::class, [
                'label_format' => 'score.cms.option.edit.editor',
                'required' => false,
                'attr' => ["placeholder" => "cke5_basic"]
            ]);

        $builder->get('params')
            ->addModelTransformer(new CallbackTransformer(
                function ($params) {
                    // zistit co zobrazi vo formulari
                    $p = json_decode($params, true);
                    if ($p && array_key_exists("editor", $p)){
                        return $p["editor"];
                    }
                    return "";
                },
                function ($editor) {
                    // toto sa ulozi do databazy po spracovani formularu
                    return json_encode(["editor" =>  $editor]);
                }
            ))
        ;

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Option::class
        ));
    }

    


}
