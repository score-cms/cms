<?php

namespace Score\CmsBundle\Form\Option;

use Score\CmsBundle\Entity\Option;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class OptionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code', null, [
                'label_format' => 'score.cms.option.edit.%name%',
                'attr' => [ 'disabled' => true ]
            ])
            ->add('name', null, [
                'label_format' => 'score.cms.option.edit.%name%'
            ])
            ->add('value', null, [
                'label_format' => 'score.cms.option.edit.%name%'
            ]); 

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $event->getForm()->remove('code');
        });

       
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Option::class
        ));
    }

    


}
