<?php

namespace Score\CmsBundle\Form;

use Score\CmsBundle\Entity\Widget\Widget;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WidgetType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'label_format' => 'score.cms.widget.edit.%name%'
            ])
            ->add('code', null, [
                'label_format' => 'score.cms.widget.edit.%name%'
            ])
            ->add('pattern', null, [
                'label_format' => 'score.cms.widget.edit.%name%',
                'attr' => ['rows' => 20]
            ])
            ->add('template', null, [
                'label_format' => 'score.cms.widget.edit.%name%',
                'attr' => ['rows' => 20]
            ])
            ->add('visibility', null, [
                'label_format' => 'score.cms.widget.edit.%name%'
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Widget::class
        ));
    }

    


}
