<?php

namespace Score\CmsBundle\Form\Page;

use Doctrine\ORM\EntityRepository;
use Score\CmsBundle\Entity\Metatag\MetatagPage;
use Score\CmsBundle\Entity\Multisite\Site;
use Score\CmsBundle\Entity\Page\Page;
use Score\CmsBundle\Form\Metatag\MetatagPageType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PageForm extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('name', null, [
                'label' => 'score.page.edit.name'
            ])
            ->add('title', null, [
                'label' => 'score.page.edit.title'
            ])
            ->add('seo_id', null, [
                'label' => 'score.page.edit.seoId'
            ])
            ->add('menuName', null, [
                'label_format' => 'score.page.edit.%name%'
            ])
            ->add('status', ChoiceType::class, [
                'label' => 'score.page.edit.status.label', 'choices' => [
                    'score.page.edit.empty' => '',
                    'score.page.edit.status.public' => 'public',
                    'score.page.edit.status.hidden' => 'hidden'
                ]
            ])
            ->add('layout', ChoiceType::class, [
                'label' => 'score.page.edit.layout.label', 'choices' => [
                    'score.page.edit.empty' => '',
                    'score.page.edit.layout.MLCR' => 'menu-left-center-right',
                    'score.page.edit.layout.MLC' => 'menu-left-center',
                    'score.page.edit.layout.MC' => 'menu-center',
                    'score.page.edit.layout.MCR' => 'menu-center-right',
                ]
            ])
            ->add('icon', FileType::class, [
                'data_class' => null,
                'required' => false,
                'help' => 'score.cms.document.edit.iconHelp',
                'label' => 'score.page.edit.icon'
            ])
            ->add('removeIcon', HiddenType::class, [
                'mapped' => false,
                "data" => "false"
            ])
            ->add('teaser', TextareaType::class, [
                'label' => 'score.page.edit.teaser',
                'required' => false
            ])
            ->add('content', TextareaType::class, [
                'label' => 'score.page.edit.content', 'required' => false
            ])
            ->add('multisite', EntityType::class, [
                'class' => Site::class,
                'multiple' => true,
                'expanded' => true,
                'choice_label' => 'name',
                'label' => 'score.page.edit.multisite',
                'required' => false
            ])
            ->add('metatags', CollectionType::class, [
                'entry_type' => MetatagPageType::class,
                'entry_options' => [
                    'label' => false,
                    'attr' => [
                        "class" => "item-wrapper"
                    ]
                ],
                'prototype_data' => new MetatagPage(),
                'allow_delete' => true,
                'allow_add' => true,
                'label' => false,
                'by_reference' => false,
            ])
//            ->add('redirectToPage', null, [
//                    'label_format' => 'score.page.edit.%name%',
//                    'placeholder' => '–– Bez presmerovania ––',
//                    'choices' => [],

//                'query_builder' => function (EntityRepository $er) {
//                    return $er->createQueryBuilder('t')
//                        ->where('t.visibility = :v')
//                        ->andWhere('t.isDeleted = 0 OR t.isDeleted IS NULL')
//                        ->andWhere('t.redirectToPage IS NULL')
//                        ->andWhere('t.name != :notName')
//                        ->setParameter('v', true)
//                        ->setParameter('notName', "root")
//                        ->orderBy('t.createdAt', 'DESC')
//                        ->setMaxResults(10)
//                        ;
//                },
//            ])
            ->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
                $data = $event->getData();
                $form = $event->getForm();
                if (!$data) {
                    return;
                }
                $p = $data->getRedirectToPage();
                $form->add('redirectToPage', null, [
                    'label_format' => 'score.page.edit.%name%',
                    'placeholder' => '–– Bez presmerovania ––',
                    'choice_value' => function (?Page $entity) {
                        return $entity ? $entity->getId() : '';
                    },
                    'query_builder' => function (EntityRepository $er) use ($p) {
                        return $er->createQueryBuilder('t')
                            ->where('t.id = :v')
                            ->setParameter('v', $p ? $p->getId() : 0);
                    },
                ]);
            })
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $data = $event->getData();
                $form = $event->getForm();
                if (!$data) {
                    return;
                }
                $form->add('redirectToPage', null, [
                    'choice_value' => function (?Page $entity) {
                        return $entity ? $entity->getId() : '';
                    },
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('t')
                            ->where('t.visibility = :v')
                            ->andWhere('t.isDeleted = 0 OR t.isDeleted IS NULL')
                            ->andWhere('t.redirectToPage IS NULL')
                            ->andWhere('t.name != :notName')
                            ->setParameter('v', true)
                            ->setParameter('notName', "root")
                            ->orderBy('t.createdAt', 'DESC')//                            ->setMaxResults(10)
                            ;
                    },
                ]);
            });
    }


    public function configureOptions(OptionsResolver $resolver) {
        parent::configureOptions($resolver); // TODO: Change the autogenerated stub
    }

    public function getName() {
        return 'page';
    }
}
