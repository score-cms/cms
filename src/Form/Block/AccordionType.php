<?php

namespace Score\CmsBundle\Form\Block;

use Score\CmsBundle\Entity\Block\Accordion;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class AccordionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', null, [
                'label_format' => 'score.cms.accordion.edit.%name%',
                'required' => true,
            ])
            ->add('title', null, [
                'label_format' => 'score.cms.accordion.edit.%name%',
                'required' => false,
            ])
            ->add('type', ChoiceType::class, [
                'label_format' => 'score.cms.accordion.edit.%name%',
                'required' => false,
                "choices" => [
                    "Akordeón typ 1" => "typ1",
                    "Akordeón typ 2" => "typ2",
                    "Akordeón typ 3" => "typ3",
                    "Akordeón typ 4" => "typ4",
                    "Akordeón typ 5" => "typ5",
                ]
            ])
            ->add('description', null, [
                'label_format' => 'score.cms.accordion.edit.%name%',
                'required' => false,
            ])
            ->add('visibility', null, [
                'label_format' => 'score.cms.accordion.edit.%name%',
                'required' => false,
            ])
            ->add('items', CollectionType::class, [
                'entry_type' => AccordionItemType::class,
                'entry_options' => [
                    "allow_open" => $options['allow_open'],
                    "allow_summary" => $options['allow_summary'],        
                ],
                'prototype' => true,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                //'required' => false,
                'label' => false,
                //'label_format' => 'score.cms.accordion.edit.indexLabel.%name%'
                'label_format' => 'score.cms.accordion.edit.itemLabel'
            ])
        ;
        //var_dump($options);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Accordion::class,
            'allow_summary' => null,
            'allow_open' => null,
]);
    }
}
