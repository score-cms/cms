<?php

namespace Score\CmsBundle\Form\Block;

use Score\CmsBundle\Entity\Block\AccordionItem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;

class AccordionItemType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options): void {
        $builder->add('title', null, [
            'required' => true,
            'label_format' => 'score.cms.accordion.edit.%name%'
        ]);

        if ($options['allow_summary'] === true) {
            $builder->add('summary', TextareaType::class, [
                'label_format' => 'score.cms.accordion.edit.%name%',
                'required' => false,
            ]);
        }

        $builder->add('content', TextareaType::class, [
            'required' => true,
            'label_format' => 'score.cms.accordion.edit.%name%',
            'attr' => ["rows" => 4],
            'constraints' => [new Length([
                'max' => 2700,
                'normalizer' => 'strip_tags'
            ])],

        ]);

        if ($options['allow_open'] === true) {
            $builder->add('isOpened', null, [
                'label_format' => 'score.cms.accordion.edit.%name%',
                'required' => false,
            ]);
        }

        $builder->add('sortOrder', HiddenType::class)

            // ->add('visibility')
            // ->add('createdAt')
            // ->add('editedAt')
            // ->add('accordion')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void {
        $resolver->setDefaults([
            'data_class' => AccordionItem::class,
            'allow_summary' => null,
            'allow_open' => null,
        ]);
    }
}
