<?php

namespace Score\CmsBundle\Form\Block;

use Score\CmsBundle\Entity\Block\Block;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\CallbackTransformer;

//use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FullBlockType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, ['label' => 'score.cms.block.edit.name'])
            ->add('lang', null, ['label' => 'score.cms.block.edit.lang'])
            ->add('module', null, ['label' => 'score.cms.block.edit.module'])
            ->add('action', null, ['label' => 'score.cms.block.edit.action'])
            ->add('params', null, ['label' => 'score.cms.block.edit.params'])
            ->add('type', null, ['label' => 'score.cms.block.edit.type'])
            ->add('content', null, ['label' => 'score.cms.block.edit.content'])
            ->add('visibility', null, ['label' => 'score.cms.block.edit.visibility'])
            ;

        //     $builder->get('params')
        //     ->addModelTransformer(new CallbackTransformer(
        //         function ($paramsAsArray) {
        //             // transform the array to a string
        //             //return implode(', ', $paramsAsArray);
        //             return json_encode($paramsAsArray);
        //         },
        //         function ($paramsAsString) {
        //             // transform the string back to an array
        //             //return explode(', ', $paramsAsString);
        //             return json_decode($paramsAsString, true);
        //         }
        //     ))
        // ;
           
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Block::class,
        ));
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'score_block';
    }
}
