<?php

namespace Score\CmsBundle\Form\Block;

use Score\CmsBundle\Entity\Block\Block;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

//use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BlockType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, ['label' => 'score.cms.block.edit.name'])
            ->add('content', null, ['label' => 'score.cms.block.edit.content'])
            ->add('type', ChoiceType::class,
                [
                    'label' => 'score.cms.block.edit.type',
                    'choices' => $options['groups']
                ]
            )
            ->add('visibility', CheckboxType::class, [
                'label' => 'score.cms.block.edit.visibility', 
                'required' => false
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Block::class,
            'groups' => []
        ));
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'score_block';
    }
}
