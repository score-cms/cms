<?php

namespace Score\CmsBundle\Form\Document;

use Score\CmsBundle\Entity\Document\DocumentFile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DocumentFileType extends AbstractType {

    private $fileUploadLimit;

    public function __construct(string $fileUploadLimit) {
        $this->fileUploadLimit = $fileUploadLimit;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void {
        $builder
            ->add('isExternal', null, [
                'label_format' => 'score.cms.document.edit.%name%',
                'required' => false,
            ])
            ->add('url', null, [
                'label_format' => 'score.cms.document.edit.%name%',
                'required' => false,
            ])
            // ->add('filename', null, [
            //     'label_format' => 'score.cms.document.edit.%name%'
            // ])
            // ->add('size', null, [
            //     'label_format' => 'score.cms.document.edit.%name%'
            // ])
            // ->add('type', null, [
            //     'label_format' => 'score.cms.document.edit.%name%'
            // ])
            ->add('language', ChoiceType::class, [
                'label_format' => 'score.cms.document.edit.%name%',
                'choices' => [
                    '' => '',
                    'score.cms.document.edit.lang.sk' => 'SK',
                    'score.cms.document.edit.lang.en' => 'EN',
                    'score.cms.document.edit.lang.cz' => 'CS',
                    'score.cms.document.edit.lang.d' => 'D',
                    'score.cms.document.edit.lang.fr' => 'FR',
                ],
                'required' => false,
            ])
            ->add('info', null, [
                'label_format' => 'score.cms.document.edit.%name%',
                'help' => 'score.cms.document.edit.infoHelp',
                'required' => false,
            ])
            ->add('isCurrent', null, [
                'label_format' => 'score.cms.document.edit.%name%',
                'required' => false,
            ])
            // ->add('visibility', null, [
            //     'label_format' => 'score.cms.document.edit.%name%'
            // ])
            // ->add('createdAt', null, [
            //     'label_format' => 'score.cms.document.edit.%name%'
            // ])
            // ->add('editedAt', null, [
            //     'label_format' => 'score.cms.document.edit.%name%'
            // ])
            // ->add('lastCheckAt', null, [
            //     'label_format' => 'score.cms.document.edit.%name%'
            // ])
            // ->add('nextCheckAt', null, [
            //     'label_format' => 'score.cms.document.edit.%name%'
            // ])
            // ->add('document', null, [
            //     'label_format' => 'score.cms.document.edit.%name%'
            // ])
            ->add('file', FileType::class, [
                'label_format' => 'score.cms.document.edit.%name%',
                'mapped' => false,
                'required' => false,
                'help' => 'Momentálne je na nahrávanie dokumentov nastavený limit ' . $this->fileUploadLimit . ' MB.',
//                'help_attr' => [
//                    'style' => 'color: red;'
//                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void {
        $resolver->setDefaults([
            'data_class' => DocumentFile::class,
        ]);
    }
}
