<?php

namespace Score\CmsBundle\Form\Document;

use Score\CmsBundle\Entity\Document\DocumentDescription;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DocumentDescriptionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('documentDomain', null, [
                'label_format' => 'score.cms.document.edit.%name%',
                "required" => true
            ])
            ->add('description', null, [
                'label' => 'score.cms.document.edit.descDescription',
                'attr' => ['rows' => 5]
            ])
            ->add('public', null, [
                'label' => 'score.cms.document.edit.descPublic',
            ])
            // ->add('visibility', null, [
            //     'label_format' => 'score.cms.document.edit.%name%'
            // ])
            // ->add('createdAt', null, [
            //     'label_format' => 'score.cms.document.edit.%name%'
            // ])
            // ->add('editedAt', null, [
            //     'label_format' => 'score.cms.document.edit.%name%'
            // ])
            // ->add('lastCheckAt', null, [
            //     'label_format' => 'score.cms.document.edit.%name%'
            // ])
            // ->add('nextCheckAt', null, [
            //     'label_format' => 'score.cms.document.edit.%name%'
            // ])
            // ->add('document', null, [
            //     'label_format' => 'score.cms.document.edit.%name%'
            // ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => DocumentDescription::class,
        ]);
    }
}
