<?php

namespace Score\CmsBundle\Form\Document;

use Doctrine\ORM\EntityRepository;
use Score\CmsBundle\Entity\Block\Block;
use Score\CmsBundle\Entity\Document\Document;
use Score\CmsBundle\Entity\Document\DocumentCategory;
use Score\CmsBundle\Entity\Document\DocumentFile;
use Score\CmsBundle\Entity\Metatag\MetatagDocument;
use Score\CmsBundle\Entity\Page\Page;
use Score\CmsBundle\Form\Metatag\MetatagDocumentType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class DocumentType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options): void {

        // $id = $builder->getData()->getId();
        $years = range(Date('Y'), 1960);
        $builder
            ->add('name', null, [
                'label_format' => 'score.cms.document.edit.%name%'
            ])
            ->add('fullName', null, [
                'label_format' => 'score.cms.document.edit.%name%'
            ])
            ->add('teaser', TextareaType::class, [
                'required' => false,
                'label_format' => 'score.cms.document.edit.%name%'
            ])
            ->add('description', null, [
                'label_format' => 'score.cms.document.edit.%name%'
            ])
            ->add('iconFile', FileType::class, [
                'data_class' => null,
                'required' => false,
                'mapped' => false,
                'help' => 'score.cms.document.edit.iconHelp',
                'label_format' => 'score.cms.document.edit.%name%'
            ])
            ->add('removeIcon', HiddenType::class, [
                'mapped' => false,
                "data" => "false"
            ])
            ->add('hasDetail', null, [
                'label_format' => 'score.cms.document.edit.%name%'
            ])
            ->add('public', null, [
                'label_format' => 'score.cms.document.edit.%name%'
            ])
            // ->add('search', null, [
            //     'label_format' => 'score.cms.document.edit.%name%'
            // ])
            // ->add('version', null, [
            //     'label_format' => 'score.cms.document.edit.%name%'
            // ])
            // ->add('code', null, [
            //     'label_format' => 'score.cms.document.edit.%name%'
            // ])
            ->add('seoId', null, [
                'label_format' => 'score.cms.document.edit.%name%'
            ])
            ->add('year', ChoiceType::class, [
                'label_format' => 'score.cms.document.edit.%name%',
                'required' => false,
                'choices' => array_combine($years, $years),
            ])
//            ->add('documentLevels', null, [
//                'label_format' => 'score.cms.document.edit.%name%'
//            ])
            ->add('documentLevels', null, [
                'label_format' => 'score.cms.document.edit.%name%',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->andWhere('c.visibility = :v')
                        ->setParameter('v', true)
                        ->orderBy('c.seoName', 'ASC');
                },
                'multiple' => true,
//                'expanded' => true,
            ])
            // ->add('visibility', null, [
            //     'label_format' => 'score.cms.document.edit.%name%'
            // ])
            // ->add('createdAt', null, [
            //     'label_format' => 'score.cms.document.edit.%name%'
            // ])
            // ->add('editedAt', null, [
            //     'label_format' => 'score.cms.document.edit.%name%'
            // ])
            // ->add('lastCheckAt', null, [
            //     'label_format' => 'score.cms.document.edit.%name%'
            // ])
            // ->add('nextCheckAt', null, [
            //     'label_format' => 'score.cms.document.edit.%name%'
            // ])
            ->add('documentCategories', null, [
                'label_format' => 'score.cms.document.edit.%name%',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->andWhere('c.visibility = :v')
                        ->setParameter('v', true)
                        ->orderBy('c.seoName', 'ASC');
                },
                'multiple' => true,
//                'expanded' => true,
            ])
            // ->add('documentCategories', null, [
            //     'label_format' => 'score.cms.document.edit.%name%',
            //     'row_attr' => ['class' => 'dropdown'],
            //     'multiple' => true,
            //     'expanded' => true,
            //     'attr' => [
            //         'class' => 'dropdown-menu',
            //         'aria-labelledby' => 'some_same_id',
            //         'style' => 'padding:1em;'
            //     ],
            //     'label_attr' => [
            //         'class' => 'btn btn-info dropdown-toggle',
            //         'type' => "button",
            //         'data-toggle' => "dropdown",
            //         'aria-haspopup' => "true",
            //         'aria-expanded' => "false",
            //         'id' => 'some_same_id',
            //     ],

            // ])
            ->add('documentFiles', CollectionType::class, [
                'entry_type' => DocumentFileType::class,
                'entry_options' => [
                    'label' => false,
                    'attr' => [
                        "style" => "border: 1px solid lightgrey; padding: 1em;",
                        "class" => "item-wrapper"
                    ]
                ],
                'prototype_data' => (new DocumentFile())->setIsCurrent(true),
                'allow_delete' => true,
                'allow_add' => true,
                'label' => false,
                // 'mapped' => false
            ])
            ->add('documentDescriptions', CollectionType::class, [
                'entry_type' => DocumentDescriptionType::class,
                'entry_options' => [
                    'label' => false,
                    'attr' => [
                        "style" => "border: 1px solid lightgrey; padding: 1em;",
                        "class" => "item-wrapper"
                    ]
                ],
                'allow_delete' => true,
                'allow_add' => true,
                'label' => false,
            ])
            // ->add('relevantsWithMe', null, [
            //     'label_format' => 'score.cms.document.edit.%name%'
            // ])
//            ->add('myRelevants', null, [
//                'label_format' => 'score.cms.document.edit.%name%',
//                'query_builder' => function (EntityRepository $er) use ($id) {
//                    return $er->createQueryBuilder('c')
//                        ->andWhere('c.visibility = :v')
//                        ->andWhere('c.isDeleted = 0 OR c.isDeleted IS NULL')
//                        ->andWhere('c.id != :id')
//                        ->setParameter('v', true)
//                        ->setParameter('id', $id)
//                        ->orderBy('c.id', 'DESC');
//                },
//                'multiple' => true,
////                'expanded' => true,
//            ])
            ->add('documentBlocks', EntityType::class, [
                'placeholder' => '',
                'label' => 'score.page.edit.block.add',
                'class' => Block::class,
                'choices' => [],
//                'query_builder' => function (EntityRepository $er) {
//                    return $er->getBlocksListToForm();
//                },
//                'choice_label' => function ($block) {
////                    return $block->getName() . " – " . $block->getType();
//                    return $block->getName();
//                },
                'mapped' => false,
                'required' => false,
            ])
            ->add('metatags', CollectionType::class, [
                'entry_type' => MetatagDocumentType::class,
                'entry_options' => [
                    'label' => false,
                    'attr' => [
                        "class" => "item-wrapper"
                    ]
                ],
                'prototype_data' => new MetatagDocument(),
                'allow_delete' => true,
                'allow_add' => true,
                'label' => false,
                'by_reference' => false,
            ])
            ->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
                $data = $event->getData();
                $form = $event->getForm();
                if (!$data) {
                    return;
                }
                $ids = $data->getMyRelevants()->map(function (Document $d) {
                    return $d->getId();
                })->toArray();
                $form->add('myRelevants', null, [
                    'label_format' => 'score.cms.document.edit.%name%',
                    'query_builder' => function (EntityRepository $er) use ($ids) {
                        return $er->createQueryBuilder('c')
                            ->andWhere('c.visibility = :v')
                            ->andWhere('c.isDeleted = 0 OR c.isDeleted IS NULL')
                            ->andWhere('c.id IN(:ids)')
                            ->setParameter('v', true)
                            ->setParameter('ids', $ids)
                            ->orderBy('c.name', 'ASC');
                    },
                    'multiple' => true,
//                'expanded' => true,
                ]);
            })
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $data = $event->getData();
                $form = $event->getForm();
                if (!$data) {
                    return;
                }
//                $id = $data->getId();
                $form->add('myRelevants', null, [
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('c')
                            ->andWhere('c.visibility = :v')
                            ->andWhere('c.isDeleted = 0 OR c.isDeleted IS NULL')
                            ->setParameter('v', true)
                        ;
                    },
                    'multiple' => true,
//                'expanded' => true,
                ]);
            });
    }

    public function configureOptions(OptionsResolver $resolver): void {
        $resolver->setDefaults([
            'data_class' => Document::class,
        ]);
    }
}
