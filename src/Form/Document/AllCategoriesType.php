<?php

namespace Score\CmsBundle\Form\Document;


use Score\CmsBundle\Entity\Document\AllCategories;
use Score\CmsBundle\Entity\Document\DocumentCategory;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AllCategoriesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $builder->add('categories', CollectionType::class, [
            'entry_type' => DocumentCategoryType::class,
            'entry_options' => [
                'label' => false, 
                'attr' => [
                    //"style" => "display:flex; align-items:center;",
                    "class" => "item-wrapper"
                    ]
            ],
            'prototype_data' => new DocumentCategory(),
            'allow_delete' => true,
            'allow_add' => true,
            'label' => false,
            //'attr' => ["style" => "display:flex;"]
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AllCategories::class,
        ]);
    }
}