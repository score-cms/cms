<?php

namespace Score\CmsBundle\Form\Metatag;

use Score\CmsBundle\Entity\Metatag\MetatagArticle;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MetatagArticleType extends MetatagType {

    public function configureOptions(OptionsResolver $resolver): void {
        $resolver->setDefaults([
            'data_class' => MetatagArticle::class,
        ]);
    }
}
