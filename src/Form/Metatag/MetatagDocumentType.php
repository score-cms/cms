<?php

namespace Score\CmsBundle\Form\Metatag;

use Score\CmsBundle\Entity\Metatag\MetatagDocument;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MetatagDocumentType extends MetatagType {

    public function configureOptions(OptionsResolver $resolver): void {
        $resolver->setDefaults([
            'data_class' => MetatagDocument::class,
        ]);
    }
}
