<?php

namespace Score\CmsBundle\Form\Metatag;

use Score\CmsBundle\Entity\Metatag\MetatagEvent;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MetatagEventType extends MetatagType {
    public function configureOptions(OptionsResolver $resolver): void {
        $resolver->setDefaults([
            'data_class' => MetatagEvent::class,
        ]);
    }
}
