<?php

namespace Score\CmsBundle\Form\Metatag;

use Score\CmsBundle\Entity\Metatag\MetatagPage;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MetatagPageType extends MetatagType {

    public function configureOptions(OptionsResolver $resolver): void {
        $resolver->setDefaults([
            'data_class' => MetatagPage::class,
        ]);
    }
}
