<?php

namespace Score\CmsBundle\Form\Metatag;

use Score\CmsBundle\Entity\Metatag\MetatagPattern;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MetatagPatternType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options): void {
        $builder
            ->add('name', null, [
                'label_format' => 'score.cms.metatag.%name%',
            ])
            ->add('type', null, [
                'label_format' => 'score.cms.metatag.%name%',
            ])
            ->add('group', null, [
                'label_format' => 'score.cms.metatag.%name%'
            ])
            ->add('sortOrder', null, [
                'label_format' => 'score.cms.metatag.%name%'
            ])
            ->add('visibility', null, [
                'label_format' => 'score.cms.metatag.%name%',
                'row_attr' => ['class' => "visibility-row"],
            ])
//            ->add('helpText', null, [
//                'label_format' => 'score.cms.metatag.%name%',
//                'row_attr' => ['class' => "helpText-row"]
//            ])
            ->add('pattern', TextareaType::class, [
                'label_format' => 'score.cms.metatag.%name%',
                'row_attr' => ['class' => "pattern-row"],
                'help' => 'Html kod do ktoreho sa namiesto %s vykresli hodnota. Napr: <meta name="author" content="%s" />',

            ])
            // ->add('createdAt', null, [
            //     'label_format' => 'score.cms.metatag.%name%'
            // ])
            // ->add('editedAt', null, [
            //     'label_format' => 'score.cms.metatag.%name%'
            // ])
            // ->add('lastCheckAt', null, [
            //     'label_format' => 'score.cms.metatag.%name%'
            // ])
            // ->add('nextCheckAt', null, [
            //     'label_format' => 'score.cms.metatag.%name%'
            // ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void {
        $resolver->setDefaults([
            'data_class' => MetatagPattern::class,
        ]);
    }
}
