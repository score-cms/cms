<?php

namespace Score\CmsBundle\Form\Metatag;


use Score\CmsBundle\Entity\Metatag\AllMetatagPatterns;
use Score\CmsBundle\Entity\Metatag\MetatagPattern;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AllMetatagPatternsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $builder->add('metatagPatterns', CollectionType::class, [
            'entry_type' => MetatagPatternType::class,
            'entry_options' => [
                'label' => false,
                'attr' => [
                    "class" => "item-wrapper"
                    ]
            ],
            'prototype_data' => new MetatagPattern(),
            'allow_delete' => true,
            'allow_add' => true,
            'label' => false,
            //'attr' => ["style" => "display:flex;"]
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AllMetatagPatterns::class,
        ]);
    }
}