<?php

namespace Score\CmsBundle\Form;

use Score\CmsBundle\Entity\Group;
use Score\CmsBundle\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class UserType extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // $allRoles = [];
        // foreach ($options["groups"] as $group) {
        //     $allRoles[$group->getName()] = $group->getId();
        // }
        $builder
            ->add('username', null, [
                'label_format' => 'score.cms.user.edit.%name%'
            ])
            ->add('email', null, [
                'label_format' => 'score.cms.user.edit.%name%'
            ])
            // ->add('newroles', ChoiceType::class, [
            //     'choices' => $allRoles,
            //     'multiple' => true,
            //     'expanded' => true,
            //     'label' => 'Prava',
            //     'mapped' => false
                
            // ])
            ->add('groups', null, [
                'label_format' => 'score.cms.user.edit.%name%',
                'multiple' => true,
                'expanded' => true,
                'attr' => [
                    'class' => 'dropdown-menu',
                    'aria-labelledby' => 'some_same_id',
                    'style' => 'padding:1em;'
                ],
                'label_attr' => [
                    'class' => 'btn btn-info dropdown-toggle',
                    'type' => "button",
                    'data-toggle' => "dropdown",
                    'aria-haspopup' => "true",
                    'aria-expanded' => "false",
                    'id' => 'some_same_id',
                ],

            ])
            ->add('isActive', CheckboxType::class, [
                'required' => false,
                'label_format' => 'score.cms.user.edit.%name%'
            ]);
    }


    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'groups' => [],
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'score_cmsbundle_user';
    }
}
