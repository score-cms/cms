<?php

namespace Score\CmsBundle\Form;

use Score\CmsBundle\Entity\Widget\Widget;
use Score\CmsBundle\Entity\Widget\WidgetValue;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WidgetValueType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'label_format' => 'score.cms.widget.edit.%name%',
                'required' => true,
            ])
            ->add('code', null, [
                'label_format' => 'score.cms.widget.edit.%name%',
                'required' => true,
            ])
            ->add('note', TextareaType::class, [
                'label_format' => 'score.cms.widget.edit.%name%',
                'required' => false,
            ])
            ->add('data', null, [
                'label_format' => 'score.cms.widget.edit.%name%',
                'required' => false,
            ])
            ->add('visibility', null, [
                'label_format' => 'score.cms.widget.edit.%name%',
                'required' => false,
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => WidgetValue::class
        ));
    }

    


}
