<?php

namespace Score\CmsBundle\Form\Event;

use Doctrine\ORM\EntityRepository;
use Score\CmsBundle\Entity\Block\Block;
use Score\CmsBundle\Entity\Event\Event;
use Score\CmsBundle\Entity\Metatag\MetatagEvent;
use Score\CmsBundle\Entity\Multisite\Site;
use Score\CmsBundle\Form\Metatag\MetatagEventType;
use Score\CmsBundle\Form\Metatag\MetatagType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class EventType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options): void {
        $builder
            ->add('name', null, [
                'label' => 'score.event.form.name'
            ])
            ->add('slug', null, [
                'label' => 'score.event.form.slug'
            ])
            ->add('content', TextareaType::class, [
                'label' => 'score.event.form.body', 'required' => false
            ])
            ->add('teaser', TextareaType::class, [
                'label' => 'score.event.form.teaser',
                'required' => false
            ])
            ->add('date_from', DateType::class, [
                "widget" => 'single_text',
                'label' => 'score.event.form.date_from',
                'required' => false
            ])
            ->add('date_to', DateType::class, [
                "widget" => 'single_text',
                'label' => 'score.event.form.date_to',
                'required' => false
            ])
            ->add('url_link', null, [
                'label' => 'score.event.form.url_link'
            ])
            ->add('location', null, [
                'label' => 'score.event.form.location'
            ])
            ->add('organizer', null, [
                'label' => 'score.event.form.organizer'
            ])
            ->add('phone', null, [
                'label' => 'score.event.form.phone'
            ])
            ->add('email', null, [
                'label' => 'score.event.form.email'
            ])
            ->add('published', ChoiceType::class, [
                'choices' => [
                    'score.event.form.empty' => '',
                    'score.event.form.published.yes' => '1',
                    'score.event.form.published.no' => '2'
                ],
                'label' => 'score.event.form.published'
            ])
            ->add('contact_person', null, [
                'label' => 'score.event.form.contact_person'
            ])
            ->add('icon', FileType::class, [
                'data_class' => null,
                'required' => false,
                'help' => 'score.cms.document.edit.iconHelp',
                'label' => 'score.event.form.icon'
            ])
            ->add('removeIcon', HiddenType::class, [
                'mapped' => false,
                "data" => "false"
            ])
//            ->add('category')
            ->add('eventCategories', null, [
                'label_format' => 'score.event.form.%name%',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->where('t.visibility = :v')
                        ->setParameter('v', true)
                        ->orderBy('t.seoName', 'ASC');
                },
                'multiple' => true,
                'expanded' => true,
            ])

            //->add('editedAt')
            ->add('multisite', EntityType::class, [
                'class' => Site::class,
                'multiple' => true,
                'expanded' => true,
                'choice_label' => 'name',
                'label' => 'score.event.form.multisite',
                'required' => false
            ])
            ->add('eventBlocks', EntityType::class, [
                'placeholder' => '',
                'label' => 'score.page.edit.block.add',
                'class' => Block::class,
                'choices' => [],
//                'query_builder' => function (EntityRepository $er) {
//                    return $er->getBlocksListToForm();
//                },
//                'choice_label' => function ($block) {
////                    return $block->getName() . " – " . $block->getType();
//                    return $block->getName();
//                },
                'mapped' => false,
                'required' => false,
            ])
            ->add('metatags', CollectionType::class, [
                'entry_type' => MetatagEventType::class,
                'entry_options' => [
                    'label' => false,
                    'attr' => [
                        "class" => "item-wrapper"
                    ]
                ],
                'prototype_data' => new MetatagEvent(),
                'allow_delete' => true,
                'allow_add' => true,
                'label' => false,
                'by_reference' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void {
        $resolver->setDefaults([
            'data_class' => Event::class,
        ]);
    }
}
