<?php

namespace Score\CmsBundle\Form;

use Score\CmsBundle\Entity\Group;
use Score\CmsBundle\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class UserPassType extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
//            ->add('oldpass', PasswordType::class , [
//                'label' => "Stare Heslo",
//                'mapped' => false,
//                'required' => false,
//            ])
            ->add('newpass1', PasswordType::class, [
                'label' => "Nove Heslo",
                'mapped' => false
            ])
            ->add('newpass2', PasswordType::class, [
                'label' => "Nove Heslo Znovu",
                'mapped' => false
            ]);
    }


    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'score_cmsbundle_user';
    }
}
