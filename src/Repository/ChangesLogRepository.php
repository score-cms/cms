<?php

namespace Score\CmsBundle\Repository;

use DateTime;
use Score\CmsBundle\Entity\ChangesLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ChangesLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method ChangesLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method ChangesLog[]    findAll()
 * @method ChangesLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChangesLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ChangesLog::class);
    }

    public function getDistinctNames()
    {
        $result = $this->createQueryBuilder('l')
            ->select("l.entity")
            ->groupBy("l.entity")
            ->orderBy('l.entity', 'ASC')
            ->getQuery()
            ->getResult();

        $names = array_column($result, 'entity');

        return $names; 

    }

    public function getOneEntityList($className, $showAll = false)
    {
        $start = new DateTime($showAll ? "1991" : "-1 week");
        $result = $this->createQueryBuilder('l')
            ->select("l")
            ->where('l.entity = :cn')
            ->andWhere('l.editedAt >= :start')
            ->setParameter('cn', $className)
            ->setParameter('start',$start)                      

           // ->groupBy("l.identifier")
            ->orderBy('l.editedAt', 'DESC')
            ->getQuery()
            ->getResult();

        //$names = array_column($result, 'entity');

        return $result; 
        
    }


}
