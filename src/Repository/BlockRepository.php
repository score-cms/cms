<?php

namespace Score\CmsBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Score\CmsBundle\Entity\Block\Block;

class BlockRepository extends EntityRepository {
    public function loadBlocksList() {
        return $this->createQueryBuilder('b')
            ->andWhere('b.type != :nt')
            ->andWhere('b.isDeleted IS NULL OR b.isDeleted != :true') // pretoze null to aj tak nebralo
            ->setParameter('nt', "pageContent")
            ->setParameter('true', true)
            ->orderBy('b.editedAt', 'DESC')
            ->getQuery()
//            ->setMaxResults(90)
            ->getResult();
    }

    public function loadAvailableBlocksList() {
        return $this->getEntityManager()->createQuery('
                SELECT b.id AS value, b.name, b.type
                FROM ' . Block::class . ' b
                WHERE b.type != :nt AND (b.isDeleted != 1 OR b.isDeleted IS NULL)
                ORDER BY b.editedAt DESC
            ')
            ->setParameters([
                'nt' => "pageContent",
            ])
            ->getResult();

    }

    public function loadStaticBlocksList() {
        return $this->createQueryBuilder('b')
            ->andWhere('b.type = :type')
            ->andWhere('b.isDeleted IS NULL OR b.isDeleted != :true') // pretoze null to aj tak nebralo
            ->setParameter('type', "static")
            ->setParameter('true', true)
            ->orderBy('b.editedAt', 'DESC')
            ->getQuery()
            ->getResult();
    }

//    public function getBlocksListToForm() {
//        return $this->createQueryBuilder('b')
//            ->where('b.visibility = :true')
//            ->andWhere('b.isDeleted IS NULL OR b.isDeleted != :true') // pretoze null to aj tak nebralo
//            ->andWhere('b.type != :nt')
//            ->setParameter('true', true)
//            ->setParameter('nt', "pageContent")
//            ->orderBy('b.editedAt', 'DESC');
//            //->getQuery();
//            //->getResult();
//    }

    public function getCount() {
        $qb = $this->createQueryBuilder('b');
        $qb->select('count(b.id)');
        $count = $qb->getQuery()->getSingleScalarResult();
        return $count;
    }
}
