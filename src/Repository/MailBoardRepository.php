<?php

namespace Score\CmsBundle\Repository;

use DateTime;
use Score\CmsBundle\Entity\MailBoard;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MailBoard|null find($id, $lockMode = null, $lockVersion = null)
 * @method MailBoard|null findOneBy(array $criteria, array $orderBy = null)
 * @method MailBoard[]    findAll()
 * @method MailBoard[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MailBoardRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MailBoard::class);
    }

    
    public function getMailsToSend($maxAttempts = null)
    {
        if ($maxAttempts === null) {
            $max = 10;
        } else {
            $max = min($maxAttempts, 38); // max 38
        }
        $start = new DateTime();
        $result = $this->createQueryBuilder('m')
            ->select("m")
            ->andwhere('m.attempts < :max')
            ->andWhere('m.isSent = :false')
            ->andWhere('m.firstTry IS NULL OR m.firstTry < :start')
            ->setParameter('max', $max)
            ->setParameter('false', false)
            ->setParameter('start',$start)                      

            ->getQuery()
            ->getResult();

        return $result; 
        
    }


}
