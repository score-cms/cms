<?php

namespace Score\CmsBundle\Repository;

use DateTime;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Score\CmsBundle\Entity\Article\Article;
use Score\CmsBundle\Entity\Article\ArticleCategory;
use Score\CmsBundle\Entity\User;

/**
 * EventRepository
 */
class ArticleRepository extends EntityRepository {

    public function findTopArticlesByCategory($category, $maxResult = 4) {

        $qd = $this->createQueryBuilder('a');
        $query = $qd->select('a')
            ->andWhere('a.published = 1')
            ->andWhere('a.isDeleted = 0')
            ->leftJoin("a.articleCategories", "t")
            ->andWhere('t.seoName = :category')
            ->add('orderBy', 'a.priority ASC, a.publishedFrom DESC')
            ->setParameter("category", $category)
            ->setFirstResult(0)
            ->setMaxResults($maxResult)
            ->getQuery();

        $items = $query->getResult();
        return $items;
    }

    public function findTopArticles($maxResult = 4) {

        $qd = $this->createQueryBuilder('o');
        $query = $qd->select('o')
            ->andWhere('o.published = 1')
            ->andWhere('o.isDeleted = 0 OR o.isDeleted IS NULL')
            ->add('orderBy', 'o.priority ASC, o.publishedFrom DESC')
            ->setFirstResult(0)
            ->setMaxResults($maxResult)
            ->getQuery();

        $items = $query->getResult();
        return $items;
    }


    public function findSidebarArticles($article, $maxResult = 4) {
        $qd = $this->createQueryBuilder('o');
        $query = $qd->select('o')
            ->andWhere('o.published = 1')
            ->andWhere('o.isDeleted = 0 OR o.isDeleted IS NULL')
            ->andWhere('o.id != :currentId')
            ->setParameter('currentId', intval($article->getId()))
            ->andWhere('(o.publishedFrom < :publishedFrom or o.publishedFrom is null) and (o.publishedTo > :publishedTo or o.publishedTo is null)')
            ->setParameter('publishedFrom', new \DateTime())
            ->setParameter('publishedTo', new \DateTime())
            ->add('orderBy', 'o.priority DESC, o.publishedFrom DESC')
            ->setFirstResult(0)
            ->setMaxResults($maxResult)
            ->getQuery();

        $items = $query->getResult();
        return $items;
    }

    public function findAllArticlesForUser(User $user, $checkAccess = false, $count = "all") {
        $builder = $this->createQueryBuilder('a');
        if ($checkAccess) {
            $builder->leftJoin("a.accesses", "acc")
                ->leftJoin("acc.user", "u")
                ->andWhere("u = :user")
                ->setParameter("user", $user);
        }
        $builder->andWhere('a.isDeleted = 0 OR a.isDeleted IS NULL')
            ->add('orderBy', 'a.publishedFrom DESC, a.priority DESC, a.name ASC');
        if ($count !== "all") {
            $builder->setMaxResults(intval($count));
        }
        return $builder->getQuery()->getResult();
    }

    public function findArticlesByPage($settings = ['page' => 1, 'pageSize' => 10], $criteria = []) {
        $qd = $this->createQueryBuilder('o');
        $qd->select('o');

        if (array_key_exists('visible', $criteria) and $criteria['visible'] == true) {
            $qd->andWhere('o.published = :published');
            $qd->setParameter('published', 1);

            $qd->andWhere('o.isDeleted = 0 OR o.isDeleted IS NULL');

            $qd->andWhere('(o.publishedFrom < :publishedFrom or o.publishedFrom is null) and (o.publishedTo > :publishedTo or o.publishedTo is null)');
            $qd->setParameter('publishedFrom', new \DateTime());
            $qd->setParameter('publishedTo', new \DateTime());
        }

        $qd->add('orderBy', 'o.priority DESC, o.publishedFrom DESC, o.name ASC');

        $query = $qd->getQuery();
        $pageSize = $settings['pageSize'];
        $page = $settings['page'];
        // load doctrine Paginator
        $paginator = new \Doctrine\ORM\Tools\Pagination\Paginator($query);

        // you can get total items
        $totalItems = count($paginator);

        // get total pages
        $pagesCount = ceil($totalItems / $pageSize);

        // now get one page's items:
        $paginator
            ->getQuery()
            ->setFirstResult($pageSize * ($page - 1)) // set the offset
            ->setMaxResults($pageSize); // set the limit
        $list = array();
        foreach ($paginator as $pageItem) {
            $list[] = $pageItem;
        }
        return ['list' => $list, 'total' => $totalItems, 'pagesCount' => $pagesCount];
    }


}
