<?php

namespace Score\CmsBundle\Repository;

use Doctrine\ORM\Tools\Pagination\Paginator as Paginator;

class EventDatagrid extends \Score\BaseBundle\Repository\Datagrid {

    public function getCols() {
  return array(
            '0' => 'a.id',
            '1' => 'a.name',
            '2' => 'a.published',
            '3' => 'a.organizer',
            '4' => 'a.dateFrom',
            '5' => 'a.dateTo'
        );
}

    public function buildDataAsArray($paginator)
    {
        $data = array(); 
        foreach ($paginator as $object)
        {
            $published = ($object->getPublished() == '1') ? '<i class="fa fa-check"></i>' : '<i class="fa fa-times"></i>';
            $publishedFrom  = ($object->getDateFrom() != null) ? $object->getDateFrom()->format('d.m.y') : '-';
            $publishedTo  = ($object->getDateTo() != null) ? $object->getDateTo()->format('d.m.y') : '-';
            
            $data[] = array(
                $object->getId(),
                '<a href="'.$this->getRouter()->generate('score_cms_event_edit',array('id' => $object->getId())).'">' . $object->getName() . '</a>',
                $published,
                $object->getOrganizer(),
                $publishedFrom,
                $publishedTo,
                '<a href="'.$this->getRouter()->generate('score_cms_event_delete',array('id' => $object->getId())).'" class="delete-event">Zmazať</a>'
            );
        }
        return $data;
    }


}
