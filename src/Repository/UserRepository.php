<?php

namespace Score\CmsBundle\Repository;

use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository implements UserLoaderInterface {
    public function loadUserByUsername($username) {
        return $this->createQueryBuilder('u')
            ->where('u.username = :username OR u.email = :email')
            ->andWhere('u.isActive = :isActive')
            ->setParameter('username', $username)
            ->setParameter('email', $username)
            ->setParameter('isActive', true)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getCount() {
        $qb = $this->createQueryBuilder('user');
        $qb->select('count(user.id)');
        $count = $qb->getQuery()->getSingleScalarResult();
        return $count;
    }

    public function getListForAccess($type) {
        // $roles = ["ROLE_ADMIN"];
        $roles = [];

        if ($type === "page") {
            // $roles = array_merge($roles, ["ROLE_PAGE_MANAGER", "ROLE_TOP_MANAGER"]);
            $roles = array_merge($roles, ["ROLE_PAGE_MANAGER"]);
        } else if ($type === "article") {
            $roles = array_merge($roles, ["ROLE_ARTICLE_MANAGER"]);
        } else if ($type === "event") {
            $roles = array_merge($roles, ["ROLE_EVENT_MANAGER"]);
        } else if ($type === "document") {
            $roles = array_merge($roles, ["ROLE_DOCUMENT_MANAGER"]);
        }

        return $this->createQueryBuilder('u')
            ->andWhere('u.isActive = :isActive')
            ->leftJoin("u.groups", "g")
            ->andWhere('g.role IN (:roles)')
            ->setParameter('isActive', true)
            ->setParameter('roles', $roles)
            ->getQuery()
            ->getResult();
    }
}
