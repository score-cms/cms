<?php

namespace Score\CmsBundle\Repository;

use Doctrine\ORM\Tools\Pagination\Paginator as Paginator;

class GroupDatagrid extends Score\BaseBundle\Repository\Datagrid {

    public function getCols() {
  return array(

            '0' => 'a.name'
        );
}

    public function buildDataAsArray($paginator)
    {
        $data = array();
        foreach ($paginator as $object)
        {
            $data[] = array(
                '<a href="'.$this->getRouter()->generate('admin_group_edit',array('id' => $object->getId())).'">' . $object->getName() . '</a>',
            );
        }
        return $data;
    }


}
