<?php

namespace Score\CmsBundle\Repository;

use Doctrine\ORM\Tools\Pagination\Paginator as Paginator;

class ArticleDatagrid extends \Score\BaseBundle\Repository\Datagrid
{

    protected $translator;

    public function __construct($em, $entityName, $router, $translator)
    {
        $this->entityManager = $em;
        $this->entityName = $entityName;
        $this->router = $router;
        $this->translator = $translator;
    }

    /**
     * Get the value of translator
     */
    public function getTranslator()
    {
        return $this->translator;
    }

    /**
     * Set the value of translator
     *
     * @return  self
     */
    public function setTranslator($translator)
    {
        $this->translator = $translator;

        return $this;
    }



    public function getCols()
    {
        return array(
            '0' => 'a.id',
            '1' => 'a.name',
            '2' => 'a.category',
            '3' => 'a.published',
            '4' => 'a.author',
            '5' => 'a.publishedFrom',
            '6' => 'a.publishedTo'
        );
    }

    public function buildDataAsArray($paginator)
    {
        $data = array();
        foreach ($paginator as $object) {
            $published = ($object->getPublished() == '1') ? '<i class="fa fa-check"></i>' : '<i class="fa fa-times"></i>';
            $publishedFrom  = ($object->getPublishedFrom() != null) ? $object->getPublishedFrom()->format('d.m.y') : '-';
            $publishedTo  = ($object->getPublishedTo() != null) ? $object->getPublishedTo()->format('d.m.y') : '-';

            $data[] = array(
                $object->getId(),
                '<a href="' . $this->getRouter()->generate('score_cms_article_edit', array('id' => $object->getId())) . '">' . $object->getName() . '</a>',
                //$this->getTranslator()->trans($object->getCategoryNames()),
                $object->getCategoryNames(),
                $published,
                $object->getAuthor(),
                $publishedFrom,
                $publishedTo,
                '<a href="' . $this->getRouter()->generate('score_cms_article_delete', array('id' => $object->getId())) . '" class="delete-article">Zmazať</a>'
            );
        }
        return $data;
    }
}
