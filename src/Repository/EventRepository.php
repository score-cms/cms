<?php

namespace Score\CmsBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Score\CmsBundle\Entity\User;

/**
 * EventRepository
 */
class EventRepository extends EntityRepository {

    public function findTopEventsByCategory($category, $maxResult = 4) {

        $qd = $this->createQueryBuilder('a');
        $query = $qd->select('a')
            ->andWhere('a.published = 1')
            ->andWhere('a.isDeleted = 0')
            ->leftJoin("a.eventCategories", "t")
            ->andWhere('t.seoName = :category')
            ->add('orderBy', 'a.date_from DESC')
            ->setParameter("category", $category)
            ->setFirstResult(0)
            ->setMaxResults($maxResult)
            ->getQuery();

        $items = $query->getResult();
        return $items;
    }


    public function findTopEvents($maxResult = 4) {

        $qd = $this->createQueryBuilder('o');
        $query = $qd->select('o')
            ->andWhere('o.published = 1')
            ->andWhere('o.isDeleted = 0 OR o.isDeleted IS NULL')
            ->add('orderBy', ' o.date_from DESC')
            ->setFirstResult(0)
            ->setMaxResults($maxResult)
            ->getQuery();

        $items = $query->getResult();
        return $items;
    }

    public function findSidebarEvents($event, $maxResult = 4) {

        $qd = $this->createQueryBuilder('o');
        $query = $qd->select('o')
            ->andWhere('o.published = 1')
            ->andWhere('o.isDeleted = 0 OR o.isDeleted IS NULL')
            ->andWhere('o.id != :currentId')
            ->setParameter('currentId', intval($event->getId()))
            ->add('orderBy', ' o.date_from DESC')
            ->setFirstResult(0)
            ->setMaxResults($maxResult)
            ->getQuery();

        $items = $query->getResult();
        return $items;
    }

    public function findAllEventsForUser(User $user, $checkAccess = false) {
        $builder = $this->createQueryBuilder('e');
        if ($checkAccess) {
            $builder->leftJoin("e.accesses", "acc")
                ->leftJoin("acc.user", "u")
                ->andWhere("u = :user")
                ->setParameter("user", $user);
        }
        $builder
            ->andWhere('e.isDeleted = 0 OR e.isDeleted IS NULL')
            ->addSelect('CASE WHEN e.date_to IS NULL THEN 1 ELSE 0 END as HIDDEN list_order_is_null') // nevyplneny datum nech je prvy
            ->add('orderBy', 'list_order_is_null DESC, e.date_to DESC, e.name ASC');
        return $builder->getQuery()->getResult();
    }


    public function findEventsByPage($settings = array('page' => 1, 'pageSize' => 10), $criteria = []) {
        $qd = $this->createQueryBuilder('o');
        $qd->select('o');
        if (array_key_exists('visible', $criteria) and $criteria['visible'] == true) {
            $qd->andWhere('o.published = :published')
                ->andWhere('o.isDeleted = 0 OR o.isDeleted IS NULL')
            ;
            $qd->setParameter('published', 1);
        }

        $query = $qd->getQuery();
        $pageSize = $settings['pageSize'];
        $page = $settings['page'];
        // load doctrine Paginator
        $paginator = new \Doctrine\ORM\Tools\Pagination\Paginator($query);

        // you can get total items
        $totalItems = count($paginator);

        // get total pages
        $pagesCount = ceil($totalItems / $pageSize);

        // now get one page's items:
        $paginator
            ->getQuery()
            ->setFirstResult($pageSize * ($page - 1)) // set the offset
            ->setMaxResults($pageSize); // set the limit
        $list = array();
        foreach ($paginator as $pageItem) {
            $list[] = $pageItem;
        }
        return ['list' => $list, 'total' => $totalItems, 'pagesCount' => $pagesCount];
    }


}
