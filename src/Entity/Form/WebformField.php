<?php

namespace Score\CmsBundle\Entity\Form;

use App\Bundle\TripLogBundle\Entity\Trip;
use Doctrine\ORM\Mapping as ORM;
use Score\BaseBundle\Entity\BaseEntity;

/**
 * @ORM\Table(name="cms_form_field")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity()
 */
class WebformField extends BaseEntity
{

    /**
     * Many Fields have one form. This is the owning side.
     * @ORM\ManyToOne(targetEntity="Webform", inversedBy="fields")
     * @ORM\JoinColumn(name="form_id", referencedColumnName="id")
     */
    protected $form;


    /**
     * @ORM\Column(type="string", name="identifier", length=255)
     */
    protected $identifier;

    /**
     * @ORM\Column(type="string", name="f_title", length=1000)
     */
    protected $title;

    /**
     * @ORM\Column(type="string", name="f_type", length=255)
     */
    protected $type;

    /**
     * @ORM\Column(type="boolean", name="f_required")
     */
    protected $required;

    /**
     * @ORM\Column(type="string", name="choices", length=5000, nullable=true)
     */
    protected $choices;

    /**
     * @ORM\Column(type="integer", name="sort_order")
     */
    protected $sortOrder;

    /**
     * @ORM\Column(type="string", name="help_text", length=1000, nullable=true)
     */
    protected $helpText;

    /**
     * @ORM\Column(type="integer", name="f_rows", nullable=true)
     */
    protected $rows;

    /**
     * @ORM\Column(type="integer", name="f_length", nullable=true)
     */
    protected $length;

    /**
     * @ORM\Column(type="string", name="f_min", length=1000, nullable=true)
     */
    protected $min;

    /**
     * @ORM\Column(type="string", name="f_max", length=1000, nullable=true)
     */
    protected $max;


    public function getForm(): ?Webform
    {
        return $this->form;
    }

    public function setForm(?Webform $form): self
    {
        $this->form = $form;

        return $this;
    }


    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getRequired(): ?bool
    {
        return $this->required;
    }

    public function setRequired(bool $required): self
    {
        $this->required = $required;

        return $this;
    }

    public function getChoices(): ?string
    {
        return $this->choices;
    }

    public function setChoices(?string $choices): self
    {
        $this->choices = $choices;

        return $this;
    }

    public function getChoicesArr(): ?array
    {
        return json_decode($this->choices, true);
    }

    public function setChoicesArr(?array $choices): self
    {
        $this->choices = json_encode($choices);

        return $this;
    }

    public function getSortOrder(): ?int
    {
        return $this->sortOrder;
    }

    public function setSortOrder(int $sortOrder): self
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    public function getHelpText(): ?string
    {
        return $this->helpText;
    }

    public function setHelpText(?string $helpText): self
    {
        $this->helpText = $helpText;

        return $this;
    }

    public function getRows(): ?int
    {
        return $this->rows;
    }

    public function setRows(?int $rows): self
    {
        $this->rows = $rows;

        return $this;
    }

    public function getLength(): ?int
    {
        return $this->length;
    }

    public function setLength(?int $length): self
    {
        $this->length = $length;

        return $this;
    }


    public function getMin(): ?string
    {
        return $this->min;
    }

    public function setMin(?string $min): self
    {
        $this->min = $min;

        return $this;
    }


    public function getMax(): ?string
    {
        return $this->max;
    }

    public function setMax(?string $max): self
    {
        $this->max = $max;

        return $this;
    }

}
