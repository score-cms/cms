<?php

namespace Score\CmsBundle\Entity\Form;

use App\Repository\Score\CmsBundle\Entity\Form\WebformRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Score\BaseBundle\Entity\BaseEntity;
use Score\CmsBundle\Entity\User;

/**
 * @ORM\Table(name="cms_form")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity()
 */
class Webform extends BaseEntity
{
    /**
     * @ORM\Column(type="integer", name="creator_id")
     */
    protected $creatorId;

    /**
     * One form has many fields. This is the inverse side.
     * @ORM\OneToMany(targetEntity="WebformField", mappedBy="form")
     */
    protected $fields;

    /**
     * One form has many values. This is the inverse side.
     * @ORM\OneToMany(targetEntity="WebformValue", mappedBy="form")
     */
    protected $values;

    /**
     * @ORM\Column(type="string", name="identifier", length=1000)
     */
    protected $identifier;

    /**
     * @ORM\Column(type="string", name="f_name", length=1000)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", name="f_description", length=5000, nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(type="string", name="success_message", length=1000, nullable=true)
     */
    protected $successMessage;

    /**
     * @ORM\Column(type="boolean", name="is_active")
     */
    protected $isActive = true;

    /**
     * @ORM\Column(type="boolean", name="send_mail", nullable=true)
     */
    protected $sendMail;

    /**
     * @ORM\Column(type="string", name="mail_list", length=1000, nullable=true)
     */
    protected $mailList;

    /**
     * @ORM\Column(type="string", name="f_params", length=1000, nullable=true)
     */
    protected $params;


    public function __construct()
    {
        $this->fields = new ArrayCollection();
        $this->values = new ArrayCollection();
    }


    /**
     * @return Collection
     */
    public function getFields(): Collection
    {
        return $this->fields;
    }

    public function addField(WebformField $field): self
    {
        if (!$this->fields->contains($field)) {
            $this->fields[] = $field;
            $field->setForm($this);
        }

        return $this;
    }

    public function removeField(WebformField $field): self
    {
        if ($this->fields->removeElement($field)) {
            // set the owning side to null (unless already changed)
            if ($field->getForm() === $this) {
                $field->setForm(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection
     */
    public function getValues(): Collection
    {
        return $this->values;
    }

    public function addValue(WebformValue $value): self
    {
        if (!$this->values->contains($value)) {
            $this->values[] = $value;
            $value->setForm($this);
        }

        return $this;
    }

    public function removeValue(WebformValue $value): self
    {
        if ($this->values->removeElement($value)) {
            // set the owning side to null (unless already changed)
            if ($value->getForm() === $this) {
                $value->setForm(null);
            }
        }

        return $this;
    }


    public function getCreatorId(): ?int
    {
        return $this->creatorId;
    }

    public function setCreatorId(int $creatorId): self
    {
        $this->creatorId = $creatorId;

        return $this;
    }


    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }


    public function getSuccessMessage(): ?string
    {
        return $this->successMessage;
    }

    public function setSuccessMessage(?string $successMessage): self
    {
        $this->successMessage = $successMessage;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getSendMail(): ?bool
    {
        return $this->sendMail;
    }

    public function setSendMail(bool $sendMail): self
    {
        $this->sendMail = $sendMail;

        return $this;
    }

    public function getMailList(): ?string
    {
        return $this->mailList;
    }

    public function setMailList(?string $mailList): self
    {
        $this->mailList = $mailList;

        return $this;
    }

    public function getMailListArr(): ?array
    {
        return json_decode($this->mailList, true);
    }

    public function setMailListArr(?array $mailList): self
    {
        $this->mailList = json_encode($mailList);

        return $this;
    }

    public function getParams(): ?string
    {
        return $this->params;
    }

    public function setParams(?string $params): self
    {
        $this->params = $params;

        return $this;
    }
    public function getParamsArr(): ?array
    {
        return json_decode($this->params, true);
    }

    public function setParamsArr(?array $params): self
    {
        $this->params = json_encode($params);

        return $this;
    }

    public function getFilledCount(): ?int
    {
        $result = [];
        foreach ($this->values as $value) {
            if (!in_array($value->getIdentifier(), $result)) {
                $result[] = $value->getIdentifier();
            }
        }
        return count($result);
    }
}
