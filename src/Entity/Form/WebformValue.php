<?php

namespace Score\CmsBundle\Entity\Form;

use Doctrine\ORM\Mapping as ORM;
use Score\BaseBundle\Entity\BaseEntity;

/**
 * @ORM\Table(name="cms_form_value")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity()
 */
class WebformValue extends BaseEntity
{

    /**
     * Many Fields have one form. This is the owning side.
     * @ORM\ManyToOne(targetEntity="Webform", inversedBy="values")
     * @ORM\JoinColumn(name="form_id", referencedColumnName="id")
     */
    protected $form;

    /**
     * @ORM\Column(type="integer", name="user_id", nullable=true)
     */
    protected $userId;

    /**
     * @ORM\Column(type="string", name="field_identifier", length=255)
     */
    protected $fieldIdentifier;

    /**
     * @ORM\Column(type="string", name="identifier", length=255)
     */
    protected $identifier;

    /**
     * @ORM\Column(type="string", name="value", length=5000)
     */
    protected $value;


    /**
     * @ORM\Column(type="boolean", name="is_valid")
     */
    protected $valid;


    public function getForm(): ?Webform
    {
        return $this->form;
    }

    public function setForm(?Webform $form): self
    {
        $this->form = $form;

        return $this;
    }


    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getFieldIdentifier(): ?string
    {
        return $this->fieldIdentifier;
    }

    public function setFieldIdentifier(string $fieldIdentifier): self
    {
        $this->fieldIdentifier = $fieldIdentifier;

        return $this;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setValue( $value)
    {
        $this->value = $value;

        return $this;
    }

    public function getValueArr()
    {
        return json_decode($this->value, true);
    }

    public function setValueArr( $value)
    {
        $this->value = json_encode($value);

        return $this;
    }

    public function getValid(): ?bool
    {
        return $this->valid;
    }

    public function setValid(bool $valid): self
    {
        $this->valid = $valid;

        return $this;
    }
}
