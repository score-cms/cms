<?php
namespace  Score\CmsBundle\Entity\Multisite;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class SiteItemArticle extends SiteItem {

    /**
     * @ORM\ManyToOne(targetEntity="\Score\CmsBundle\Entity\Article\Article", inversedBy="siteItemArticle")
     * @ORM\JoinColumn(name="item_id", referencedColumnName="id")
     */
    protected $article;

    /**
     * @ORM\ManyToOne(targetEntity="Site")
     * @ORM\JoinColumn(name="site_id", referencedColumnName="id")
     */
    protected $site;
    

    /**
     * Get the value of article
     */ 
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * Set the value of article
     *
     * @return  self
     */ 
    public function setArticle($article)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get the value of site
     */ 
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set the value of site
     *
     * @return  self
     */ 
    public function setSite($site)
    {
        $this->site = $site;

        return $this;
    }
}
