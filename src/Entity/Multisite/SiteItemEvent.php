<?php
namespace  Score\CmsBundle\Entity\Multisite;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class SiteItemEvent extends SiteItem {

    /**
     * @ORM\ManyToOne(targetEntity="\Score\CmsBundle\Entity\Event\Event", inversedBy="siteItemEvent")
     * @ORM\JoinColumn(name="item_id", referencedColumnName="id")
     */
    protected $event;

    /**
     * @ORM\ManyToOne(targetEntity="Site")
     * @ORM\JoinColumn(name="site_id", referencedColumnName="id")
     */
    protected $site;
    

 

    /**
     * Get the value of site
     */ 
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set the value of site
     *
     * @return  self
     */ 
    public function setSite($site)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get the value of event
     */ 
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set the value of event
     *
     * @return  self
     */ 
    public function setEvent($event)
    {
        $this->event = $event;

        return $this;
    }
}
