<?php
namespace  Score\CmsBundle\Entity\Multisite;

use Doctrine\ORM\Mapping as ORM;
use Score\CmsBundle\Entity\Multisite\SiteItem;

/**
 * @ORM\Entity
 */
class SiteItemMenu extends SiteItem {

    /**
     * @ORM\ManyToOne(targetEntity="\Score\CmsBundle\Entity\Menu", inversedBy="siteItemMenu")
     * @ORM\JoinColumn(name="item_id", referencedColumnName="id")
     */
    protected $menu;

    /**
     * @ORM\ManyToOne(targetEntity="Site")
     * @ORM\JoinColumn(name="site_id", referencedColumnName="id")
     */
    protected $site;
    


    /**
     * Get the value of site
     */ 
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set the value of site
     *
     * @return  self
     */ 
    public function setSite($site)
    {
        $this->site = $site;

        return $this;
    }


    /**
     * Get the value of menu
     */ 
    public function getMenu()
    {
        return $this->menu;
    }

    /**
     * Set the value of menu
     *
     * @return  self
     */ 
    public function setMenu($menu)
    {
        $this->menu = $menu;

        return $this;
    }
}
