<?php
namespace  Score\CmsBundle\Entity\Multisite;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class SiteItemBlock extends SiteItem {

    /**
     * @ORM\ManyToOne(targetEntity="\Score\CmsBundle\Entity\Block\Block", inversedBy="siteItemBlock")
     * @ORM\JoinColumn(name="item_id", referencedColumnName="id")
     */
    protected $block;

    /**
     * @ORM\ManyToOne(targetEntity="Site")
     * @ORM\JoinColumn(name="site_id", referencedColumnName="id")
     */
    protected $site;
    

  

    /**
     * Get the value of site
     */ 
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set the value of site
     *
     * @return  self
     */ 
    public function setSite($site)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get the value of block
     */ 
    public function getBlock()
    {
        return $this->block;
    }

    /**
     * Set the value of block
     *
     * @return  self
     */ 
    public function setBlock($block)
    {
        $this->block = $block;

        return $this;
    }
}
