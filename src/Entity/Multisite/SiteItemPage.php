<?php
namespace  Score\CmsBundle\Entity\Multisite;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class SiteItemPage extends SiteItem {

    /**
     * @ORM\ManyToOne(targetEntity="\Score\CmsBundle\Entity\Page\Page", inversedBy="siteItemPage")
     * @ORM\JoinColumn(name="item_id", referencedColumnName="id")
     */
    protected $page;

    /**
     * @ORM\ManyToOne(targetEntity="Site")
     * @ORM\JoinColumn(name="site_id", referencedColumnName="id")
     */
    protected $site;
    


    /**
     * Get the value of site
     */ 
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set the value of site
     *
     * @return  self
     */ 
    public function setSite($site)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get the value of page
     */ 
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set the value of page
     *
     * @return  self
     */ 
    public function setPage($page)
    {
        $this->page = $page;

        return $this;
    }
}
