<?php
namespace  Score\CmsBundle\Entity\Multisite;
use Score\BaseBundle\Entity\BaseEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="item_type", type="string")
 * @ORM\DiscriminatorMap({"article" = "SiteItemArticle","event" = "SiteItemEvent","page" = "SiteItemPage" ,"menu" = "SiteItemMenu", "block" = "SiteItemBlock" })
 * @ORM\Table(name="cms_site_items")
 * @ORM\HasLifecycleCallbacks()
 */
class SiteItem extends BaseEntity {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", nullable = true)
     */
    protected $itemId;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getItemId()
    {
        return $this->itemId;
    }

  

    public function setItemId($itemId)
    {
        $this->itemId = $itemId;
    }

    
}
