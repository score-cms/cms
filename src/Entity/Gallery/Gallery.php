<?php

namespace Score\CmsBundle\Entity\Gallery;

use Score\CmsBundle\Entity\Gallery\GalleryImage;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Score\BaseBundle\Entity\BaseEntity;

/**
 * @ORM\Table(name="cms_gallery")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class Gallery extends BaseEntity
{

      /**
     * @ORM\Column(type="integer", name="creator_id", nullable=true)
     */
    protected $creatorId;
    
    /**
     * One Gallery has many Images. This is the inverse side.
     * @ORM\OneToMany(targetEntity="GalleryImage", mappedBy="gallery", cascade={"persist", "remove"})
     */
    protected $images;

    /**
     * @ORM\Column(type="string", name="g_name", length=1000, nullable=true)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", name="g_description", length=4000, nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(type="string", name="g_end_message", length=1000, nullable=true)
     */
    protected $endMessage;

    /**
     * @ORM\Column(type="boolean", name="g_is_active", nullable=true)
     */
    protected $isActive;

    /**
     * @ORM\Column(type="string", name="g_type", length=255)
     */
    protected $type;

    /**
     * @ORM\Column(type="string", name="g_identifier", length=255)
     */
    protected $identifier;


    /**
     * @ORM\Column(type="string", name="g_ratio", length=255, nullable=true)
     */
    protected $ratio;

    /**
     * @ORM\Column(type="string", name="g_params", length=1000, nullable=true)
     */
    protected $params;



    public function __construct()
    {
        $this->images = new ArrayCollection();
    }


    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getEndMessage(): ?string
    {
        return $this->endMessage;
    }

    public function setEndMessage(?string $endMessage): self
    {
        $this->endMessage = $endMessage;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(?bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * @return Collection|GalleryImage[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(GalleryImage $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setGallery($this);
        }

        return $this;
    }

    public function removeImage(GalleryImage $image): self
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getGallery() === $this) {
                $image->setGallery(null);
            }
        }

        return $this;
    }

    public function getCreatorId(): ?int
    {
        return $this->creatorId;
    }

    public function setCreatorId(?int $creatorId): self
    {
        $this->creatorId = $creatorId;

        return $this;
    }

    public function getRatio(): ?string
    {
        return $this->ratio;
    }

    public function setRatio(string $ratio): self
    {
        $this->ratio = $ratio;

        return $this;
    }

    public function getParams(): ?string
    {
        return $this->params;
    }

    public function setParams(?string $params): self
    {
        $this->params = $params;

        return $this;
    }
    public function getParamsArr(): ?array
    {
        return json_decode($this->params, true);
    }

    public function setParamsArr(?array $params): self
    {
        $this->params = json_encode($params);

        return $this;
    }
}
