<?php

namespace Score\CmsBundle\Entity\Gallery;

use Score\CmsBundle\Entity\Gallery\Gallery;
use Doctrine\ORM\Mapping as ORM;
use Score\BaseBundle\Entity\BaseEntity;

/**
 * @ORM\Table(name="cms_gallery_image")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class GalleryImage extends BaseEntity
{

    /**
     * Many GalleryImages have one Gallery. This is the owning side.
     * @ORM\ManyToOne(targetEntity="Gallery", inversedBy="images")
     * @ORM\JoinColumn(name="gallery_id", referencedColumnName="id")
     */
    protected $gallery;

    /**
     * @ORM\Column(type="string", name="g_name", length=1000, nullable=true)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", name="g_description", length=1000, nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(type="boolean", name="g_is_active")
     */
    protected $isActive;

    /**
     * @ORM\Column(type="string", name="g_identifier", length=255)
     */
    protected $identifier;

    /**
     * @ORM\Column(type="string", name="g_original_filename", length=1000)
     */
    protected $originalFilename;

    /**
     * @ORM\Column(type="string", name="g_extension", length=255)
     */
    protected $extension;

    /**
     * @ORM\Column(type="integer", name="g_sort_order")
     */
    protected $sortOrder;

    /**
     * @ORM\Column(type="integer", name="g_size", nullable=true)
     */
    protected $size;

    /**
     * @ORM\Column(type="string", name="g_params", length=1000, nullable=true)
     */
    protected $params;

    /**
     * @ORM\Column(type="string", name="g_meta", length=1000, nullable=true)
     */
    protected $meta;


    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    public function getOriginalFilename(): ?string
    {
        return $this->originalFilename;
    }

    public function setOriginalFilename(string $originalFilename): self
    {
        $this->originalFilename = $originalFilename;

        return $this;
    }

    public function getGallery(): ?Gallery
    {
        return $this->gallery;
    }

    public function setGallery(?Gallery $gallery): self
    {
        $this->gallery = $gallery;

        return $this;
    }

    public function getExtension(): ?string
    {
        return $this->extension;
    }

    public function setExtension(string $extension): self
    {
        $this->extension = $extension;

        return $this;
    }

    public function getSortOrder(): ?int
    {
        return $this->sortOrder;
    }

    public function setSortOrder(int $sortOrder): self
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    public function getSize(): ?int
    {
        return $this->size;
    }

    public function setSize(?int $size): self
    {
        $this->size = $size;

        return $this;
    }
    
    public function getParams(): ?string
    {
        return $this->params;
    }

    public function setParams(?string $params): self
    {
        $this->params = $params;

        return $this;
    }  

    public function getParamsArr(): ?array
    {
        return json_decode($this->params, true);
    }

    public function setParamsArr(?array $params): self
    {
        $this->params = json_encode($params);

        return $this;
    }  

    public function getMeta(): ?string
    {
        return $this->meta;
    }

    public function setMeta(?string $meta): self
    {
        $this->meta = $meta;

        return $this;
    }

    public function getMetaArr(): ?array
    {
        return json_decode($this->meta, true);
    }

    public function setMetaArr(?array $meta): self
    {
        $this->meta = json_encode($meta);

        return $this;
    }
}
