<?php

namespace Score\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Expr\Comparison;
use Doctrine\ORM\Mapping as ORM;

// use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Score\CmsBundle\Entity\Access\Access;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

// use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Score\BaseBundle\Entity\BaseEntity;
use Score\CmsBundle\Entity\Group;
use Score\CmsBundle\Repository\UserRepository;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;

/**
 * @UniqueEntity("email",message="register.email.unique_error")
 * @ORM\Table(name="cms_user")
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class User extends BaseEntity implements UserInterface, PasswordAuthenticatedUserInterface {

    /**
     * Many Users have Many Groups.
     * @ORM\ManyToMany(targetEntity="Group")
     * @ORM\JoinTable(name="cms_conn_user_group",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     *      )
     */
    protected $groups;

    /**
     * @ORM\OneToMany(targetEntity=Access::class, mappedBy="user", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $accesses;

    /**
     * @ORM\Column(name="username", type="string", length=50, unique=true)
     */
    protected $username;

    /**
     * @Assert\NotBlank
     * @Assert\Email
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     */
    protected $email;

    /**
     * @Assert\NotBlank
     * @ORM\Column(name="password", type="string", length=100)
     */
    protected $password; // aa = $2y$13$.O0A4gC1GednLFKFcZY7POsF5ZHTCrfCxW0gHG/QUvsQ5ixYbXH5.

    /**
     * @ORM\Column(name="first_name", type="string", length=100, nullable=true)
     */
    protected $firstName;

    /**
     * @ORM\Column(name="last_name", type="string", length=100, nullable=true)
     */
    protected $lastName;

    /**
     * @ORM\Column(name="phone", type="string", length=200, nullable=true)
     */
    protected $phone;

    /**
     * @ORM\Column(name="description", type="string", length=1000, nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(name="is_active", type="boolean", nullable=true)
     */
    protected $isActive;


    /**
     * @ORM\Column(name="settings", type="string", length=2000, nullable=true)
     */
    protected $settings;

    /**
     * @ORM\Column(name="icon", type="string", length=300, nullable=true)
     */
    protected $icon;

    /**
     * @ORM\Column(name="new_pass_hash", type="string", length=100, nullable=true)
     */
    protected $newPassHash;

    /**
     * @ORM\Column(name="slug", type="string", length=300, nullable=true)
     */
    protected $slug;

    /**
     * @ORM\Column(name="source", type="string", length=300, nullable=true)
     */
    protected $source;

    /**
     * @ORM\Column(name="is_public", type="boolean", nullable=true)
     */
    protected $isPublic;

    /**
     * @ORM\Column(name="u_params", type="string", length=1000, nullable=true)
     */
    protected $params;


    protected $keywords;
    protected $emailVisibility;
    protected $phoneVisibility;
    protected $addressVisibility;
    protected $sendInfoMail;
    protected $startLevel;
    protected $organizationsCount;
    protected $rawPassword;


    public function __toString() {
        return $this->email;
    }


    public function __construct() {
        $this->groups = new ArrayCollection();
        $this->accesses = new ArrayCollection();
    }


    public function eraseCredentials() {
        //$this->password = null;
    }

    public function isAccountNonExpired() {
        return true;
    }

    public function isAccountNonLocked() {
        return true;
    }

    public function isCredentialsNonExpired() {
        return true;
    }

    public function isEqualTo(UserInterface $user) {
        if ($this->id !== $user->getId()) {
            return false;
        }
        if ($this->password !== $user->getPassword()) {
            return false;
        }
        /*
          if ($this->salt !== $user->getSalt()) {
          return false;
          }
         */
        if ($this->username !== $user->getUsername()) {
            return false;
        }

        return true;
    }

    public function isEnabled() {
        return $this->isActive;
    }

    public function getId() {
        return $this->id;
    }

    public function getUsername() {
        return $this->username;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getPassword(): string {
        return $this->password;
    }

    public function getIsActive() {
        return $this->isActive;
    }

    public function getStartLevel() {
        return $this->startLevel;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setUsername($username) {
        $this->username = $username;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function setIsActive($isActive) {
        $this->isActive = $isActive;
    }

    public function setStartLevel($startLevel) {
        $this->startLevel = $startLevel;
    }


    public function getPhone() {
        return $this->phone;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setPhone($phone) {
        $this->phone = $phone;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function getSettings() {
        return $this->settings;
    }

    public function setSettings($settings) {
        $this->settings = $settings;
    }

    public function getIcon() {
        return $this->icon;
    }

    public function setIcon($icon) {
        $this->icon = $icon;
    }

    public function getUserLocality() {
        return $this->userLocality;
    }

    public function setUserLocality($userLocality) {
        $this->userLocality = $userLocality;
    }



//    /**
//     * Return all groups except system
//     */
//    public function getContentGroups()
//    {
//        return [];
//        $userGroups = $this->getUserGroups();
//        $groups = array();
//        foreach ($userGroups as $userGroup)
//        {
//            if ($userGroup->getGroup()->getName() != 'ACL')
//                $groups[] = $userGroup->getGroup();
//        }
//        return $groups;
//    }

    public function getUserIdentifier(): string {
        return $this->email;
    }

    public function getSalt() {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    public function getRoles() {
        $roles = [];
        foreach ($this->getGroups() as $group) {
            if ($group->getVisibility() && ($group->getRole() === "ROLE_ADMIN" || substr($group->getRole(), -strlen("_MANAGER")) === "_MANAGER"))
                $roles[] = $group->getRole();
        }
        if (count($roles) >= 1) // specify inArray roles which have access to ScoreCms if use also public user -> in_array("ROLE_ADMIN", $roles)
            $roles[] = "ROLE_CMS";
        return $roles;
        //return array('ROLE_ADMIN');

    }


    /** @see \Serializable::serialize() */
    public function serialize() {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized) {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }


    public function getIsAdmin() {
        return in_array("ROLE_ADMIN", $this->getRoles());
    }


    public function getFirstName(): ?string {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self {
        $this->lastName = $lastName;

        return $this;
    }

    public function getNewPassHash(): ?string {
        return $this->newPassHash;
    }

    public function setNewPassHash(?string $newPassHash): self {
        $this->newPassHash = $newPassHash;

        return $this;
    }

    public function getSlug(): ?string {
        return $this->slug;
    }

    public function setSlug(?string $slug): self {
        $this->slug = $slug;

        return $this;
    }

    public function getSource(): ?string {
        return $this->source;
    }

    public function setSource(?string $source): self {
        $this->source = $source;

        return $this;
    }


    /**
     * @return Collection|Group[]
     */
    public function getGroups(): Collection {
        return $this->groups;
    }

    public function addGroup(Group $group) //: self
    {
        if (!$this->groups->contains($group)) {
            $this->groups[] = $group;
//            $group->addUser($this);
        }

        return $this;
    }

    public function removeGroup(Group $group) //: self
    {
        $this->groups->removeElement($group);
//        $group->removeUser($this);

        return $this;
    }

    public function removeAllGroups(): self {
        foreach ($this->getGroups() as $g) {
            $this->removeGroup($g);
        }

        return $this;
    }


    /**
     * Get the value of rawPassword
     */
    public function getRawPassword() {
        return $this->rawPassword;
    }

    /**
     * Set the value of rawPassword
     *
     * @return  self
     */
    public function setRawPassword($rawPassword) {
        $this->rawPassword = $rawPassword;

        return $this;
    }

    protected $newroles; // pre mapovanie vo formulari

    public function getNewroles() // aktualne role
    {
        $roles = [];
        foreach ($this->getGroups() as $g) {
            $roles[] = $g->getId();
        }
        return $roles;
    }

    public function getNewroles2() // role z rormulara
    {
        return $this->newroles;
    }

    public function setNewroles($newroles) {
        $this->newroles = $newroles;

        return $this;
    }

    public function getRoleNames() {
        $roles = [];
        foreach ($this->getGroups() as $g) {
            $roles[] = $g->getName();
        }
        return $roles;
    }


    public function getIsPublic(): ?bool {
        return $this->isPublic;
    }

    public function setIsPublic(?bool $isPublic): self {
        $this->isPublic = $isPublic;

        return $this;
    }

    public function getParams(): ?string {
        return $this->params;
    }

    public function setParams(?string $params): self {
        $this->params = $params;

        return $this;
    }


    /**
     * @return Collection<int, Access>
     */
    public function getAccesses(): Collection {
        return $this->accesses;
    }

    public function addAccess(Access $access): self {
        if (!$this->accesses->contains($access)) {
            $this->accesses[] = $access;
            $access->setUser($this);
        }
        return $this;
    }

    public function removeAccess(Access $access): self {
        $this->accesses->removeElement($access);
        return $this;
    }

}
