<?php

namespace Score\CmsBundle\Entity\Page;

use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Expr\Comparison;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

use Score\BaseBundle\Entity\BaseEntity;
use Score\CmsBundle\Entity\Access\Access;
use Score\CmsBundle\Entity\Access\AccessPage;
use Score\CmsBundle\Entity\Block\BlockJoint;
use Score\CmsBundle\Entity\Block\BlockJointPage;
use Score\CmsBundle\Entity\Menu;
use Score\CmsBundle\Entity\Metatag\MetatagPage;
use Score\CmsBundle\Entity\User;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @UniqueEntity("seoId",message="page.seoId.unique")
 * @ORM\Entity(repositoryClass="Score\CmsBundle\Entity\Page\PageRepository")
 * @ORM\Table(name="cms_page")
 * @ORM\HasLifecycleCallbacks()
 */
class Page extends BaseEntity {

    /**
     * @ORM\Column(type="string", length=200)
     */
    protected $name;

    /**
     * @ORM\Column(name="title", type="string", length=200)
     */
    protected $title;

    /**
     * @ORM\Column(name="menu_name", type="string", length=200, nullable = true)
     */
    protected $menuName;

    /**
     * @ORM\Column(type="string", length=200, nullable = true)
     */
    protected $linkName;

    /**
     * @ORM\ManyToOne(targetEntity="Page", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    protected $parentPage;

    /**
     * @ORM\Column(type="integer", nullable = true)
     */
    protected $lvl;

    /**
     * @ORM\Column(type="integer", nullable = true)
     */
    protected $sortOrder;

    /**
     * @ORM\Column(type="string", length=255, nullable = true, unique=true)
     */
    protected $seoId;

    /**
     * @ORM\Column(name="content", type="text", length=10000, nullable = true)
     */
    protected $content;

    /**
     * @ORM\Column(type="string", length=100, nullable = true)
     */
    protected $status;

    /**
     * @ORM\Column(type="string", nullable = true)
     */
    protected $lang;

    /**
     * @ORM\Column(type="integer", nullable = true)
     */
    protected $parentId;

    /**
     * @ORM\Column(name="config_data", type="text", length=10000, nullable = true)
     */
    protected $configData;

    /**
     * @ORM\Column(type="string", length=100, nullable = true)
     */
    protected $layout;

    protected $keywordItemPage;

    /**
     * @ORM\Column(type="string", length=2000, nullable = true)
     */
    protected $redirectUrl;

    /**
     * @ORM\Column(type="string", length=100, nullable = true)
     */
    protected $redirectWindow;

    /**
     * @ORM\Column(type="string", length=1000, nullable = true)
     */
    protected $icon;

    /**
     * @ORM\Column(type="string", length=2000, nullable = true)
     */
    protected $teaser;

    /**
     * @ORM\OneToMany(targetEntity="\Score\CmsBundle\Entity\Multisite\SiteItemPage", mappedBy="page",cascade={"persist","remove"})
     */
    protected $siteItemPage = [];

    /**
     * @ORM\Column(name="p_search", type="text", nullable=true)
     */
    protected $search;

    protected $multisite;

    /**
     * @ORM\OneToMany(targetEntity=AccessPage::class, mappedBy="page", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $accesses;

    /**
     * @ORM\OneToMany(targetEntity=BlockJointPage::class, mappedBy="page", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"sortOrder"="ASC", "place"="ASC"})
     */
    protected $blockJoints;

    /**
     * @ORM\OneToMany(targetEntity="Page", mappedBy="parentPage")
     * @ORM\OrderBy({"sortOrder"="ASC"})
     */
    protected $children;

    /**
     * @ORM\OneToMany(targetEntity=MetatagPage::class, mappedBy="page", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $metatags;

    /**
     * @ORM\ManyToOne(targetEntity="Page", inversedBy="redirectedPages")
     * @ORM\JoinColumn(name="redirect_to_id", referencedColumnName="id", nullable=true)
     */
    protected $redirectToPage;

    /**
     * @ORM\OneToMany(targetEntity="Page", mappedBy="redirectToPage")
     */
    protected $redirectedPages;

    /**
     * @ORM\Column(type="string", name="user_id", length=255, nullable=true)
     */
    protected $userId;

    /**
     * @ORM\OneToMany(targetEntity=Menu::class, mappedBy="page", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $menus;


    /**
     * Constructor
     */
    public function __construct() {
        $this->accesses = new ArrayCollection();
        $this->blockJoints = new ArrayCollection();
        $this->metatags = new ArrayCollection();
        $this->children = new ArrayCollection();
        $this->redirectedPages = new ArrayCollection();
        $this->menus = new ArrayCollection();
    }

    public function __toString() {
        return $this->getLvlPrefix() . $this->getName();
    }

    public function getKeywordItemPage() {
        return $this->keywordItemPage;
    }

    public function setKeywordItemPage($keywordItemPage) {
        $this->keywordItemPage = $keywordItemPage;
        return $this;
    }


    public function getLayout() {
        return $this->layout;
    }

    public function setLayout($layout) {
        $this->layout = $layout;
        return $this;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Page
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Page
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Get menuName
     *
     * @return string
     */
    public function getMenuName() {
        return $this->menuName;
    }

    /**
     * Set menuName
     *
     * @param string $menuName
     * @return Page
     */
    public function setMenuName($menuName) {
        $this->menuName = $menuName;

        return $this;
    }


    /**
     * Set linkName
     *
     * @param string $linkName
     * @return Page
     */
    public function setLinkName($linkName) {
        $this->linkName = $linkName;

        return $this;
    }

    /**
     * Get linkName
     *
     * @return string
     */
    public function getLinkName() {
        return $this->linkName;
    }

    /**
     * Set lvl
     *
     * @param integer $lvl
     * @return Page
     */
    public function setLvl($lvl) {
        $this->lvl = $lvl;

        return $this;
    }

    /**
     * Get lvl
     *
     * @return integer
     */
    public function getLvl() {
        return $this->lvl;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return Page
     */
    public function setSortOrder($sortOrder) {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer
     */
    public function getSortOrder() {
        return $this->sortOrder;
    }

    /**
     * Set seoId
     *
     * @param string $seoId
     * @return Page
     */
    public function setSeoId($seoId) {
        $this->seoId = $seoId;

        return $this;
    }

    /**
     * Get seoId
     *
     * @return string
     */
    public function getSeoId() {
        return $this->seoId;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Page
     */
    public function setContent($content) {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Page
     */
    public function setStatus($status) {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus() {
        return $this->status;
    }


    /**
     * Set lang
     *
     * @param string $lang
     * @return Page
     */
    public function setLang($lang) {
        $this->lang = $lang;

        return $this;
    }

    /**
     * Get lang
     *
     * @return string
     */
    public function getLang() {
        return $this->lang;
    }

    /**
     * Set parentPage
     *
     * @param Page $parentPage
     * @return Page
     */
    public function setParentPage(Page $parentPage = null) {
        $this->parentPage = $parentPage;

        return $this;
    }

    /**
     * Get parentPage
     *
     * @return Page
     */
    public function getParentPage() {
        return $this->parentPage;
    }

    function getParentId() {
        return $this->parentId;
    }

    function setParentId($parentId) {
        $this->parentId = $parentId;
    }

    public function getRedirectUrl() {
        return $this->redirectUrl;
    }

    public function getRedirectWindow() {
        return $this->redirectWindow;
    }

    public function getIcon() {
        return $this->icon;
    }

    public function iconThumb($width, $height) {
        if (!$this->getIcon()) {
            return '';
//            return 'default_' . $width . 'x' . $height . '.png';
        }

        $parts = explode('.', $this->getIcon());
        $extension = end($parts);
        return str_replace('.' . $extension, '', $this->getIcon()) . '_thumb_' . $width . 'x' . $height . '.' . $extension;
    }


    public function getTeaser() {
        return $this->teaser;
    }

    public function setRedirectUrl($redirectUrl) {
        $this->redirectUrl = $redirectUrl;
    }

    public function setRedirectWindow($redirectWindow) {
        $this->redirectWindow = $redirectWindow;
    }

    public function setIcon($icon) {
        $this->icon = $icon;
    }

    public function setTeaser($teaser) {
        $this->teaser = $teaser;
    }

    public function getLvlPrefix() {
        if (!$this->getLvl()) return "";
        return str_repeat('- ', $this->getLvl() - 1);
    }

    public function hasLeftBar() {
        if ('menu-left-center-right' == $this->getLayout() or 'menu-left-center' == $this->getLayout()) {
            return true;
        }
    }

    public function hasRightBar() {
        if ('menu-left-center-right' == $this->getLayout() or 'menu-center-right' == $this->getLayout()) {
            return true;
        }
    }

    function getConfigData() {
        return $this->configData;
    }

    function setConfigData($configData): void {
        $this->configData = $configData;
    }


    /**
     * Get the value of siteItemPage
     */
    public function getSiteItemPage() {
        return $this->siteItemPage;
    }

    /**
     * Set the value of siteItemPage
     *
     * @return  self
     */
    public function setSiteItemPage($siteItemPage) {
        $this->siteItemPage = $siteItemPage;

        return $this;
    }

    /**
     * Get the value of multisite
     */
    public function getMultisite() {
        return $this->multisite;
    }

    /**
     * Set the value of multisite
     *
     * @return  self
     */
    public function setMultisite($multisite) {
        $this->multisite = $multisite;

        return $this;
    }


    /**
     * Get the value of search
     */
    public function getSearch() {
        return $this->search;
    }

    /**
     * Set the value of search
     *
     * @return  self
     */
    public function setSearch($search) {
        $this->search = $search;

        return $this;
    }


    /**
     * @return Collection<int, Access>
     */
    public function getAccesses(): Collection {
        return $this->accesses;
    }

    public function addAccess(AccessPage $access): self {
        if (!$this->accesses->contains($access)) {
            $this->accesses[] = $access;
            $access->setPage($this);
        }

        return $this;
    }

    public function removeAccess(Access $access): self {
        $this->accesses->removeElement($access);
        //$access->setPage(null);
        return $this;
    }


    /**
     * @return Collection<int, BlockJointPage>
     */
    public function getBlockJoints(): Collection {
        return $this->blockJoints;
    }

    public function addBlockJoint(BlockJointPage $blockJoint): self {
        if (!$this->blockJoints->contains($blockJoint)) {
            $this->blockJoints[] = $blockJoint;
            $blockJoint->setPage($this);
        }
        return $this;
    }

    public function removeBlockJoint(BlockJoint $blockJoint): self {
        $this->blockJoints->removeElement($blockJoint);
        return $this;
    }

    public function getChildren() {
        return $this->children;
    }

    public function getAllChildren() {
        $children = $this->getChildren()->toArray();
        $subChildren = [];

        foreach ($this->getChildren() as $ch) {
            foreach ($ch->getAllChildren() as $sub) {
                $subChildren[] = $sub;
            }
        }

        $colection = new ArrayCollection(
            array_merge($children, $subChildren)
        );

        return $colection;
    }

    public function getAllParents($withRoot = false) {
        $parent = $this->getParentPage();
        $parents = [];

        while ($parent) {
            if ($parent->getName() !== "root" || $withRoot) {
                $parents[] = $parent;
            }
            $parent = $parent->getParentPage();
        }

        $colection = new ArrayCollection($parents);

        return $colection;
    }

    /**
     * @return Collection<int, MetatagPage>
     */
    public function getMetatags(): Collection {
        return $this->metatags;
    }

    public function addMetatag(MetatagPage $metatag): self {
        if (!$this->metatags->contains($metatag)) {
            $this->metatags[] = $metatag;
            $metatag->setPage($this);
        }
        return $this;
    }

    public function removeMetatag(MetatagPage $metatag): self {
        $this->metatags->removeElement($metatag);
        return $this;
    }


    public function setRedirectToPage(Page $redirectToPage = null) {
        $this->redirectToPage = $redirectToPage;

        return $this;
    }

    /**
     * @return Page
     */
    public function getRedirectToPage() {
        return $this->redirectToPage;
    }


    /**
     * @return Collection<int, Page>
     */
    public function getRedirectedPages() {
        return $this->redirectedPages;
    }

    public function setUserId($userId) {
        $this->userId = $userId;

        return $this;
    }

    public function getUserId() {
        return $this->userId;
    }


    /**
     * @return Collection<int, Menu>
     */
    public function getMenus(): Collection {
        return $this->menus;
    }

    public function addMenu(Menu $menu): self {
        if (!$this->menus->contains($menu)) {
            $this->menus[] = $menu;
            $menu->setPage($this);
        }
        return $this;
    }

    public function removeMenu(Menu $menu): self {
        $this->menus->removeElement($menu);
        return $this;
    }

}
