<?php


namespace Score\CmsBundle\Entity\Page;

use Doctrine\DBAL\DBALException;
use Score\BaseBundle\Services\BaseRepository;
use Score\CmsBundle\Entity\Block\Block;

class PageRepository extends BaseRepository {
//    public function findBlockJoints($page)
//    {
//
//        $conn =  $this->getEntityManager()->getConnection();
//
//        $sql = 'SELECT b.* FROM cms_page_block pb
//                LEFT JOIN cms_block b ON pb.block_id = b.id where pb.page_id = :page_id  ';
//        $records = $conn->fetchAll($sql,array('page_id'=>$page->getId()));
//
//
//        $list = array();
//        foreach($records as $record)
//        {
//            $block = new Block();
//            $this->updateEntityFromArray($block,$record);
//            $list[] = $block;
//        }
//
//        return $list;
//
//    }
//
//
//        $sql = '
//            SELECT n2.id, n2.name, n2.parent_id, n2.sort_order
//            FROM cms_page n2
//            WHERE n2.id
//                  IN
//                        (SELECT n1.id
//                            FROM cms_page n1
//                            WHERE n1.id >= :top_id
//                            START WITH n1.id = :id
//                            CONNECT BY PRIOR  n1.parent_id = n1.id )
//            OR n2.parent_id
//                    IN
//                        (SELECT n1.id
//                            FROM cms_page n1
//                            WHERE n1.id >= :top_id
//                            START WITH n1.id = :id
//                            CONNECT BY PRIOR  n1.parent_id = n1.id )
//
//            ORDER BY parent_id ASC, sort_order ASC
//        ';


//    public function getPageTree($page, $topId = null) {
//
//        if (!$topId) {
//            $root = $this->findOneBy(["name"=> "root"]);
//            $topId = $root->getId();
//        }
//
//
//        $conn = $this->getEntityManager()->getConnection();
//
//        $sql = '
//            SELECT n2.id, n2.name, n2.parent_id, n2.sort_order
//            FROM cms_page n2
//            WHERE n2.id
//                  IN
//                        (SELECT n1.id
//                            FROM cms_page n1
//                            WHERE n1.id >= :top_id
//                            START WITH n1.id = :id
//                            CONNECT BY PRIOR  n1.parent_id = n1.id )
//            OR n2.parent_id = :top_id
//            ORDER BY parent_id NULLS FIRST, id ASC
//            ';
//        $stmt = $conn->prepare($sql);
//        $resultSet = $stmt->executeQuery([
//            'top_id' => $topId,
//            'id' => $page->getId()
//        ]);
//        $arr = $resultSet->fetchAllAssociative();
//
//        function buildTree(&$items, $parentId = null) {
//            $treeItems = [];
//            foreach ($items as $idx => $item) {
//                if ((empty($parentId)) || (!empty($item['PARENT_ID']) && !empty($parentId) && $item['PARENT_ID'] == $parentId)) {
//                    $items[$idx]['children'] = buildTree($items, $items[$idx]['ID']);
//                    $treeItems [] = [
//                        'id' => $items[$idx]['ID'],
//                        'name' => $items[$idx]['NAME'],
//                        'children' => $items[$idx]['children']
//                    ];
//                }
//            }
//            return $treeItems;
//        }
//
//        $result = [];
//        try {
//            $modified = buildTree($arr);
//            $result = $modified[0];
//        } catch (DBALException $e) {
//            $result = [];
//        }
//        return $result;
//    }

}
