<?php

namespace Score\CmsBundle\Entity\Page;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="cms_page_block")
 */
class PageBlock { //trieda sa uz nepouziva, je len na konvertovanie zo starsich verzii - Route("/admin/page/pageblock-to-blockjoint", name="score_cms_page_pageblock_to_blockjoint")

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", nullable = true)
     */
    protected $sortOrder;

    /**
     * @ORM\Column(type="string", length=100, nullable = true)
     */
    protected $status;

    /**
     * @ORM\Column(type="date", nullable = true)
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="string", nullable = true)
     */
    protected $lang;

    /**
     * @ORM\ManyToOne(targetEntity="Page")
     * @ORM\JoinColumn(name="page_id", referencedColumnName="id")
     */
    protected $page;

    /**
     * @ORM\ManyToOne(targetEntity="Score\CmsBundle\Entity\Block\Block")
     * @ORM\JoinColumn(name="block_id", referencedColumnName="id")
     */
    protected $block;

    /**
     * @ORM\Column(type="string", nullable = true)
     */
    protected $place;

    public function getId()
    {
        return $this->id;
    }

    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getLang()
    {
        return $this->lang;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function getBlock()
    {
        return $this->block;
    }

    public function getPlace()
    {
        return $this->place;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    public function setLang($lang)
    {
        $this->lang = $lang;
    }

    public function setPage($page)
    {
        $this->page = $page;
    }

    public function setBlock($block)
    {
        $this->block = $block;
    }

    public function setPlace($place)
    {
        $this->place = $place;
    }

}
