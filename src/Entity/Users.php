<?php

namespace Score\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class Users
{
    protected $users;

    public function __construct($users)
    {
        $this->users = new ArrayCollection($users);
    }

    public function getUsers()
    {
        return $this->users;
    }
}
