<?php

namespace Score\CmsBundle\Entity\Event;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Score\BaseBundle\Entity\BaseEntity;
use Doctrine\ORM\Mapping as ORM;
use Score\CmsBundle\Entity\Access\Access;
use Score\CmsBundle\Entity\Access\AccessEvent;
use Score\CmsBundle\Entity\Block\BlockJoint;
use Score\CmsBundle\Entity\Block\BlockJointEvent;
use Score\CmsBundle\Entity\Metatag\MetatagEvent;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * @UniqueEntity("slug",message="event.seoId.unique")
 * @ORM\Entity(repositoryClass="Score\CmsBundle\Repository\EventRepository")
 * @ORM\Table(name="cms_event")
 * @ORM\HasLifecycleCallbacks()
 */
class Event extends BaseEntity {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=200)
     */
    protected $name;

    /**
     * @ORM\Column(type="text", nullable = true)
     */
    protected $content;

    /**
     * @ORM\Column(type="text", nullable = true)
     */
    protected $teaser;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $date_from;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $date_to;

    /**
     * @ORM\Column(type="string",length=200, nullable=true)
     */
    protected $timeFrom;

    /**
     * @ORM\Column(type="string",length=200, nullable=true)
     */
    protected $timeTo;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $published_from;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $published_to;

    /**
     * @ORM\Column(type="string",length=1000, nullable=true)
     */
    protected $url_link;

    /**
     * @ORM\Column(type="string",length=255, nullable=true)
     */
    protected $location;

    /**
     * @ORM\Column(type="string",length=255, nullable=true)
     */
    protected $organizer;

    /**
     * @ORM\Column(type="string",length=200, nullable=true)
     */
    protected $phone;

    /**
     * @ORM\Column(type="string",length=200, nullable=true)
     */
    protected $email;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $published;

    /**
     * @ORM\Column(type="string", length=255, nullable = true, unique=true)
     */
    protected $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable = true)
     */
    protected $contact_person;


    /**
     * @ORM\Column(type="integer")
     */
    protected $createdBy;

    /**
     * @ORM\Column(type="string", length=255, nullable = true)
     */
    protected $icon;

    /**
     * @ORM\Column(type="string", length=255, nullable = true)
     */
    protected $category;

    /**
     * @ORM\OneToMany(targetEntity="\Score\CmsBundle\Entity\Multisite\SiteItemEvent", mappedBy="event",cascade={"persist","remove"})
     */
    protected $siteItemEvent = [];

    /**
     * @ORM\Column(name="e_search", type="text", nullable=true)
     */
    protected $search;

    protected $multisite;

    /**
     * @ORM\OneToMany(targetEntity=AccessEvent::class, mappedBy="event", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $accesses;

    /**
     * @ORM\OneToMany(targetEntity=BlockJointEvent::class, mappedBy="event", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"sortOrder"="ASC", "place"="ASC"})
     */
    protected $blockJoints;


    /**
     * @ORM\OneToMany(targetEntity=MetatagEvent::class, mappedBy="event", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $metatags;


    /**
     * @ORM\ManyToMany(targetEntity=EventCategory::class, inversedBy="events", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="cms_conn_event_category",
     *      joinColumns={@ORM\JoinColumn(name="event_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")}
     *      )
     * @ORM\OrderBy({"seoName" = "DESC"})
     */
    protected $eventCategories;
    
    
    

    public function __construct() {
        $this->accesses = new ArrayCollection();
        $this->blockJoints = new ArrayCollection();
        $this->metatags = new ArrayCollection();
        $this->eventCategories = new ArrayCollection();
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set content
     *
     * @param text $content
     */
    public function setContent($content) {
        $this->content = $content;
    }

    /**
     * Get content
     *
     * @return text
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * Set date_from
     *
     * @param datetime $dateFrom
     */
    public function setDateFrom($dateFrom) {
        $this->date_from = $dateFrom;
    }

    /**
     * Get date_from
     *
     * @return datetime
     */
    public function getDateFrom() {
        return $this->date_from;
    }

    /**
     * Set date_to
     *
     * @param datetime $dateTo
     */
    public function setDateTo($dateTo) {
        $this->date_to = $dateTo;
    }

    /**
     * Get date_to
     *
     * @return datetime
     */
    public function getDateTo() {
        return $this->date_to;
    }

    /**
     * Set url_link
     *
     * @param string $urlLink
     */
    public function setUrlLink($urlLink) {
        $this->url_link = $urlLink;
    }

    /**
     * Get url_link
     *
     * @return string
     */
    public function getUrlLink() {
        return $this->url_link;
    }

    /**
     * Set location
     *
     * @param string $location
     */
    public function setLocation($location) {
        $this->location = $location;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation() {
        return $this->location;
    }

    /**
     * Set organizer
     *
     * @param string $organizer
     */
    public function setOrganizer($organizer) {
        $this->organizer = $organizer;
    }

    /**
     * Get organizer
     *
     * @return string
     */
    public function getOrganizer() {
        return $this->organizer;
    }

    /**
     * Set phone
     *
     * @param string $phone
     */
    public function setPhone($phone) {
        $this->phone = $phone;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone() {
        return $this->phone;
    }

    /**
     * Set email
     *
     * @param string $email
     */
    public function setEmail($email) {
        $this->email = $email;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }


    /**
     * Set slug
     *
     * @param string $slug
     */
    public function setSlug($slug) {
        $this->slug = $slug;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * Set contact_person
     *
     * @param string $contactPerson
     */
    public function setContactPerson($contactPerson) {
        $this->contact_person = $contactPerson;
    }

    /**
     * Get contact_person
     *
     * @return string
     */
    public function getContactPerson() {
        return $this->contact_person;
    }

    /**
     * Set category
     */
    public function setCategory($category) {
        $this->category = $category;
    }

    /**
     * Get category
     */
    public function getCategory() {
        return $this->category;
    }

    /**
     * Set published_from
     *
     * @param datetime $publishedFrom
     */
    public function setPublishedFrom($publishedFrom) {
        $this->published_from = $publishedFrom;
    }

    /**
     * Get published_from
     *
     * @return datetime
     */
    public function getPublishedFrom() {
        return $this->published_from;
    }

    /**
     * Set published_to
     *
     * @param datetime $publishedTo
     */
    public function setPublishedTo($publishedTo) {
        $this->published_to = $publishedTo;
    }

    /**
     * Get published_to
     *
     * @return datetime
     */
    public function getPublishedTo() {
        return $this->published_to;
    }

    public function getCreatedBy() {
        return $this->createdBy;
    }

    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }

    public function getIcon() {
        return $this->icon;
    }

    public function setIcon($icon) {
        $this->icon = $icon;
    }

    public function getTimeFrom() {
        return $this->timeFrom;
    }

    public function getTimeTo() {
        return $this->timeTo;
    }

    public function setTimeFrom($timeFrom) {
        $this->timeFrom = $timeFrom;
    }

    public function setTimeTo($timeTo) {
        $this->timeTo = $timeTo;
    }

    public function iconThumb($width, $height) {
        if (!$this->getIcon()) {
            return '';
//            return 'default_' . $width . 'x' . $height . '.png';
        }

        $parts = explode('.', $this->getIcon());
        $extension = end($parts);
        return str_replace('.' . $extension, '', $this->getIcon()) . '_thumb_' . $width . 'x' . $height . '.' . $extension;
    }

    public function getTerminString($format = 'd.m.Y') {
        if (null != $this->getDateFrom()) {
            $start = $this->getDateFrom()->format($format);
            $end = $this->getDateTo()->format($format);
            return $start . ' - ' . $end;
        }
    }

    /**
     * Get the value of published
     */
    public function getPublished() {
        return $this->published;
    }

    /**
     * Set the value of published
     *
     * @return  self
     */
    public function setPublished($published) {
        $this->published = $published;

        return $this;
    }

    /**
     * Get the value of teaser
     */
    public function getTeaser() {
        return $this->teaser;
    }

    /**
     * Set the value of teaser
     *
     * @return  self
     */
    public function setTeaser($teaser) {
        $this->teaser = $teaser;

        return $this;
    }

    /**
     * Get the value of siteItemEvent
     */
    public function getSiteItemEvent() {
        return $this->siteItemEvent;
    }

    /**
     * Set the value of siteItemEvent
     *
     * @return  self
     */
    public function setSiteItemEvent($siteItemEvent) {
        $this->siteItemEvent = $siteItemEvent;

        return $this;
    }

    /**
     * Get the value of multisite
     */
    public function getMultisite() {
        return $this->multisite;
    }

    /**
     * Set the value of multisite
     *
     * @return  self
     */
    public function setMultisite($multisite) {
        $this->multisite = $multisite;

        return $this;
    }


    /**
     * Get the value of search
     */
    public function getSearch() {
        return $this->search;
    }

    /**
     * Set the value of search
     *
     * @return  self
     */
    public function setSearch($search) {
        $this->search = $search;

        return $this;
    }


    /**
     * @return Collection<int, AccessEvent>
     */
    public function getAccesses(): Collection {
        return $this->accesses;
    }

    public function addAccess(AccessEvent $access): self {
        if (!$this->accesses->contains($access)) {
            $this->accesses[] = $access;
            $access->setEvent($this);
        }

        return $this;
    }

    public function removeAccess(AccessEvent $access): self {
        $this->accesses->removeElement($access);
        return $this;
    }

    /**
     * @return Collection<int, BlockJointEvent>
     */
    public function getBlockJoints(): Collection {
        return $this->blockJoints;
    }

    public function addBlockJoint(BlockJointEvent $blockJoint): self {
        if (!$this->blockJoints->contains($blockJoint)) {
            $this->blockJoints[] = $blockJoint;
            $blockJoint->setEvent($this);
        }
        return $this;
    }

    public function removeBlockJoint(BlockJoint $blockJoint): self {
        $this->blockJoints->removeElement($blockJoint);
        return $this;
    }
    /**
     * @return Collection<int, MetatagEvent >
     */
    public function getMetatags(): Collection {
        return $this->metatags;
    }

    public function addMetatag(MetatagEvent $metatag): self {
        if (!$this->metatags->contains($metatag)) {
            $this->metatags[] = $metatag;
            $metatag->setEvent($this);
        }
        return $this;
    }

    public function removeMetatag(MetatagEvent $metatag): self {
        $this->metatags->removeElement($metatag);
        return $this;
    }


    /**
     * @return Collection<int, EventCategory>
     */
    public function getEventCategories(): Collection {
        return $this->eventCategories;
    }

    public function addEventCategory(EventCategory $eventCategory): self {
        if (!$this->eventCategories->contains($eventCategory)) {
            $this->eventCategories[] = $eventCategory;
            $eventCategory->addEvent($this);
        }

        return $this;
    }

    public function removeEventCategory(EventCategory $eventCategory): self {
        if ($this->eventCategories->removeElement($eventCategory)) {
            $eventCategory->removeEvent($this);
        }

        return $this;
    }
}
