<?php

namespace Score\CmsBundle\Entity\Event;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Score\BaseBundle\Entity\BaseEntity;

/**
 * @ORM\Entity()
 * @ORM\Table(name="cms_event_category")
 * @ORM\HasLifecycleCallbacks()
 */
class EventCategory extends BaseEntity {

    /**
     * @ORM\Column(name="a_name", type="string", length=1000)
     */
    protected $name;

    /**
     * @ORM\Column(name="a_public", type="boolean", nullable=true)
     */
    protected $public;

    /**
     * @ORM\Column(name="a_seo_name", type="string", length=1000, nullable=true)
     */
    protected $seoName;

    /**
     * @ORM\ManyToMany(targetEntity=Event::class, mappedBy="eventCategories")
     */
    protected $events;

    public function __construct() {
        $this->events = new ArrayCollection();
    }

    public function __toString() {
        return $this->name;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getPublic(): ?bool {
        return $this->public;
    }

    public function setPublic(?bool $public): self {
        $this->public = $public;

        return $this;
    }

    public function getSeoName(): ?string {
        return $this->seoName;
    }

    public function setSeoName(?string $seoName): self {
        $this->seoName = $seoName;

        return $this;
    }

    /**
     * @return Collection<int, Event>
     */
    public function getEvents(): Collection {
        return $this->events;
    }

    public function addEvent(Event $event): self {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
        }

        return $this;
    }

    public function removeEvent(Event $event): self {
        $this->events->removeElement($event);

        return $this;
    }
}
