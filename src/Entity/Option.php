<?php

namespace Score\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


use Score\BaseBundle\Entity\BaseEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="cms_option")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(
 *     fields={"code"},
 *     message="option.code.unique"
 * )
 */
class Option extends BaseEntity
{
    /**
     * @ORM\Column(name="code", type="string", length=255, nullable=true, unique=true)
     */
    protected $code;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    protected $name;

    /**
     * @ORM\Column(name="value", type="text", nullable=true)
     */
    protected $value;

    
    /**
     * @ORM\Column(name="o_params", type="text", nullable=true)
     */
    protected $params = "[]";

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
    
    public function getParams(): ?string
    {
        return $this->params;
    }

    public function setParams(?string $params): self
    {
        $this->params = $params;

        return $this;
    }

    public function getParamsArr(): ?array
    {
        return json_decode($this->params, true);
    }

    public function setParamsArr(?array $params): self
    {
        $this->params = json_encode($params);

        return $this;
    }
}
