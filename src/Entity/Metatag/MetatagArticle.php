<?php

namespace Score\CmsBundle\Entity\Metatag;

use Doctrine\ORM\Mapping as ORM;
use Score\CmsBundle\Entity\Article\Article;
use Score\CmsBundle\Entity\Metatag\Metatag;

/**
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class MetatagArticle extends Metatag {

    /**
     * @ORM\ManyToOne(targetEntity=Article::class, inversedBy="metatags")
     * @ORM\JoinColumn(name="entity_id", referencedColumnName="id")
     */
    protected $article;



    public function getArticle(): ?Article {
        return $this->article;
    }

    public function setArticle(?Article $article): self {
        $this->article = $article;
        return $this;
    }

    public function getEntity() {
        return $this->getArticle();
    }
}
