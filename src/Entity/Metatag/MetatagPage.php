<?php

namespace Score\CmsBundle\Entity\Metatag;

use Doctrine\ORM\Mapping as ORM;
use Score\CmsBundle\Entity\Metatag\Metatag;
use Score\CmsBundle\Entity\Page\Page;

/**
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class MetatagPage extends Metatag {

    /**
     * @ORM\ManyToOne(targetEntity=Page::class, inversedBy="metatags")
     * @ORM\JoinColumn(name="entity_id", referencedColumnName="id")
     */
    protected $page;


    public function getPage(): ?Page {
        return $this->page;
    }

    public function setPage(?Page $page): self {
        $this->page = $page;
        return $this;
    }

    public function getEntity() {
        return $this->getPage();
    }
}
