<?php

namespace Score\CmsBundle\Entity\Metatag;

use Doctrine\ORM\Mapping as ORM;
use Score\CmsBundle\Entity\Event\Event;
use Score\CmsBundle\Entity\Metatag\Metatag;

/**
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class MetatagEvent extends Metatag {

    /**
     * @ORM\ManyToOne(targetEntity=Event::class, inversedBy="metatags")
     * @ORM\JoinColumn(name="entity_id", referencedColumnName="id")
     */
    protected $event;

    public function getEvent(): ?Event {
        return $this->event;
    }

    public function setEvent(?Event $event): self {
        $this->event = $event;
        return $this;
    }

    public function getEntity() {
        return $this->getEvent();
    }
}
