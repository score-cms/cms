<?php

namespace Score\CmsBundle\Entity\Metatag;

use Doctrine\ORM\Mapping as ORM;
use Score\CmsBundle\Entity\Document\Document;
use Score\CmsBundle\Entity\Metatag\Metatag;

/**
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class MetatagDocument extends Metatag {


    /**
     * @ORM\ManyToOne(targetEntity=Document::class, inversedBy="metatags")
     * @ORM\JoinColumn(name="entity_id", referencedColumnName="id")
     */
    protected $document;


    public function getDocument(): ?Document {
        return $this->document;
    }

    public function setDocument(?Document $document): self {
        $this->document = $document;
        return $this;
    }

    public function getEntity() {
        return $this->getDocument();
    }
}
