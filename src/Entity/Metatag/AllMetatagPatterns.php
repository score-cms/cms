<?php

namespace Score\CmsBundle\Entity\Metatag;

use Doctrine\Common\Collections\ArrayCollection;

class AllMetatagPatterns {

    protected $metatagPatterns;

    public function __construct($metatagPatterns) {
        $this->metatagPatterns = new ArrayCollection($metatagPatterns);
    }

    public function getMetatagPatterns() {
        return $this->metatagPatterns;
    }
}
