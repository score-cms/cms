<?php

namespace Score\CmsBundle\Entity\Metatag;

use Score\CmsBundle\Repository\MetatagRepository;
use Doctrine\ORM\Mapping as ORM;
use Score\BaseBundle\Entity\BaseEntity;

/**
 * @ORM\Entity(repositoryClass=MetatagRepository::class)
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="tag_entity", type="string")
 * @ORM\DiscriminatorMap({"article"="MetatagArticle", "document"="MetatagDocument", "event"="MetatagEvent", "page"="MetatagPage" })
 * @ORM\Table(name="cms_metatag")
 * @ORM\HasLifecycleCallbacks()
 */
class Metatag extends BaseEntity {

    /**
     * @ORM\ManyToOne(targetEntity=MetatagPattern::class, inversedBy="metatags")
     * @ORM\JoinColumn(name="pattern_id", referencedColumnName="id")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $metatagPattern;

    /**
     * @ORM\Column(name="entity_id", type="integer", nullable=true)
     */
    protected $entityId;

    /**
     * @ORM\Column(name="m_value", type="string", length=2000)
     */
    protected $value;

    /**
     * @ORM\Column(name="m_userId", type="integer", nullable=true)
     */
    protected $userId;


    // custom
    public function getEntityId() {
        return $this->entityId;
    }

    public function __toString() {
        return $this->getVisibility() && $this->getMetatagPattern()->getVisibility()
            ? $this->getMetatagHtml()
            : "";
    }


    public function getMetatagPattern(): ?MetatagPattern {
        return $this->metatagPattern;
    }

    public function setMetatagPattern(MetatagPattern $metatagPattern): self {
        $this->metatagPattern = $metatagPattern;

        return $this;
    }

    public function getValue(): ?string {
        return $this->value;
    }

    public function setValue(string $value): self {
        $this->value = $value;

        return $this;
    }

    public function getUserId(): ?int {
        return $this->userId;
    }

    public function setUserId(?int $userId): self {
        $this->userId = $userId;

        return $this;
    }

    public function getMetatagHtml(): ?string {
        return sprintf(
            $this->getMetatagPattern()->getPattern(),
            $this->getValue()
        );
    }
}
