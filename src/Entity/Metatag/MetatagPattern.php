<?php

namespace Score\CmsBundle\Entity\Metatag;

use Score\CmsBundle\Repository\MetatagPatternRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Score\BaseBundle\Entity\BaseEntity;

/**
 * @ORM\Entity(repositoryClass=MetatagPatternRepository::class)
 * @ORM\Table(name="cms_metatag_pattern")
 * @ORM\HasLifecycleCallbacks()
 */
class MetatagPattern extends BaseEntity {


    /**
     * @ORM\Column(name="m_name", type="string", length=255)
     */
    protected $name = "";

    /**
     * @ORM\Column(name="m_type", type="string", length=255)
     */
    protected $type = "head";

    /**
     * @ORM\Column(name="m_group", type="string", length=255)
     */
    protected $group = "Metatag";

    /**
     * @ORM\Column(name="m_sortOrder", type="integer")
     */
    protected $sortOrder = 999;

    /**
     * @ORM\Column(name="m_pattern", type="string", length=2000)
     */
    protected $pattern;

    /**
     * @ORM\Column(name="m_helpText", type="string", length=1000, nullable=true)
     */
    protected $helpText;

    /**
     * @ORM\OneToMany(targetEntity=Metatag::class, mappedBy="metatagPattern", orphanRemoval=true)
     */
    protected $metatags;

    public function __construct() {
        $this->metatags = new ArrayCollection();
    }

    public function __toString() {
        return $this->getName();
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getType(): ?string {
        return $this->type;
    }

    public function setType(string $type): self {
        $this->type = $type;

        return $this;
    }

    public function getGroup(): ?string {
        return $this->group;
    }

    public function setGroup(string $group): self {
        $this->group = $group;

        return $this;
    }

    public function getSortOrder(): ?int {
        return $this->sortOrder;
    }

    public function setSortOrder(int $sortOrder): self {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    public function getPattern(): ?string {
        return $this->pattern;
    }

    public function setPattern(string $pattern): self {
        $this->pattern = $pattern;

        return $this;
    }

    public function getHelpText(): ?string {
        return $this->helpText;
    }

    public function setHelpText(string $helpText): self {
        $this->helpText = $helpText;

        return $this;
    }

    /**
     * @return Collection<int, Metatag>
     */
    public function getMetatags(): Collection {
        return $this->metatags;
    }

    public function addMetatag(Metatag $metatag): self {
        if (!$this->metatags->contains($metatag)) {
            $this->metatags[] = $metatag;
            $metatag->setMetatagPattern($this);
        }

        return $this;
    }

    public function removeMetatag(Metatag $metatag): self {
        if ($this->metatags->removeElement($metatag)) {
            // set the owning side to null (unless already changed)
            if ($metatag->getMetatagPattern() === $this) {
                $metatag->setMetatagPattern(null);
            }
        }

        return $this;
    }
}
