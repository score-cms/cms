<?php

namespace Score\CmsBundle\Entity\Widget;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Score\BaseBundle\Entity\BaseEntity;
use Score\CmsBundle\Entity\Widget\Widget;

/**
 * @ORM\Entity()
 * @ORM\Table(name="cms_widget_value")
 * @ORM\HasLifecycleCallbacks()
 */
class WidgetValue extends BaseEntity {

    
    /**
     * @ORM\ManyToOne(targetEntity=Widget::class, inversedBy="widgetValues")
     */
    protected $widget;

    /**
     * @ORM\Column(name="w_code", type="string", length=255)
     */
    protected $code;

    /**
     * @ORM\Column(name="w_name", type="string", length=500, nullable=true)
     */
    protected $name;

    /**
     * @ORM\Column(name="w_note", type="string", length=1000, nullable=true)
     */
    protected $note;

    /**
     * @ORM\Column(name="w_data", type="text", nullable=true)
     */
    protected $data;

    
    public function getWidget(): ?Widget
    {
        return $this->widget;
    }

    public function setWidget(?Widget $widget): self
    {
        $this->widget = $widget;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getData(): ?string
    {
        return $this->data;
    }

    public function setData(?string $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getDataArr(): ?array
    {
        return json_decode($this->data, true);
    }

    public function setDataArr(?array $data): self
    {
        $this->data = json_encode($data);

        return $this;
    }
}
