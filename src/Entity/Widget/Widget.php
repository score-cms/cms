<?php

namespace Score\CmsBundle\Entity\Widget;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Score\BaseBundle\Entity\BaseEntity;
use Score\CmsBundle\Entity\Widget\WidgetValue;

/**
 * @ORM\Entity()
 * @ORM\Table(name="cms_widget")
 * @ORM\HasLifecycleCallbacks()
 */
class Widget extends BaseEntity {

    /**
     * @ORM\OneToMany(targetEntity=WidgetValue::class, mappedBy="widget")
     * @ORM\OrderBy({"createdAt"="ASC"})
     */
    protected $widgetValues;


    /**
     * @ORM\Column(name="w_code", type="string", length=255, unique=true)
     */
    protected $code;

    /**
     * @ORM\Column(name="w_name", type="string", length=500, nullable=true)
     */
    protected $name;

    /**
     * @ORM\Column(name="w_pattern", type="text", nullable=true)
     */
    protected $pattern = "[]";

    /**
     * @ORM\Column(name="w_template", type="text", nullable=true)
     */
    protected $template;


    public function __construct()
    {
        $this->widgetValues = new ArrayCollection();
    }


    /**
     * @return Collection<int, WidgetValue>
     */
    public function getWidgetValues(): Collection
    {
        return $this->widgetValues;
    }

    public function addWidgetValue(WidgetValue $widgetValue): self
    {
        if (!$this->widgetValues->contains($widgetValue)) {
            $this->widgetValues[] = $widgetValue;
            $widgetValue->setWidget($this);
        }

        return $this;
    }

    public function removeWidgetValue(WidgetValue $widgetValue): self
    {
        if ($this->widgetValues->removeElement($widgetValue)) {
            // set the owning side to null (unless already changed)
            if ($widgetValue->getWidget() === $this) {
                $widgetValue->setWidget(null);
            }
        }

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPattern(): ?string
    {
        return $this->pattern;
    }

    public function setPattern(?string $pattern): self
    {
        $this->pattern = $pattern;

        return $this;
    }

    public function getPatternArr(): ?array
    {
        return json_decode($this->pattern, true);
    }

    public function setPatternArr(?array $pattern): self
    {
        $this->pattern = json_encode($pattern);

        return $this;
    }

    public function getTemplate(): ?string
    {
        return $this->template;
    }

    public function setTemplate(?string $template): self
    {
        $this->template = $template;

        return $this;
    }
}
