<?php

namespace Score\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Score\BaseBundle\Entity\BaseEntity;
use Score\CmsBundle\Entity\Page\Page;

/**
 * @ORM\Entity
 * @ORM\Table(name="cms_menu")
 * @ORM\HasLifecycleCallbacks()
 */
class Menu extends BaseEntity {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=200)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=1000 , nullable = true)
     */
    protected $link;

    /**
     * @ORM\Column(type="integer", nullable = true)
     */
    protected $lvl;

    /**
     * @ORM\Column(type="integer", nullable = true)
     */
    protected $sortOrder;

    /**
     * @ORM\Column(name="m_type", type="string", length=250, nullable = true)
     */
    protected $type;

    /**
     * @ORM\Column(type="string", nullable = true)
     */
    protected $lang;

    /**
     * @ORM\Column(type="integer", nullable = true)
     */
    protected $rootId;

    /**
     * @ORM\Column(type="string", nullable = true)
     */
    protected $linkType;

    /**
     * @ORM\Column(type="string", nullable = true)
     */
    protected $linkTypeData;

    /**
     * @ORM\OneToMany(targetEntity="\Score\CmsBundle\Entity\Multisite\SiteItemMenu", mappedBy="menu",cascade={"persist","remove"})
     */
    protected $siteItemMenu = [];

    protected $multisite;

    /**
     * @ORM\ManyToOne(targetEntity="\Score\CmsBundle\Entity\Page\Page", inversedBy="menus")
     * @ORM\JoinColumn(name="m_page_id", referencedColumnName="id", nullable=true)
     */
    protected $page = null;

    /**
     * @ORM\Column(name="parent_id", type="integer", nullable = true)
     */
    protected $parentId;

    /**
     * @ORM\ManyToOne(targetEntity="Menu", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id2", referencedColumnName="id")
     */
    protected $parent;

    /**
     * @ORM\OneToMany(targetEntity="Menu", mappedBy="parent")
     * @ORM\OrderBy({"sortOrder"="ASC"})
     */
    protected $children;


    /**
     * Constructor
     */
    public function __construct() {
        $this->children = new ArrayCollection();
    }

    public function getId() {
        return $this->id;
    }

    public function getName() {
        if ($this->getPage()) {
            return $this->getPage()->getMenuName();
        }
        return $this->name;
    }

    public function getLink() {
        if ($this->getPage()) {
            return $this->getPage()->getSeoId();
        }
        return $this->link;
    }

    public function getParentId() {
        return $this->parentId;
    }

    public function getLvl() {
        return $this->lvl;
    }

    public function getSortOrder() {
        return $this->sortOrder;
    }

    public function getLang() {
        return $this->lang;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setLink($link) {
        $this->link = $link;
    }

    public function setParentId($parentId) {
        $this->parentId = $parentId;
    }

    public function setLvl($lvl) {
        $this->lvl = $lvl;
    }

    public function setSortOrder($sortOrder) {
        $this->sortOrder = $sortOrder;
    }

    public function setLang($lang) {
        $this->lang = $lang;
    }

    public function getRootId() {
        return $this->rootId;
    }

    public function setRootId($rootId) {
        $this->rootId = $rootId;
    }

    public function getLinkType() {
        return $this->linkType;
    }

    public function getLinkTypeData() {
        return $this->linkTypeData;
    }

    public function setLinkType($linkType) {
        $this->linkType = $linkType;
    }

    public function setLinkTypeData($linkTypeData) {
        $this->linkTypeData = $linkTypeData;
    }

    public function getLinkTypeId() {
        if ($this->getPage()) {
            return $this->getPage()->getId();
        }
        if ('page' == $this->getLinkType()) {
            $data = json_decode($this->getLinkTypeData(), true);
            return $data['id'];
        } else {
            return null;
        }
    }

    public function getPublicLink() {
        if ($this->getPage()) {
            return $this->getPage()->getSeoId();
        }
        if ('page' == $this->getLinkType()) {
            //return  $this->getLink();
            $routeInfo = json_decode($this->getLinkTypeData(), true);
            return $routeInfo['seoId'];
        } else {
            return $this->getLink();
        }
    }


    /**
     * Get the value of siteItemMenu
     */
    public function getSiteItemMenu() {
        return $this->siteItemMenu;
    }

    /**
     * Set the value of siteItemMenu
     *
     * @return  self
     */
    public function setSiteItemMenu($siteItemMenu) {
        $this->siteItemMenu = $siteItemMenu;

        return $this;
    }


    public function getType() {
        return $this->type;
    }

    public function setType($type) {
        $this->type = $type;
        return $this;
    }

    public function setPage(Page $page = null) {
        $this->page = $page;
        return $this;
    }

    /**
     * @return Page
     */
    public function getPage() {
        return $this->page;
    }


    public function setParent(Menu $parent = null) {
        $this->parent = $parent;
        return $this;
    }

    public function getParent(): ?Menu {
        return $this->parent;
    }

    public function addChild(Menu $child): self {
        if (!$this->children->contains($child)) {
            $this->children[] = $child;
            $child->setParent($this);
        }
        return $this;
    }

    public function removeChild(Menu $child): self {
        $this->children->removeElement($child);
        $child->setParent(null);
        return $this;
    }

    public function getChildren() {
        return $this->children;
    }

    public function getAllChildren() {
        $children = $this->getChildren()->toArray();
        $subChildren = [];

        foreach ($this->getChildren() as $ch) {
            foreach ($ch->getAllChildren() as $sub) {
                $subChildren[] = $sub;
            }
        }

        $colection = new ArrayCollection(
            array_merge($children, $subChildren)
        );

        return $colection;
    }

    public function getAllParents($withRoot = false) {
        $parent = $this->getParent();
        $parents = [];

        while ($parent) {
            if ($parent->getRootId() || $withRoot) {
                $parents[] = $parent;
            }
            $parent = $parent->getParent();
        }

        $colection = new ArrayCollection($parents);

        return $colection;
    }
}
