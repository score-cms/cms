<?php

namespace Score\CmsBundle\Entity;

use Score\BaseBundle\Entity\BaseEntity;
use Score\CmsBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Score\CmsBundle\Repository\GroupRepository;

/**
 * @ORM\Entity(repositoryClass="Score\CmsBundle\Repository\GroupRepository")
 * @ORM\Table(name="cms_group")
 * @ORM\HasLifecycleCallbacks()
 */
class Group extends BaseEntity
{
    
    /**
     * @ORM\Column(name="name", type="string", length=100)
     */
    protected $name;

    /**
     * @ORM\Column(name="role", type="string", length=100)
     */
    protected $role;


    public function __toString() {
        return $this->name;
    }

    
    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(string $role): self
    {
        $this->role = strtoupper($role);

        return $this;
    }

    

}
