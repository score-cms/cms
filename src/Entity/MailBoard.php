<?php

namespace Score\CmsBundle\Entity;

use DateTime;
use Score\CmsBundle\Repository\MailBoardRepository;
use Score\BaseBundle\Entity\BaseEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MailBoardRepository::class)
 * @ORM\Table(name="cms_mailer_board")
 * @ORM\HasLifecycleCallbacks()
 */
class MailBoard extends BaseEntity
{
    /**
     * @ORM\Column(name="mail_to", type="string", length=500)
     */
    protected $mailTo;

    /**
     * @ORM\Column(name="mail_from", type="string", length=255)
     */
    protected $mailFrom;

    /**
     * @ORM\Column(name="m_subject", type="string", length=1000)
     */
    protected $subject;

    /**
     * @ORM\Column(name="m_body", type="string", length=4000)
     */
    protected $body;

    /**
     * @ORM\Column(name="is_sent", type="boolean")
     */
    protected $isSent;

    /**
     * @ORM\Column(name="sent_at", type="datetime", nullable=true)
     */
    protected $sentAt;

    /**
     * @ORM\Column(name="attempts", type="integer", nullable=true)
     */
    protected $attempts;

    /**
     * @ORM\Column(name="attempts_log", type="string", length=1000, nullable=true)
     */
    protected $attemptsLog;
    
    /**
     * @ORM\Column(name="last_try", type="datetime", nullable=true)
     */
    protected $lastTry;
    
    /**
     * @ORM\Column(name="first_try", type="datetime", nullable=true)
     */
    protected $firstTry;




    

    public function addAttemptNow()
    {
        $now = new DateTime();

        $this->setAttempts($this->getAttempts() + 1)
                ->setAttemptsLog(
                    ($this->getAttemptsLog() ? $this->getAttemptsLog() . "," : "") . $now->format('c')
                )
                ->setLastTry($now);
    }

    public function setIsSentNow()
    {
        $now = new DateTime();

        $this->setIsSent(true)
            ->setSentAt($now);
    }










    public function getMailTo(): ?string
    {
        return $this->mailTo;
    }

    public function setMailTo(string $mailTo): self
    {
        $this->mailTo = $mailTo;

        return $this;
    }

    public function getMailFrom(): ?string
    {
        return $this->mailFrom;
    }

    public function setMailFrom(string $mailFrom): self
    {
        $this->mailFrom = $mailFrom;

        return $this;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getBody(): ?string
    {
        return $this->body;
    }

    public function setBody(string $body): self
    {
        $this->body = $body;

        return $this;
    }

    public function getIsSent(): ?bool
    {
        return $this->isSent;
    }

    public function setIsSent(bool $isSent): self
    {
        $this->isSent = $isSent;

        return $this;
    }

    public function getSentAt(): ?\DateTimeInterface
    {
        return $this->sentAt;
    }

    public function setSentAt(?\DateTimeInterface $sentAt): self
    {
        $this->sentAt = $sentAt;

        return $this;
    }

    public function getAttempts(): ?int
    {
        return $this->attempts;
    }

    public function setAttempts(?int $attempts): self
    {
        $this->attempts = $attempts;

        return $this;
    }

    public function getLastTry(): ?\DateTimeInterface
    {
        return $this->lastTry;
    }

    public function setLastTry(?\DateTimeInterface $lastTry): self
    {
        $this->lastTry = $lastTry;

        return $this;
    }

    public function getFirstTry(): ?\DateTimeInterface
    {
        return $this->firstTry;
    }

    public function setFirstTry(?\DateTimeInterface $firstTry): self
    {
        $this->firstTry = $firstTry;

        return $this;
    }

    public function getAttemptsLog(): ?string
    {
        return $this->attemptsLog;
    }

    public function setAttemptsLog(?string $attemptsLog): self
    {
        $this->attemptsLog = $attemptsLog;

        return $this;
    }
}
