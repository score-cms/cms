<?php

namespace Score\CmsBundle\Entity\Article;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Score\BaseBundle\Entity\BaseEntity;
use Score\CmsBundle\Entity\Access\Access;
use Score\CmsBundle\Entity\Access\AccessArticle;
use Score\CmsBundle\Entity\Block\BlockJoint;
use Score\CmsBundle\Entity\Block\BlockJointArticle;
use Score\CmsBundle\Entity\Metatag\MetatagArticle;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * @ORM\Entity(repositoryClass="Score\CmsBundle\Repository\ArticleRepository")
 * @ORM\Table(name="cms_article")
 * @ORM\HasLifecycleCallbacks()
 */
class Article extends BaseEntity {

    protected static $author_enum = array
    (
        '1' => '',
        '2' => 'SAŽP',
    );

    public static function getAuthorEnum() {
        return self::$author_enum;
    }

    public function getAuthorName() {
        return self::$author_enum[$this->getAuthor()];
    }


    /**
     * @ORM\Column(type="string", length=500)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $icon;

    /**
     * @ORM\Column(type="text", length=2000, nullable=true)
     */
    protected $teaser;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $body;

    /**
     * @ORM\Column(type="integer")
     */
    protected $published;

    /**
     * @ORM\Column(type="integer")
     */
    protected $createdBy;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $priority;

    /**
     * @ORM\Column(type="string", length=200)
     */
    protected $author;

    /**
     * @ORM\Column(type="datetime", name="published_from",nullable=true)
     */
    protected $publishedFrom;

    /**
     * @ORM\Column(type="datetime", name="published_to",nullable=true)
     */
    protected $publishedTo;

    /**
     * @ORM\Column(type="string", length=255, nullable = true, unique=true)
     */
    protected $slug;

    /**
     * @ORM\Column(type="string",name="seo_title", length=255, nullable=true)
     */
    protected $seoTitle;

    /**
     * @ORM\Column(type="string",length=5000, nullable=true)
     */
    protected $metadata;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $search;

    /**
     * @ORM\OneToMany(targetEntity="\Score\CmsBundle\Entity\Multisite\SiteItemArticle", mappedBy="article",cascade={"persist","remove"})
     */
    protected $siteItemArticle = [];

    /**
     * @ORM\ManyToMany(targetEntity=ArticleCategory::class, inversedBy="articles", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="cms_conn_article_category",
     *      joinColumns={@ORM\JoinColumn(name="article_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")}
     *      )
     * @ORM\OrderBy({"seoName" = "ASC"})
     */
    protected $articleCategories;


    /**
     * @ORM\OneToMany(targetEntity=AccessArticle::class, mappedBy="article", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $accesses;

    /**
     * @ORM\OneToMany(targetEntity=BlockJointArticle::class, mappedBy="article", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"sortOrder"="ASC", "place"="ASC"})
     */
    protected $blockJoints;

    /**
     * @ORM\OneToMany(targetEntity=MetatagArticle::class, mappedBy="article", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $metatags;

    protected $multisite;


    protected $keywords;
    protected $type;


    public function __construct() {
        $this->accesses = new ArrayCollection();
        $this->blockJoints = new ArrayCollection();
        $this->metatags = new ArrayCollection();
        $this->articleCategories = new ArrayCollection();
    }


    public function getPublishedFrom() {
        return $this->publishedFrom;
    }

    public function getPublishedTo() {
        return $this->publishedTo;
    }

    public function getSeoTitle() {
        return $this->seoTitle;
    }

    public function setPublishedFrom($publishedFrom) {
        $this->publishedFrom = $publishedFrom;
        return $this;
    }

    public function setPublishedTo($publishedTo) {
        $this->publishedTo = $publishedTo;
        return $this;
    }

    public function setSeoTitle($seoTitle) {
        $this->seoTitle = $seoTitle;
        return $this;
    }

    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getIcon() {
        return $this->icon;
    }

    public function getTeaser() {
        return $this->teaser;
    }

    public function getBody() {
        return $this->body;
    }

    public function getPublished() {
        return $this->published;
    }

    public function getPriority() {
        return $this->priority;
    }

    public function getAuthor() {
        return $this->author;
    }

    public function getSlug() {
        return $this->slug;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function setIcon($icon) {
        $this->icon = $icon;
        return $this;
    }

    public function setTeaser($teaser) {
        $this->teaser = $teaser;
        return $this;
    }

    public function setBody($body) {
        $this->body = $body;
        return $this;
    }

    public function setPublished($published) {
        $this->published = $published;
        return $this;
    }

    public function setPriority($priority) {
        $this->priority = $priority;
        return $this;
    }

    public function setAuthor($author) {
        $this->author = $author;
        return $this;
    }

    public function setSlug($slug) {
        $this->slug = $slug;
        return $this;
    }

    public function __toString() {
        return $this->getName();
    }

    public function getCreatedBy() {
        return $this->createdBy;
    }

    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
        return $this;
    }

    public function getType() {
        return $this->type;
    }

    public function setType($type) {
        $this->type = $type;
        return $this;
    }

    public function getMetadata() {
        return $this->metadata;
    }

    public function setMetadata($metadata) {
        $this->metadata = $metadata;
        return $this;
    }

    public function iconThumb($width, $height) {
        if (!$this->getIcon()) {
            return '';
//            return 'default_' . $width . 'x' . $height . '.png';
        }

        $parts = explode('.', $this->getIcon());
        $extension = end($parts);
        return str_replace('.' . $extension, '', $this->getIcon()) . '_thumb_' . $width . 'x' . $height . '.' . $extension;
    }

    public function getPublishedDate($format = 'd.m. Y') {
        if ($this->getPublished() == 1) {
            if (null != $this->getPublishedFrom()) {
                return $this->getPublishedFrom()->format($format);
            } else {
                return date($format);
            }
        } else {
            return 'nepublikované';
        }
    }

    public function getStrip() {
        return mb_substr(strip_tags($this->getTeaser()), 0, 200) . '...';
    }

    public function getTeaserStrip($length = 200) {
        $tidy = new \tidy();
        $strip = mb_substr($this->getTeaser(), 0, 200) . '...';
        $strip = $tidy->repairString($strip, array('show-body-only' => true));
        return $strip;
    }

    protected function getMetadataArray() {
        $data = $this->getMetadata();
        $arr = json_decode($data, true);
        return $arr;
    }


    /**
     * Get the value of search
     */
    public function getSearch() {
        return $this->search;
    }

    /**
     * Set the value of search
     *
     * @return  self
     */
    public function setSearch($search) {
        $this->search = $search;
        return $this;
    }


    /**
     * Get the value of siteItemArticle
     */
    public function getSiteItemArticle() {
        return $this->siteItemArticle;
    }

    /**
     * Set the value of siteItemArticle
     *
     * @return  self
     */
    public function setSiteItemArticle($siteItemArticle) {
        $this->siteItemArticle = $siteItemArticle;
        return $this;
    }

    /**
     * Get the value of multisite
     */
    public function getMultisite() {
        return $this->multisite;
    }

    /**
     * Set the value of multisite
     *
     * @return  self
     */
    public function setMultisite($multisite) {
        $this->multisite = $multisite;
        return $this;
    }

    public function getCategoryNames() {
        return implode(", ", $this->getArticleCategories()->map(function ($c) {
            return $c->getName();
        })->toArray());
    }

    /**
     * @return Collection<int, ArticleCategory>
     */
    public function getArticleCategories(): Collection {
        return $this->articleCategories;
    }

    public function addArticleCategory(ArticleCategory $articleCategory): self {
        if (!$this->articleCategories->contains($articleCategory)) {
            $this->articleCategories[] = $articleCategory;
            $articleCategory->addArticle($this);
        }

        return $this;
    }

    public function removeArticleCategory(ArticleCategory $articleCategory): self {
        if ($this->articleCategories->removeElement($articleCategory)) {
            $articleCategory->removeArticle($this);
        }

        return $this;
    }


    /**
     * @return Collection<int, Access>
     */
    public function getAccesses(): Collection {
        return $this->accesses;
    }

    public function addAccess(AccessArticle $access): self {
        if (!$this->accesses->contains($access)) {
            $this->accesses[] = $access;
            $access->setArticle($this);
        }

        return $this;
    }

    public function removeAccess(Access $access): self {
        $this->accesses->removeElement($access);
        return $this;
    }

    /**
     * @return Collection<int, BlockJointPage>
     */
    public function getBlockJoints(): Collection {
        return $this->blockJoints;
    }

    public function addBlockJoint(BlockJointArticle $blockJoint): self {
        if (!$this->blockJoints->contains($blockJoint)) {
            $this->blockJoints[] = $blockJoint;
            $blockJoint->setArticle($this);
        }
        return $this;
    }

    public function removeBlockJoint(BlockJoint $blockJoint): self {
        $this->blockJoints->removeElement($blockJoint);
        return $this;
    }


    /**
     * @return Collection<int, MetatagArticle>
     */
    public function getMetatags(): Collection {
        return $this->metatags;
    }

    public function addMetatag(MetatagArticle $metatag): self {
        if (!$this->metatags->contains($metatag)) {
            $this->metatags[] = $metatag;
            $metatag->setArticle($this);
        }
        return $this;
    }

    public function removeMetatag(MetatagArticle $metatag): self {
        $this->metatags->removeElement($metatag);
        return $this;
    }
}
