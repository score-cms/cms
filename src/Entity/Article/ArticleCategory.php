<?php

namespace Score\CmsBundle\Entity\Article;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Score\BaseBundle\Entity\BaseEntity;

/**
 * @ORM\Entity()
 * @ORM\Table(name="cms_article_category")
 * @ORM\HasLifecycleCallbacks()
 */
class ArticleCategory extends BaseEntity {

    /**
     * @ORM\Column(name="a_name", type="string", length=1000)
     */
    protected $name;

    /**
     * @ORM\Column(name="a_public", type="boolean", nullable=true)
     */
    protected $public;

    /**
     * @ORM\Column(name="a_seo_name", type="string", length=1000, nullable=true)
     */
    protected $seoName;

    /**
     * @ORM\ManyToMany(targetEntity=Article::class, mappedBy="articleCategories")
     */
    protected $articles;

    public function __construct() {
        $this->articles = new ArrayCollection();
    }

    public function __toString() {
        return $this->name;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getPublic(): ?bool {
        return $this->public;
    }

    public function setPublic(?bool $public): self {
        $this->public = $public;

        return $this;
    }

    public function getSeoName(): ?string {
        return $this->seoName;
    }

    public function setSeoName(?string $seoName): self {
        $this->seoName = $seoName;

        return $this;
    }

    /**
     * @return Collection<int, Article>
     */
    public function getArticles(): Collection {
        return $this->articles;
    }

    public function addArticle(Article $article): self {
        if (!$this->articles->contains($article)) {
            $this->articles[] = $article;
        }

        return $this;
    }

    public function removeArticle(Article $article): self {
        $this->articles->removeElement($article);

        return $this;
    }
}
