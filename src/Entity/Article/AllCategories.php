<?php

namespace Score\CmsBundle\Entity\Article;

use Doctrine\Common\Collections\ArrayCollection;

class AllCategories {

    protected $categories;

    public function __construct($categories) {
        $this->categories = new ArrayCollection($categories);
    }

    public function getCategories() {
        return $this->categories;
    }
}
