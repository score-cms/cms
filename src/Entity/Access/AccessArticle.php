<?php

namespace Score\CmsBundle\Entity\Access;

use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Expr\Comparison;
use Score\CmsBundle\Repository\AccessRepository;
use Doctrine\ORM\Mapping as ORM;
use Score\BaseBundle\Entity\BaseEntity;
use Score\CmsBundle\Entity\Article\Article;
use Score\CmsBundle\Entity\Document\Document;
use Score\CmsBundle\Entity\Event\Event;
use Score\CmsBundle\Entity\Page\Page;
use Score\CmsBundle\Entity\User;

/**
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class AccessArticle extends Access {

    /**
     * @ORM\ManyToOne(targetEntity=Article::class, inversedBy="accesses")
     * @ORM\JoinColumn(name="entity_id", referencedColumnName="id")
     */
    protected $article;

    public function getArticle(): ?Article {
        return $this->article;
    }

    public function setArticle(?Article $article): self {
        $this->article = $article;
        return $this;
    }

}
