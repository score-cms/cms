<?php

namespace Score\CmsBundle\Entity\Access;

use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Expr\Comparison;
use Score\CmsBundle\Repository\AccessRepository;
use Doctrine\ORM\Mapping as ORM;
use Score\BaseBundle\Entity\BaseEntity;
use Score\CmsBundle\Entity\Article\Article;
use Score\CmsBundle\Entity\Document\Document;
use Score\CmsBundle\Entity\Event\Event;
use Score\CmsBundle\Entity\Page\Page;
use Score\CmsBundle\Entity\User;

/**
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class AccessEvent extends Access {

    /**
     * @ORM\ManyToOne(targetEntity=Event::class, inversedBy="accesses")
     * @ORM\JoinColumn(name="entity_id", referencedColumnName="id")
     */
    protected $event;

    public function getEvent(): ?Event {
        return $this->event;
    }

    public function setEvent(?Event $event): self {
        $this->event = $event;
        return $this;
    }
}
