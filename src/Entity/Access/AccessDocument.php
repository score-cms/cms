<?php

namespace Score\CmsBundle\Entity\Access;

use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Expr\Comparison;
use Score\CmsBundle\Repository\AccessRepository;
use Doctrine\ORM\Mapping as ORM;
use Score\BaseBundle\Entity\BaseEntity;
use Score\CmsBundle\Entity\Article\Article;
use Score\CmsBundle\Entity\Document\Document;
use Score\CmsBundle\Entity\Event\Event;
use Score\CmsBundle\Entity\Page\Page;
use Score\CmsBundle\Entity\User;

/**
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class AccessDocument extends Access {

    /**
     * @ORM\ManyToOne(targetEntity=Document::class, inversedBy="accesses")
     * @ORM\JoinColumn(name="entity_id", referencedColumnName="id")
     */
    protected $document;


    public function getDocument(): ?Document {
        return $this->document;
    }

    public function setDocument(?Document $document): self {
        $this->document = $document;
        return $this;
    }
}
