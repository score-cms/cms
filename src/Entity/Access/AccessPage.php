<?php

namespace Score\CmsBundle\Entity\Access;

use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Expr\Comparison;
use Score\CmsBundle\Repository\AccessRepository;
use Doctrine\ORM\Mapping as ORM;
use Score\BaseBundle\Entity\BaseEntity;
use Score\CmsBundle\Entity\Article\Article;
use Score\CmsBundle\Entity\Document\Document;
use Score\CmsBundle\Entity\Event\Event;
use Score\CmsBundle\Entity\Page\Page;
use Score\CmsBundle\Entity\User;

/**
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class AccessPage extends Access {

    /**
     * @ORM\ManyToOne(targetEntity=Page::class, inversedBy="accesses")
     * @ORM\JoinColumn(name="entity_id", referencedColumnName="id")
     */
    protected $page;


    public function getPage(): ?Page {
        return $this->page;
    }

    public function setPage(?Page $page): self {
        $this->page = $page;
        return $this;
    }

    function getIsHeritable() {
        return $this->getParamByKey("is_heritable", false);
    }

    function setIsHeritable($isHeritable): self {
        $this->setParamByKey("is_heritable", $isHeritable);
        return $this;
    }
}
