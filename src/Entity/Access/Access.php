<?php

namespace Score\CmsBundle\Entity\Access;

use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Expr\Comparison;
use Score\CmsBundle\Repository\AccessRepository;
use Doctrine\ORM\Mapping as ORM;
use Score\BaseBundle\Entity\BaseEntity;
use Score\CmsBundle\Entity\Article\Article;
use Score\CmsBundle\Entity\Document\Document;
use Score\CmsBundle\Entity\Event\Event;
use Score\CmsBundle\Entity\Page\Page;
use Score\CmsBundle\Entity\User;
use Score\CmsBundle\Entity\Access\AccessArticle;

/**
 * @ORM\Entity(repositoryClass=AccessRepository::class)
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="access_entity", type="string")
 * @ORM\DiscriminatorMap({"article"="AccessArticle", "document"="AccessDocument", "event"="AccessEvent", "page"="AccessPage" })
 * @ORM\Table(name="cms_access")
 * @ORM\HasLifecycleCallbacks()
 */
class Access extends BaseEntity {

    /**
     * @ORM\Column(name="a_role", type="string", length=255, nullable=true)
     */
    protected $role;

    /**
     * @ORM\Column(name="a_params", type="string", length=2000, nullable=true)
     */
    protected $params;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="accesses")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    protected $user;

    /**
     * @ORM\Column(name="entity_id", type="integer", nullable=true)
     */
    protected $entityId;


    public function getRole(): ?string {
        return $this->role;
    }

    public function setRole(?string $role): self {
        $this->role = $role;

        return $this;
    }

    public function getParams(): ?string {
        return $this->params;
    }

    public function setParams(?string $params): self {
        $this->params = $params;

        return $this;
    }

    public function getParamsArr(): ?array {
        return json_decode($this->params, true);
    }

    public function setParamsArr(?array $params): self {
        $this->params = json_encode($params);

        return $this;
    }

    public function getParamByKey($key, $defaultValue = null) {
        $arr = $this->getParamsArr();
        if (is_array($arr) && array_key_exists($key, $arr)) {
            return $arr[$key];
        }
        return $defaultValue;
    }

    public function setParamByKey($key, $value) {
        $arr = $this->getParamsArr();
        if (is_array($arr)) {
            $arr[$key] = $value;
        } else {
            $arr = [$key => $value];
        }
        return $this->setParamsArr($arr);
    }

    public function getUser(): ?User {
        return $this->user;
    }

    public function setUser(?User $user): self {
        $this->user = $user;

        return $this;
    }
}
