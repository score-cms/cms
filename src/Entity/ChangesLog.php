<?php

namespace Score\CmsBundle\Entity;

use Score\CmsBundle\Repository\ChangesLogRepository;
use Doctrine\ORM\Mapping as ORM;
use Score\BaseBundle\Entity\BaseEntity;

/**
 * @ORM\Entity(repositoryClass=ChangesLogRepository::class)
 * @ORM\Table(name="cms_log")
 * @ORM\HasLifecycleCallbacks()
 */
class ChangesLog extends BaseEntity
{
    /**
     * @ORM\Column(name="user_id", type="integer", nullable=true)
     */
    protected $userId;

    /**
     * @ORM\Column(name="user_email", type="string", length=255, nullable=true)
     */
    protected $userEmail;

    /**
     * @ORM\Column(name="identifier", type="string", length=255, nullable=true)
     */
    protected $identifier;

    /**
     * @ORM\Column(name="event_name", type="string", length=255, nullable=true)
     */
    protected $event;

    /**
     * @ORM\Column(name="entity_name", type="string", length=500, nullable=true)
     */
    protected $entity;

    /**
     * @ORM\Column(name="entity_id", type="integer", nullable=true)
     */
    protected $entityId;

    /**
     * @ORM\Column(name="field_name", type="string", length=255, nullable=true)
     */
    protected $field;

    /**
     * @ORM\Column(name="old_value", type="text", nullable=true)
     */
    protected $oldValue;

    /**
     * @ORM\Column(name="new_value", type="text", nullable=true)
     */
    protected $newValue;

    /**
     * @ORM\Column(name="can_revert", type="boolean", nullable=true)
     */
    protected $canRevert;




    public function getEntity(): ?string
    {
        return $this->entity;
    }

    public function setEntity(?string $entity): self
    {
        $this->entity = $entity;

        return $this;
    }

    public function getField(): ?string
    {
        return $this->field;
    }

    public function setField(?string $field): self
    {
        $this->field = $field;

        return $this;
    }

    public function getOldValue(): ?string
    {
        return $this->oldValue;
    }

    public function setOldValue(?string $oldValue): self
    {
        $this->oldValue = $oldValue;

        return $this;
    }

    public function getNewValue(): ?string
    {
        return $this->newValue;
    }

    public function setNewValue(?string $newValue): self
    {
        $this->newValue = $newValue;

        return $this;
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function setUserId(?int $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getUserEmail(): ?string
    {
        return $this->userEmail;
    }

    public function setUserEmail(?string $userEmail): self
    {
        $this->userEmail = $userEmail;

        return $this;
    }

    public function getEvent(): ?string
    {
        return $this->event;
    }

    public function setEvent(?string $event): self
    {
        $this->event = $event;

        return $this;
    }

    public function getEntityId(): ?int
    {
        return $this->entityId;
    }

    public function setEntityId(?int $entityId): self
    {
        $this->entityId = $entityId;

        return $this;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(?string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    
    public function getCanRevert(): ?bool
    {
        return $this->canRevert;
    }

    public function setCanRevert(?bool $canRevert): self
    {
        $this->canRevert = $canRevert;

        return $this;
    }


}
