<?php

namespace Score\CmsBundle\Entity\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Score\BaseBundle\Entity\BaseEntity;

/**
 * @ORM\Entity()
 * @ORM\Table(name="cms_document_description")
 * @ORM\HasLifecycleCallbacks()
 */
class DocumentDescription extends BaseEntity
{
    /**
     * @ORM\Column(name="d_description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(name="d_public", type="boolean", nullable=true)
     */
    protected $public;

    /**
     * @ORM\ManyToOne(targetEntity=Document::class, inversedBy="documentDescriptions")
     * @ORM\JoinColumn(name="document_id", referencedColumnName="id")
     */
    protected $document;

    /**
     * @ORM\ManyToOne(targetEntity=DocumentDomain::class, inversedBy="documentDescriptions")
     * @ORM\JoinColumn(name="document_domain_id", referencedColumnName="id")
     */
    protected $documentDomain;

    /**
     * @ORM\Column(name="d_sort_order", type="integer", nullable=true)
     */
    protected $sortOrder;

    
    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPublic(): ?bool
    {
        return $this->public;
    }

    public function setPublic(?bool $public): self
    {
        $this->public = $public;

        return $this;
    }

    public function getDocumentDomain(): ?DocumentDomain
    {
        return $this->documentDomain;
    }

    public function setDocumentDomain(?DocumentDomain $documentDomain): self
    {
        $this->documentDomain = $documentDomain;

        return $this;
    }

    public function getDocument(): ?Document
    {
        return $this->document;
    }

    public function setDocument(?Document $document): self
    {
        $this->document = $document;

        return $this;
    }
    
    public function getSortOrder(): ?int
    {
        return $this->sortOrder;
    }

    public function setSortOrder(?int $sortOrder): self
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }


}
