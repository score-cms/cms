<?php

namespace Score\CmsBundle\Entity\Document;

use Doctrine\Common\Collections\ArrayCollection;

class AllLevels
{

    protected $levels;

    public function __construct($levels)
    {
        $this->levels = new ArrayCollection($levels);
    }

    public function getLevels()
    {
        return $this->levels;
    }
}
