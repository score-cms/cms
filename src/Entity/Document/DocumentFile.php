<?php

namespace Score\CmsBundle\Entity\Document;

use Doctrine\ORM\Mapping as ORM;
use Score\BaseBundle\Entity\BaseEntity;

/**
 * @ORM\Entity()
 * @ORM\Table(name="cms_document_file")
 * @ORM\HasLifecycleCallbacks()
 */
class DocumentFile extends BaseEntity {
    /**
     * @ORM\ManyToOne(targetEntity=Document::class, inversedBy="documentFiles")
     * @ORM\JoinColumn(name="document_id", referencedColumnName="id")
     */
    protected $document;

    /**
     * @ORM\Column(name="d_is_external", type="boolean", nullable=true)
     */
    protected $isExternal;

    /**
     * @ORM\Column(name="d_url", type="string", length=500)
     */
    protected $url;

    /**
     * @ORM\Column(name="d_filename", type="string", length=1000, nullable=true)
     */
    protected $filename;

    /**
     * @ORM\Column(name="d_size", type="string", length=255, nullable=true)
     */
    protected $size;

    /**
     * @ORM\Column(name="d_type", type="string", length=255, nullable=true)
     */
    protected $type;

    /**
     * @ORM\Column(name="d_language", type="string", length=255, nullable=true)
     */
    protected $language;

    /**
     * @ORM\Column(name="d_info", type="string", length=255)
     */
    protected $info;

    /**
     * @ORM\Column(name="d_is_current", type="boolean", nullable=true)
     */
    protected $isCurrent;

    /**
     * @ORM\Column(name="d_code", type="string", length=255, nullable=true)
     */
    protected $code;

    /**
     * @ORM\Column(name="d_extension", type="string", length=255, nullable=true)
     */
    protected $extension;


    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preEditedInfoUpdate() {

        if ($this->getIsExternal())
            return;

        $stringSize = $this->parseSizeString($this->getSize());
        $this->setInfo(strtoupper($this->getExtension()) . ", " . $stringSize);

        if ($this->getLanguage()) {
            $this->setInfo($this->getLanguage() . ', ' . $this->getInfo());
        }
    }

    public function getSizeHumanReadable() {
        return $this->parseSizeString($this->getSize());
    }

    /**
     * Converts bytes into human readable file size.
     *
     * @param string $bytes
     * @return string human readable file size (2,87 Мб)
     * @author Mogilev Arseny
     */
    protected function parseSizeString($bytes) {
        $result = null;
        $bytes = floatval($bytes);
        $arBytes = [
            [
                "UNIT" => "TB",
                "VALUE" => pow(1024, 4)
            ], [
                "UNIT" => "GB",
                "VALUE" => pow(1024, 3)
            ], [
                "UNIT" => "MB",
                "VALUE" => pow(1024, 2)
            ], [
                "UNIT" => "KB",
                "VALUE" => 1024
            ], [
                "UNIT" => "B",
                "VALUE" => 1
            ],
        ];

        foreach ($arBytes as $arItem) {
            if ($bytes >= $arItem["VALUE"]) {
                $result = $bytes / $arItem["VALUE"];
                $result = str_replace(".", ",", strval(round($result, 1))) . " " . $arItem["UNIT"];
                break;
            }
        }
        return $result;
    }

    public function getDocument(): ?Document {
        return $this->document;
    }

    public function setDocument(?Document $document): self {
        $this->document = $document;

        return $this;
    }

    public function getIsExternal(): ?bool {
        return $this->isExternal;
    }

    public function setIsExternal(?bool $isExternal): self {
        $this->isExternal = $isExternal;

        return $this;
    }

    public function getUrl(): ?string {
        return $this->url;
    }

    public function setUrl(string $url): self {
        $this->url = $url;

        return $this;
    }

    public function getFilename(): ?string {
        return $this->filename;
    }

    public function setFilename(string $filename): self {
        $this->filename = $filename;

        return $this;
    }

    public function getSize(): ?string {
        return $this->size;
    }

    public function setSize(?string $size): self {
        $this->size = $size;

        return $this;
    }

    public function getType(): ?string {
        return $this->type;
    }

    public function setType(?string $type): self {
        $this->type = $type;

        return $this;
    }

    public function getLanguage(): ?string {
        return $this->language;
    }

    public function setLanguage(?string $language): self {
        $this->language = $language;

        return $this;
    }

    public function getInfo(): ?string {
        return $this->info;
    }

    public function setInfo(string $info): self {
        $this->info = $info;

        return $this;
    }

    public function getIsCurrent(): ?bool {
        return $this->isCurrent;
    }

    public function setIsCurrent(?bool $isCurrent): self {
        $this->isCurrent = $isCurrent;

        return $this;
    }

    public function getCode(): ?string {
        return $this->code;
    }

    public function setCode(?string $code): self {
        $this->code = $code;

        return $this;
    }

    public function getExtension(): ?string {
        return $this->extension;
    }

    public function setExtension(?string $extension): self {
        $this->extension = $extension;

        return $this;
    }

}
