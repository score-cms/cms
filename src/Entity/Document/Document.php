<?php

namespace Score\CmsBundle\Entity\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Score\BaseBundle\Entity\BaseEntity;
use Score\CmsBundle\Entity\Access\Access;
use Score\CmsBundle\Entity\Access\AccessDocument;
use Score\CmsBundle\Entity\Block\BlockJoint;
use Score\CmsBundle\Entity\Block\BlockJointDocument;
use Score\CmsBundle\Entity\Metatag\MetatagDocument;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @UniqueEntity("seoId",message="document.seoId.unique")
 * @ORM\Entity()
 * @ORM\Table(name="cms_document")
 * @ORM\HasLifecycleCallbacks()
 */
class Document extends BaseEntity {
    /**
     * @ORM\Column(name="d_name", type="string", length=1000)
     */
    protected $name;

    /**
     * @ORM\Column(name="d_full_name", type="string", length=2000)
     */
    protected $fullName;

    /**
     * @ORM\Column(name="d_teaser", type="string", length=2000, nullable=true)
     */
    protected $teaser;

    /**
     * @ORM\Column(name="d_description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(name="d_has_detail", type="boolean")
     */
    protected $hasDetail = true;

    /**
     * @ORM\Column(name="d_public", type="boolean", nullable=true)
     */
    protected $public = true;

    /**
     * @ORM\Column(name="d_search", type="text", nullable=true)
     */
    protected $search;

    /**
     * @ORM\Column(name="d_version", type="integer", nullable=true)
     */
    protected $version;

    /**
     * @ORM\Column(name="d_code", type="string", length=255, nullable=true, unique=true)
     */
    protected $code;

    /**
     * @ORM\Column(name="d_seoId", type="string", length=500, unique=true)
     */
    protected $seoId;

    /**
     * @ORM\Column(name="d_year", type="string", length=250, nullable=true)
     */
    protected $year;

    /**
     * @ORM\Column(name="d_icon", type="string", length=1000, nullable=true)
     */
    protected $icon;

    /**
     * @ORM\Column(name="d_params", type="string", length=2000, nullable=true)
     */
    protected $params;

    /**
     * @ORM\ManyToMany(targetEntity=DocumentLevel::class, inversedBy="documents", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="cms_conn_document_level",
     *      joinColumns={@ORM\JoinColumn(name="document_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="Level_id", referencedColumnName="id")}
     *      )
     * @ORM\OrderBy({"name" = "ASC"})
     */
    protected $documentLevels;

    /**
     * @ORM\ManyToMany(targetEntity=DocumentCategory::class, inversedBy="documents", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="cms_conn_document_category",
     *      joinColumns={@ORM\JoinColumn(name="document_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")}
     *      )
     * @ORM\OrderBy({"name" = "ASC"})
     */
    protected $documentCategories;

    /**
     * @ORM\OneToMany(targetEntity=DocumentDescription::class, mappedBy="document", cascade={"persist", "remove"})
     * @ORM\OrderBy({"sortOrder" = "ASC"})
     */
    protected $documentDescriptions;

    /**
     * @ORM\OneToMany(targetEntity=DocumentFile::class, mappedBy="document", cascade={"persist", "remove"})
     * @ORM\OrderBy({"createdAt" = "ASC"})
     */
    protected $documentFiles;

    /**
     * Many Documents have Many Documents. // vracia len jednu cast spolocnych
     * @ORM\ManyToMany(targetEntity="Document", mappedBy="myRelevants")
     */
    protected $relevantsWithMe;

    /**
     * Many Documents have many Documents. // vracia vsetky spolocne obojstranne
     * @ORM\ManyToMany(targetEntity="Document", inversedBy="relevantsWithMe")
     * @ORM\JoinTable(name="cms_conn_document_document",
     *      joinColumns={@ORM\JoinColumn(name="document_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="relevant_document_id", referencedColumnName="id")}
     *      )
     * @ORM\OrderBy({"name" = "ASC"})
     */
    protected $myRelevants;

    /**
     * @ORM\OneToMany(targetEntity=AccessDocument::class, mappedBy="document", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $accesses;

    /**
     * @ORM\OneToMany(targetEntity=BlockJointDocument::class, mappedBy="document", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"sortOrder"="ASC", "place"="ASC"})
     */
    protected $blockJoints;

    /**
     * @ORM\OneToMany(targetEntity=MetatagDocument::class, mappedBy="document", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $metatags;


    public function __construct() {
        $this->accesses = new ArrayCollection();
        $this->blockJoints = new ArrayCollection();
        $this->metatags = new ArrayCollection();
        $this->documentCategories = new ArrayCollection();
        $this->documentLevels = new ArrayCollection();
        $this->documentDescriptions = new ArrayCollection();
        $this->documentFiles = new ArrayCollection();
        $this->relevantsWithMe = new ArrayCollection();
        $this->myRelevants = new ArrayCollection();
    }

    public function __toString() {
//        return $this->fullName;
        return $this->getName();
    }

    protected $actualFile = null;

    public function getActualFile(): ?DocumentFile {
        if (!$this->actualFile) {
            $file = $this->getDocumentFiles()->filter(function ($f) {
                return $f->getIsCurrent() === true;
            })->last(); // najde file alebo false
            $this->actualFile = $file ?: null;
        }
        return $this->actualFile;
    }


    public function iconThumb($width = 100, $height = 100) {
        if (!$this->getIcon()) {
            return '';
//            return 'default_' . $width . 'x' . $height . '.png';
        }

        $parts = explode('.', $this->getIcon());
        $extension = end($parts);
        return str_replace('.' . $extension, '', $this->getIcon()) . '_thumb_' . $width . 'x' . $height . '.' . $extension;
    }


    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getFullName(): ?string {
        return $this->fullName;
    }

    public function setFullName(string $fullName): self {
        $this->fullName = $fullName;

        return $this;
    }

    public function getTeaser(): ?string {
        return $this->teaser;
    }

    public function setTeaser(?string $teaser): self {
        $this->teaser = $teaser;

        return $this;
    }

    public function getDescription(): ?string {
        return $this->description;
    }

    public function setDescription(?string $description): self {
        $this->description = $description;

        return $this;
    }

    public function getHasDetail(): ?bool {
        return $this->hasDetail;
    }

    public function setHasDetail(bool $hasDetail): self {
        $this->hasDetail = $hasDetail;

        return $this;
    }

    public function getPublic(): ?bool {
        return $this->public;
    }

    public function setPublic(?bool $public): self {
        $this->public = $public;

        return $this;
    }

    public function getSearch(): ?string {
        return $this->search;
    }

    public function setSearch(?string $search): self {
        $this->search = $search;

        return $this;
    }

    public function getVersion(): ?int {
        return $this->version;
    }

    public function setVersion(?int $version): self {
        $this->version = $version;

        return $this;
    }

    public function getCode(): ?string {
        return $this->code;
    }

    public function setCode(?string $code): self {
        $this->code = $code;
        return $this;
    }

    public function getSeoId(): ?string {
        return $this->seoId;
    }

    public function setSeoId(string $seoId): self {
        $this->seoId = $seoId;
        return $this;
    }


    public function getYear(): ?string {
        return $this->year;
    }

    public function setYear(?string $year): self {
        $this->year = $year;
        return $this;
    }


    public function getIcon(): ?string {
        return $this->icon;
    }

    public function setIcon(?string $icon): self {
        $this->icon = $icon;
        return $this;
    }


    public function getParams(): ?string {
        return $this->params;
    }

    public function setParams(?string $params): self {
        $this->params = $params;
        return $this;
    }


    public function setParamsArr(?array $params): self {
        $this->params = json_encode($params);
        return $this;
    }

    public function getParamsArr(): ?array {
        return json_decode($this->params, true);
    }

    public function getParamByKey($key, $defaultValue = null) {
        $arr = $this->getParamsArr();
        if (is_array($arr) && array_key_exists($key, $arr)) {
            return $arr[$key];
        }
        return $defaultValue;
    }

    public function setParamByKey($key, $value): self {
        $arr = $this->getParamsArr();
        if (is_array($arr)) {
            $arr[$key] = $value;
        } else {
            $arr = [$key => $value];
        }
        return $this->setParamsArr($arr);
    }

    /**
     * @return Collection<int, DocumentLevel>
     */
    public function getDocumentLevels(): Collection {
        return $this->documentLevels;
    }

    public function addDocumentLevel(DocumentLevel $documentLevel): self {
        if (!$this->documentLevels->contains($documentLevel)) {
            $this->documentLevels[] = $documentLevel;
            $documentLevel->addDocument($this);
        }

        return $this;
    }

    public function removeDocumentLevel(DocumentLevel $documentLevel): self {
        if ($this->documentLevels->removeElement($documentLevel)) {
            $documentLevel->removeDocument($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, DocumentCategory>
     */
    public function getDocumentCategories(): Collection {
        return $this->documentCategories;
    }

    public function addDocumentCategory(DocumentCategory $documentCategory): self {
        if (!$this->documentCategories->contains($documentCategory)) {
            $this->documentCategories[] = $documentCategory;
            $documentCategory->addDocument($this);
        }

        return $this;
    }

    public function removeDocumentCategory(DocumentCategory $documentCategory): self {
        if ($this->documentCategories->removeElement($documentCategory)) {
            $documentCategory->removeDocument($this);
        }

        return $this;
    }


    /**
     * @return Collection<int, DocumentFile>
     */
    public function getDocumentFiles(): Collection {
        return $this->documentFiles;
    }

    public function addDocumentFile(DocumentFile $documentFile): self {
        if (!$this->documentFiles->contains($documentFile)) {
            $this->documentFiles[] = $documentFile;
            $documentFile->setDocument($this);
        }

        return $this;
    }

    public function removeDocumentFile(DocumentFile $documentFile): self {
        if ($this->documentFiles->removeElement($documentFile)) {
            // set the owning side to null (unless already changed)
            if ($documentFile->getDocument() === $this) {
                $documentFile->setDocument(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, DocumentDescription>
     */
    public function getDocumentDescriptions(): Collection {
        return $this->documentDescriptions;
    }

    public function addDocumentDescription(DocumentDescription $documentDescription): self {
        if (!$this->documentDescriptions->contains($documentDescription)) {
            $this->documentDescriptions[] = $documentDescription;
            $documentDescription->setDocument($this);
        }

        return $this;
    }

    public function removeDocumentDescription(DocumentDescription $documentDescription): self {
        if ($this->documentDescriptions->removeElement($documentDescription)) {
            // set the owning side to null (unless already changed)
            if ($documentDescription->getDocument() === $this) {
                $documentDescription->setDocument(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Document>
     */
    public function getRelevantsWithMe(): Collection {
        return $this->relevantsWithMe;
    }

    public function addRelevantsWithMe(Document $relevantWithMe): self {
        if (!$this->relevantsWithMe->contains($relevantWithMe)) {
            //$this->relevantsWithMe[] = $relevantWithMe;
            $relevantWithMe->addMyRelevant($this);
        }

        return $this;
    }

    public function removeRelevantsWithMe(Document $relevantWithMe): self {
        if ($this->relevantsWithMe->removeElement($relevantWithMe)) {
            $relevantWithMe->removeMyRelevant($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Document>
     */
    public function getMyRelevants(): Collection {
        // return $this->myRelevants;
        return new ArrayCollection(
            array_merge($this->myRelevants->toArray(), $this->relevantsWithMe->toArray())
        );
    }

    public function addMyRelevant(Document $myRelevant): self {
        if (!$this->myRelevants->contains($myRelevant)) {
            $this->myRelevants[] = $myRelevant;
            //$myRelevant->addRelevantsWithMe($this);
        }

        return $this;
    }

    public function removeMyRelevant(Document $myRelevant): self {
        $this->myRelevants->removeElement($myRelevant);
        $this->removeRelevantsWithMe($myRelevant);

        return $this;
    }

    public function getExtension(): ?string {
        return $this->getActualFile() && !$this->getActualFile()->getIsExternal()
            ? ("." . $this->getActualFile()->getExtension())
            : "";
    }

    public function getInfo(): ?string {
        return $this->getActualFile()
            ? (" (" . $this->getActualFile()->getInfo() . ")")
            : "";
    }


    /**
     * @return Collection<int, Access>
     */
    public function getAccesses(): Collection {
        return $this->accesses;
    }

    public function addAccess(AccessDocument $access): self {
        if (!$this->accesses->contains($access)) {
            $this->accesses[] = $access;
            $access->setDocument($this);
        }

        return $this;
    }

    public function removeAccess(Access $access): self {
        $this->accesses->removeElement($access);
        return $this;
    }

    /**
     * @return Collection<int, BlockJointDocument>
     */
    public function getBlockJoints(): Collection {
        return $this->blockJoints;
    }

    public function addBlockJoint(BlockJointDocument $blockJoint): self {
        if (!$this->blockJoints->contains($blockJoint)) {
            $this->blockJoints[] = $blockJoint;
            $blockJoint->setDocument($this);
        }
        return $this;
    }

    public function removeBlockJoint(BlockJoint $blockJoint): self {
        $this->blockJoints->removeElement($blockJoint);
        return $this;
    }

    /**
     * @return Collection<int, MetatagDocument >
     */
    public function getMetatags(): Collection {
        return $this->metatags;
    }

    public function addMetatag(MetatagDocument $metatag): self {
        if (!$this->metatags->contains($metatag)) {
            $this->metatags[] = $metatag;
            $metatag->setDocument($this);
        }
        return $this;
    }

    public function removeMetatag(MetatagDocument $metatag): self {
        $this->metatags->removeElement($metatag);
        return $this;
    }

}
