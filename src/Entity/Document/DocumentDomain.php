<?php

namespace Score\CmsBundle\Entity\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Score\BaseBundle\Entity\BaseEntity;

/**
 * @ORM\Entity()
 * @ORM\Table(name="cms_document_domain")
 * @ORM\HasLifecycleCallbacks()
 */
class DocumentDomain extends BaseEntity
{
    /**
     * @ORM\Column(name="d_name", type="string", length=1000)
     */
    protected $name;

    /**
     * @ORM\Column(name="d_public", type="boolean", nullable=true)
     */
    protected $public;

    /**
     * @ORM\Column(name="d_seo_name", type="string", length=1000, nullable=true)
     */
    protected $seoName;

    /**
     * @ORM\OneToMany(targetEntity=DocumentDescription::class, mappedBy="documentDomain", cascade={"persist", "remove"})
     */
    protected $documentDescriptions;

    public function __construct()
    {
        $this->documentDescriptions = new ArrayCollection();
    }

    
    public function __toString()
    {
        return $this->name;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPublic(): ?bool
    {
        return $this->public;
    }

    public function setPublic(?bool $public): self
    {
        $this->public = $public;

        return $this;
    }

    public function getSeoName(): ?string
    {
        return $this->seoName;
    }

    public function setSeoName(?string $seoName): self
    {
        $this->seoName = $seoName;

        return $this;
    }

    /**
     * @return Collection<int, DocumentDescription>
     */
    public function getDocumentDescriptions(): Collection
    {
        return $this->documentDescriptions;
    }

    public function addDocumentDescription(DocumentDescription $documentDescription): self
    {
        if (!$this->documentDescriptions->contains($documentDescription)) {
            $this->documentDescriptions[] = $documentDescription;
            $documentDescription->setDocumentDomain($this);
        }

        return $this;
    }

    public function removeDocumentDescription(DocumentDescription $documentDescription): self
    {
        if ($this->documentDescriptions->removeElement($documentDescription)) {
            // set the owning side to null (unless already changed)
            if ($documentDescription->getDocumentDomain() === $this) {
                $documentDescription->setDocumentDomain(null);
            }
        }

        return $this;
    }


}
