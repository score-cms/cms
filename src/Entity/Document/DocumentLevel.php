<?php

namespace Score\CmsBundle\Entity\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Score\BaseBundle\Entity\BaseEntity;

/**
 * @ORM\Entity()
 * @ORM\Table(name="cms_document_level")
 * @ORM\HasLifecycleCallbacks()
 */
class DocumentLevel extends BaseEntity
{
    /**
     * @ORM\Column(name="d_name", type="string", length=1000)
     */
    protected $name;

    /**
     * @ORM\Column(name="d_public", type="boolean", nullable=true)
     */
    protected $public;

    /**
     * @ORM\Column(name="d_seo_name", type="string", length=1000, nullable=true)
     */
    protected $seoName;

    /**
     * @ORM\ManyToMany(targetEntity=Document::class, mappedBy="documentLevels")
     */
    protected $documents;

    public function __construct()
    {
        $this->documents = new ArrayCollection();
    }

    
    public function __toString()
    {
        return $this->name;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPublic(): ?bool
    {
        return $this->public;
    }

    public function setPublic(?bool $public): self
    {
        $this->public = $public;

        return $this;
    }

    public function getSeoName(): ?string
    {
        return $this->seoName;
    }

    public function setSeoName(?string $seoName): self
    {
        $this->seoName = $seoName;

        return $this;
    }

    /**
     * @return Collection<int, Document>
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function addDocument(Document $document): self
    {
        if (!$this->documents->contains($document)) {
            $this->documents[] = $document;
        }

        return $this;
    }

    public function removeDocument(Document $document): self
    {
        $this->documents->removeElement($document);

        return $this;
    }
}
