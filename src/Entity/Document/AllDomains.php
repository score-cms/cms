<?php

namespace Score\CmsBundle\Entity\Document;
use Doctrine\Common\Collections\ArrayCollection;

class AllDomains
{

    protected $domains;

    public function __construct($domains)
    {
        $this->domains = new ArrayCollection($domains);
    }

    public function getDomains()
    {
        return $this->domains;
    }
}
