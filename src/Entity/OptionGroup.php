<?php

namespace Score\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Score\BaseBundle\Entity\BaseEntity;
use \Score\CmsBundle\Entity\Option;
use \Score\CmsBundle\Entity\User;

/**
 * @ORM\Entity
 * @ORM\Table(name="cms_optiongroup")
 * @ORM\HasLifecycleCallbacks()
 */
class OptionGroup extends BaseEntity
{

    /**
     * @ORM\Column(name="g_name", type="string", length=255, nullable=true)
     */
    protected $name;

    /**
     * @ORM\Column(name="g_checkusers", type="boolean")
     */
    protected $checkUsers = false;

    /**
     * Many (but one) OptionGroup have Many Options.
     * @ORM\ManyToMany(targetEntity=Option::class)
     * @ORM\JoinTable(name="cms_conn_option_optiongroup",
     *      joinColumns={@ORM\JoinColumn(name="optiongroup_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="optionr_id", referencedColumnName="id", unique=true)}
     *      )
     */     
    protected $options;

    /**
     * Many OptionGroups have Many Users.
     * @ORM\ManyToMany(targetEntity=User::class)
     * @ORM\JoinTable(name="cms_conn_user_optiongroup",
     *      joinColumns={@ORM\JoinColumn(name="o_group_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     *      )
     */
    protected $users;

    public function __construct()
    {
        $this->options = new ArrayCollection();
        $this->users = new ArrayCollection();
    }


    /**
     * Get the value of name
     */ 
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }


    public function getCheckUsers(): ?bool
    {
        return $this->checkUsers;
    }

    public function setCheckUsers(bool $checkUsers): self
    {
        $this->checkUsers = $checkUsers;

        return $this;
    }

    /**
     * @return Collection|Option[]
     */
    public function getOptions(): Collection
    {
        return $this->options;
    }

    public function addOption(Option $option): self
    {
        if (!$this->options->contains($option)) {
            $this->options[] = $option;
        }

        return $this;
    }

    public function removeOption(Option $option): self
    {
        $this->options->removeElement($option);

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        $this->users->removeElement($user);

        return $this;
    }


}
