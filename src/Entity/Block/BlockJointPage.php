<?php

namespace Score\CmsBundle\Entity\Block;

use Doctrine\ORM\Mapping as ORM;
use Score\CmsBundle\Entity\Block\BlockJoint;
use Score\CmsBundle\Entity\Page\Page;

/**
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class BlockJointPage extends BlockJoint {

    /**
     * @ORM\ManyToOne(targetEntity=Page::class, inversedBy="blockJoints")
     * @ORM\JoinColumn(name="entity_id", referencedColumnName="id")
     */
    protected $page;


    public function getPage(): ?Page {
        return $this->page;
    }

    public function setPage(?Page $page): self {
        $this->page = $page;
        return $this;
    }

    public function getEntity() {
        return $this->getPage();
    }
}
