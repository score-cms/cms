<?php

namespace Score\CmsBundle\Entity\Block;

use Doctrine\ORM\Mapping as ORM;
use Score\CmsBundle\Entity\Document\Document;
use Score\CmsBundle\Entity\Block\BlockJoint;

/**
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class BlockJointDocument extends BlockJoint {


    /**
     * @ORM\ManyToOne(targetEntity=Document::class, inversedBy="blockJoints")
     * @ORM\JoinColumn(name="entity_id", referencedColumnName="id")
     */
    protected $document;


    public function getDocument(): ?Document {
        return $this->document;
    }

    public function setDocument(?Document $document): self {
        $this->document = $document;
        return $this;
    }

    public function getEntity() {
        return $this->getDocument();
    }
}
