<?php

namespace Score\CmsBundle\Entity\Block;

use Doctrine\ORM\Mapping as ORM;
use Score\BaseBundle\Entity\BaseEntity;
use Score\CmsBundle\Entity\Block\BlockJointArticle;
use Score\CmsBundle\Entity\Block\BlockJointDocument;
use Score\CmsBundle\Entity\Block\BlockJointEvent;
use Score\CmsBundle\Entity\Block\BlockJointPage;

/**
 * @ORM\Entity()
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="joint_entity", type="string")
 * @ORM\DiscriminatorMap({"article"="BlockJointArticle", "document"="BlockJointDocument", "event"="BlockJointEvent", "page"="BlockJointPage" })
 * @ORM\Table(name="cms_block_joint")
 * @ORM\HasLifecycleCallbacks()
 */
class BlockJoint extends BaseEntity {

    /**
     * @ORM\ManyToOne(targetEntity="Block", inversedBy="blockJoints")
     * @ORM\JoinColumn(name="block_id", referencedColumnName="id")
     */
    protected $block;

    /**
     * @ORM\Column(name="entity_id", type="integer", nullable=true)
     */
    protected $entityId;

    /**
     * @ORM\Column(name="sort_order", type="integer", nullable = true)
     */
    protected $sortOrder;

    /**
     * @ORM\Column(name="j_place", type="string", nullable = true)
     */
    protected $place;

    /**
     * @ORM\Column(name="j_params", type="string", length=2000, nullable=true)
     */
    protected $params;


    // custom
    public function getEntityId() {
        return $this->entityId;
    }

    // custom
    public function getBlockId() {
        return $this->getBlock()->getId();
    }

    public function getSortOrder() {
        return $this->sortOrder;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getLang() {
        return $this->lang;
    }

    public function getBlock():Block {
        return $this->block;
    }

    public function getPlace() {
        return $this->place;
    }

    public function setSortOrder($sortOrder) {
        $this->sortOrder = $sortOrder;
        return $this;
    }

    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }

    public function setLang($lang) {
        $this->lang = $lang;
        return $this;
    }

    public function setBlock(Block $block) {
        $this->block = $block;
        return $this;
    }

    public function setPlace($place) {
        $this->place = $place;
        return $this;
    }

    public function getParams(): ?string {
        return $this->params;
    }

    public function setParams(?string $params): self {
        $this->params = $params;

        return $this;
    }

    public function getParamsArr(): ?array {
        return json_decode($this->params, true);
    }

    public function setParamsArr(?array $params): self {
        $this->params = json_encode($params);

        return $this;
    }

    public function getParamByKey($key, $defaultValue = null) {
        $arr = $this->getParamsArr();
        if (is_array($arr) && array_key_exists($key, $arr)) {
            return $arr[$key];
        }
        return $defaultValue;
    }

    public function setParamByKey($key, $value) {
        $arr = $this->getParamsArr();
        if (is_array($arr)) {
            $arr[$key] = $value;
        } else {
            $arr = [$key => $value];
        }
        return $this->setParamsArr($arr);
    }
}
