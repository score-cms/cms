<?php

namespace Score\CmsBundle\Entity\Block;

use Doctrine\ORM\Mapping as ORM;
use Score\BaseBundle\Entity\BaseEntity;

/**
 * @ORM\Entity()
 * @ORM\Table(name="cms_accordion_item")
 * @ORM\HasLifecycleCallbacks()
 */
class AccordionItem extends BaseEntity
{
    /**
     * @ORM\Column(name="title", type="string", length=1000)
     */
    protected $title;

    /**
     * @ORM\Column(name="summary", type="string", length=2000, nullable=true)
     */
    protected $summary;

    /**
     * @ORM\Column(name="content", type="string", length=4000)
     */
    protected $content;

    /**
     * @ORM\Column(name="is_opened", type="boolean", nullable=true)
     */
    protected $isOpened = false;

    /**
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    protected $sortOrder;

    /**
     * @ORM\ManyToOne(targetEntity=Accordion::class, inversedBy="items")
     * @ORM\JoinColumn(name="accordion_id", referencedColumnName="id", nullable=false)
     */
    protected $accordion;


    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getIsOpened(): ?bool
    {
        return $this->isOpened;
    }

    public function setIsOpened(?bool $isOpened): self
    {
        $this->isOpened = $isOpened;

        return $this;
    }

    public function getSortOrder(): ?int
    {
        return $this->sortOrder;
    }

    public function setSortOrder(?int $sortOrder): self
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    public function getAccordion(): ?Accordion
    {
        return $this->accordion;
    }

    public function setAccordion(?Accordion $accordion): self
    {
        $this->accordion = $accordion;

        return $this;
    }

    public function getSummary(): ?string
    {
        return $this->summary;
    }

    public function setSummary(?string $summary): self
    {
        $this->summary = $summary;

        return $this;
    }
}
