<?php

namespace Score\CmsBundle\Entity\Block;

use Doctrine\ORM\Mapping as ORM;
use Score\CmsBundle\Entity\Article\Article;

/**
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class BlockJointArticle extends BlockJoint {

    /**
     * @ORM\ManyToOne(targetEntity=Article::class, inversedBy="blockJoints")
     * @ORM\JoinColumn(name="entity_id", referencedColumnName="id")
     */
    protected $article;



    public function getArticle(): ?Article {
        return $this->article;
    }

    public function setArticle(?Article $article): self {
        $this->article = $article;
        return $this;
    }

    public function getEntity() {
        return $this->getArticle();
    }
}
