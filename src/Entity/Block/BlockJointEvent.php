<?php

namespace Score\CmsBundle\Entity\Block;

use Doctrine\ORM\Mapping as ORM;
use Score\CmsBundle\Entity\Block\BlockJoint;
use Score\CmsBundle\Entity\Event\Event;

/**
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class BlockJointEvent extends BlockJoint {

    /**
     * @ORM\ManyToOne(targetEntity=Event::class, inversedBy="blockJoints")
     * @ORM\JoinColumn(name="entity_id", referencedColumnName="id")
     */
    protected $event;

    public function getEvent(): ?Event {
        return $this->event;
    }

    public function setEvent(?Event $event): self {
        $this->event = $event;
        return $this;
    }

    public function getEntity() {
        return $this->getEvent();
    }
}
