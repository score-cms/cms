<?php

namespace Score\CmsBundle\Entity\Block;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Score\BaseBundle\Entity\BaseEntity;
use \Score\CmsBundle\Entity\Block\AccordionItem;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="cms_accordion")
 * @ORM\HasLifecycleCallbacks()
 */
class Accordion extends BaseEntity {

    /**
     * @Assert\NotBlank
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    protected $name;

    /**
     * @ORM\Column(name="user_id", type="integer", nullable=true)
     */
    protected $userId;

    /**
     * @ORM\Column(name="a_title", type="string", length=1000, nullable=true)
     */
    protected $title;

    /**
     * @ORM\Column(name="a_type", type="string", length=200, nullable=true)
     */
    protected $type;

    /**
     * @ORM\Column(name="a_description", type="text", nullable = true)
     */
    protected $description;


    /**
     * @ORM\OneToMany(targetEntity=AccordionItem::class, mappedBy="accordion", orphanRemoval=true, cascade={"persist", "remove"})
     * @ORM\OrderBy({"sortOrder" = "ASC"})
     */
    protected $items;

    public function __construct() {
        $this->items = new ArrayCollection();
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(?string $name): self {
        $this->name = $name;
        return $this;
    }

    public function getUserId(): ?int {
        return $this->userId;
    }

    public function setUserId(?int $userId): self {
        $this->userId = $userId;

        return $this;
    }


    public function getTitle(): ?string {
        return $this->title;
    }

    public function setTitle(?string $title): self {
        $this->title = $title;
        return $this;
    }


    public function getType(): ?string {
        return $this->type;
    }

    public function setType(?string $type): self {
        $this->type = $type;
        return $this;
    }


    public function getDescription(): ?string {
        return $this->description;
    }

    public function setDescription(?string $description): self {
        $this->description = $description;
        return $this;
    }


    /**
     * @return Collection|AccordionItem[]
     */
    public function getItems(): Collection {
        return $this->items;
    }

    public function addItem(AccordionItem $item): self {
        if (!$this->items->contains($item)) {
            $this->items[] = $item;
            $item->setAccordion($this);
        }

        return $this;
    }

    public function removeItem(AccordionItem $item): self {
        if ($this->items->removeElement($item)) {
            // set the owning side to null (unless already changed)
            if ($item->getAccordion() === $this) {
                $item->setAccordion(null);
            }
        }

        return $this;
    }
}
