<?php

namespace Score\CmsBundle\Entity\Block;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Score\BaseBundle\Entity\BaseEntity;
use Score\CmsBundle\Repository\BlockRepository;

/**
 * Block
 * @ORM\Entity(repositoryClass=BlockRepository::class)
 * @ORM\Table(name="cms_block")
 * @ORM\HasLifecycleCallbacks()
 */
class Block extends BaseEntity {


    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="perex", type="text", nullable=true)
     */
    protected $perex;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    protected $content;

    /**
     * @var string
     *
     * @ORM\Column(name="lang", type="string", length=50, nullable=true)
     */
    protected $lang;

    /**
     * @var string
     *
     * @ORM\Column(name="module", type="string", length=200, nullable=true)
     */
    protected $module;

    /**
     * @var string
     *
     * @ORM\Column(name="action", type="string", length=200, nullable=true)
     */
    protected $action;

    /**
     * @var string
     *
     * @ORM\Column(name="params", type="text", nullable=true)
     */
    protected $params;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=200, nullable=true)
     */
    protected $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    protected $sortOrder;

    /**
     * @ORM\OneToMany(targetEntity=BlockJoint::class, mappedBy="block", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $blockJoints;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;


    /**
     * Set name
     *
     * @param string $name
     * @return Block
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set perex
     *
     * @param string $perex
     * @return Block
     */
    public function setPerex($perex) {
        $this->perex = $perex;

        return $this;
    }

    /**
     * Get perex
     *
     * @return string
     */
    public function getPerex() {
        return $this->perex;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Block
     */
    public function setContent($content) {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * Set lang
     *
     * @param string $lang
     * @return Block
     */
    public function setLang($lang) {
        $this->lang = $lang;

        return $this;
    }

    /**
     * Get lang
     *
     * @return string
     */
    public function getLang() {
        return $this->lang;
    }

    /**
     * Set module
     *
     * @param string $module
     * @return Block
     */
    public function setModule($module) {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module
     *
     * @return string
     */
    public function getModule() {
        return $this->module;
    }

    /**
     * Set action
     *
     * @param string $action
     * @return Block
     */
    public function setAction($action) {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return string
     */
    public function getAction() {
        return $this->action;
    }

    /**
     * Set params
     *
     * @param string $params
     * @return Block
     */
    public function setParams($params) {
        $this->params = $params;

        return $this;
    }

    /**
     * Get params
     *
     * @return string
     */
    public function getParams() {
        return $this->params;
    }

    public function setParamsArr($params) {
        $this->params = json_encode($params);

        return $this;
    }

    public function getParamsArr() {
        return json_decode($this->params, true);
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Block
     */
    public function setType($type) {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return Block
     */
    public function setSortOrder($sortOrder) {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer
     */
    public function getSortOrder() {
        return $this->sortOrder;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->blockJoints = new ArrayCollection();
    }

    public function getMenuId() {
        $params = $this->getParams();
        $settings = json_decode($params, true);
        return $settings['menuId'];
    }


    /**
     * Get the value of description
     *
     * @return  string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @param string $description
     *
     * @return  self
     */
    public function setDescription(string $description) {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, BlockJoint>
     */
    public function getBlockJoints(): Collection {
        return $this->blockJoints;
    }

    public function addBlockJoint(BlockJoint $blockJoint): self {
        if (!$this->blockJoints->contains($blockJoint)) {
            $this->blockJoints[] = $blockJoint;
            $blockJoint->setBlock($this);
        }
        return $this;
    }

    public function removeBlockJoint(BlockJoint $blockJoint): self {
        $this->blockJoints->removeElement($blockJoint);
        return $this;
    }


}
