<?php

namespace Score\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Score\BaseBundle\Entity\BaseEntity;
/**
 * @ORM\Entity
 * @ORM\Table(name="cms_stat")
* @ORM\HasLifecycleCallbacks()
 */
class Stat extends BaseEntity
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\Column(name="browser", type="text")
     */
    protected $browser;

     /**
     * @ORM\Column(name="source_type", type="string", length=255, nullable=true)
     */
    protected $sourceType;

     /**
     * @ORM\Column(name="stat_type", type="string", length=255, nullable=true)
     */
    protected $statType;

     /**
     * @ORM\Column(name="source_id", type="string", length=255, nullable=true)
     */
    protected $sourceId;

    /**
     * @ORM\Column(name="referer", type="string", length=1000, nullable=true)
     */
    protected $referer;

     /**
     * @ORM\Column(name="source_url", type="string", length=1000, nullable=true)
     */
    protected $sourceUrl;

     /**
     * @ORM\Column(name="source_title", type="string", length=1000, nullable=true)
     */
    protected $sourceTitle;

     /**
     * @ORM\Column(name="session_id", type="string", length=255, nullable=true)
     */
    protected $sessionId;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getBrowser(): ?string
    {
        return $this->browser;
    }

    public function setBrowser(string $browser): self
    {
        $this->browser = $browser;

        return $this;
    }

    public function getSourceType(): ?string
    {
        return $this->sourceType;
    }

    public function setSourceType(?string $sourceType): self
    {
        $this->sourceType = $sourceType;

        return $this;
    }

    public function getSourceId(): ?string
    {
        return $this->sourceId;
    }

    public function setSourceId(?string $sourceId): self
    {
        $this->sourceId = $sourceId;

        return $this;
    }

    public function getReferer(): ?string
    {
        return $this->referer;
    }

    public function setReferer(?string $referer): self
    {
        $this->referer = $referer;

        return $this;
    }

    public function getSessionId(): ?string
    {
        return $this->sessionId;
    }

    public function setSessionId(?string $sessionId): self
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    /**
     * Get the value of statType
     */ 
    public function getStatType()
    {
        return $this->statType;
    }

    /**
     * Set the value of statType
     *
     * @return  self
     */ 
    public function setStatType($statType)
    {
        $this->statType = $statType;

        return $this;
    }

    /**
     * Get the value of sourceTitle
     */ 
    public function getSourceTitle()
    {
        return $this->sourceTitle;
    }

    /**
     * Set the value of sourceTitle
     *
     * @return  self
     */ 
    public function setSourceTitle($sourceTitle)
    {
        $this->sourceTitle = $sourceTitle;

        return $this;
    }

    /**
     * Get the value of sourceUrl
     */ 
    public function getSourceUrl()
    {
        return $this->sourceUrl;
    }

    /**
     * Set the value of sourceUrl
     *
     * @return  self
     */ 
    public function setSourceUrl($sourceUrl)
    {
        $this->sourceUrl = $sourceUrl;

        return $this;
    }

   
}
