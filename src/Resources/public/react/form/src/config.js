// const URL_domain = "http://aaa.test";
const URL_domain = "";

export const URL_api_load_form = URL_domain + "/api/form-load/"; // +id
export const URL_api_save_data = URL_domain + "/api/form-save-data";
export const URL_api_save_form = URL_domain + "/admin/api/form-save";
export const URL_api_load_data = URL_domain + "/admin/api/form-load-data/"; // +id


export const MAX_items = 25;
export const MAX_choices = 15;
export const MAX_rows = 10;
export const MAX_stars = 12;
export const MAX_characters = 2500;

export const STARCOLOR = {
    empty: "#ddd",
    hover: "#c3b617",
    rated: "#6e620b",
};

export const TEXTS = {
    successSentHeader: "Ďakujeme, formulár bol úspešne odoslaný.",
    submitForm: "Odoslať",
    save: "Uložiť",
    afterSubmitButton: "Znovu vyplniť",
    alertSubmit: "Vyplnené údaje sú uložené.",
    alertFormUpdate: "Formulár je uložený.",
    robotsInputLabel: "Toto pole nevypĺňajte!"
}

export const dataViewOptions = [
    {
        name: "Zoznam",
        key: "list",
    },
    {
        name: "Tabuľka",
        key: "table",
    },
]

export const inputTypes = [
    {
        name: "Jednoriadkové textové pole",
        type: "text",
        icon: "text-width"
    },
    {
        // name: "Viacriadkové textové pole",
        name: "Textový blok",
        type: "textarea",
        icon: "align-justify"
    },
    {
        name: "Jednoduché výberove pole",
        type: "select",
        icon: "caret-square-o-down",
        emptyLabel: "Vyberte možnosť"
    },
    {
        name: "Prepínacie voľby",
        type: "radio",
        icon: "list-ul"
    },
    {
        name: "Viacnásobné voľby",
        type: "checkbox",
        icon: "tasks"
    },
    {
        name: "Hodnotenie",
        type: "star",
        icon: "star"
    },
    {
        name: "Email",
        type: "email",
        icon: "at"
    },
    {
        name: "Číslo",
        type: "number",
        icon: "sort-numeric-desc",
    },
    {
        name: "Telefón",
        type: "tel",
        icon: "mobile",
    },
    {
        name: "Dátum",
        type: "date",
        icon: "calendar",
    },
]

export const defaultItem = {
    identifier: "t",
    title: "name",
    helpText: "",
    type: "",
    sortOrder: 0,
    required: true,
    // choices: [],
    // rows: 3,
    // length: 5,
    // min: undefined,
    // max: undefined
}
export const defaultItemChoice = {
    label: "Nová voľba",
    value: "",
    sortOrder: 0,
    valid: true,
}


export const RTEtoolbarConfig = {
    // Optionally specify the groups to display (displayed in the order listed).
    //display: ['INLINE_STYLE_BUTTONS', 'BLOCK_TYPE_BUTTONS', 'BLOCK_TYPE_DROPDOWN', 'HISTORY_BUTTONS'],
    display: ['HISTORY_BUTTONS', 'BLOCK_TYPE_DROPDOWN', 'INLINE_STYLE_BUTTONS', 'LINK_BUTTONS', 'BLOCK_TYPE_BUTTONS'],
    INLINE_STYLE_BUTTONS: [
      {label: 'Bold', style: 'BOLD'},
      // {label: 'Italic', style: 'ITALIC'},
      // {label: 'Underline', style: 'UNDERLINE'}
    ],
    BLOCK_TYPE_DROPDOWN: [
      {label: 'normálny text', style: 'unstyled'},
      //{label: 'Heading Large', style: 'header-one'},
      {label: 'Nadpis H2', style: 'header-two'},
      {label: 'Nadpis H3', style: 'header-three'}
    ],
    BLOCK_TYPE_BUTTONS: [
      {label: 'Nezoradený', style: 'unordered-list-item'},
      {label: 'Zoradený', style: 'ordered-list-item'}
    ]
  };