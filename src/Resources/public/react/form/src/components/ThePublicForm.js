import React, {useEffect, useReducer, useState} from "react";
import {TEXTS, URL_api_load_form, URL_api_save_data} from "../config";
import {validatePublicForm} from "../model/actions";
import axios from "axios";
import ReactHtmlParser from 'react-html-parser'
import PublicFormItem from "./public/PublicFormItem";
import Loader from "./shared/Loader";
import SuccessMessage from "./public/SuccessMessage";
import Messages from "./shared/Messages";


const ThePublicForm = props => {

    const {
        formId: formIdDefault
    } = props

    /**
     *  STATE HOOKS
     */
    const [sent, setSent] = useState(false)
    const [fetching, setFetching] = useState(0)
    const [formData, setFormData] = useState([])
    const [messages, setMessages] = useState([])

    const [stateData, setStateData] = useReducer(
        (state, newState) => ({...state, ...newState}),
        {
            formId: formIdDefault ? formIdDefault : null,
            formDescription: null,
            items: [],
            successMessage: "",
            isActive: true,
        }
    )

    const {formId, formDescription, items, successMessage} = stateData
    const disabled = !stateData.isActive

    useEffect(() => {
        // console.log(formId)
        setFetching(f => f + 1)
        axios.get(URL_api_load_form + formId)
            .then(r => {
                // console.log(18, "ok", r.data)
                if (r.data.status !== "ok")
                    throw new Error(r.data.message)
                setStateData({
                    ...r.data.data, 
                    items: r.data.data.items?.concat({identifier:'access_check', title: TEXTS.robotsInputLabel, type: 'text'})
                })
            })
            .catch(e => {
                setMessages(m => m.concat([{text: e.message}]))
                // console.log(19, "err", e)
            })
            .then(() => {
                // console.log(20, "fin")
                setFetching(f => f - 1)
            })

    }, [formId])

    const handleChange = d => {
        let oldData = formData.find(v => v.identifier === d.identifier)
        let newData = formData.map(v => v === oldData ? d : v)

        if (!oldData) {
            newData.push(d)
        }
        // console.log(22, d)
        // console.log(24, oldData.identifier===d.identifier, oldData.value===d.value)

        setFormData(newData)

    }

    const handleSubmit = e => {
        e.preventDefault();

        let form = validatePublicForm(items, formData);
        setFormData(form.data)
        if (form.valid) {
            // console.log(25, "ok", form)
            // let formData = new FormData(document.getElementById('form'));
            let formData = new FormData();
            formData.append("formId", formId)
            formData.append("formData", JSON.stringify(form.data))

            setFetching(f => f + 1)
            axios.post(URL_api_save_data, formData)
                .then(r => {
                    // console.log(8.3, "ok", r.data)
                    if (r.data.status !== "ok")
                        throw new Error(r.data.message)
                    setSent(true)
                    setFormData([])
                    // setMessages(m => m.concat([{text: TEXTS.alertFormUpdate}]))
                })
                .catch(e => {
                    setMessages(m => m.concat([{text: e.message}]))
                    // console.log(9.3, "err", e)
                })
                .then(() => {
                    // console.log(10.3, "fin")
                    setFetching(f => f - 1)
                })

        } else {
            // console.log(26, "invalid", form, items)
        }


    }


    // console.log(successMessage)

    return (
        <div className={"cms-form"}>
            <Messages messages={messages} onMessages={setMessages}/>
            <Loader fetching={fetching > 0}/>
            <SuccessMessage
                sent={sent}
                successMessage={successMessage}
                onSent={() => setSent(s => !s)}
            />
            {items.length > 0 && !sent &&
            <>
                <div className={"cms-form-description"}>{ReactHtmlParser(formDescription)}</div>
                <form className={"items"} id={"form_" + formId} action={URL_api_save_data} method={"POST"}>
                    {items.map(v =>
                        <PublicFormItem key={v.identifier} item={v} disabled={disabled}
                                        itemData={formData.find(d => d.identifier === v.identifier)}
                                        onChange={handleChange}/>
                    )}
                    <button type="submit" className="btn btn-primary govuk-button" data-module="govuk-button" onClick={handleSubmit} disabled={disabled}>{TEXTS.submitForm}</button>
                </form>
            </>
            }
        </div>
    );
}

export default ThePublicForm;
