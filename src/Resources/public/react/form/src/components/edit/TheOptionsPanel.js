import React from "react";
import {inputTypes, defaultItem, defaultItemChoice, TEXTS} from "../../config.js"
import {getUid} from "../../model/actions";

const TheOptionsPanel = props => {

    const {
        onSubmit: handleSubmit,
        onAddItem: handleAddItemTop
    } = props

    const handleSave = _ => {
        handleSubmit()
    }

    const handleAddItem = type => {
        let item = {
            ...defaultItem,
            type: type,
            title: inputTypes.find(v => v.type === type)?.name,
            identifier: getUid()
        }
        switch (type) {
            case "select":
            case "checkbox":
            case "radio":
                item.choices = [{...defaultItemChoice}];
                break;
            case "tel":
                item.helpText = "Format: 0944123456 / +421 944 123 456";
                break;
            case "textarea":
                item.rows = 3;
                break;
            case "star":
                item.length= 5;
                break;
            default:
                break;
        }
        handleAddItemTop(item)
    }

    return (
        <div className={"options-panel"}>
            <h3>Pridať položku</h3>
            <div className={"items"}>
                {inputTypes.map(v =>
                    <button key={v.type} className={"item"} onClick={() => handleAddItem(v.type)}>
                        <i className={"fa fa-" + v.icon}/>&nbsp;
                        {v.name}
                    </button>
                )}
            </div>
            <button onClick={handleSave} className={"btn-save"}>{TEXTS.save}</button>
        </div>
    )
}

export default TheOptionsPanel;
