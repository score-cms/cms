import React, {useState} from "react";
import ItemWrapper from "./ItemWrapper";
import {arrayMoveImmutable} from 'array-move';

const TheItems = props => {

    const {
        items,
        onEditItems: handleEditItems
    } = props

    const [editingItemIndex, setEditingItemIndex] = useState(null)

    const handleSort = oldIndex => move => {
        let newIndex = move === "up" ? oldIndex - 1 : oldIndex + 1
        if (oldIndex === editingItemIndex) {
            setEditingItemIndex(newIndex)
        }
        let newItems = arrayMoveImmutable(items, oldIndex, newIndex)
            .map((v, k) => ({...v, sortOrder: k}))
        handleEditItems(newItems)
    }


    const handleEditItem = oldItem => newItem => {
        let newItems = items.map(v => v === oldItem ? newItem : v)
        handleEditItems(newItems)
    }


    const handleDeleteItem = item => {
        let newItems = items.filter(v => v !== item)
            .map((v, k) => ({...v, sortOrder: k}))
        if (newItems.length <= editingItemIndex)
            setEditingItemIndex(i => i - 1)
        handleEditItems(newItems)
    }


    return (
        <div className={"items"}>
            {
                items.map((v, k) =>
                    <ItemWrapper
                        key={k}
                        item={v}
                        indexes={[0, k, items.length - 1]}
                        isEditing={k === editingItemIndex}
                        onShowEdit={() => setEditingItemIndex(i => i !== k ? k : null)}
                        onSort={handleSort(k)}
                        onEdit={handleEditItem(v)}
                        onDelete={() => handleDeleteItem(v)}
                    />
                )
            }
        </div>
    )
}

export default TheItems;
