import React from "react";
import EditButtons from "./EditButtons";
import {arrayMoveImmutable} from "array-move";
import {encode_utf8} from "../../model/actions";
import {defaultItemChoice, MAX_characters, MAX_choices, MAX_rows, MAX_stars} from "../../config";
import StarRating from "../shared/StarRating";

const ItemWrapper = props => {

    const {
        item,
        indexes,
        isEditing,
        onEdit: handleItemEdit,
        onShowEdit: handleShowEdit,
        onSort: handleSort,
        onDelete: handleDelete
    } = props

    const handleEdit = (key, value) => {
        let newItem = {
            ...item,
            [key]: value
        }
        handleItemEdit(newItem)
    }

    return (
        <div className={"item" + (isEditing ? " item-editing" : "")}>
            {
                isEditing
                    ? <Edit item={item} onEdit={handleEdit}/>
                    : <Preview item={item} disabled={true}/>
            }

            <EditButtons
                item={item}
                indexes={indexes}
                onShowEdit={handleShowEdit}
                onSort={handleSort}
                onDelete={handleDelete}/>
        </div>
    )

}

export default ItemWrapper;


const Edit = props => {
    const {
        item,
        onEdit: handleEdit
    } = props

    const handleTitle = (e) => {
        handleEdit("title", e.target.value)
    }
    const handleHelpText = (e) => {
        handleEdit("helpText", e.target.value)
    }
    const handleRequired = value => {
        // console.log(value)
        handleEdit("required", value === true)
    }

    const handleTextareaRows = e => {
        let n = parseInt(e.target.value)
        if (n > 1 && n <= MAX_rows)
            handleEdit("rows", n)
    }

    const handleTextareaMax = e => {
        let n = Math.abs(parseInt(e.target.value || 0))
        if (n <= MAX_characters)
            handleEdit("max", n)
    }

    const handleStarsLength = e => {
        let n = parseInt(e.target.value)
        if (n > 1 && n <= MAX_stars)
            handleEdit("length", n)
    }

    const handleChoices = choices => {
        handleEdit("choices", choices)
    }

    return (
        <div>
            <label htmlFor={"cms_title_" + item.sortOrder}>Názov:</label>
            <input id={"cms_title_" + item.sortOrder} type="text" disabled={false} value={item.title}
                   onChange={handleTitle}/>

            <label htmlFor={"cms_help_" + item.sortOrder}>Pomocný text:</label>
            <input id={"cms_help_" + item.sortOrder} type="text" className={"small"} disabled={false}
                   value={item.helpText} onChange={handleHelpText}/>

            {
                item.type === "textarea" &&
                <>
                    <label htmlFor={"cms_rows_" + item.sortOrder}>Počet riadkov:</label>
                    <input id={"cms_rows_" + item.sortOrder} type="number" disabled={false} value={item.rows}
                           onChange={handleTextareaRows}/>
                    <label htmlFor={"cms_max_" + item.sortOrder}>Maximálny počet znakov, odpočítavanie:</label>
                    <small>{`Najviac však ${MAX_characters}. Ak je 0, nezobrazuje sa odpočítavanie.`}</small>
                    <input id={"cms_max_" + item.sortOrder} type="number" disabled={false} value={item.max || 0}
                           onChange={handleTextareaMax}/>
                </>
            }
            {
                item.type === "star" &&


                <>
                    <label htmlFor={"cms_length_" + item.sortOrder}>Počet hviezdičiek:</label>
                    <input id={"cms_length_" + item.sortOrder} type="number" disabled={false} value={item.length}
                           onChange={handleStarsLength}/>
                    <StarRating
                        // rating={null}
                        numberOfStars={item.length}
                    />
                </>
            }

            <label htmlFor={"cms_req_" + item.sortOrder}>
                <input id={"cms_req_" + item.sortOrder} type="checkbox" checked={item.required}
                       onChange={() => handleRequired(!item.required)}/>
                {"Uživatel je povinný toto vyplniť"}
            </label>

            {
                ["select", "radio", "checkbox"].includes(item.type) &&
                <ItemChoices
                    required={item.required}
                    choices={item.choices}
                    onEdit={handleChoices}
                />
            }
        </div>
    )
}


const ItemChoices = props => {

    const {
        required,
        choices,
        onEdit: handleEdit
    } = props

    const handleSortChoice = (oldIndex, move) => {
        let newIndex = move === "up" ? oldIndex - 1 : oldIndex + 1
        let newChoices = arrayMoveImmutable(choices, oldIndex, newIndex)
            .map((v, k) => ({...v, sortOrder: k}))
        handleEdit(newChoices)
    }


    const handleAddChoice = type => {
        if (choices.length >= MAX_choices)
            return;

        let newChoices = choices.slice()
        let label = defaultItemChoice.label + " " + (choices.length + 1)
        newChoices.push({
            ...defaultItemChoice,
            label,
            value: encode_utf8(label),
            sortOrder: choices.length
        })
        handleEdit(newChoices)
    }

    const handleChoiceLabel = oldChoice => e => {
        let newChoice = {
            ...oldChoice,
            label: e.target.value,
            value: encode_utf8(e.target.value)
        }
        // console.log(newChoice)
        let newChoices = choices.map(v => v === oldChoice ? newChoice : v)
        handleEdit(newChoices)
    }


    const handleDeleteChoice = item => {
        let newChoices = choices.filter(v => v !== item)
            .map((v, k) => ({...v, sortOrder: k}))
        handleEdit(newChoices)
    }

    const handleValid = choice => {
        let newChoices = choices.map(v => v === choice ? {...v, valid: !v.valid} : v)
        handleEdit(newChoices)
    }

    return (
        <div>
            <ul className={"choices"}>
                {choices.map((v, k) =>
                    <li key={v.sortOrder}>
                        <div>
                            <label htmlFor={"cms_choice_" + k}>{"Volba" + (v.customAnswer ? " iné" : "") + ":"}</label>
                            <input id={"cms_choice_" + k} type="text" value={v.label}
                                   onChange={handleChoiceLabel(v)}/>
                        </div>
                        <button onClick={() => handleSortChoice(k, "up")} disabled={k < 1}>
                            <i className="fa fa-arrow-up"></i>
                        </button>
                        <button onClick={() => handleSortChoice(k, "down")} disabled={k >= choices.length - 1}>
                            <i className="fa fa-arrow-down"></i>
                        </button>
                        <button onClick={() => handleDeleteChoice(v)}>
                            <i className="fa fa-trash"></i>
                        </button>
                        {required &&
                            <label>
                                <input type="checkbox" checked={v.valid}
                                       onChange={() => handleValid(v)}/>
                                {"Správna odpoveď"}
                            </label>
                        }
                    </li>
                )}
            </ul>
            <div>
                <button onClick={handleAddChoice}>
                    <i className="fa fa-plus"></i>&nbsp;
                    Pridat volbu
                </button>
                {/*<button onClick={() => handleAddChoice("custom")}*/}
                {/*        disabled={choices.filter(v => v.customAnswer)?.length > 0}>*/}
                {/*    <i className="fa fa-plus-square"></i>&nbsp;*/}
                {/*    Pridat volbu ine s vlastnym textom*/}
                {/*</button>*/}
            </div>
        </div>
    )

}


export const Preview = props => {

    const {item, disabled: disabledDefault} = props;

    const renderComponent = _ => {

        let disabled = disabledDefault === true;

        switch (item.type) {
            case "text":
            case "email":
            case "number":
            case "tel":
            case "date":
                return (<input type={item.type} required={item.required} placeholder={""} disabled={disabled}
                               value={""}/>);
            case "textarea":
                return (
                    <textarea required={item.required} rows={item.rows} placeholder={""} disabled={disabled}/>);
            case "select":
                return (
                    <select required={item.required} disabled={disabled}>
                        {item.choices.map((v, k) => <option key={k} value={v.value}>{v.label}</option>)}
                    </select>
                );
            case "radio":
            case "checkbox":
                return (item.choices.map((v, k) =>
                    <label key={k}>
                        <input name={item.identifier}
                               type={item.type} required={item.required} disabled={disabled} value={v.value}/>
                        {v.label}
                    </label>
                ));
            case "star":
                return <StarRating
                    // rating={50}
                    numberOfStars={item.length}
                    // onChange={undefined}
                    disabled={disabled}
                />;
            default:
                break;
        }
    }

    return (
        <div>
            <label>{item.title}</label>
            {renderComponent()}
            <small>{item.helpText}</small>
        </div>
    )
}



