import React from "react";

const EditButtons = props => {

    const {
        indexes,
        onShowEdit: handleShowEdit,
        onSort: handleSort,
        onDelete: handleDelete

    } = props


    return (
        <div className={"buttons-line"}>
            <button onClick={handleShowEdit}>
                <i className="fa fa-edit"></i>
            </button>
            <button onClick={() => handleSort("up")} disabled={indexes[0] >= indexes[1]}>
                <i className="fa fa-arrow-up"></i>
            </button>
            <button onClick={() => handleSort("down")} disabled={indexes[1] >= indexes[2]}>
                <i className="fa fa-arrow-down"></i>
            </button>
            <button onClick={handleDelete}>
                <i className="fa fa-trash"></i>
            </button>
        </div>
    )
}

export default EditButtons;
