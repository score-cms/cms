import React, {useState, useEffect, useReducer} from "react";
import {dataViewOptions, URL_api_load_data, URL_api_load_form} from "../config";
import axios from "axios";
import Loader from "./shared/Loader";
import Entry from "./dataViewer/Entry";
import Messages from "./shared/Messages";
import EntryTable from "./dataViewer/EntryTable";

// TODO error hla3ky

const TheDataViewerWrapper = props => {
    const {
        formId: formIdDefault,
        entryId
    } = props

    /**
     *  STATE HOOKS
     */
    const [fetching, setFetching] = useState(0)
    const [entries, setEntries] = useState([])
    const [messages, setMessages] = useState([])
    const [dataViewOption, setDataViewOption] = useState("list")

    const [stateData, setStateData] = useReducer(
        (state, newState) => ({...state, ...newState}),
        {
            formId: formIdDefault ? formIdDefault : null,
            items: [],

        }
    )

    const {formId, items} = stateData

    const handleEntries = es => {
        let newEntries = es.map(e => {
            let referer;
            let values = [];
            e.values.forEach(v => {
                if (v.fieldIdentifier === "referer") {
                    referer = v.value;
                } else {
                    values.push(v)
                }
            })
            return({...e, values, referer})
        })
        setEntries(newEntries);
    }

    useEffect(() => {
        setFetching(f => f + 2)
        axios.get(URL_api_load_form + formId)
            .then(r => {
                // console.log(8.4, "ok", r.data)
                if (r.data.status !== "ok")
                    throw new Error(r.data.message)
                setStateData({items: r.data.data.items,})
            })
            .catch(e => {
                setMessages(m => m.concat([{text: e.message}]))
                // console.log(9.4, "err", e)
            })
            .then(() => {
                // console.log(10.4, "fin")
                setFetching(f => f - 1)
            })

        let entryUrl = entryId ? `/${entryId}` : "";
        axios.get(URL_api_load_data + formId + entryUrl)
            .then(r => {
                // console.log(8.4, "ok", r.data)
                if (r.data.status !== "ok")
                    throw new Error(r.data.message)
                handleEntries(r.data.entries)
            })
            .catch(e => {
                setMessages(m => m.concat([{text: e.message}]))
                // console.log(9.4, "err", e)
            })
            .then(() => {
                // console.log(10.4, "fin")
                setFetching(f => f - 1)
            })
    }, [formId, entryId])

    return (
        <>
            <Messages messages={messages} onMessages={setMessages}/>
            <div className={"cms-form-edit-wrapper"}>
                <Loader fetching={fetching > 0}/>
                <div>
                    { dataViewOptions.map(option => 
                            <button key={option.key} onClick={() => setDataViewOption(option.key)} >{option.name}</button>
                    )}
                    { dataViewOption === "table" &&
                        <EntryTable formId={formId} entries={entries} items={items}/>
                    }
                    { dataViewOption === "list" &&
                        <div className={"items"}>
                            {entries?.map(v =>
                                <Entry key={v.identifier} entry={v} items={items}/>
                            )}
                        </div> 
                    }
                </div>
            </div>
        </>
    );
}

export default TheDataViewerWrapper;
