import React from "react";
import {getItemValue} from "../../model/actions";

const Entry = props => {

    const {
        entry,
        items
    } = props

    if (!entry || !items?.length)
        return null;

    return (
        <div className={"item"}>
            <h2>{entry.date}</h2>
            {
                entry.values.map((v, k) =>
                    <ValueLine key={k} item={items.find(i => i.identifier === v.fieldIdentifier)} value={v}/>
                )
            }
            {entry?.referer &&
                <span>{`Vyplnené na adrese:  ${entry.referer}`}</span>
            }
        </div>
    )
}

export default Entry;


const ValueLine = props => {
    const {item, value} = props

    if (!value || !item){
        return (
            <div>
                <span className={"text-danger"}>Chyba s hodnotou.</span> &nbsp;
                [{String(value?.fieldIdentifier || item?.title).toString()}]
                [{String(value?.value || value).toString()}]
            </div>
        );
    }
    return (
        <>
            <b>{item.title}</b> &nbsp;
            {getItemValue(item, value.value)} 
            <br/>
        </>
    )
}
