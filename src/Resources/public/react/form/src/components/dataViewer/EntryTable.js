import React from "react";
import {getItemValue} from "../../model/actions";
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
// import ReactHTMLTableToExcel from 'react-html-table-to-excel_xlsx';
//import ReactHTMLTableToExcel from 'react-html-to-excel';

const EntryTable = props => {

    const {
        formId,
        entries,
        items
    } = props

    if (!entries || !items?.length)
        return null;

    return (
        <>
            <ReactHTMLTableToExcel
                table="table-to-xls"
                filename={formId}
                sheet="form"
                buttonText="Stiahnuť tabuľku"/>

            <table id="table-to-xls">
                <thead>
                    <tr>
                        <th>Dátum</th>
                        <th>Čas</th>
                        <th>URL Adresa</th>
                        {items.map(item => 
                            <th key={item.identifier} >{item.title}</th>
                        )}
                    </tr>
                </thead>
                <tbody>
                    {entries.map((entry, k) =>
                        <tr key={k}>
                            {entry.date.split(' - ').map(v =>
                                <td key={v} >{v}</td>
                            )}
                            <td>{entry.referer}</td>
                            { items.map(item =>
                                <td key={item.identifier}>
                                    {getItemValue(item, entry.values.find(v => v.fieldIdentifier === item.identifier)?.value)} 
                                </td>
                            )}
                        </tr>
                    )}
                </tbody>
            </table>
        </>
    )
}

export default EntryTable;
