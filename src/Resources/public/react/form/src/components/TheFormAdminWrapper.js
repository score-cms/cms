import React, {useState, useEffect, useReducer} from "react";
import TheOptionsPanel from "./edit/TheOptionsPanel";
import TheItems from "./edit/TheItems";
import {MAX_items, TEXTS, URL_api_load_form, URL_api_save_form, RTEtoolbarConfig} from "../config";
import RichTextEditor from "react-rte";
import axios from "axios";
import Loader from "./shared/Loader";
import Messages from "./shared/Messages";


const TheFormAdminWrapper = props => {
    const {
        formId: formIdDefault
    } = props

    /**
     *  STATE HOOKS
     */
    const [fetching, setFetching] = useState(0)
    const [messages, setMessages] = useState([])

    const [stateData, setStateData] = useReducer(
        (state, newState) => ({...state, ...newState}),
        {
            formId: formIdDefault ? formIdDefault : null,
            formName: "Formulár",
            formDescription: RichTextEditor.createEmptyValue(),
            items: [],
            successMessage: "",
            isActive: true,
            visibility: true,
            sendMail: false,
            mailList: [],
            //params: []
        }
    )

    const {formId, formName, formDescription, items, successMessage, isActive, visibility, sendMail, mailList} = stateData


    useEffect(() => {
        if (formId && formId !== "0") {
            setFetching(f => f + 1)
            axios.get(URL_api_load_form + formId)
                .then(r => {
                    // console.log(8.1, "ok", r.data)
                    if (r.data.status !== "ok")
                        throw new Error(r.data.message)
                    setStateData({
                        ...r.data.data,
                        mailList: r.data.data.mailList?.map(v=>({value:v})) || [],
                        formDescription: RichTextEditor.createValueFromString(r.data.data.formDescription || "", 'html')
                    })
                })
                .catch(e => {
                    setMessages(m => m.concat([{text: e.message}]))
                    // console.log(9.1, "err", e)
                })
                .then(() => {
                    // console.log(10.1, "fin")
                    setFetching(f => f - 1)
                })
        }
    }, [formId])


    /**
     * METHODS
     */

    const handleAddItem = item => {
        if (items.length <= MAX_items)
            setStateData({
                items: items.concat([{...item, sortOrder: items.length}])
            })
    }

    const handleEditItems = items => {
        setStateData({items})
    }

    const handleFormName = (e) => {
        setStateData({
            formName: e.target.value
        })
    }

    const handleFormDescription = formDescription => {
        setStateData({formDescription})
    }

    const handleFormSuccessMessage = (e) => {
        setStateData({
            successMessage: e.target.value
        })
    }

    const handleFormIsActive = () => {
        setStateData({
            isActive: !isActive
        })
    }

    const handleFormVisibility = () => {
        setStateData({
            visibility: !visibility
        })
    }

    const handleSendMail = () => {
        setStateData({
            sendMail: !sendMail
        })
    }

    const handleMailList = mailList => {
        setStateData({mailList})
    }


    const handleSubmit = () => {
        // console.log(7, items)
        // console.log(getUid().length)
        if (fetching !== 0) return;

        let formData = new FormData();
        formData.append("formId", formId)
        formData.append("formName", formName)
        formData.append("formDescription", formDescription.toString('html'))
        formData.append("successMessage", successMessage)
        formData.append("isActive", isActive)
        formData.append("visibility", visibility)
        formData.append("items", JSON.stringify(items))
        formData.append("sendMail", sendMail)
        let list = mailList.map(v => v.value).filter((v,k,a) => a.indexOf(v) === k && /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(v)) // validacia e-mailov
        formData.append("mailList", JSON.stringify(list))
        //formData.append("params", JSON.stringify(params))
        // console.log(formData.getAll("formId"))

        setFetching(f => f + 1)
        axios.post(URL_api_save_form, formData)
            .then(r => {
                // console.log(8, "ok", r.data)
                if (r.data.status !== "ok")
                    throw new Error(r.data.message)
                setStateData({
                    ...r.data.data,
                    mailList: r.data.data.mailList?.map(v=>({value:v})) || [],
                    formDescription: RichTextEditor.createValueFromString(r.data.data.formDescription || "", 'html')
                })
                setMessages(m => m.concat([{text: TEXTS.alertFormUpdate, type: 'ok'}]))
            })
            .catch(e => {
                setMessages(m => m.concat([{text: e.message}]))
                // console.log(9, "err", e)
            })
            .then(() => {
                // console.log(10, "fin")
                setFetching(f => f - 1)
            })
    }

    return (
        <form noValidate onSubmit={e => e.preventDefault()}>
            <Messages messages={messages} onMessages={setMessages}/>
            <div className={"cms-form-edit-wrapper"}>
                <Loader fetching={fetching > 0}/>
                <TheOptionsPanel
                    onSubmit={handleSubmit}
                    onAddItem={handleAddItem}
                />
                <div>
                    <div className="form-info">
                        <div className="form-row">
                            <label htmlFor="form-name">Názov formuláru:</label>
                            <input name="form-name" id="form-name" value={formName} onChange={handleFormName}/>
                        </div>
                        <div className="form-row">
                            <h3>Popis – vysvetlenie formuláru:</h3>
                            <RichTextEditor 
                                toolbarConfig={RTEtoolbarConfig}
                                name="form-desc" 
                                value={formDescription} 
                                onChange={handleFormDescription}
                            />
                        </div>
                        <div className="form-row">
                            <label htmlFor="form-success-message">Informácia po odoslaní:</label>
                            <textarea name="form-success-message" id="form-success-message" value={successMessage}
                                      rows={3} onChange={handleFormSuccessMessage}/>
                            <small>Pod nadpisom: {TEXTS.successSentHeader}</small>
                        </div>
                        <div className="form-row">
                            <label htmlFor={"cms_form_is_active"}>
                                <input id={"cms_form_is_active"} name={"cms_n_form_is_active"} type="checkbox" checked={isActive}
                                       onChange={handleFormIsActive}/>
                                {"Formulár je aktívny"}
                            </label>
                        </div>
                        <div className="form-row">
                            <label htmlFor={"cms_form_is_visibility"}>
                                <input id={"cms_form_is_visibility"} name={"cms_n_form_is_visibility"} type="checkbox" checked={visibility}
                                       onChange={handleFormVisibility}/>
                                {"Formulár je verejný"}
                            </label>
                        </div>
                        <div className="form-row">
                            <label htmlFor={"cms_form_send_mail"}>
                                <input id={"cms_form_send_mail"} name={"cms_n_form_send_mail"} type="checkbox" checked={sendMail}
                                       onChange={handleSendMail}/>
                                {"Po vyplnení poslať upozornenie na e-mail"}
                            </label>
                        </div>
                        {sendMail && <MailList mailList={mailList} onChange={handleMailList} />}
                    </div>
                    <TheItems
                        items={items}
                        onEditItems={handleEditItems}
                    />
                </div>
            </div>
        </form>
    );
}

export default TheFormAdminWrapper;

const MailList = props => {

    const {
        mailList, 
        onChange:handleTopChange
    } = props

    const handleAdd = () => {
        handleTopChange(mailList.concat({value:""}))
    }    
    
    const handleRemove = mail => {
        handleTopChange(mailList.filter(m => m !== mail))
    } 

    const handleUpdate = mail => e => {
        let value = {value: e.target.value};
        handleTopChange(mailList.map(v => v === mail ? value : v))
    }

    return(
        <div>
            {mailList?.map((mail, k) => 
                <div className="form-group row" key={k} style={{display:'flex', alignItems:'baseline', gap:'0.5em'}}>
                    <label htmlFor={"cms_form_mail_list_" + k}>{"E-mail"}</label>
                    <div className="col">
                        <input id={"cms_form_mail_list_" + k} name={"email_" + k} type="email" value={mail?.value || ""} onChange={handleUpdate(mail)}/>
                    </div>
                    <div className="col">
                        <button type="button" onClick={() => handleRemove(mail)}><i className="fa fa-trash"></i></button>
                    </div>
                </div>
            )}
            <button type="button" onClick={handleAdd}>{"Pridať e-mail"}</button>
        </div>
    )
}

