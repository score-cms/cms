import React from "react";
import StarRating from "../shared/StarRating";
import {inputTypes, MAX_characters} from "../../config";
import {getAriaIds} from "../../model/actions";


export const PublicFormItem = props => {

    const {
        item,
        itemData,
        disabled,
        onChange: handleChangeTop
    } = props;


    return (
        <div
            className={"mb-3 govuk-form-group cms-form-item" + (itemData?.valid === true ? " valid" : itemData?.valid === false ? " invalid govuk-form-group--error" : "") + (item?.identifier === "access_check" ? " cms-form-sr-only" : "")}
        >
            <fieldset className="govuk-fieldset" aria-describedby={getAriaIds(item, itemData)}>
                {["radio", "checkbox"].includes(item.type) ?
                    <legend className="float-none govuk-fieldset__legend cms-form-label" 
                        htmlFor={item.identifier} id={"l-"+item.identifier}>
                            {item.title} <span className={item.required === true ? "required" : ""}/>
                    </legend>
                :
                    <label className="form-label govuk-label cms-form-label" 
                        htmlFor={item.identifier} id={"l-"+item.identifier}>
                            {item.title} <span className={item.required === true ? "required" : ""}/>
                    </label>
                }

            <span id={item.identifier + "-hint"} className="form-text govuk-hint">{item.helpText}</span>
            {itemData?.valid === false &&
                <span id={item.identifier + "-error"} className="invalid-feedback d-block govuk-error-message">
                    <span className="govuk-visually-hidden visually-hidden">Chyba:</span> {itemData.value === undefined ? `Toto pole je povinné` : "Toto pole  nie je vyplnené správne."}
                </span>
            }
            <FormElement
                item={item}
                itemData={itemData}
                disabled={disabled}
                onChange={handleChangeTop}
            />
            </fieldset>
        </div>
    )
}


export default PublicFormItem;


const FormElement = props => {

    const {
        item,
        itemData: data,
        disabled,
        onChange: handleChangeTop
    } = props;

    const handleChange = (e) => {

        let value;
        if (item.type === "star") {
            value = e;
        } else if (item.type === "textarea") {
            value = parseInt(item.max || 0) && item.max <= e.target.value 
                ? e.target.value?.slice(0, item.max)
                : e.target.value?.slice(0, MAX_characters) ;
        } else if (item.type === "checkbox") {
            let v = e.target.value;
            let oldValue = data?.value?.length ? data.value : []
            if (oldValue.includes(v)) {
                value = oldValue.filter(old => old !== v)
            } else {
                value = oldValue.concat(v)
            }
        } else {
            value = e.target.value;
        }

        handleChangeTop({
            identifier: item.identifier,
            value
        })
    }

    let charactersCountText = (now, max) => {
        if (!max || max < 2)
            return ("");
        let rest = parseInt(max - (now || 0));
        if (!rest || rest < 1) rest = 0;
        return `Zostáva Vám ${rest} znak${rest === 1 ? "" : [2,3,4].includes(rest) ? "y" : "ov"}.`;
    }

    const getValidClass = (v, t) => {
        if (v === true) {
            return "is-valid";
        } else if (v === false){
            return `is-invalid govuk-${t}--error`;
        }
        return "";
    }

    switch (item.type) {
        case "text":
        case "email":
        case "number":
        case "tel":
        case "date":
            return (<input id={item.identifier} aria-describedby={getAriaIds(item, data)}
                        className={`form-control govuk-input ${getValidClass(data?.valid ,"input")}`}
                        type={item.type} required={item.required} placeholder={""} disabled={disabled}
                        autoComplete={"on"} name={item.identifier} value={data?.value || ""}
                        onChange={handleChange}/>);
        case "textarea":
            return (
                <>
                    <textarea id={item.identifier} aria-describedby={getAriaIds(item, data)}
                        className={`form-control govuk-textarea ${getValidClass(data?.valid , item.type)}`}
                        required={item.required} rows={item.rows} placeholder={""} disabled={disabled}
                        value={data?.value || ""} onChange={handleChange}/>
                    <span className="govuk-hint" id={item.identifier + "-hint-max"}>{charactersCountText(data?.value?.length, item?.max)}</span>
                </>);
        case "select":
            return (
                <select id={item.identifier} aria-describedby={getAriaIds(item, data)}
                    className={`form-select govuk-select ${getValidClass(data?.valid , item.type)}`}
                    required={item.required} value={data?.value || ""} onChange={handleChange} disabled={disabled}>
                    <option value={""}>{inputTypes.find(v => v.type === "select").emptyLabel}</option>
                    {item.choices.map((v, k) => <option key={k} value={v.value}>{v.label}</option>)}
                </select>
            );
        case "radio":
            return (
            <div className="govuk-radios">
                {(item.choices.map((v, k) =>
                    <div key={k} className="form-check govuk-radios__item">
                            <input className={`form-check-input govuk-radios__input ${getValidClass(data?.valid ,"")}`} name={item.identifier} disabled={disabled} id={`${item.identifier}-${k}`} 
                                type={"radio"} required={item.required} checked={data?.value === v.value}
                                onChange={handleChange} value={v.value}/>
                            <label htmlFor={`${item.identifier}-${k}`} 
                                className={"form-check-label govuk-label govuk-radios__label" + (data?.value?.includes(v.value) === true && (!v.valid || !v.value) ? " incorrect" : "")}
                            >{v.label}</label>
                    </div>
                ))}
            </div>)
        case "checkbox":
            return (
            <div className="d-block govuk-checkboxes">
                {(item.choices.map((v, k) =>
                    <div key={k} className="form-check govuk-checkboxes__item">
                            <input className={`form-check-input govuk-checkboxes__input ${getValidClass(data?.valid ,"")}`} name={item.identifier + "[]"} disabled={disabled} id={`${item.identifier}-${k}`} 
                                type={"checkbox"} required={item.required} checked={data?.value?.includes(v.value) === true}
                                onChange={handleChange} value={v.value}/>
                            <label htmlFor={`${item.identifier}-${k}`} 
                                className={"form-check-label govuk-label govuk-checkboxes__label" + (data?.value?.includes(v.value) === true && (!v.valid || !v.value) ? " incorrect" : "")}
                            >{v.label}</label>
                    </div>
                ))}
            </div>)
        case "star":
            return <StarRating
                rating={data?.value || 0}
                numberOfStars={item.length}
                disabled={disabled}
                onChange={handleChange}
            />;
        default:
            return null;
    }
}
