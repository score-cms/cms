import React from 'react';
import {TEXTS} from "../../config";
import {convertStringToHtml} from "../../model/actions";

const SuccessMessage = props => {

    const {
        sent,
        successMessage,
        onSent: handleSent
    } = props

    return (
        sent
        ?
        <div className={"cms-form-success-message"}>
            <h2>{TEXTS.successSentHeader}</h2>
            <div className={"success-message"}>
                {convertStringToHtml(successMessage)}
            </div>
            <button className="btn btn-primary govuk-button" data-module="govuk-button" onClick={handleSent}>{TEXTS.afterSubmitButton}</button>
        </div>
        : null
    )
}

export default SuccessMessage
