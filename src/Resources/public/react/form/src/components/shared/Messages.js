import React from 'react';

const Messages = props => {

    const {
        messages,
        onMessages
    } = props;

    const handleClearMessage = (m,e) => {
        e.stopPropagation();
        onMessages(ms => ms.filter(v => v !== m));
    }

    return (
        <div className={"alerts-wrapper"}>
            {messages?.length > 0 && messages.map((message, key) =>
                message.text &&
                    <div key={key} className={"alert" + (message.type === "ok" ? " alert-success" : " alert-danger")} role="alert">
                        <button type="button" className="close" data-dismiss="alert" aria-label="Close" onClick={(e) => handleClearMessage(message, e)}>
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <p className="m-0">{message.text}</p>
                    </div>
            )}
        </div>
    );
}
export default Messages
