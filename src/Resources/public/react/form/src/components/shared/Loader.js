import React from 'react';

const Loader = ({fetching}) => {
    return (
        fetching
        ?
        <div className={"cms-form-loader"}>
            <div className="lds-dual-ring"></div>
        </div>
        : null
    )
}

export default Loader
