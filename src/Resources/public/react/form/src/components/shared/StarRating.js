import React, {useState} from 'react';
import {STARCOLOR} from "../../config";

const initRating = (r, n) => {
    if ((r === 0 || r >= 1) && n)
        return (parseFloat(r) / 100 * n)
    return (n ? n / 2 : 2)
}

const StarRating = props => {

    const {
        rating: defaultRating,
        numberOfStars,
        disabled,
        onChange: handleRating
    } = props

    const [rating, setRating] = useState(initRating(defaultRating, numberOfStars));
    const [hover, setHover] = useState(null);

    const getColor = i => {
        if (hover !== null)
            return (i <= hover ? STARCOLOR.hover : STARCOLOR.empty);
        return (i <= rating ? STARCOLOR.rated : STARCOLOR.empty);
    }

    const handleChange = rate => {
        setHover(null);
        setRating(rate);
        if (handleRating)
            handleRating(Math.round(rate * 100 / numberOfStars));
    }

    return (
        <div className="cms-star-rating">
            {[...Array(numberOfStars || 5)].map((star, i) => {
                i += 1;
                return (
                    <button
                        type="button"
                        key={i}
                        style={{color: getColor(i)}}
                        onClick={() => handleChange(i)}
                        onMouseEnter={() => setHover(i)}
                        onFocus={() => setHover(i)}
                        onMouseLeave={() => setHover(null)}
                        onBlur={() => setHover(null)}
                        disabled={disabled === true}
                    >
                        <span className="star">&#9733;</span>
                    </button>
                );
            })}
        </div>
    );
};


export default StarRating;
