import React from 'react';
import {render} from 'react-dom';
import './style/styles.scss';

import TheFormAdminWrapper from './components/TheFormAdminWrapper';
import ThePublicForm from './components/ThePublicForm';
import TheDataViewerWrapper from "./components/TheDataViewerWrapper";

let admin = document.getElementById('cms-form-admin-root');
let data = document.getElementById('cms-form-admin-data');
let forms = document.querySelectorAll('[data-cms="react-form"]')


if (admin) {
    render(<TheFormAdminWrapper formId={(admin.dataset.formId)}/>, admin);
}

if (data) {
    render(<TheDataViewerWrapper formId={(data.dataset.formId)} entryId={(data.dataset.entryId)}/>, data);
}

if (forms?.length) {
    forms.forEach((el) => {
        render(<ThePublicForm formId={(el.dataset.formId)}/>, el);
    })
}
