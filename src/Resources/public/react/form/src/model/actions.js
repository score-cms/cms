import ReactHtmlParser from "react-html-parser";

export const encode_utf8 = s => {
    if (!s) return "";
    return btoa(
        unescape(
            encodeURIComponent(s)
        )
    );
}

export const decode_utf8 = s => {
    if (!s) return "";
    return decodeURIComponent(
        escape(
            atob(s)
        )
    );
}

export const getUid = _ => {
    let rand = Math.random().toString(36).substr(2);
    return (
        rand.slice(0, 4) + Date.now().toString(36) + rand.slice(-4)
    );
}

export const convertStringToHtml = s => {
    if (!s) return "";
    return (ReactHtmlParser(s.split("\n").join("<br/>")));
}


export const getAriaIds = (item, data) => {
    if (!item?.identifier) return "";
    let ids = [
        data?.valid === false ? item.identifier + "-error" : "" ,
        item.helpText ? item.identifier + "-hint" : "",
        item.type === "textarea" && item.max ? item.identifier + "-hint-max" : "",
    ];
    return ids.filter(v => v).join(" ")
}

export const getItemValue = (item, value) => {
    switch (item?.type) {
        case "text":
        case "email":
            return (value || "");
        case "textarea":
            return (convertStringToHtml(value));
        case "select":
        case "radio":
            return (decode_utf8(value));
        case "checkbox":
            return (value?.map(v => decode_utf8(v)).join(", ") || "");
        case "star":
            return (value ? (value + "%") : "");
        default:
            return String(value || "").toString()
    }
}

export const validatePublicForm = (items, datas) => {
    let result = {valid: false, data: []}

    const checkValue = (item, value) => {
        switch (item.type) {
            case "text":
            case "textarea":
                return (value?.length > 1);
            case "number":
                return (value === '0' || parseFloat(value) > 0 || parseFloat(value) < 0);
            case "date":
                return (Date.parse(value) > 1);
            case "email":
                let re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
                return re.test(value);
            case "tel":
                let rt = /^(((\+|0{2})[0-9]{3})|0)(\s?[0-9]{3}\s?[0-9]{3}\s?[0-9]{3})$/;
                return (rt.test(value));
            case "select":
            case "radio":
                return (value?.length >= 1 && isValidAnswer(item.choices, value));
            case "checkbox":
                return (Array.isArray(value) && value.length >= 1 && isValidAnswer(item.choices, value));
            case "star":
                return (value > 1 && value < 101);
            default:
                return false;
        }

    }

    const isValidAnswer = (choices, value) => {

        if (Array.isArray(value)) {
            let arr = value.map(v =>
                (choices?.find(ch => ch.value === v)?.valid === true && v !== "")
            )
            return (arr.filter(v => v).length === value.length)
        } else {
            return (choices?.find(v => v.value === value)?.valid === true);
        }
    }

    items.forEach(item => {
        let data = datas.find(d => d.identifier === item.identifier)
        if (!data) {
            data = {
                identifier: item.identifier,
                valid: !item.required
            }
        } else {
            data = {
                ...data,
                valid: !item.required
            }
        }
        if (item.required) {
            data.valid = checkValue(item, data.value)
        }
        result.data.push(data)
    })

    result.valid = (result.data.filter(v => v.valid).length === result.data.length)

    return result;
}
