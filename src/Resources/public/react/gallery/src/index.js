import React from 'react';
import {render} from 'react-dom';
import './style/styles.scss';

import TheAdminWrapper from './components/TheAdminWrapper';
import ThePublicGalleryWrapper from './components/ThePublicGalleryWrapper';

let admin = document.getElementById('cms-gallery-admin-root');
let galleries = document.querySelectorAll('[data-cms="react-gallery"]')


if (admin) {
    render(<TheAdminWrapper galleryId={admin.dataset.galleryId}/>, admin);
}


if (galleries?.length) {
    galleries.forEach((g) => {
        render(<ThePublicGalleryWrapper galleryId={g.dataset.galleryId}/>, g);
    })
}
