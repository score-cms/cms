import parse from "html-react-parser";
import { URL_load_image_controller, URL_load_image_filesystem } from "../config";

export const flipArray = (arr) => {
  return arr.map((v, k, a) => a[a.length - 1 - k]);
};

export function parseMessage(str) {
  let args = [].slice.call(arguments, 1), i = 0;
  return str.replace(/%s/g, () => args[i++]);
}

function parseURL(str) {
  let args = [].slice.call(arguments, 1), i = 0;
  return str.replace(/%s/g, () => args[i++]);
}

export const parseHtml = (s) => {
  if (!s) return "";
  return parse(s);
};

export const convertHtmlBreakLines = (s) => {
  if (!s) return "";
  return parse(s.split("\n").join("<br/>"));
};

export const getSrcsetAndSizes = (gs, img, sizes = null) => {
  if (sizes === null) {
    sizes = [400, 600, 800, 1000, 1280, 1550, 1920];
  }
  let r = { srcSet: "", sizes: "" };
  if (!img) return r;

  sizes.forEach((s, k) => {
    let isLast = sizes.length === k + 1;
    r.sizes += isLast ? `${s}px` : `(max-width: ${s + 20}px) ${s}px, `;
    r.srcSet += getImageDynamicSrc(gs, img, s) + ` ${s}w ${isLast ? "" : ", "}`;
  });
  return r;
};

const getClosestSize = (s,a) => {
  if (!s || !a?.length) return "";
  let closest;
  a.forEach(v => {
      if (!closest && v >= s)
          closest = v;
  })
  if (!closest) closest = a[a.length - 1];
  return closest;
}

export const getImageDynamicSrc = ({dir, subs, strategy}, img, size = 800, type = "preview") => {
  if (!img) return "";

  if (type === "thumbnail") {
    let size_ready = subs?.thumbnail?.includes(size) ? size : getClosestSize(size,subs?.thumbnail)
    return parseURL( URL_load_image_filesystem, dir, img.galleryId, "thumbnail", size_ready, img.identifier, img.extension ) // <dir>,<galleryId>,<type[preview|thumbnail]>,<size>,<imageId>,<ext>
  }

  if (strategy === "load") {
    let size_ready = subs?.preview?.includes(size) ? size : getClosestSize(size,subs?.preview)
    return parseURL( URL_load_image_filesystem, dir, img.galleryId, "preview", size_ready, img.identifier, img.extension ) // <dir>,<galleryId>,<type[preview|thumbnail]>,<size>,<imageId>,<ext>
  }

  return parseURL( URL_load_image_controller, img.galleryId, img.identifier, size, img.extension );
};

