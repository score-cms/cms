//const URL_domain = "http://cms.test";
const URL_domain = "";

export const URL_api_load_gallery = URL_domain + "/api/gallery-load/"; // +id
export const URL_api_save_gallery = URL_domain + "/admin/api/gallery-save";
export const URL_api_add_images = URL_domain + "/admin/api/gallery/upload-images";
export const URL_load_image_controller = URL_domain + "/file/g/%s/%s-%s.%s"; // + <galleryId>,<imageId>,<ratio>,<ext>
export const URL_load_image_filesystem = URL_domain + "%s/%s/%s/%s/%s.%s"; // + <dir>,<galleryId>,<type[preview|thumbnail]>,<size>,<imageId>,<ext>

// export const MAX_images = 150;
// export const MAX_cols = 12;

export const RUG_Rules = {
  limit: 99,
  size: 2.7 * 1000, //kb
  width: {
    min: 550, // 1280,
    max: 3500, // 1920,
  },
  height: {
    min: 550, // 720,
    max: 3000, // 1080,
  },
  //accept: ["png", "jpg", "jpeg", "gif"],
  accept: ["png", "jpg", "jpeg"],
};

export const TEXTS = {
  alertGalleryUpdate: "Galéria bola aktualizovaná a zmeny boli uložené.",
  submit: "Uložiť",
  RUG_Warning: {
    accept: `Povolené typy súborov sú: ${RUG_Rules.accept.join(", ")}. Súbor %s je typu %s.`,
    limit: `Povolený počet obrázkov na jednu galériu je ${RUG_Rules.limit} obrázkov. Súbor s% prekročil limit.`,
    size: `Súbor %s prekročil maximálnu povolenú veľkosť súboru. Limit je ${ RUG_Rules.size / 1000 }MB.`,
    minWidth: `Obrázok má príliš malé rozlíšenie. Minimálna šírka je ${RUG_Rules.width.min}px. Súbor %s je príliš malý.`,
    minHeight: `Obrázok má príliš malé rozlíšenie. Minimálna výška je ${RUG_Rules.height.min}px. Súbor %s je príliš malý.`,
    maxWidth: `Obrázok má príliš veľké rozlíšenie. Maximálna šírka je ${RUG_Rules.width.max}px. Súbor %s je príliš veľký.`,
    maxHeight: `Obrázok má príliš veľké rozlíšenie. Maximálna výška je ${RUG_Rules.height.max}px. Súbor %s je príliš veľký.`,
  },
  RUG_requestError: "Nahrávanie súboru zlyhalo. Súbor %s sa nepodarilo nahrať.",
};


export const helpPoints = [
  `Pri vytváraní novej galérie vyplňte názov a uložte, obrázky je možné pridať až dodatočne.`,
  `Existujú dve tlačidlá s textom ${TEXTS.submit}, ale obe fungujú rovnako. Uložia všetko.`,
  `Ak chcete zmeniť poradie obrázkov, musíte na obrázok kliknúť, podržať ho a potiahnuť.`,
  `Ak obrázok nemá vyplnený alternatívny názov, nezobrazí sa ani vo verejnej galérii. (V náhľade áno.) Alternatívny názov je povinný hlavne preto, aby sa aj nevidiaci dostal k informáciám, ktoré vidiaci vidí. Môže sa potom zobraziť aj vo vyhľadávaní Google. Preto stručne popíšte obrázok.`,
  // `Voliteľný popis obrázka sa momentálne zobrazuje iba v galérii slider.`,
  `Pridávanie a odstraňovanie obrázkov bez uloženia neovplyvní verejnú galériu.`,
  `Po kliknutí na obrázok v galérii sa zobrazí voliteľný popis.`,
];

// treba pridat aj CSS lebo to tu nieje. Je v admine stranky http://neska.sk/g
export const galleryTypes = [{
    name: "Predvolená",
    type: "default",
    params: { bar: "left", showIndex:false, direction: "row", lightbox: false, size: 100, gap: 0 },
    options:[],
    disabled: true
  }, {
    name: "Slider",
    type: "carousel",
    params: { bar: "left", showIndex:false },
    options: [{
      name: "Pozícia miniatur",
      key: 'bar',
      choices: [{
        name: "vľavo",
        value: 'left'
      },{
        name: "vpravo",
        value: 'right'
      },{
        name: "hore",
        value: 'top'
      },{
        name: "dole",
        value: 'bottom'
      }]
    },{
      name: "Zobrazovať číslo obrázka",
      key: 'showIndex',
      choices: [{
        name: "Áno",
        value: true
      },{
        name: "Nie",
        value: false
      }]
    }]
  }, {
    // name: "Staticky",
    name: "Automaticky usporiadaná",
    type: "static",
    params: { direction: "row", lightbox: false },
    options: [{
      name: "Smer radenia obrázkov",
      key: 'direction',
      choices:[{
        name: "v riadkoch",
        value: 'row'
      },{
        name: "v stĺpcoch",
        value: "column"
      }]
    }, {
      name: "Možnosť zobrazenia na celej obrazovke",
      key: 'lightbox',
      choices:[{
        name: "Zakázať",
        value: false
      },{
        name: "Povoliť",
        value: true
      }]
    }]
  }, {
    name: "Klikatelná mriežka",
    type: "swipe",
    params: { size: 100, gap: 0, thumbnailType: "thumbnail" },
    options: [{
      name: "Veľkosť obrázkov",
      key: "size",
      choices: [{
        name: "malá",
        value: 100
      },{
        name: "stredná",
        value: 200
      },{
        name: "velká",
        value: 400
      }]
    },{
      name: "Veľkosť medzery",
      key: "gap",
      choices: [{
        name: "žiadna",
        value: 0
      },{
        name: "malá",
        value: 2
      },{
        name: "stredná",
        value: 5
      },{
        name: "velká",
        value: 8
      }]
    },{
      name: "Typ miniatury",
      key: "thumbnailType",
      choices: [{
        name: "Upravená",
        value: "thumbnail"
      },{
        name: "Originál",
        value: "preview"
      }]
    }]
  }, 
  // {
  //   name: "Obrázky v mriežke nekvalitné",
  //   type: "grid",
  //   params: {},
  //   options: [],
  //   disabled: true,
  // },
];

export const defaultGS = {
  dir: '/cms_gallery',
  subs: {
    'thumbnail' : ['100', '200', '400'],
    'preview' : ['800', '1280', '1920']
  },
  strategy: "load"
};

export const RTEtoolbarConfig = {
  // Optionally specify the groups to display (displayed in the order listed).
  //display: ['INLINE_STYLE_BUTTONS', 'BLOCK_TYPE_BUTTONS', 'BLOCK_TYPE_DROPDOWN', 'HISTORY_BUTTONS'],
  display: ['HISTORY_BUTTONS', 'BLOCK_TYPE_DROPDOWN', 'INLINE_STYLE_BUTTONS', 'LINK_BUTTONS', 'BLOCK_TYPE_BUTTONS'],
  INLINE_STYLE_BUTTONS: [
    {label: 'Bold', style: 'BOLD'},
    // {label: 'Italic', style: 'ITALIC'},
    // {label: 'Underline', style: 'UNDERLINE'}
  ],
  BLOCK_TYPE_DROPDOWN: [
    {label: 'normálny text', style: 'unstyled'},
    //{label: 'Heading Large', style: 'header-one'},
    {label: 'Nadpis H2', style: 'header-two'},
    {label: 'Nadpis H3', style: 'header-three'}
  ],
  BLOCK_TYPE_BUTTONS: [
    {label: 'Nezoradený', style: 'unordered-list-item'},
    {label: 'Zoradený', style: 'ordered-list-item'}
  ]
};