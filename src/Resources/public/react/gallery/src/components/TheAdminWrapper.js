import React, { useState, useEffect, useReducer, useMemo } from "react";
import { defaultGS, galleryTypes, helpPoints, RTEtoolbarConfig, TEXTS, URL_api_load_gallery, URL_api_save_gallery } from "../config";
import RichTextEditor from "react-rte";
import axios from "axios";
import TheUploader from "./admin/TheUploader";
import Loader from "./shared/Loader";
import Messages from "./shared/Messages";
import GalleryTypes from "./admin/GalleryTypes";

const TheAdminWrapper = (props) => {
  const { galleryId: galleryIdDefault } = props;

  /**
   *  STATE HOOKS
   */
  const [fetching, setFetching] = useState(0);
  const [messages, setMessages] = useState([]);

  const [stateData, setStateData] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    {
      galleryId: galleryIdDefault ? galleryIdDefault : null,
      type: galleryTypes[0].type,
      params: galleryTypes[0].params,
      name: "Galéria",
      description: RichTextEditor.createEmptyValue(),
      images: [],
      endMessage: "",
      isActive: true,
      visibility: true,
    }
  );

  const {  galleryId,  name,  description,  type,  params,  images,  endMessage,  isActive,  visibility  } = stateData;

  const [showForm, setShowForm] = useState(false);


  useEffect(() => {
    if (galleryId && galleryId !== "0") {
      setFetching((f) => f + 1);
      axios
        .get(URL_api_load_gallery + galleryId)
        .then((r) => {
          // console.log(8.1, "ok", r.data)
          if (r.data.status !== "ok") throw new Error(r.data.message);
          setStateData({
            ...r.data.data,
            description: RichTextEditor.createValueFromString(r.data.data.description || "", "html"),
          });
        })
        .catch((e) => {
          setMessages((m) => m.concat([{ text: e.message }]));
          // console.log(9.1, "err", e)
        })
        .then(() => {
          // console.log(10.1, "fin")
          setFetching((f) => f - 1);
        });
    }
  }, [galleryId]);

  
  const gs = useMemo(() => {
    if (!stateData.galleryDir) return defaultGS;
    return({
      dir: stateData?.galleryDir,
      subs: stateData?.imageSubDirs,
      strategy: stateData?.imageStrategy
    });
  },[stateData])


  /**
   * METHODS
   */

  const handleType = ({type,params}) => {
    setStateData({
      type, params
    });
  };

  const handleGalleryName = (e) => {
    setStateData({
      name: e.target.value,
    });
  };

  const handleGalleryDescription = (description) => {
    setStateData({ description });
  };

  // const handleGalleryEndMessage = (e) => {
  //   setStateData({
  //     endMessage: e.target.value,
  //   });
  // };

  const handleGalleryIsActive = () => {
    setStateData({
      isActive: !isActive,
    });
  };

  const handleGalleryVisibility = () => {
    setStateData({
      visibility: !visibility,
    });
  };

  const handleSubmit = () => {

    let galleryData = new FormData();
    galleryData.append("galleryId", galleryId);
    galleryData.append("name", name);
    galleryData.append("description", description.toString("html"));
    galleryData.append("endMessage", endMessage);
    galleryData.append("isActive", isActive);
    galleryData.append("visibility", visibility);
    galleryData.append("ratio", '');
    galleryData.append("type", type);
    galleryData.append("params", JSON.stringify(params));
    galleryData.append("images", JSON.stringify(images));

    setFetching((f) => f + 1);
    axios
      .post(URL_api_save_gallery, galleryData)
      .then((r) => {
        // console.log(8, "ok", r.data)
        if (r.data.status !== "ok") throw new Error(r.data.message);
        setMessages((m) =>
          m.concat([{ type: "ok", text: TEXTS.alertGalleryUpdate }])
        );
        setStateData({
          ...r.data.data,
          description: RichTextEditor.createValueFromString(r.data.data.description || "", "html"),
        });
      })
      .catch((e) => {
        setMessages((m) => m.concat([{ text: e.message }]));
        // console.log(9, "err", e)
      })
      .then(() => {
        // console.log(10, "fin")
        setFetching((f) => f - 1);
      });
  };

  return (
    <>
      <Messages messages={messages} onMessages={setMessages} />
      <div className={"cms-gallery-edit-wrapper"}>
        <Loader fetching={fetching > 0} />

        <div>
          <div>
            <h3>Pomocník - návod</h3>
            <ul>
              {helpPoints.map((v,k)=><li key={k}>{v}</li>)}
            </ul>
          </div>
          <div className="gallery-info">
            <div className="gallery-row">
              <label htmlFor="gallery-name">Názov galérie:</label>
              <input
                style={{width:'100%'}}
                name="gallery-name"
                id="gallery-name"
                value={name}
                onChange={handleGalleryName}
              />
            </div>
            <div className="gallery-row">
              <h3>Popis – uvedenie galérie:</h3>
              <RichTextEditor
                toolbarConfig={RTEtoolbarConfig}
                name="gallery-desc"
                value={description}
                onChange={handleGalleryDescription}
              />
            </div>
            {/* <div className="gallery-row">
              <label htmlFor="gallery-end-message">
                Informácia pod galériou:
              </label>
              <textarea
                style={{width:'100%'}}
                name="gallery-end-message"
                id="gallery-end-message"
                value={endMessage}
                rows={3}
                onChange={handleGalleryEndMessage}
              />
              <small>Najlepšie nevypĺňať</small>
            </div> */}
            <div className="gallery-row">
              <label htmlFor={"cms_gallery_is_active"}>
                <input
                  style={{marginRight:'0.5em'}}
                  id={"cms_gallery_is_active"}
                  type="checkbox"
                  checked={isActive}
                  onChange={handleGalleryIsActive}
                />
                {"Galéria je aktívna"}
              </label>
            </div>
            <div className="gallery-row">
              <label htmlFor={"cms_gallery_is_visibility"}>
                <input
                  style={{marginRight:'0.5em'}}
                  id={"cms_gallery_is_visibility"}
                  type="checkbox"
                  checked={visibility}
                  onChange={handleGalleryVisibility}
                />
                {"Galéria je verejná"}
              </label>
            </div>
            <GalleryTypes
                onChange={handleType}
                params={params}
                type={type}
             />
            <div>
            <button  onClick={handleSubmit}>{TEXTS.submit}</button>
            <button style={ showForm ? {backgroundColor: 'inherit'}:{}} onClick={()=>setShowForm(true)}> Upraviť názov a popis </button>
            <button style={ showForm ? {}:{backgroundColor: 'inherit'}} onClick={()=>setShowForm(false)}> Upraviť poradie alebo vymazať </button>
            </div>
          </div>
          <div>
            {galleryId && galleryId !== "0" && (
              <TheUploader
                gs={gs}
                showForm={showForm}
                fetching={fetching > 0}
                galleryId={galleryId}
                images={images}
                //dir={stateData.galleryDir}
                onImagesUpdate={(images) => setStateData({ images })}
                onAddMessage={({ type, text }) =>
                  setMessages((m) => m.concat([{ type, text }]))
                }
              />
            )}
          </div>         
          <button onClick={handleSubmit} className={"btn-save"}>
            {TEXTS.submit}
          </button>
        </div>
      </div>
    </>
  );
};

export default TheAdminWrapper;
