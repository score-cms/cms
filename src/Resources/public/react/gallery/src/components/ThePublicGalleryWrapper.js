import axios from "axios";
import React, { useState, useEffect, useMemo } from "react";
import "react-image-gallery/styles/scss/image-gallery.scss";
import { defaultGS, galleryTypes, URL_api_load_gallery } from "../config";
import { parseHtml, convertHtmlBreakLines } from "../model/actions";
import GridGallery from "./public/GridGallery";
import CarouselGallery from "./public/CarouselGallery";
import StaticGallery from "./public/StaticGallery";
import PhotoswipeGallery from "./public/PhotoswipeGallery";

const ThePublicGalleryWrapper = (props) => {
  const { galleryId } = props;

  const [deviceWidth] = useState(
    parseInt(document.querySelector("html").clientWidth)
  );

  const [id] = useState(galleryId);
  const [layout, setLayout] = useState(null);
  const [gallery, setGallery] = useState(null);

  useEffect(() => {
    axios.get(URL_api_load_gallery + id).then((r) => {
      if (r.data.status !== "ok") throw new Error(r.data.message);
      setGallery(r.data.data);
      setLayout(galleryTypes.find((v) => v.type === r.data.data.type));
    });
  }, [id]);

  const gs = useMemo(() => {
    if (!gallery) return defaultGS;
    return({
      dir: gallery?.galleryDir,
      subs: gallery?.imageSubDirs,
      strategy: gallery?.imageStrategy
    });
  },[gallery])

  if (!gallery || !layout) return null;

  return (
    <div className="cms-gallery">
      <div className={"cms-gallery-description"}>
        {parseHtml(gallery.description)}
      </div>
      <GalleryComponent 
          deviceWidth={deviceWidth} 
          layout={layout} 
          params={gallery.params || {}} 
          images={gallery.images || []} 
          gs={gs}
      />
      {/* <div className={"cms-gallery-footer-message"}>
        {convertHtmlBreakLines(gallery.endMessage)}
      </div> */}
    </div>
  );
};

export default ThePublicGalleryWrapper;

const GalleryComponent = (props) => {
  const {deviceWidth, layout, params, images, gs } = props;

  const galleryProps = {...layout.params, ...params, deviceWidth, images, gs }

  switch (layout.type) {
    case "static":
      return <StaticGallery {...galleryProps} />;
    case "swipe":
      return <PhotoswipeGallery {...galleryProps} />;
    case "grid":
      return <GridGallery {...galleryProps} />;
    case "carousel":
    default:
      return <CarouselGallery {...galleryProps} />;
  }
};
