import React, { useState, useCallback } from "react";
import { getImageDynamicSrc } from "../../model/actions";

import "photoswipe/dist/photoswipe.css";
import "photoswipe/dist/default-skin/default-skin.css";

import { Gallery, Item } from "react-photoswipe-gallery";

const PhotoswipeGallery = (props) => {
  const { deviceWidth, images, size, gap, thumbnailType, gs } = props;

  const [previewSize] = useState(parseInt(deviceWidth * 0.9)); // 90% of page width

  const responsive = useCallback(
    (img) => {
      let r = { srcSet: "", sizes: "" };
      if (!img) return r;
      r.sizes += `(max-width: ${450}px) ${100}px`;
      r.srcSet += getImageDynamicSrc(gs, img, 100, thumbnailType === "preview" ? "preview" : "thumbnail") + ` 100w`;
      if (size >= 200 && previewSize >= 450) {
        r.sizes += `, (max-width: ${850}px) ${200}px`;
        r.srcSet += `, ${getImageDynamicSrc(gs, img, 200, thumbnailType === "preview" ? "preview" : "thumbnail")} 200w`;
      }
      if (size >= 400 && previewSize >= 850) {
        r.sizes += `, ${400}px`;
        r.srcSet += `, ${getImageDynamicSrc(gs, img, 400, thumbnailType === "preview" ? "preview" : "thumbnail")} 400w`;
      }
      r.sizes += `, ${size}px`;
      return r;
    },
    [size, previewSize, thumbnailType, gs]
  );

  if (!images?.length) return null;
  return (
    <div
      style={{
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "center",
        alignItems: "center",
        gap: gap ? `0.${gap}em` : 0,
      }}
      className={`cms-gallery-swipe-${thumbnailType}-type`}
      data-cms-gallery-size={size}
    >
      <Gallery
        shareButton={false}
        fullscreenButton={false}
        zoomButton={false}
        options={{
          //getDoubleTapZoom: () => 1,
          maxSpreadZoom: 1,
        }}
      >
        {images.map((p) => (
          <Item
            key={p.identifier}
            id={p.identifier}
            original={getImageDynamicSrc(gs, p, previewSize)}
            thumbnail={getImageDynamicSrc(gs, p, 150)}
            width={previewSize}
            height={(previewSize / p.x) * p.y}
            title={p.description}
          >
            {({ ref, open }) => (
              <img
                alt={p.name}
                ref={ref}
                onClick={open}
                src={getImageDynamicSrc(gs, p, size, thumbnailType === "preview" ? "preview" : "thumbnail")}
                {...responsive(p)}
              />
            )}
          </Item>
        ))}
      </Gallery>
    </div>
  );
};

export default PhotoswipeGallery;
