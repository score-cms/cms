import React, { useMemo } from "react";
import ImageGallery from "react-image-gallery";
import "react-image-gallery/styles/scss/image-gallery.scss";
import { getImageDynamicSrc, getSrcsetAndSizes } from "../../model/actions";

//console.log(18, document.querySelector('html').clientWidth);

const CarouselGallery = (props) => { // + deviceWidth
  const { images: defaultIimages, bar, showIndex, gs } = props;

  const images = useMemo(() => {
    if (!defaultIimages?.length) return [];

    return defaultIimages.map((p, k) => {
      // let ratio = p.ratio.split("/");
      // let w = parseInt((75 * ratio[0]) / ratio[1]);
      const {sizes, srcSet} = getSrcsetAndSizes(gs, p)
      return {
        //fullscreen : getImageDynamicSrc(gs, p, 1920),
        original: getImageDynamicSrc(gs, p, 800),
        originalAlt: p.name,
        thumbnail: getImageDynamicSrc(gs, p, 100, "thumbnail"),
        // thumbnailHeight: 75,
        // thumbnailWidth: w,
        thumbnailHeight: 100,
        thumbnailWidth: 100,
        thumbnailLabel: showIndex === true ? k + 1 : undefined,
        thumbnailAlt: p.name,
        description: p.description,
        sizes,srcSet
      };
    });
  }, [defaultIimages, showIndex, gs]);

  if (!images?.length) return null;

  return (
    <ImageGallery
      items={images}
      thumbnailPosition={bar || "left"}
      showBullets={false}
      slideDuration={700}
      slideInterval={3000}
    />
  );
};

export default CarouselGallery;
