import React, { useState, useMemo, useCallback } from "react";
import Gallery from "react-photo-gallery";
import Carousel, { Modal, ModalGateway } from "react-images";
import { getImageDynamicSrc, getSrcsetAndSizes } from "../../model/actions";

const StaticGallery = (props) => {
  const { gs, images: defaultIimages, direction, lightbox } = props;

  const [currentImage, setCurrentImage] = useState(0);
  const [viewerIsOpen, setViewerIsOpen] = useState(false);

  const openLightbox = useCallback((event, { photo, index }) => {
    setCurrentImage(index);
    setViewerIsOpen(true);
  }, []);

  const closeLightbox = () => {
    setCurrentImage(0);
    setViewerIsOpen(false);
  };


  const images = useMemo(() => {
    if (!defaultIimages?.length) return [];
    return defaultIimages.map((p, k) => {
      let ss =  getSrcsetAndSizes(gs, p);
      return {
        src: getImageDynamicSrc(gs, p, 600),
        srcSet: ss.srcSet,
        sizes: ss.sizes,
        width: p.x,
        height: p.y,
        alt: p.name,
        title: p.description,
      };
    });
  }, [defaultIimages, gs]);

  if (!images?.length) return null;

  if (lightbox !== true){
    return <Gallery direction={direction} photos={images} />;
  }

    return (
      <div>
        <Gallery photos={images} direction={direction} onClick={openLightbox} />
        <ModalGateway>
          {viewerIsOpen ? (
            <Modal onClose={closeLightbox}>
              <Carousel
                currentIndex={currentImage}
                views={images.map(x => ({
                  ...x,
                  srcset: x.srcSet,
                  caption: x.title
                }))}
              />
            </Modal>
          ) : null}
        </ModalGateway>
      </div>
    );  
    
};

export default StaticGallery;
