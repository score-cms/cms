import React from "react";
import { galleryTypes } from "../../config";

const GalleryTypes = (props) => {
  const { type, params, onChange: handleChange } = props;

  const handleUpdate = ({ type, params }) => {
    handleChange({ type, params });
  };

  const handleParamsUpdate = ({ type }) => p => {
      handleUpdate({ type, params: { ...params, ...p } });
    };

  if (!type) return null;

  return (
    <div>
      <h3>{"Typ galérie"}</h3>
      {galleryTypes.map((t) => (
        <div key={t.type}>
          <label>
            <input
              style={{marginRight:'0.5em'}}
              disabled={t.disabled === true}
              value={t.type}
              name="type"
              type="radio"
              checked={t.type === type}
              onChange={() => handleUpdate(t)}
            />
            {t.name}
          </label>
          <TypeOptions
            show={t.type === type}
            type={t}
            params={params}
            onChange={handleParamsUpdate(t)}
          />
        </div>
      ))}
    </div>
  );
};

export default GalleryTypes;

const TypeOptions = (props) => {
  const { show, params, type, onChange: handleChange } = props;

  const handleUpdate = (key, value) => {
    handleChange({ [key]: value });
  };

  if (!show) return null;

  return (
    <div>
      {type.options.map((o) => (
        <fieldset
          key={o.key}
          style={{
            marginLeft: "2em",
            marginBottom: "0.5em",
          }}
        >
          <legend>{o.name}</legend>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              flexWrap: "wrap",
              gap: '1em'
            }}
          >
            {o.choices.map((v) => (
              <label key={v.value}>
                <input
                  style={{marginRight:'0.5em'}}
                  disabled={o.disabled === true || v.disabled === true}
                  value={v.value}
                  name={o.key}
                  type="radio"
                  checked={params[o.key] === v.value}
                  onChange={() => handleUpdate(o.key, v.value)}
                />
                {v.name}
              </label>
            ))}
          </div>
        </fieldset>
      ))}
    </div>
  );
};
