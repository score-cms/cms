import React from "react";
import { getImageDynamicSrc } from "../../model/actions";

const ImagesForm = (props) => {
  const { gs, images, onImagesUpdate: handleImagesUpdate } = props;

  const handleUpdate = (img) => (key, value) => {
    let newImages = images.map((v) => (v === img ? { ...v, [key]: value } : v));
    handleImagesUpdate(newImages);
  };

  if (!images?.length) return null;

  return (
    <div style={{ margin: "0.7em auto", display: "flex", flexWrap: "wrap" }}>
      {images.map((img) => (
        <ImageForm key={img.identifier} gs={gs} image={img} onUpdate={handleUpdate(img)} />
      ))}
    </div>
  );
};

export default ImagesForm;
//getImageUrl
const ImageForm = (props) => {
  const { gs, image, onUpdate: handleUpdate } = props;

  const handleIsActiveChange = () => {
    handleUpdate("isActive", !image.isActive);
  };  
  const handleNameChange = (e) => {
    handleUpdate("name", e.target.value);
  };
  const handleDescriptionChange = (e) => {
    handleUpdate("description", e.target.value);
  };

  return (
    <div
      style={{
        fontSize: '14px',
        width: 200,
        height: 200,
        margin: '0.1em',
        backgroundImage: `url(${getImageDynamicSrc(gs, image, 200, "thumbnail")})`,
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-around",
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          padding:'0.1em 0.2em',
          backgroundColor: "rgba(255,255,255,0.7)",
        }}
      >
        <input
          //style={{ backgroundColor: "rgba(255,255,255,0.5)", padding: "0.2em" }}
          style={{marginRight:'0.5em'}}
          id={image.identifier + "_is_active"}
          type={"checkbox"}
          onChange={handleIsActiveChange}
          checked={image.isActive}
        />
        <label htmlFor={image.identifier + "_is_active"}>
          {" Obrázok je viditelný "}
        </label>
      </div>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          padding:'0.1em 0.2em',
          backgroundColor: "rgba(255,255,255,0.7)",
        }}
      >
        <label htmlFor={image.identifier + "_name"}>
         {" Alternatívny názov "}<span style={{ color: "red" }}>*</span>
        </label>
        <input
          style={{ backgroundColor: "rgba(255,255,255,0.5)", padding: "0.2em" }}
          id={image.identifier + "_name"}
          type={"text"}
          onChange={handleNameChange}
          value={image.name || ""}
        />
      </div>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          padding:'0.1em 0.2em',
          backgroundColor: "rgba(255,255,255,0.7)",
        }}
      >
        <label htmlFor={image.identifier + "_description"}>
          {"Nepovinný popis"}
        </label>
        <textarea
          style={{ backgroundColor: "rgba(255,255,255,0.5)", padding: "0.2em" }}
          rows={3}
          id={image.identifier + "_description"}
          onChange={handleDescriptionChange}
          value={image.description || ""}
        />
      </div>
    </div>
  );
};
