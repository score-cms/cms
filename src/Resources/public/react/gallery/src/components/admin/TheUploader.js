import React, {  useMemo, useCallback } from "react";
import { arrayMoveImmutable } from "array-move";
import RUG from "react-upload-gallery";
//import RUG, { DragArea, DropArea, Card, List } from "react-upload-gallery";
import { RUG_Rules, TEXTS, URL_api_add_images } from "../../config";
import { getImageDynamicSrc, parseMessage} from '../../model/actions'

// Add style manually
import "react-upload-gallery/dist/style.css"; // or scss
import ImagesForm from "./ImagesForm";

const TheUploader = (props) => {
  const {
    gs,
    showForm,
    galleryId,
    images: rawImages,
    fetching,
    onImagesUpdate: handleImagesUpdate,
    onAddMessage: handleAddMessage,
  } = props;

  const parseSize = useCallback((size) => {
    if (Math.abs(size) > 2 * 1000 * 1000)
      return Math.round(size / (100 * 1000)) / 10 + "MB";
    if (Math.abs(size) > 2 * 1000) return Math.round(size / 100) / 10 + "KB";
    return Math.round(size) + "B";
  }, []);

  const rugImages = useMemo(() => {
    if (!rawImages?.length) return [];
    let newImages = rawImages.map((p) => ({
      id: p.identifier,
      name: p.filename,
      size: parseSize(p.size),
      source: getImageDynamicSrc(gs, p, 200, "thumbnail"),
    }));
    return newImages;
  }, [parseSize, rawImages, gs]);

  const handleSortEnd = (rgis, { oldIndex, newIndex }) => {
    let newImages = arrayMoveImmutable(rawImages.slice(), oldIndex, newIndex)
      .map((v, k) => ({ ...v, sortOrder: k }));
    //console.log(12, oldIndex, newIndex, rawImages, newImages);
    handleImagesUpdate(newImages);
  };

  const handleAdd = (img) => {
    if (img.r.status !== "ok")
      return handleAddMessage({ type: "error", text: img.r.message });

    let newImages = [{
        name: "",
        description: "",
        galleryId: img.r.galleryId,
        extension: img.r.extension,
        identifier: img.id,
        isActive: true,
      }].concat(rawImages)
      .map((v, k) => ({ ...v, sortOrder: k }));
    //console.log(10, img, newImages.x);
    handleImagesUpdate(newImages);
  };

  const handleDelete = (img) => {
    let newImages = rawImages
      .filter((v) => v.identifier !== img.id)
      .map((v, k) => ({ ...v, sortOrder: k }));
    //console.log(11, img, newImages);
    handleImagesUpdate(newImages);
  };

  const handleWarning = (type, rules, x) => {
    //console.log(12, type, rules.file.size, rules.file.type, rules);
    handleAddMessage({ type: "warning", text: parseMessage(TEXTS.RUG_Warning[type], rules.file.name, rules.file.type) });
  };

  const handleError = ({ status, response, image }) => {
    //console.log(14, status, response, image);
    handleAddMessage({ type: "error", text: parseMessage(TEXTS.RUG_requestError, image.name) });
  };

  if (fetching) return null;

  return (
      <RUG
        initialState={rugImages}
        action={URL_api_add_images}
        source={(r) => r.src}
        alias={(r) => ({ id: r.identifier, r })}
        send={{ galleryId }}
        rules={RUG_Rules}
        accept={RUG_Rules.accept}
        //sorting={{ pressDelay: 50 }}
        onError={handleError}
        onWarning={handleWarning}
        onSortEnd={handleSortEnd}
        onSuccess={handleAdd}
        onDeleted={handleDelete}
      > 
        { showForm &&
          <ImagesForm
            gs={gs}
            fetching={fetching}       
            images={rawImages}
            onImagesUpdate={handleImagesUpdate}
          />     
        }      
      </RUG>    
  );

  // return (
  //   <RUG
  //     initialState={rugImages}
  //     action={URL_api_add_images}
  //     source={(response) => response.src}
  //     onSortEnd={handleSortEnd}
  //   >
  //     <DropArea>
  //       {(isDrag) => (
  //         <div className={isDrag ? " dragging" : ""}>
  //           <DragArea className={"rug-items __card __sorting"}>
  //             {(img) => (
  //               <div key={img.uid} className="rug-item">
  //                 <Card image={img} />
  //               </div>
  //             )}
  //           </DragArea>
  //         </div>
  //       )}
  //     </DropArea>
  //   </RUG>
  // );
};

export default TheUploader;
