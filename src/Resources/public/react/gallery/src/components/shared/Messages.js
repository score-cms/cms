import React, { useCallback } from 'react';

const Messages = props => {

    const {
        messages,
        onMessages
    } = props;

     const getClassName = useCallback((type) => {
       switch (type) {
         case "ok":
           return "alert-success";
         case "warning":
           return "alert-warning";
         //            case "info" :
         case "error":
         default:
           return "alert-danger";
       }
     }, []);   
    
     const handleClearMessage = (m,e) => {
        e.stopPropagation();
        onMessages(ms => ms.filter(v => v !== m))
    }

    return (
        <div className={"alerts-wrapper"}>
            {messages?.length > 0 &&
            messages.map((message, key) =>
                message.text &&
                <div key={key} className={"alert " + (getClassName(message.type))}
                     role="alert">
                    <button type="button" className="close" data-dismiss="alert" aria-label="Close"
                            onClick={(e) => handleClearMessage(message, e)}>
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <p className="m-0">{message.text}</p>
                </div>
            )}
        </div>
    );
}
export default Messages
