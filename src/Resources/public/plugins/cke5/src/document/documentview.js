// document/documentview.js

import {
	View,
	LabeledFieldView,
	createLabeledInputText,
	ButtonView,
	submitHandler,
	createDropdown,
	SwitchButtonView
} from '@ckeditor/ckeditor5-ui';

import { icons } from '@ckeditor/ckeditor5-core';
import { URL_loadDocuments, labels } from './config';

export default class FormView extends View {
	constructor(locale) {
		super(locale);

		this.urlPrefixDocumentShow = "";
		this.urlPrefixDocumentDownload = "";
		this.document = {};
		this.documents = [];
		this.domain = {};
		this.domains = [];
        this.category = {};
		this.categories = [];
		this.showDetail = true;
		this.isDomainLink = false;

		fetch(URL_loadDocuments)
			.then((r) => r.json())
			.then((d) => {
                if(d.status === "ok") {
					this.urlPrefixDocumentShow = d.urlPrefixDocumentShow;
                    this.urlPrefixDocumentDownload = d.urlPrefixDocumentDownload;
                    this.documents = d.documents;
                    this.domains = d.domains;
                    this.categories = d.categories;
                }
			});

		this.categoryDropdown = this.createDropdownFor('category', labels.category);
		this.domainDropdown = this.createDropdownFor('domain', labels.domain);
		this.startsWith = this.createStartsWithInput();
		this.documentDropdown = this.createDropdownFor('document', labels.document);
		this.showDetailSwitch =  this.createSwitchInputFor('showDetail', labels.showDetail);
		this.isDomainLinkSwitch = this.createSwitchInputFor('isDomainLink', labels.isDomainLink, false);

		// Create the save and cancel buttons.
		this.saveButtonView = this._createButton(labels.save, icons.check, 'ck-button-save');
		this.saveButtonView.type = 'submit';
		this.cancelButtonView = this._createButton(labels.cancel, icons.cancel, 'ck-button-cancel');
		this.cancelButtonView.delegate('execute').to(this, 'cancel');

		this.childViews = this.createCollection([
			this.categoryDropdown,
			this.domainDropdown,
            this.startsWith,
			this.documentDropdown,			
			this.showDetailSwitch,
			this.isDomainLinkSwitch,
            this.saveButtonView,
			this.cancelButtonView,
		]);

		this.setTemplate({
			tag: 'form',
			attributes: {
				class: ['ck', 'ck-document-form'],
				tabindex: '-1',
			},
			children: this.childViews,
		});
	}

	createStartsWithInput() {
		const labeledInput = new LabeledFieldView(this.locale, createLabeledInputText);

		labeledInput.label = labels.startsWith;

		return labeledInput;
	}

	createDropdownFor(type, label = labels.dropdownDefault) {

		const dropdown = createDropdown(this.locale);

		dropdown.buttonView.set({
			label: label,
			withText: true,
		});

		dropdown.buttonView.on('execute', () => {
            if (dropdown?.isOpen === false) {
                
                dropdown.panelView.children.clear();

                let list = [];
				let docList = this.getFilteredDocumentList();
                if (type === "domain") {
                    list = this.domains;
                } else if (type === "category") {
                    list = this.categories;
                } else { // document
					list = docList;
                } 

                const items = [{name: "", seo: ""}, ...list].map((item) => {
                    const button = new ButtonView(this.locale);
                    button.set({
                        label: item.name,
                        icon: item.seo === this[type]?.seo ? icons.check : icons.threeVerticalDots,
                        tooltip: false,
                        class: 'ck jh-button',
                        withText: true,
                    });
                    button.on('execute', () => {
                        dropdown.set('isOpen', false);
                        dropdown.buttonView.set({ label: item.name || label });

                        this[type] = item;
						this.checkisDomainLinkSwitchActive()
                    });
                    return(button);
                });

                dropdown.panelView.children.addMany(items);
            }

        });

		return dropdown;
	}

	_createButton(label, icon, className) {
		const button = new ButtonView(this.locale);

		button.set({
			label,
			icon,
			tooltip: true,
			class: className,
		});

		return button;
	}	
	
	createSwitchInputFor(type, label, isEnabled = true) {
		const switcher = new SwitchButtonView(this.locale);
		switcher.set({
			isEnabled,
			label: label,
			withText: true,
			tooltip: true,
			isOn: this[type]
		});

		switcher.on('execute', () => {
			let isOn = !this[type];
			this[type] = isOn;
			switcher.set({isOn})
			this.checkisDomainLinkSwitchActive()
		});

		return switcher;
	}

	getFilteredDocumentList() {
		let list = this.documents;
		if (this.domain) // document filter domains
			list = list.filter(v => !this.domain?.name || v.domains.includes(this.domain.name))
		if (this.category) // document filter categories
			list = list.filter(v => !this.category?.name || v.categories.includes(this.category.name))
		if (this.startsWith.fieldView.element.value){ // document filter by starts with
			let val = this.startsWith.fieldView.element.value.toLowerCase();
			list = list.filter(v => (
				v.name.toLowerCase().startsWith(val) || v.seo.replace('-', ' ').startsWith(val)
			))
		}

		if (!this.document?.seo  || list.map(d=>d.seo).includes(this.document.seo)) {
			this.document = {};
			this.documentDropdown.buttonView.set({ label: labels.document });
		}

		return list

	}

	checkisDomainLinkSwitchActive() {
		let active = this.isDomainLinkSwitch.isEnabled
		let newActive = this.showDetail && !!this.domain?.name;
		if (active !== newActive) {
			let isOn = this.isDomainLink && newActive
			this.isDomainLinkSwitch.set({
				isOn,
				isEnabled: newActive === true,
			})
			this.isDomainLink = isOn;
		}
	}

	render() {
		super.render();

		submitHandler({
			view: this,
		});
	}


	focus() {
		this.childViews.first.focus();
	}
}
