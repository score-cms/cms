// document/document.js

import DocumentEditing from './documentediting';
import DocumentUI from './documentui';
import Plugin from '@ckeditor/ckeditor5-core/src/plugin';

export default class Document extends Plugin {
    static get requires() {
        return [ DocumentEditing, DocumentUI ];
    }
}