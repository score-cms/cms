//const domain = "http://aaa.test";
const domain = "";

// export const URL_loadDocuments = domain + '/api/documents';
export const URL_loadDocuments = domain + '/admin/score/api/document/cke-plugin-data';


export const labels = {
        toolbarLabel: "Dokumenty",
        document: "Dokument",
        category: "Kategoria",
        domain: "Oblasť",
        startsWith: "Názov sa začína na",
        showDetail: "Odkaz na detail dokumentu", // label alternative: 'Nezobrazovať detail dokumentu ale rovno ho stiahnuť'
        isDomainLink: "Rozbaliť vybranú oblasť",
        save: "Uložiť",
        cancel: "Zrušiť",
        dropdownDefault: "Dropdown"
};

