// document/documentediting.js

import Plugin from '@ckeditor/ckeditor5-core/src/plugin';

export default class DocumentEditing extends Plugin {
    init() {
        //console.log( 'DocumentEditing#init() got called' );
        this._defineSchema();
        this._defineConverters();
    }

    _defineSchema() {
        const schema = this.editor.model.schema;

        // Extend the text node's schema to accept the document attribute.
        schema.extend('$text', {
            allowAttributes: ['document']
        });
    }


    _defineConverters() {
        const conversion = this.editor.conversion;

        // Conversion from a model attribute to a view element.
        conversion.for('downcast').attributeToElement({
            model: 'document',
            // Callback function provides access to the model attribute value
            // and the DowncastWriter.
            view: (modelDocumentValue, conversionApi) => {
                const {writer} = conversionApi;
                let download = {}
                if (modelDocumentValue?.action === "download") {
                    download = {download: modelDocumentValue?.url?.split("/").reverse()[0]}
                }

                return writer.createAttributeElement('a', {
                    href: modelDocumentValue?.url || "",
                    ['data-document-code']: modelDocumentValue?.code || "",
                    ['data-document-domain']: modelDocumentValue?.domain || "",
                    ['data-document-action']: modelDocumentValue?.action || "",
                    ['data-cms']: "document-link",
                    ...download
                });
            }
        });


        // Conversion from a view element to a model attribute.
        conversion.for('upcast').elementToAttribute({
            view: {
                name: 'a',
                attributes: ['href', 'data-cms', 'data-document-action']
            },
            model: {
                key: 'document',
                // Callback function provides access to the view element.
                value: viewElement => {
                    const document = {
                        url: viewElement.getAttribute('href'),
                        code: viewElement.getAttribute('data-document-code'),
                        domain: viewElement.getAttribute('data-document-domain'),
                        action: viewElement.getAttribute('data-document-action'),
                    }
                    return document;
                }
            },
            converterPriority: 'high'

        });


    }


}

