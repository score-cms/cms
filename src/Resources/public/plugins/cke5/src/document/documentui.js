// document/documentui.js

import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import ButtonView from '@ckeditor/ckeditor5-ui/src/button/buttonview';
import './styles.css';											// ADDED
import { ContextualBalloon, clickOutsideHandler } from '@ckeditor/ckeditor5-ui'; // ADDED
import FormView from './documentview';							// ADDED
import { labels } from './config';
// import toolbarIcon from './servers-interface-symbol.svg';
// import toolbarIcon from './favorite-data-folder.svg';
// import toolbarIcon from './folder-with-data-interface-symbol.svg';
import toolbarIcon from './document-search-interface-symbol.svg';


export default class DocumentUI extends Plugin {
    static get requires() {
        return [ ContextualBalloon ];
    }

    init() {
		const editor = this.editor;

        // Create the balloon and the form view.
		this._balloon = this.editor.plugins.get( ContextualBalloon );
		this.formView = this._createFormView();

		editor.ui.componentFactory.add( 'document', () => {
			const button = new ButtonView();

			button.label = labels.toolbarLabel;
			button.tooltip = true;
            button.icon = toolbarIcon;
			//button.withText = true;

			// Show the UI on button click.
			this.listenTo( button, 'execute', () => {
				this._showUI();
			} );

			return button;
		} );
	}



    _createFormView() {
        const editor = this.editor;
        const formView = new FormView( editor.locale );

        this.listenTo( formView, 'submit', () => {

            const document = formView.document;

            editor.model.change( writer => {

                let url = "";
                let text = "";

                if (formView.showDetail === false) {
                    url = formView.urlPrefixDocumentDownload + document.seo +  document.ext;
                    text = document.name + document.info;
                } else {
                    url = formView.urlPrefixDocumentShow + document.seo;
                    text = document.name;
                } 

                if (formView.isDomainLink){
                    url += "#" + formView.domain.seo;
                }
                
                editor.model.insertContent(
                    writer.createText( text, { document:  {
                        url, code: document.code,
                        domain: formView.isDomainLink ? formView.domain?.seo : "",
                        action: formView.showDetail === false ? "download" : "show"
                    }} )
                    //position
                );
                //writer.setSelection( positionAfter );

				writer.removeSelectionAttribute( 'document' );
				
            } 
            
            
            );
            this._hideUI();

        } );


        // Hide the form view after clicking the "Cancel" button.
        this.listenTo( formView, 'cancel', () => {
            this._hideUI();
        } );

        // Hide the form view when clicking outside the balloon.
        clickOutsideHandler( {
            emitter: formView,
            activator: () => this._balloon.visibleView === formView,
            contextElements: [ this._balloon.view.element ],
            callback: () => this._hideUI()
        } );


        return formView;
    }

    _getBalloonPositionData() {
        const view = this.editor.editing.view;
        const viewDocument = view.document;
        let target = null;

        // Set a target position by converting view selection range to DOM.
        target = () => view.domConverter.viewRangeToDom(
            viewDocument.selection.getFirstRange()
        );

        return {
            target
        };
    }
    
    _showUI() {
        this._balloon.add( {
            view: this.formView,
            position: this._getBalloonPositionData()
        } );

        this.formView.focus();
    }    


    _hideUI() {
		this.formView.document = {};
		this.formView.domain = {};
        this.formView.category = {};
		this.formView.showDetail = true;
		this.formView.isDomainLink = false;     
        
        this.formView.categoryDropdown.buttonView.set({label: labels.category});
		this.formView.domainDropdown.buttonView.set({label: labels.domain});
        this.formView.startsWith.fieldView.value = '';
		this.formView.documentDropdown.buttonView.set({label: labels.document});
		this.formView.showDetailSwitch.set({ isOn: true, isEnabled: true })
		this.formView.isDomainLinkSwitch.set({ isOn: false, isEnabled: false })


        this.formView.element.reset();

        this._balloon.remove( this.formView );

        this.editor.editing.view.focus();
    }    
    

}