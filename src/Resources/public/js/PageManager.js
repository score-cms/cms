var PageManager = function ()
{
    this.reorder_pages_url = '';
    this.reorder_blocks_url = '';
};

PageManager.prototype.bindTriggers = function () {
    $(document).on('click', '.page-subpage-trigger', function (e) {
        e.preventDefault();

        var url = $(this).attr('href');
        var target_box_index = $(this).attr('data-target-box');
        var target_box = $('#' + target_box_index);
        var target_box_chain = {'page_parent_0': 'page_parent_1', 'page_parent_1': 'page_parent_2', 'page_parent_2': 'page_parent_3'};
        var current_name = $(this).text();


        target_box.find('.todo-list').html('');
        $.ajax({
            url: url,
            dataType: 'json'
        }).done(function (response) {


            if (0 < response.subpages.length)
            {
                $.each(response.subpages, function (key, subpage) {
                    var subpage_item = $('<li/>', {'id': 'page_' + subpage.id});
                    var subpage_handler = $('<span/>', {'class': 'handle ui-sortable-handle', 'html': '<i class="fa fa-arrows-v"></i>'});
                    var subpage_link = $('<a/>', {'href': subpage.link, 'text': subpage.name, 'class': 'text page-subpage-trigger', 'data-target-box': target_box_chain[target_box_index ]});
                    var subpage_tools = $('<div/>', {'class': 'tools'});
                    var edit_subpage = $('<a/>', {'html': '<i class="fa fa-edit"></i>', 'href': subpage.edit_link});
                    var delete_subpage = $('<a/>', {'html': '<i class="fa fa-trash"></i>', 'href': subpage.delete_link});

                    subpage_tools.append(edit_subpage);
                    subpage_tools.append(delete_subpage);
                    subpage_item.append(subpage_handler);
                    subpage_item.append(subpage_tools);

                    subpage_item.append(subpage_link);
                    target_box.find('.todo-list').append(subpage_item);
                });
            }
            else
            {
                var subpage_item = $('<li/>', {'html': '<div class="alert alert-warning text-center">Žiadne podstránky</div>'});
                target_box.find('.todo-list').append(subpage_item);
            }

            target_box.find('.box-title').html(current_name);
            target_box.find('.add-page').attr('href', response.add_link);
            $(".ui-sortable").sortable("refresh");

        });
    });


};


PageManager.prototype.reorderPages = function(order_list){
    var url = this.reorder_pages_url;
    var data = order_list;
     $.ajax({
            url: url,
            data: data,
            dataType: 'json'
        }).done(function (response) {


           

        });
    
};


PageManager.prototype.reorderBlocks = function(order_list,page_id){
    var url = this.reorder_blocks_url;
     $.ajax({
            url: url,
            data: order_list+'&page_id='+page_id,
            dataType: 'json'
        }).done(function (response) {


           

        });
    
};