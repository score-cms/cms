$('.add-block-trigger').on('click', function (e) {
    e.preventDefault();

    var source = $(this);
    var target = $('#block-'+source.parents('.add-block-wrap').find('.place_choice').val());

    var currenItemsCount = parseInt(target.attr('data-count'));
    var newItemsCount = currenItemsCount + 1;
    target.attr('data-count', newItemsCount);

   var newItemTitle = target.attr('data-block-title');
    var newItem = $('<li/>', {'class': 'dd-item', 'data-id': newItemsCount});
    var newItemCnt = $('<div/>', {'class': 'dd-handle', 'text': newItemTitle});
    newItem.append(newItemCnt);

    target.append(newItem);
    
});

$('.add-page-menu-item-trigger').on('click', function (e) {
    e.preventDefault();

    var currenItemsCount = parseInt($('#menu-root').attr('data-count'));
    var newItemsCount = currenItemsCount + 1;
    $('#menu-root').attr('data-count', newItemsCount);

    var newItemTitle = $(this).attr('data-title');
    var newItemUrl = $(this).attr('data-url');
    var pageId = $(this).attr('data-id');
    var newItem = $('<li/>', {'class': 'dd-item', 'data-id': newItemsCount, 'data-url': newItemUrl, 'data-name': newItemTitle, 'data-type': 'page', 'data-type-id': pageId});
    var newItemCnt = $('<div/>', {'class': 'dd-handle', 'text': newItemTitle});
    newItem.append(newItemCnt);

    $('#menu-root').append(newItem);
});

$('.remove-menu-item').on('click', function (e) {
    e.preventDefault();
    var targetId = $(this).attr('data-target');
    $('#' + targetId).remove();
});

$('.save-menu-trigger').on('click', function (e) {
    e.preventDefault();
    var data = $('.dd').nestable('serialize');
    var menuId = $('#menu-root').attr('data-id');
    var url= $(this).attr('data-url');
    $('#menu-save-loader').addClass('active');
    

    $.ajax({
        url:url,
        data: {'items': data, 'root_id': menuId},
        dataType: 'json'
    }).done(function (response) {

        $('#menu-save-loader').removeClass('active');
        location.reload();
    });
});


$('.edit-menu-item').on('click', function (e) {
    e.preventDefault();
    var target = $('#' + $(this).attr('data-target'));
    if (target.hasClass('active'))
    {
        target.removeClass('active');
        //target.find('.dd-handle').css('height', '30px');
        target.hide();
    } else
    {
        target.addClass('active');
        //target.find('.dd-handle').css('height', '280px');
        target.show();
    }
});

$('.menu-item-edit-name').on('input',function(){
    var currentItem = $(this).parents('.dd-item');
    $(this).parents('.dd-item-wrap').find('.dd-handle').html($(this).val());
    currentItem.attr('data-name',$(this).val());
});

$('.menu-item-edit-url').on('input',function(){
    var currentItem = $(this).parents('.dd-item');
    currentItem.attr('data-url',$(this).val());
});
