// ============================================================================================

// METATAG COLLECION
let indexM = 1000;
let el = $('[id$="_metatags"]');

$('body').on('click', '.remove-item', function () { // vymazanie polozky
    $(this).closest('.item-wrapper').remove()
})

const addButtons = () => {
    el.find('.item-wrapper').each(function () {
        if ($(this).find('.remove-item').length < 1) {
            $(this).append($('<p><button type="button" class="remove-item btn btn-sm btn-danger" >Odstrániť</button></p>'))
        }
    })
}

$('.add-metatag-item').click(function () {
    el.append($(el.data("prototype").replace(/__name__/g, indexM++)))
    addButtons()
})
addButtons()

// start // zmena typu inputu text/date
let searchWords = ['modified', 'date', 'datum'];

const updateFormInput = ({wrapper, changeTo}) => {
    if (changeTo === "text" && !wrapper.find('input[type="text"]').length) {
        wrapper.find('input[type="date"]').attr("type", "text")
    } else if (changeTo === "date" && !wrapper.find('input[type="date"]').length) {
        wrapper.find('input[type="text"]').attr("type", "date")
    }
}

const updateRow = row => {
    let optionWords = url_slug(row.find('option:selected')?.text())?.split("-") || [];
    let wrapper = row.find('[id$="_value"]').closest('.form-group')
    if (searchWords.filter((n) => optionWords.indexOf(n) !== -1).length) {
        updateFormInput({wrapper, changeTo: "date"})
    } else {
        updateFormInput({wrapper, changeTo: "text"})
    }
}

el.on('change', '[id$="_metatagPattern"]', function (e) {
    updateRow($(this).closest('.item-wrapper'))
})

el.find('.item-wrapper').each(function () {
    updateRow($(this))
})

// end // zmena typu inputu text/date

// ============================================================================================


// THUMBNAIL UPDATE
// ZMENA (ilustracny obrazok)
const updateThumbnail = (imgEl, files) => {
    if (files && files[0]) {
        let file = files[0]
        let reader = new FileReader();
        reader.onload = function (e) {
            imgEl.attr('src', e.target.result);
            imgEl.closest('.thumbnail').css('display', 'block')
        }
        reader.readAsDataURL(file);
    }
}

// NAHLAD PRI ZMENE IKONY (ilustracny obrazok)
$('.icon-wrap input[type="file"]').on('change', function (e) {
    let wrapper = $(this).closest('.icon-wrap')
    updateThumbnail(wrapper.find('.thumbnail img'), this.files)
    wrapper.find('.reset-img input[type="hidden"]').val("false")
})

// VYMAZAT (ilustracny obrazok)
$('.icon-wrap .reset-img button').on('click', function (e) {
    let wrapper = $(this).closest('.icon-wrap')
    wrapper.find('input[type="file"]').val("")
    // wrapper.find('.thumbnail img').attr('src', "")
    wrapper.find('.thumbnail').css('display', 'none')
    wrapper.find('.reset-img input[type="hidden"]').val("true")
})


// ============================================================================================


// pridanie bloku
const addItem = (id, name) => {
    let el = $('<li/>', {'class': 'dd-item'})
        .append(
            $('<div/>', {'class': 'dd-item-wrap'})
                .append($('<a/>', {
                    'class': 'remove-block-trigger btn btn-sm btn-danger pull-right',
                    'href': '#',
                    'text': 'Odstrániť'
                }))
                .append($('<span/>', {'class': 'dd-handle', 'text': name}))
                .append($('<div/>', {'style': 'display: none'})
                    .append($('<input/>', {
                        'type': 'hidden',
                        'name': 'block_ids[]',
                        'value': id
                    }))
                    .append($('<input/>', {
                        'type': 'hidden',
                        'name': 'joint_ids[]',
                        'value': 0
                    }))
                )
        );

    $('.joints ol.dd-list').append(el)
}

const selectInput = $('#article_articleBlocks, #document_documentBlocks, #event_eventBlocks')

// naplnenenie blokov do selectu
let blockApiUrl = "/admin/score/api/block/availables";
let blocksRequested = false;
const loadBlocksToSelect = () => {
    if (!blocksRequested && selectInput.length) { //fetch only once
        blocksRequested = true;
        $.ajax(blockApiUrl)
            .done(r => {
                if (r.status === "ok") {
                    r.data?.forEach(v => selectInput.append($('<option/>', {value: v.value}).text(v.name)))
                }
            })
    }
}
setTimeout(loadBlocksToSelect, 10);

$(".joints .dd").nestable({maxDepth: 1});

selectInput.select2();

// pridanie bloku
selectInput.on('change', function () {
    let option = $(this).find(':selected')
    if (!option.length && !option.val()) return;
    addItem(option.val(), option.text())
    option.removeAttr('selected');
    selectInput.val("");
});

//   ODSTRANENIE BLOKU
$('body').on('click', '.joints .remove-block-trigger', function (e) {
    e.preventDefault();
    $(this).closest('li.dd-item').remove();
});

// ============================================================================================

// SLUG do url
{
    // ak nieje slug - nova udalost/clanok, tak pri pisani nazvu sa automaticky generuje slug
    let generateSlug_ae = false;
    let nameEl_ae = $('#event_name, #article_name')
    let slugEl_ae = $('#event_slug, #article_slug')

    if (slugEl_ae.val() == '')
        generateSlug_ae = true;

    nameEl_ae.on('input', function () {
        if (generateSlug_ae) {
            let val = nameEl_ae.val();
            let slug = url_slug(val);
            slugEl_ae.val(slug);
        }
    });
}

// ============================================================================================


//  COPY url to the clipboard
{
    const copyTCB = text => {
        let input = document.createElement('textarea');
        input.innerHTML = text;
        document.body.appendChild(input);
        input.select();
        let result = document.execCommand('copy');
        document.body.removeChild(input);
        return result;
    }


    $('#page_form_seo_id, #document_seoId, #event_slug, #article_slug').each(function () {
        $(this).closest('.form-group').find('label').append(
            $('<button/>', { type: "button", class: "btn btn-link" }).html('<i class="fa fa-copy"></i>').click(function () {
                let w = $(this).closest('.form-group');
                copyTCB(w.find('span.cms-domain').text().trim() + w.find('input').val().trim());
                // w.find('input').focus();
            })
        )
    })
}

// ============================================================================================



