$('.add-custom-menu-item-trigger').on('click', function (e) {
    e.preventDefault();
    var currenItemsCount = parseInt($('#menu-root').attr('data-count'));
    var newItemsCount = currenItemsCount + 1;
    $('#menu-root').attr('data-count', newItemsCount);

    var newItemTitle = $('#direct_link_name').val();
    var newItemUrl = $('#direct_link_url').val();

    var newItem = $('<li/>', {
        'class': 'dd-item clear',
        'data-id': 0,
        'data-url': newItemUrl,
        'data-name': newItemTitle,
        'data-type': 'customLink'
    });
    var newItemCnt = $('<div/>', {'class': 'dd-handle', 'text': newItemTitle});
    newItem.append(newItemCnt);

    $('#menu-root').append(newItem);
});

const addItemToMenu = ({title, url, id}) => {
    let currenItemsCount = parseInt($('#menu-root').attr('data-count'));
    let newItemsCount = currenItemsCount + 1;
    $('#menu-root').attr('data-count', newItemsCount);

    let newItem = $('<li/>', {
        'class': 'dd-item clear',
        'data-id': 0,
        'data-url': url,
        'data-name': title,
        'data-type': 'page',
        'data-type-id': id
    });
    let newItemCnt = $('<div/>', {'class': 'dd-handle', 'text': title});
    newItem.append(newItemCnt);

    $('#menu-root').append(newItem);
}

$('.add-page-menu-item-trigger').on('click', function (e) {
    e.preventDefault();

    let item = {
        title: $(this).attr('data-title'),
        url: $(this).attr('data-url'),
        id: $(this).attr('data-id')
    }

    addItemToMenu(item)
});

$('.remove-menu-item').on('click', function (e) {
    e.preventDefault();
    var targetId = $(this).attr('data-target');
    $('#' + targetId).remove();
});

$('.save-menu-trigger').on('click', function (e) {
    e.preventDefault();
    var data = $('.dd').nestable('serialize');
    var menuId = $('#menu-root').attr('data-id');
    var url = $(this).attr('data-url');
    $('#menu-save-loader').addClass('active');

    $.ajax({
        method: "POST",
        url: url,
        data: {
            'items': data,
            'root_id': menuId,
            'multisite': $('.multisiteChoice:checked').toArray().map(item => item.value).join()
        },
        dataType: 'json'
    }).done(function (response) {

        $('#menu-save-loader').removeClass('active');
        location.reload();
    });
});


$('.edit-menu-item').on('click', function (e) {
    e.preventDefault();
    var target = $('#' + $(this).attr('data-target'));
    if (target.hasClass('active')) {
        target.removeClass('active');
        //target.find('.dd-handle').css('height', '30px');
        target.hide();
    } else {
        target.addClass('active');
        //target.find('.dd-handle').css('height', '280px');
        target.show();
    }
});

$('.menu-item-edit-name').on('input', function () {
    var currentItem = $(this).closest('.dd-item');
    $(this).closest('.dd-item-wrap').find('.dd-handle').html($(this).val());
    currentItem.attr('data-name', $(this).val());
});

$('.menu-item-edit-url').on('input', function () {
    var currentItem = $(this).closest('.dd-item');
    currentItem.attr('data-url', $(this).val());
});

