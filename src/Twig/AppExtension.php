<?php
// src/Twig/AppExtension.php
namespace Score\CmsBundle\Twig;

use Score\BaseBundle\Twig\AppExtension as BaseAppExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;
use Score\CmsBundle\Twig\AppRuntime;

class AppExtension extends BaseAppExtension
{

    public function getFilters()
    {
        return [
            new TwigFilter('cms_text', [AppRuntime::class, 'replaceOptionText']),
            new TwigFilter('cms_text_all', [AppRuntime::class, 'replaceAllOptionText']),
            new TwigFilter('cms_widget', [AppRuntime::class, 'replaceWidget']),
            new TwigFilter('cms_widget_all', [AppRuntime::class, 'replaceAllWidgets']),
            new TwigFilter('cms_document_links', [AppRuntime::class, 'updateDocumentLinks']),
        ];
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('cms_get_version', [AppRuntime::class, 'getVersion']),
            new TwigFunction('cms_react_apps', [AppRuntime::class, 'loadReactApps']),
            new TwigFunction('cms_react_app_part', [AppRuntime::class, 'loadReactAppFiles']),
            new TwigFunction('cms_template', [AppRuntime::class, 'getTemplate']),
            new TwigFunction('cms_block_accordion', [AppRuntime::class, 'getAccordion']),
            new TwigFunction('cms_top_viewed_content', [AppRuntime::class, 'getTopViewedContent']),
            new TwigFunction('cms_top_search_content', [AppRuntime::class, 'getTopSearchContent']),
            new TwigFunction('cms_menu_tree', [AppRuntime::class, 'getMenuTree']),
        ];
    }
}


