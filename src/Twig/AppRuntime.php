<?php

namespace Score\CmsBundle\Twig;

use Score\BaseBundle\Services\ReactAppLoader;
use Score\BaseBundle\Twig\AppRuntime as BaseAppRuntime;
use Score\CmsBundle\Services\CmsTwigManager;
use Score\CmsBundle\Services\StatManager;

class AppRuntime extends BaseAppRuntime {
    protected $reactAppLoader;
    protected $cmsTwigManager;
    protected $version;
    protected $allTemplates;
    protected $template;
    protected $statManager;

    public function __construct(
        ReactAppLoader $reactAppLoader,
        CmsTwigManager $cmsTwigManager,
        StatManager $statManager,
        $version,
        $allTemplates,
        $template
    ) {
        $this->reactAppLoader = $reactAppLoader;
        $this->cmsTwigManager = $cmsTwigManager;
        $this->statManager = $statManager;
        $this->version = $version;
        $this->allTemplates = $allTemplates;
        $this->template = $template;
    }

    public function getVersion() {
        return $this->version;
    }

    public function loadReactApps(array $apps = []) {
        return $this->reactAppLoader->findAppsByBlockNames($apps);
    }

    public function loadReactAppFiles($extension, $pathToManifest) {
        return $this->reactAppLoader->getFilePaths($extension, $pathToManifest);
    }

    public function getTemplate() {
        if (array_key_exists($this->template, $this->allTemplates)) {
            return $this->allTemplates[$this->template];
        } elseif ($this->template) {
            return $this->template;
        } else {
            return "@ScoreCms/Public/default/layout.html.twig";
        }
    }

    public function getAccordion($id) {
        return $this->cmsTwigManager->getAccordion($id);
    }

    public function replaceAllOptionText($text) {
        return $this->cmsTwigManager->replaceAllOptionText($text);
    }

    public function replaceOptionText($text) {
        return $this->cmsTwigManager->replaceOptionText($text);
    }

    public function replaceWidget($params) {
        return $this->cmsTwigManager->replaceWidget($params);
    }

    public function replaceAllWidgets($text) {
        return $this->cmsTwigManager->replaceAllWidgets($text);
    }

    public function getTopViewedContent() {
        return $this->statManager->getTopViewedContent();
    }

    public function getTopSearchContent() {
        return $this->statManager->getTopSearchContent();
    }

    public function getMenuTree($id) {
        return $this->cmsTwigManager->getMenuTree($id);
    }

    public function updateDocumentLinks($html) {
        return $this->cmsTwigManager->updateDocumentLinks($html);
    }
}