<?php

namespace Score\CmsBundle\DataFixtures;
//namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
//use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

use Score\CmsBundle\Entity\Group;
use Score\CmsBundle\Entity\User;

class UserFixtures extends Fixture
{
    protected $passwordEncoder;

    protected $rolesAll = [
//        'ROLE_USER' => 'Užívateľ - verejnosť',
//        'ROLE_USER_AUTHORIZED' => 'Autorizovaný užívateľ - verejnosť',
        'ROLE_PAGE_MANAGER' => 'Redaktor stránky',
        'ROLE_ARTICLE_MANAGER' => 'Redaktor články',
        'ROLE_EVENT_MANAGER' => 'Redaktor udalosti',
        'ROLE_DOCUMENT_MANAGER' => 'Redaktor dokumentov',
        'ROLE_MENU_MANAGER' => 'Redaktor menu',
        'ROLE_FORM_MANAGER' => 'Správca formulárov',
        'ROLE_GALLERY_MANAGER' => 'Správca galérie',
        'ROLE_ACCORDION_MANAGER' => 'Správca akordeónu',
        'ROLE_BLOCK_MANAGER' => 'Správca statických blokov',
        'ROLE_OPTION_MANAGER' => 'Správca textov',
        'ROLE_WIDGET_MANAGER' => 'Správca widgetov',
        'ROLE_USER_MANAGER' => 'Správca uživateľov',
        'ROLE_IMAGES_MANAGER' => 'Správca obrázkov',
        'ROLE_TOP_MANAGER' => 'Hlavný redaktor',
        'ROLE_ADMIN' => 'Admin',
    ];

    public function __construct(UserPasswordHasherInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();

        foreach ($this->rolesAll as $key => $value) {
            $g = new Group();
            $g->setName($value);
            $g->setRole($key);
            $user->addGroup($g);
            $manager->persist($g);
        }

        $user->setUsername('admin');
        $user->setEmail('admin@cms3.sk');
        $user->setPassword($this->passwordEncoder->hashPassword($user, 'admin'));
        $user->setIsActive(true);
        $manager->persist($user);
        $manager->flush();
    }
}
