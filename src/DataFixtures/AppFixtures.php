<?php

namespace Score\CmsBundle\DataFixtures;
//namespace App\DataFixtures;

use Score\CmsBundle\Entity\Article\ArticleCategory;
use Score\CmsBundle\Entity\Block\BlockJointPage;
use Score\CmsBundle\Entity\Document\DocumentLevel;
use Score\CmsBundle\Entity\Form\Webform;
use Score\CmsBundle\Entity\Form\WebformField;
use Score\CmsBundle\Entity\Menu;
use Score\CmsBundle\Entity\Metatag\MetatagPattern;
use Score\CmsBundle\Entity\Page\Page;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Score\CmsBundle\Entity\Block\Block;
use Score\CmsBundle\Entity\Article\Article;
use Score\CmsBundle\Entity\Block\Accordion;
use Score\CmsBundle\Entity\Block\AccordionItem;
use Score\CmsBundle\Entity\Document\DocumentCategory;
use Score\CmsBundle\Entity\Document\DocumentDomain;
use Score\CmsBundle\Entity\Event\Event;
use Score\CmsBundle\Entity\Gallery\Gallery;
use Score\CmsBundle\Entity\Option;
use Score\CmsBundle\Entity\OptionGroup;

class AppFixtures extends Fixture {
    public function load(ObjectManager $manager) {
        //default content block
        $blockPageContent = new Block();
        $blockPageContent->setName('Obsah stránky');
        $blockPageContent->setType('pageContent');
        $blockPageContent->setVisibility(true);
        $manager->persist($blockPageContent);

        //article block
        $articleBlock = new Block();
        $articleBlock->setName('Výpis článkov');
        $articleBlock->setType('controllerContent');
        $articleBlock->setModule('Score\CmsBundle\Controller\Article\ArticleBlockController');
        $articleBlock->setAction('topAction');
        $articleBlock->setVisibility(true);
        $manager->persist($articleBlock);

        //article
        $article = new Article();
        $article->setName('CORONA INFO: Minister ohlasuje uvoľnenie pravidiel. Kaderníctva nad 1500 m nadmorskej výšky  sa otvoria pre plešatých.');
        $article->setIcon('/bundles/scorecms/idsk/img/default-image.png');
        $article->setTeaser('<p>s been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a ty</p>');
        $article->setBody('<p>s been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets conta</p>  <p>s been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets conta</p>  <p>s been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets conta</p>  <p>s been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets conta</p>  <p>&nbsp;</p>');
        $article->setPublished(1);
        $article->setPriority(20);
        $article->setAuthor('George Attila Kováč');
        $article->setPublishedFrom(new \DateTime());
        $article->setPublishedTo(new \DateTime('2030-01-01'));
        $article->setSlug('article-1');
        $article->setCreatedBy(1);
        $manager->persist($article);

        //event block
        $eventBlock = new Block();
        $eventBlock->setName('Výpis udalostí');
        $eventBlock->setType('controllerContent');
        $eventBlock->setModule('Score\CmsBundle\Controller\Event\EventBlockController');
        $eventBlock->setAction('topAction');
        $eventBlock->setVisibility(true);
        $manager->persist($eventBlock);

        //event
        $event = new Event();
        $event->setName('Celebrity Deathmatch galavečer: Rytmus vs M1 Abrams');
        $event->setIcon('/bundles/scorecms/idsk/img/default-image.png');
        $event->setTeaser('<p>s been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a ty</p>');
        $event->setContent('<p>s been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets conta</p>  <p>s been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets conta</p>  <p>s been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets conta</p>  <p>s been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets conta</p>  <p>&nbsp;</p>');
        $event->setPublished(1);
        $event->setDateFrom(new \DateTime('2021-12-24 20:00:00'));
        $event->setDateTo(new \DateTime('2021-12-24 22:00:00'));
        $event->setOrganizer('Slovenská filharmónia');
        $event->setContactPerson('Johny Nicolas Novák');
        $event->setEmail('bolonacase@cd.sk');
        $event->setPhone('+421 908 111 222');
        $event->setUrlLink('www.dcgalavecer.sk');
        $event->setSlug('event-1');
        $event->setCreatedBy(1);
        $manager->persist($event);


        $rootPage = new Page();
        $rootPage->setName('root');
        $rootPage->setTitle('root');
        $rootPage->setLvl(0);
        $rootPage->setLang('sk');
        $manager->persist($rootPage);

        $contactPage = new Page();
        $contactPage->setName('Kontakt');
        $contactPage->setTitle('Kontakt');
        $contactPage->setLvl(1);
        $contactPage->setSeoId('kontakt');
        $contactPage->setContent('Kontakt');
        $contactPage->setStatus('public');
        $contactPage->setLayout('menu-center');
        $contactPage->setVisibility(true);
        $contactPage->setParentPage($rootPage);

        $blockJoint1 = new BlockJointPage();
        $blockJoint1->setPlace('center');
        $contactPage->addBlockJoint($blockJoint1);
        $blockPageContent->addBlockJoint($blockJoint1);
        $manager->persist($contactPage);


        $gdprPage = new Page();
        $gdprPage->setName('Ochrana osobných údajov');
        $gdprPage->setTitle('Ochrana osobných údajov');
        $gdprPage->setLvl(1);
        $gdprPage->setSeoId('ochrana-osobnych-udajov');
        $gdprPage->setContent('<h1>Ochrana osobných údajov</h1>      V zmysle zákona č. 18/2018 Z. z. o ochrane osobných údajov v znení neskorších predpisov v súlade s Nariadením Európskeho parlamentu a Rady EÚ č. 2016/679 udeľujem dobrovoľne súhlas so spracovaním mojich osobných údajov, s ich zhromažďovaním, zoskupovaním s inými osobnými údajmi, zaznamenávaním, evidenciou, usporadúvaním, prehliadaním, využívaním, uchovávaním a iným spracúvaním organizáciou Slovenská agentúra životného prostredia, Tajovského 28, 97401 Banská Bystrica. Súhlas udeľujem dobrovoľne  na dobu neurčitú a som si vedomý, že je ho možné kedykoľvek odvolať, a to písomnou formou na adresu Slovenskej agentúry životného prostredia alebo e-mailom na adresu         <a href="mailto:info@sazp.sk">info@sazp.sk </a>.        ');
        $gdprPage->setStatus('public');
        $gdprPage->setLayout('menu-center');
        $gdprPage->setVisibility(true);
        $gdprPage->setParentPage($rootPage);

        $blockJoint2 = new BlockJointPage();
        $blockJoint2->setPlace('center');
        $gdprPage->addBlockJoint($blockJoint2);
        $blockPageContent->addBlockJoint($blockJoint2);
        $manager->persist($gdprPage);


        $wcagPage = new Page();
        $wcagPage->setName('Vyhlásenie o prístupnosti');
        $wcagPage->setTitle('Vyhlásenie o prístupnosti');
        $wcagPage->setLvl(1);
        $wcagPage->setSeoId('vyhlasenie-o-pristupnosti');
        $wcagPage->setContent('
        <h1>Vyhlásenie o prístupnosti</h1>
        <p>Slovenská agentúra životného prostredia má záujem zabezpečiť prístupnosť webového sídla  <a href="">???</a>
            v súlade so zákonom č. 95/2019 Z. z. o informačných technológiách vo verejnej správe a príslušnými vykonávacími predpismi v rozsahu podmienok podľa smernice (EÚ) 2016/2102. Toto vyhlásenie o prístupnosti sa vzťahuje na webové sídlo  <a href="">???</a>
        </p>
        <h3>Stav súladu</h3>
        <p>Toto webové sídlo je v čiastočnom súlade so zákonom č. 95/2019 Z. z. o informačných technológiách vo verejnej správe a s príslušnými vykonávacími predpismi v rozsahu podmienok podľa smernice (EÚ) 2016/2102 vzhľadom na prvky nesúladu uvedené nižšie.</p>

        <h3>Neprístupný obsah</h3>
        <p>Obsah uvedený nižšie nie je prístupný z týchto dôvodov:</p>
        <ol>
            <li>
                Nesúlad so smernicou (EÚ) 2016/2102 o prístupnosti webových sídel a mobilných aplikácií subjektov verejného sektora:
                <ul>
                    <li>???</li>
                </ul>
            </li>
          
        </ol>

        <h3>Vypracovanie tohto vyhlásenia o prístupnosti</h3>
        <p>Toto vyhlásenie bolo vypracované ???.</p>
        <p>Vyhodnotenie súladu webového sídla s požiadavkami zákona č. 95/2019 Z. z. o informačných technológiách vo verejnej správe a príslušnými vykonávacími predpismi v rozsahu podmienok podľa smernice (EÚ) 2016/2102 bolo vykonané samohodnotením.</p>
        <p>Vyhlásenie bolo naposledy skontrolované 24. 3. 2021.</p>

        <h3>Spätná väzba a kontaktné informácie</h3>
        <p>V prípade problémov s prístupnosťou webového sídla nás kontaktujte na e-mailovej adrese: <a href="mailto:">???</a>. Správcom obsahu a prevádzkovateľom webového sídla je Slovenská agentúra životného prostredia.</p>
        <p>Webové sídlo bolo vytvorené na podporu aktivít súvisiacich s prechodom na obehové hospodárstvo v podmienkach Slovenskej republiky a na podporu implementácie princípov zeleného rastu. Obsah webového sídla je pravidelne aktualizovaný.
        </p>
        <h3>Vynucovacie konanie</h3>
        <p>V prípade neuspokojivých odpovedí na podnety alebo žiadosti zaslané v rámci spätnej väzby subjektu verejného sektora v súlade s <a href="https://eur-lex.europa.eu/legal-content/SK/TXT/?uri=uriserv:OJ.L_.2016.327.01.0001.01.SLK&amp;toc=OJ:L:2016:327:TOC">čl. 7 ods. 1 písm. b) smernice Európskeho parlamentu</a> sa môžete obrátiť v rámci vynucovacieho konania na subjekt poverený presadzovaním smernice, ktorým je Ministerstvo investícií, regionálneho rozvoja a informatizácie Slovenskej republiky na e-mailovej adrese: <a href="mailto:standard@vicepremier.gov.sk">standard@vicepremier.gov.sk</a>.</p>');
        $wcagPage->setStatus('public');
        $wcagPage->setLayout('menu-center');
        $wcagPage->setVisibility(true);
        $wcagPage->setParentPage($rootPage);

        $blockJoint3 = new BlockJointPage();
        $blockJoint3->setPlace('center');
        $wcagPage->addBlockJoint($blockJoint3);
        $blockPageContent->addBlockJoint($blockJoint3);
        $manager->persist($wcagPage);


        $homePage = new Page();
        $homePage->setName('Domovská stránka');
        $homePage->setTitle('Domov');
        $homePage->setLvl(1);
        $homePage->setSeoId('homepage');
        $homePage->setContent('<section style="min-height:50vh;display:flex; justify-content:center; align-items:center;"><p>Vitaj na domovskej stránke, upraviť ju možeš v <a href="/admin/page/list">redakčnom systeme</a> po prihlasení sa ako admin/admin.</p></section>');
        $homePage->setStatus('public');
        $homePage->setLayout('menu-center');
        $homePage->setVisibility(true);
        $homePage->setParentPage($rootPage);

        $blockJoint4 = new BlockJointPage();
        $blockJoint4->setPlace('center');
        $homePage->addBlockJoint($blockJoint4);
        $blockPageContent->addBlockJoint($blockJoint4);
        $manager->persist($homePage);


        $errorPage = new Page();
        $errorPage->setName('Error');
        $errorPage->setTitle('Chyba');
        $errorPage->setLvl(1);
        $errorPage->setSeoId('error');
        $errorPage->setContent('<section style="min-height:50vh;display:flex; justify-content:center; align-items:center;"><p>Stránka sa nenašla</p></section>');
        $errorPage->setStatus('public');
        $errorPage->setLayout('menu-center');
        $errorPage->setVisibility(true);
        $errorPage->setParentPage($rootPage);

        $blockJoint5 = new BlockJointPage();
        $blockJoint5->setPlace('center');
        $errorPage->addBlockJoint($blockJoint5);
        $blockPageContent->addBlockJoint($blockJoint5);
        $manager->persist($errorPage);


        $menu = new Menu();
        $menu->setName('Hlavné menu');
        // $menu->setParent(null);
        $menu->setLvl(0);
        $menu->setType("main");
        $manager->persist($menu);

        $manager->flush();

        $menuItemContact = new Menu();
        $menuItemContact->setName('Kontakt');
        $menuItemContact->setRootId($menu->getId());
        $menuItemContact->setParent($menu);
        $menuItemContact->setLvl(1);
        $menuItemContact->setLinkType('customLink');
        $menuItemContact->setLink('/kontakt');
        $manager->persist($menuItemContact);

        $menuItemArticle = new Menu();
        $menuItemArticle->setName('Články');
        $menuItemArticle->setRootId($menu->getId());
        $menuItemArticle->setParent($menu);
        $menuItemArticle->setLvl(1);
        $menuItemArticle->setLinkType('customLink');
        $menuItemArticle->setLink('/clanky');
        $manager->persist($menuItemArticle);

        $menuItemEvents = new Menu();
        $menuItemEvents->setName('Udalosti');
        $menuItemEvents->setRootId($menu->getId());
        $menuItemEvents->setParent($menu);
        $menuItemEvents->setLvl(1);
        $menuItemEvents->setLinkType('customLink');
        $menuItemEvents->setLink('/udalosti');
        $manager->persist($menuItemEvents);

        $menuItemDocs = new Menu();
        $menuItemDocs->setName('Dokumenty');
        $menuItemDocs->setRootId($menu->getId());
        $menuItemDocs->setParent($menu);
        $menuItemDocs->setLvl(1);
        $menuItemDocs->setLinkType('customLink');
        $menuItemDocs->setLink('/dokumenty');
        $manager->persist($menuItemDocs);


        // FORMULAR
        $formJsons = [];
        $formJsons[] = '{"status":"ok","data":{"formId":"21f742534aa38f44","identifier":"21f742534aa38f44","formName":"Automaticky vygenerovan\u00fd \u00favodn\u00fd formul\u00e1r","formDescription":"\u003Ch2\u003EOznamovac\u00ed formul\u00e1r v\u00fdsledkov testovania\u003C\/h2\u003E\r\n\u003Cp\u003EPri prvom zaslan\u00ed inform\u00e1ci\u00ed uve\u010fte po\u010dty v\u0161etk\u00fdch dovtedy realizovan\u00fdch antig\u00e9nov\u00fdch testov. Pri zasielan\u00ed \u010fal\u0161\u00edch hl\u00e1sen\u00ed uve\u010fte po\u010det testov vykonan\u00fdch za obdobie od posledn\u00e9ho hl\u00e1senia.\u003C\/p\u003E\r\n\u003Cp\u003EFormul\u00e1r o hl\u00e1sen\u00ed pozit\u00edvnych testov (teda slovo reakt\u00edvny nahradi\u0165 pozit\u00edvny) a ponecha\u0165 v\u0161etky \u00fadaje a doplni\u0165 do textu pred dotazn\u00edkom nasledovn\u00e9 info:\u003C\/p\u003E","isActive":true,"successMessage":"Na\u0161i pracovn\u00edci sa V\u00e1m \u010doskoro ozv\u00fa.\r\nT\u00edm testovania","visibility":true,"items":[{"identifier":"59m4kwetnvuihmxq","title":"E-mail ","helpText":"","type":"email","sortOrder":0,"required":true,"choices":null,"rows":null,"length":null,"min":null,"max":null},{"identifier":"5rrqkwgc5vnrol9g","title":"Telef\u00f3nne alebo mobilne \u010d\u00edslo","helpText":"Format: 0944123456 \/ +421 944 123 456","type":"tel","sortOrder":1,"required":true,"choices":null,"rows":null,"length":null,"min":null,"max":null},{"identifier":"odlxkwetnwfhcy8v","title":"Po\u010det dn\u00ed do konca roka:","helpText":"ak je pr\u00e1ve posledn\u00fd de\u0148, uve\u010fte 0","type":"number","sortOrder":2,"required":true,"choices":null,"rows":null,"length":null,"min":null,"max":null},{"identifier":"nu41kwetnx9voine","title":"D\u00e1tum narodenia najstar\u0161ieho obyvate\u013ea va\u0161ej obce","helpText":"ak si nie ste odpove\u010fou ist\u00fd, obr\u00e1\u0165te sa na obecn\u00fd \u00farad","type":"date","sortOrder":3,"required":true,"choices":null,"rows":null,"length":null,"min":null,"max":null},{"identifier":"ydk0kwgc38evy9cn","title":"Meno a priezvisko","helpText":"Odde\u013ete medzerou ","type":"text","sortOrder":4,"required":true,"choices":null,"rows":null,"length":null,"min":null,"max":null},{"identifier":"op1ekwgc39mznh1n","title":"Ob\u013e\u00faben\u00fd de\u0148 v t\u00fd\u017edni","helpText":"Ob\u013e\u00faben\u00fd m\u00f4\u017ee byt len jeden de\u0148","type":"select","sortOrder":5,"required":true,"choices":[{"label":"Pondelok","value":"UG9uZGVsb2s=","sortOrder":0,"valid":false},{"label":"Utorok","value":"VXRvcm9r","sortOrder":1,"valid":true},{"label":"Streda","value":"U3RyZWRh","sortOrder":2,"valid":true},{"label":"\u0160tvrtok","value":"xaB0dnJ0b2s=","sortOrder":3,"valid":true},{"label":"Piatok","value":"UGlhdG9r","sortOrder":4,"valid":true},{"label":"Sobota","value":"U29ib3Rh","sortOrder":5,"valid":true},{"label":"Nede\u013ea","value":"TmVkZcS+YQ==","sortOrder":6,"valid":true}],"rows":null,"length":null,"min":null,"max":null},{"identifier":"gdu1kwgc3a4qf3uf","title":"Ob\u013e\u00faben\u00e1 z\u00e1kladn\u00e1 farba","helpText":"Ak va\u0161a farba nie je v ponuke uve\u010fte ju do  pozn\u00e1mky","type":"radio","sortOrder":6,"required":true,"choices":[{"label":"Zelen\u00e1","value":"WmVsZW7DoQ==","sortOrder":0,"valid":true},{"label":"\u010cerven\u00e1","value":"xIxlcnZlbsOh","sortOrder":1,"valid":true},{"label":"Modr\u00e1","value":"TW9kcsOh","sortOrder":2,"valid":true},{"label":"In\u00e9","value":"SW7DqQ==","sortOrder":3,"valid":true}],"rows":null,"length":null,"min":null,"max":null},{"identifier":"33ywkwgc3ald19hc","title":"\u010co radi rob\u00edte vo vo\u013enom \u010dase","helpText":"","type":"checkbox","sortOrder":7,"required":true,"choices":[{"label":"\u010c\u00edtam","value":"xIzDrXRhbQ==","sortOrder":0,"valid":true},{"label":"Kresl\u00edm","value":"S3Jlc2zDrW0=","sortOrder":1,"valid":true},{"label":"Bicyklujem","value":"QmljeWtsdWplbQ==","sortOrder":2,"valid":true},{"label":"Pot\u00e1pam sa","value":"UG90w6FwYW0gc2E=","sortOrder":3,"valid":true},{"label":"Ni\u010d","value":"TmnEjQ==","sortOrder":4,"valid":false}],"rows":null,"length":null,"min":null,"max":null},{"identifier":"s8lekwgc3975om1m","title":"Pozn\u00e1mka","helpText":"\u010co by ste n\u00e1m e\u0161te radi odk\u00e1zali","type":"textarea","sortOrder":8,"required":false,"choices":null,"rows":4,"length":null,"min":null,"max":null},{"identifier":"jzfxkwetnyjnp3x6","title":"Hodnotenie","helpText":"Za spr\u00e1vnu odpove\u010f m\u00f4\u017eete dosta\u0165 mal\u00fa pozornos\u0165 ","type":"star","sortOrder":9,"required":true,"choices":null,"rows":null,"length":6,"min":null,"max":null}],"sendMail":false,"mailList":[],"params":null}}';
        $formJsons[] = '{"status":"ok","data":{"formId":"nahlaseniechyby","identifier":"nahlaseniechyby","formName":"Nahl\u00e1senie chyby","formDescription":"\u003Cp\u003EPros\u00edm nevpisujte \u017eiadne osobn\u00e9 inform\u00e1cie (meno, kontakt a in\u00e9).\u0026nbsp;\u003C\/p\u003E\r\n\u003Cp\u003E\u003Cbr\u003E\u003C\/p\u003E","isActive":true,"successMessage":"","visibility":true,"items":[{"identifier":"th70kz4jmz2o4iyw","title":"Ak\u00fd typ chyby ste na\u0161li?","helpText":"","type":"select","sortOrder":0,"required":true,"choices":[{"label":"Valida\u010dn\u00e1 chyba","value":"VmFsaWRhxI1uw6EgY2h5YmE=","sortOrder":0,"valid":true},{"label":"Gramatick\u00e1 chyba","value":"R3JhbWF0aWNrw6EgY2h5YmE=","sortOrder":1,"valid":true},{"label":"Chyba responzivity","value":"Q2h5YmEgcmVzcG9ueml2aXR5","sortOrder":2,"valid":true},{"label":"Chyba in\u00e1","value":"Q2h5YmEgaW7DoQ==","sortOrder":3,"valid":true}],"rows":null,"length":null},{"identifier":"6qc2kz4jnj9073pi","title":"Pop\u00ed\u0161te chybu bli\u017e\u0161ie","helpText":"","type":"textarea","sortOrder":1,"required":true,"choices":null,"rows":5,"max":350,"length":null}]}}';
        $formJsons[] = '{"status":"ok","data":{"formId":"contactform","identifier":"contactform","formName":"Kontaktn\u00fd formul\u00e1r","formDescription":"\u003Cp\u003E\u003Cbr\u003E\u003C\/p\u003E","isActive":true,"successMessage":"","visibility":true,"items":[{"identifier":"fgsbl1xvv4f396oc","title":"Meno","helpText":"","type":"text","sortOrder":0,"required":true,"choices":null,"rows":null,"length":null,"min":null,"max":null},{"identifier":"r8mul1xvv668tnik","title":"E-mail","helpText":"","type":"email","sortOrder":1,"required":true,"choices":null,"rows":null,"length":null,"min":null,"max":null},{"identifier":"r63ql1xvv7joln9o","title":"Spr\u00e1va","helpText":"","type":"textarea","sortOrder":2,"required":true,"choices":null,"rows":3,"length":null,"min":null,"max":"350"}],"sendMail":false,"mailList":[],"params":null}}';
        foreach ($formJsons as $formJson) {
            $formArr = json_decode($formJson, true)["data"];
            $form = new Webform();
            $form->setName($formArr["formName"])
                ->setIdentifier($formArr["identifier"])
                ->setDescription($formArr["formDescription"])
                ->setSuccessMessage($formArr["successMessage"])
                ->setIsActive(true)
                ->setVisibility(true);
            $form->setCreatorId(0);
            foreach ($formArr["items"] as $item) {
                $field = new WebformField();
                $form->addField($field);
                $field->setIdentifier($item["identifier"])
                    ->setTitle($item["title"])
                    ->setHelpText($item["helpText"])
                    ->setType($item["type"])
                    ->setSortOrder($item["sortOrder"])
                    ->setRequired($item["required"])
                    ->setVisibility(true);
                if (array_key_exists("choices", $item))
                    $field->setChoicesArr($item["choices"]);
                if (array_key_exists("rows", $item))
                    $field->setRows($item["rows"]);
                if (array_key_exists("length", $item))
                    $field->setLength($item["length"]);
                if (array_key_exists("min", $item))
                    $field->setMin($item["min"]);
                if (array_key_exists("max", $item))
                    $field->setMax($item["max"]);

                $manager->persist($field);
            }
            $manager->persist($form);

            $formBlock = new Block();
            $formBlock->setName($form->getName())
                ->setType("form")
                ->setParamsArr(["formId" => $form->getIdentifier()]);
            $manager->persist($formBlock);
        }
        $manager->flush();

        // CMS_TEXT - OPTIONS
        $cmsOptionGroup = new OptionGroup();
        $cmsOptionGroup->setName("IDSK – Default");

        $cmsOption1 = new Option();
        $cmsOption1->setCode('idsk-header-banner');
        $cmsOption1->setName('IDSK - Informačná lišta');
        $cmsOption1->setValue('Nová verzia ID-SK Frontend nahradzuje pôvodnú verziu.');
        $cmsOption1->setVisibility(true);
        $manager->persist($cmsOption1);

        $cmsOption2 = new Option();
        $cmsOption2->setCode('idsk-header-top-body');
        $cmsOption2->setName('IDSK - Top lišta - obsah');
        $cmsOption2->setValue('<h3 class="govuk-body-s">
        Oficiálna stránka služby SLUŽBA XY
    </h3>
    <p class="govuk-body-s">
        Gestor poskytovanej služby je <a class="govuk-link">Slovenská agentúra životného prostredia</a>
    </p>');
        $cmsOption2->setVisibility(true);
        $manager->persist($cmsOption2);

        $cmsOption3 = new Option();
        $cmsOption3->setCode('idsk-header-top-title');
        $cmsOption3->setName('IDSK - Top lišta - titulok');
        $cmsOption3->setValue('Oficiálna stránka
        <button class="idsk-header-web__brand-gestor-button" aria-label="Zobraziť informácie o stránke" aria-expanded="false" data-text-for-hide="Skryť informácie o stránke" data-text-for-show="Zobraziť informácie o stránke">
            Služby XY
            <div class="idsk-header-web__link-arrow"></div>
        </button>');
        $cmsOption3->setVisibility(true);
        $manager->persist($cmsOption3);

        $cmsOption4 = new Option();
        $cmsOption4->setCode('idsk-header-logo');
        $cmsOption4->setName('IDSK - logo');
        $cmsOption4->setValue('<img src="/bundles/scorecms/idsk/img/logo-sazp.png" alt="Slovenská agentúra životného prostredia" class="idsk-header-web__main-headline-logo">');
        $cmsOption4->setVisibility(true);
        $manager->persist($cmsOption4);

        $cmsOption5 = new Option();
        $cmsOption5->setCode('idsk-intro-title');
        $cmsOption5->setName('IDSK - Intro titulok');
        $cmsOption5->setValue('Úvodná titulka');
        $cmsOption5->setVisibility(true);
        $manager->persist($cmsOption5);

        $cmsOption6 = new Option();
        $cmsOption6->setCode('idsk-intro-desc');
        $cmsOption6->setName('IDSK - Intro popis');
        $cmsOption6->setValue('Úvodný text a popis');
        $cmsOption6->setVisibility(true);
        $manager->persist($cmsOption6);

        $cmsOption7 = new Option();
        $cmsOption7->setCode('idsk-footer-logo');
        $cmsOption7->setName('IDSK - footer logo');
        $cmsOption7->setValue('<img src="/bundles/scorecms/idsk/img/logo-sazp-bw.png" alt="Slovenská agentúra životného prostredia">');
        $cmsOption7->setVisibility(true);
        $manager->persist($cmsOption7);


        $cmsOptionGroup
            ->addOption($cmsOption1)
            ->addOption($cmsOption2)
            ->addOption($cmsOption3)
            ->addOption($cmsOption4)
            ->addOption($cmsOption5)
            ->addOption($cmsOption6)
            ->addOption($cmsOption7);

        $manager->persist($cmsOptionGroup);

        $manager->flush();

        // EXAMPLE PAGE
        $examplePage = new Page();
        $examplePage->setName('Ukážková stránka');
        $examplePage->setTitle('Ukážka');
        $examplePage->setLvl(1);
        $examplePage->setSeoId('ukazka');
        $examplePage->setContent('Ukážková stránka pre formulár, akordeón a galériu. Ukážková galéria neobsahuje žiadne fotky. Fotky a popisy k nim môžete pridať v administračnom rozhraní.');
        $examplePage->setStatus('public');
        $examplePage->setLayout('menu-center');
        $examplePage->setVisibility(true);
        $examplePage->setParentPage($rootPage);

        $blockJoint6 = new BlockJointPage();
        $blockJoint6->setPlace('center');
        $examplePage->addBlockJoint($blockJoint6);
        $blockPageContent->addBlockJoint($blockJoint6);
        $manager->persist($examplePage);


        $menuItemExample = new Menu();
        $menuItemExample->setName('Ukážka');
        $menuItemExample->setRootId($menu->getId());
        $menuItemExample->setParent($menu);
        $menuItemExample->setLvl(1);
        $menuItemExample->setLinkType('customLink');
        $menuItemExample->setLink('/ukazka');
        $manager->persist($menuItemExample);

        //ukazka formular
        $block = new Block();
        $block->setName("Automaticky vygenerovaný úvodný formulár")
            ->setParamsArr(["formId" => '21f742534aa38f44'])
            ->setType("form");
        $blockJoint7 = new BlockJointPage();
        $blockJoint7->setPlace('center');
        $examplePage->addBlockJoint($blockJoint7);
        $block->addBlockJoint($blockJoint7);
        $manager->persist($block);
        $manager->persist($examplePage);


        //ukazka accordion
        $accordion = new Accordion();
        $accordion->setName("Harmonika o prírode");

        $accordionItems = [[
            "name" => "Albert Veľký",
            "desc" => "Príroda závisí od božej vôle, pretože božia vôľa je konečnou príčinou všetkého, k nej vedú iné príčiny vecí. Celý mechanizmus pôsobenia zákonov prírody a prvkov, ktoré sa im podriaďujú, je prostriedkom a nástrojom slúžiacim na realizovanie večných ideí, v čom má každá vec i každá bytosť (anjel, nebesá, človek atď.) svoju vlastnú funkciu."
        ], [
            "name" => "Aristoteles",
            "desc" => "Príroda je dianie; prameň pohybu i pokoja vo veciach i vo svete. Príroda je cieľ, pre ktorý sa všetko vo svete deje, ďalej nevyhnutnosť vo svete, zákon, a napokon vnútorný vzťah medzi javmi sveta. Je predmetom skúmania fyziky. Príroda je na základe štyroch príčin vďaka imanentnému pohybu tvorcom všetkého a svojimi zákonmi všetko zahŕňa a všetko riadi. Príroda je zákonom sebe samej. Pozemská príroda znamená stále prechádzanie od prvotných elementov k rastlinám a ďalší rozvoj od rastlín k živočíchom."
        ], [
            "name" => "Estetika",
            "desc" => "Príroda je nevyčerpateľný zdroj krásy, ako aj zvláštneho typu krásy, ktorému hovoríme prírodné krásno."
        ], [
            "name" => "Georg Wilhelm Friedrich Hegel",
            "desc" => "Príroda je reálne bytie, rodisko a terén činnosti ducha, jeho predpoklad. Príroda je predmetom skúmania vo filozofii prírody."
        ]];

        foreach ($accordionItems as $k => $item) {
            $ai = new AccordionItem();
            $ai->setTitle($item["name"])
                ->setContent($item["desc"])
                ->setSortOrder($k);
            $accordion->addItem($ai);
        }

        $manager->persist($accordion);
        $manager->flush();

        $block = new Block();
        $block->setName($accordion->getName())
            ->setType("accordion")
            ->setParamsArr(["accordionId" => $accordion->getId()]);

        $blockJoint8 = new BlockJointPage();
        $blockJoint8->setPlace('center');
        $examplePage->addBlockJoint($blockJoint8);
        $block->addBlockJoint($blockJoint8);
        $manager->persist($block);
        $manager->persist($examplePage);


        //ukazka galeria
        $jsonGallery = '{ "identifier":"cf0723543be",
            "name":"\u00davodn\u00e1 gal\u00e9ria",
            "description":"\u003Ch2\u003EGal\u00e9ria pr\u00edrody\u003C\/h2\u003E\r\n\u003Cp\u003EPr\u00edroda v pr\u00edrodn\u00fdch ved\u00e1ch je v\u0161etka hmota, energia a javy, ktor\u00e1\/-\u00e9 nevznikla\/-li z\u00e1mernou \u013eudskou \u010dinnos\u0165ou. Vo filozofii je to \u010dasto synonymum pre svet \u010di realitu, v be\u017enej komunik\u00e1cii zas pre otvoren\u00fa krajinu mimo \u013eudsk\u00fdch obydl\u00ed.\u003C\/p\u003E\r\n\u003Cp\u003EPr\u00edroda v pr\u00edrodn\u00fdch ved\u00e1ch zah\u0155\u0148a v\u0161etko od vesm\u00edru a\u017e po subatom\u00e1rne \u010dastice, vr\u00e1tane v\u0161etk\u00fdch \u017eivo\u010d\u00edchov, rastl\u00edn, miner\u00e1lov, v\u0161etk\u00fdch pr\u00edrodn\u00fdch zdrojov a pr\u00edrodn\u00fdch \u00fakazov. Zah\u0155\u0148a aj spr\u00e1vanie \u017eivo\u010d\u00edchov a procesy s\u00favisiace s ne\u017eivou hmotou.\u003C\/p\u003E\r\n\u003Cp\u003EPr\u00edrodou sa z filozofick\u00e9ho h\u013eadiska zaober\u00e1 filozofia pr\u00edrody.\u0026nbsp;\u003C\/p\u003E\r\n\u003Cp\u003EPS: fotky do gal\u00e9rie pridajte v administra\u010dnom rozhran\u00ed. A k nim aj n\u00e1zvy.\u003C\/p\u003E",
            "type":"static",
            "params":{"direction":"row"},
            "images":[] }';

        $g = json_decode($jsonGallery, true);
        $gallery = new Gallery();
        $gallery->setIdentifier($g["identifier"])
            ->setType($g["type"])
            ->setName($g["name"])
            ->setDescription($g["description"])
            ->setParamsArr($g["params"])
            ->setCreatorId(0)
            ->setIsActive(true)
            ->setVisibility(true);
        $manager->persist($gallery);

        $block = new Block();
        $block->setName($gallery->getName())
            ->setType("gallery")
            ->setParamsArr(["galleryId" => $gallery->getIdentifier()]);

        $blockJoint9 = new BlockJointPage();
        $blockJoint9->setPlace('center');
        $examplePage->addBlockJoint($blockJoint9);
        $block->addBlockJoint($blockJoint9);
        $manager->persist($block);
        $manager->persist($examplePage);

        $manager->flush();


        $dom = [
            'posudzovanie-vplyvov-na-zp' => 'Posudzovanie vplyvov na ŽP',
            'integrovana-prevencia-a-kontrola-znecistovania' => 'Integrovaná prevencia a kontrola znečisťovania',
            'prevencia-zavaznych-priemyselnych-havarii' => 'Prevencia závažných priemyselných havárií',
            'environmentalne-skody' => 'Environmentálne škody',
            'environmentalne-zataze' => 'Environmentálne záťaže',
            'klimaticka-zmena' => 'Klimatická zmena',
            'odpady' => 'Odpady',
            'hluk' => 'Hluk',
            'polnohospodarstvo' => 'Poľnohospodárstvo ',
            'lesne-hospodarstvo' => 'Lesné hospodárstvo',
            'priemysel' => 'Priemysel',
            'energetika' => 'Energetika',
            'doprava' => 'Doprava',
            'cestovny-ruch' => 'Cestovný ruch',
            'zdravie-a-zp' => 'Zdravie a ŽP',
            'ochrana-prirody-biota' => 'Ochrana prírody (biota)',
            'horninove-prostredie' => 'Horninové prostredie',
            'ovzdusie' => 'Ovzdušie',
            'poda' => 'Pôda',
            'voda' => 'Voda',
            'zivotne-prostredie' => 'Životné prostredie',
            'jadrova-bezpecnost' => 'Jadrová bezpečnosť',
        ];
        $cat = [
            'zakon' => 'Zákon',
            'vyhlaska' => 'Vyhláška',
            'nariadenie' => 'Nariadenie',
            'smernica' => 'Smernica',
            'usmernenie' => 'Usmernenie',
            'strategia' => 'Stratégia',
            'program' => 'Program',
            'plan' => 'Plán',
            'koncepcia' => 'Koncepcia',
            'medzinarodny-dohovor' => 'Medzinárodný dohovor',
            'metodicky-pokyn' => 'Metodický pokyn',
            //        'ine' => 'Iné',
        ];

        $levels = [
            'narodna' => 'Narodná',
            'medzinarodna' => 'Medzinarodná',
        ];


        foreach ($cat as $k => $n) {
            $e = new DocumentCategory();
            $e->setName($n)->setSeoName($k)->setPublic(true)->setVisibility(true);
            $manager->persist($e);
        }

        foreach ($dom as $k => $n) {
            $e = new DocumentDomain();
            $e->setName($n)->setSeoName($k)->setPublic(true)->setVisibility(true);
            $manager->persist($e);
        }

        foreach ($levels as $k => $n) {
            $e = new DocumentLevel();
            $e->setName($n)->setSeoName($k)->setPublic(true)->setVisibility(false);
            $manager->persist($e);
        }

        $manager->flush();


        $articleCategories = [
            'novinky' => 'Novinky',
            'publikacie' => 'Publikácie',
            'koncepcia' => 'Koncepcia',
        ];
        foreach ($articleCategories as $k => $n) {
            $e = new ArticleCategory();
            $e->setName($n)->setSeoName($k)->setPublic(true)->setVisibility(false);
            $manager->persist($e);
        }
        $manager->flush();

        $metatags = [
            'Author' => '<meta name="author" content="%s" />',
            'Modified' => '<meta http-equiv="last-modified" content="%s">',
            'Description' => '<meta name="description" content="%s" />',
            'Keywords' => '<meta name="keywords" content="%s" />',
            'og:title' => '<meta property="og:title" content="%s" />',
            'og:type' => '<meta property="og:type" content="%s" />',
            'og:image' => '<meta property="og:image" content="%s" />',
            'og:url' => '<meta property="og:url" content="%s" />',
        ];

        foreach ($metatags as $k => $n) {
            $e = new MetatagPattern();
            $e->setName($k)
                ->setType("head")
                ->setGroup("Metatag")
                ->setSortOrder(999)
                ->setPattern($n)
                ->setVisibility(true);
            $manager->persist($e);
        }
        $manager->flush();

    }

}
