<?php

namespace Score\CmsBundle\DataFixtures;
//namespace App\DataFixtures;

use Score\CmsBundle\Entity\Form\Webform;
use Score\CmsBundle\Entity\Form\WebformField;
use Score\CmsBundle\Entity\Menu;
use Score\CmsBundle\Entity\Page\Page;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Score\CmsBundle\Entity\Block\Block;
use Score\CmsBundle\Entity\Article\Article;
use Score\CmsBundle\Entity\Block\Accordion;
use Score\CmsBundle\Entity\Block\AccordionItem;
use Score\CmsBundle\Entity\Event\Event;
use Score\CmsBundle\Entity\Gallery\Gallery;
use Score\CmsBundle\Entity\Option;
use Score\CmsBundle\Entity\OptionGroup;

class WavexFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        return;
        $blocks = [];
        $blocks["about"] = '  
        <section class="about" id="about">
        <div class="container">
          <div class="row">
            <div class="col-md-12 ">
              <div class="title animate fadeIn">
                <h1 class="light">We are WaveX</h1>
                <br/>
                <h1>We Design your Future</h1>
                <p class="title-detail">Lorem Ipsum is simply dummy text of the printing and typesetting</p>
              </div>
            </div>
        </div>
            <div class="features row">
              <div class="col-md-3 animate fadeIn" data-delay="500"> <i class="fa fa-rocket hovicon effect-1 sub-a"></i>
                <h4>Lorem Ipsum </h4>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting</p>
              </div>
              <div class="col-md-3 animate fadeIn" data-delay="1000"> <i class="fa fa-flask hovicon effect-1 sub-a"></i>
                <h4>Lorem Ipsum </h4>
                <p >Lorem Ipsum is simply dummy text of the printing and typesetting</p>
              </div>
              <div class="col-md-3 animate fadeIn" data-delay="1500"> <i class="fa fa-envelope hovicon effect-1 sub-a"></i>
                <h4>Lorem Ipsum </h4>
                <p >Lorem Ipsum is simply dummy text of the printing and typesetting</p>
              </div>
              <div class="col-md-3 animate fadeIn" data-delay="2000"> <i class="fa fa-coffee hovicon effect-1 sub-a"></i>
                <h4>Lorem Ipsum </h4>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting</p>
              </div>
            </div>
          </div>
  </section>        ';

  $blocks["experties"] = '<section>
  <div class="experties">
          <div class="row">
            <div class="col-md-12">
            <div class="animate fadeIn">
                <h2>Our Experties</h2>
                <p class="title-detail">Lorem Ipsum is simply dummy text of the printing and typesetting</p>
              </div>
            <div class="skills">
            
              
              <div class="skillbar clearfix" data-percent="60%">
                <div class="skillbar-title"><span>HTML5</span></div>
                <div class="skillbar-bar"></div>
                <div class="skill-bar-percent animate pulse" data-delay="500">60%</div>
              </div>
              <!-- End Skill Bar -->
              
              <div class="skillbar clearfix " data-percent="88%">
                <div class="skillbar-title dark-title"><span>CSS3</span></div>
                <div class="skillbar-bar bg-dark"></div>
                <div class="skill-bar-percent gray animate pulse" data-delay="1000">88%</div>
              </div>
              <!-- End Skill Bar -->
              
              <div class="skillbar clearfix " data-percent="95%">
                <div class="skillbar-title"><span>jQuery</span></div>
                <div class="skillbar-bar"></div>
                <div class="skill-bar-percent animate pulse" data-delay="1500">95%</div>
              </div>
              <!-- End Skill Bar -->
              
              <div class="skillbar clearfix " data-percent="74%">
                <div class="skillbar-title dark-title"><span>WordPress</span></div>
                <div class="skillbar-bar bg-dark"></div>
                <div class="skill-bar-percent gray animate pulse" data-delay="2000">74%</div>
              </div>
              <!-- End Skill Bar --> 
              </div>
              
            </div>
          </div>
        </div>
      </section>';
  $blocks["team"] = '<!-- BEGIN TEAM CONTAINER -->
  <section class="team" id="team">
      <div class="container text-center">
          <!-- BEGIN HEADING -->
          <div class="title animate fadeIn">
            <h2 class="light">Yes We Are</h2>
            <h3>Creative Team</h3>
            <p class="title-detail">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque ultricies vestibulum.</p>
          </div>
          <!-- END HEADING -->
          <div class="row">
              <div class="col-md-6 animate fadeInLeftBig" data-delay="100">
                  <!-- BEGIN TEAM -->
                 <div class="team-sec team-sec1">
                         <div class="member"><img src="https://randomuser.me/api/portraits/men/75.jpg" alt=""></div>
                      <div class="detail-left">
                          <h4>John Smith</h4>
                          <strong>Creative Director</strong>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. of Quisque ultricies vestibulum molestie. Graphic Design the power of Suspendisse ultricies Graphic Design the power of Suspendisse potenti</p>
                          <div class="social-icons">
                              <a href="#." class="fb"><i class="fa fa-facebook"></i><span class="visually-hidden">facebook</span></a> 
                              <a href="#." class="tw"><i class="fa fa-twitter"></i><span class="visually-hidden">twitter</span></a>
                              <a href="#." class="it"><i class="fa fa-instagram"></i><span class="visually-hidden">instagram</span></a>
                          </div>
                      </div>
                 <div class="clearfix"></div>
                 </div>
                 
                 
                 
              </div>
              <!-- END TEAM -->
              
              <div class="col-md-6 animate fadeInRightBig" data-delay="100">
                  <!-- BEGIN TEAM -->
                 <div class="team-sec team2">
                         <div class="member"><img src="https://randomuser.me/api/portraits/women/71.jpg" alt=""></div>
                      <div class="detail-left">
                          <h4>Sofia Hales</h4>
                          <strong>Head of Engineering</strong>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. of Quisque ultricies vestibulum molestie. Graphic Design the power of Suspendisse ultricies Graphic Design the power of Suspendisse potenti</p>
                          <div class="social-icons">
                              <a href="#." class="fb"><i class="fa fa-facebook"></i><span class="visually-hidden">facebook</span></a> 
                              <a href="#." class="tw"><i class="fa fa-twitter"></i><span class="visually-hidden">twitter</span></a>
                              <a href="#." class="it"><i class="fa fa-instagram"></i><span class="visually-hidden">instagram</span></a>
                          </div>
                      </div>
                 <div class="clearfix"></div>
                 </div>
                 
                 
                 
              </div>
              
              <div class="col-md-6 animate fadeInLeftBig" data-delay="300">
                  <!-- BEGIN TEAM -->
                 <div class="team-sec team-sec2  team-sec1">
                         <div class="member"><img src="https://randomuser.me/api/portraits/men/73.jpg" alt=""></div>
                      <div class="detail-left">
                          <h4>John Doe</h4>
                          <strong>Web Designer</strong>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. of Quisque ultricies vestibulum molestie. Graphic Design the power of Suspendisse ultricies Graphic Design the power of Suspendisse potenti</p>
                          <div class="social-icons">
                              <a href="#." class="fb"><i class="fa fa-facebook"></i><span class="visually-hidden">facebook</span></a> 
                              <a href="#." class="tw"><i class="fa fa-twitter"></i><span class="visually-hidden">twitter</span></a>
                              <a href="#." class="it"><i class="fa fa-instagram"></i><span class="visually-hidden">instagram</span></a>
                          </div>
                      </div>
                 <div class="clearfix"></div>
                 </div>
                 
                 
                 
              </div>
              
              <div class="col-md-6 animate fadeInRightBig" data-delay="300">
                  <!-- BEGIN TEAM -->
                 <div class="team-sec team-sec2 team2">
                         <div class="member"><img src="https://randomuser.me/api/portraits/men/74.jpg" alt=""></div>
                      <div class="detail-left">
                          <h4>Elebana Nono</h4>
                          <strong>Web Developer</strong>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. of Quisque ultricies vestibulum molestie. Graphic Design the power of Suspendisse ultricies Graphic Design the power of Suspendisse potenti</p>
                          <div class="social-icons">
                              <a href="#." class="fb"><i class="fa fa-facebook"></i><span class="visually-hidden">facebook</span></a> 
                              <a href="#." class="tw"><i class="fa fa-twitter"></i><span class="visually-hidden">twitter</span></a>
                              <a href="#." class="it"><i class="fa fa-instagram"></i><span class="visually-hidden">instagram</span></a>
                          </div>
                      </div>
                 <div class="clearfix"></div>
                 </div>
                 
                 
                 
              </div>
              
         </div>
      </div>
  </section>
  <!-- END TEAM CONTAINER -->';
  $blocks["counters"] = '<section class="counters">
   <!-- <div class="patteren"></div> -->
  <div id="intro">
    <div class="container">
      <div class="row" id="counters">
        <div class="col-md-3">
          <div class="counter"> <span class="timer1" data-analyticsid="5551234">120</span>
            <p>projects</p>
          </div>
        </div>
        <div class="col-md-3">
          <div class="counter"> <span class="timer2">161</span>
            <p>total employees</p>
          </div>
        </div>
        <div class="col-md-3">
          <div class="counter"> <span class="timer3" id="article-finished">02</span>
            <p>our offices</p>
          </div>
        </div>
        <div class="col-md-3">
          <div class="counter"> <span class="timer4">212</span>
            <p>clients</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>';
  $blocks["portfolio"] = '<section class="portfolio no-padding-bottom" id="portfolio">
  <div class="container">
    <div class="title animate fadeIn">
      <h2 class="light">Our Work</h2>
      <h3>We Create awesome Stuff</h3>
      <p class="title-detail">Lorem Ipsum is simply dummy text of the printing and typesetting</p>
    </div>
  </div>
  <div  id="filters">
    <ul class="clearfix">
      <li><a href="#" data-filter="*" class="active">
        <h3>All</h3>
        </a></li>
      <li><a href="#" class="fancybox" data-filter=".photos">
        <h3>photos</h3>
        </a></li>
      <li><a href="#" data-filter=".videos">
        <h3>videos</h3>
        </a></li>
    </ul>
  </div>
  <div id="portfolio-items-wrap">
    <div class="portfolio-item one-third column photos">
      <div class="freshdesignweb">
        <div class="image_grid portfolio_4col">
          <ul id="list" class="portfolio_list da-thumbs">
            <li>
              <div class="slideUp"><img src="/bundles/scorecms/wavex/images/portfolio/portfolio3.jpg" alt=""></div>
              <article class="da-animate da-slideFromRight" style="display: block;"> <a class="fancybox photo-icon" href="/bundles/scorecms/wavex/images/portfolio/port-large1.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"> <i class="fa fa-camera-retro"></i>
                <h5>WaveX<br/>
                  <span>Latest Graphic Work</span></h5>
                <span>photos</span> </a></article>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="portfolio-item one-third column videos">
      <div class="freshdesignweb">
        <div class="image_grid portfolio_4col">
          <ul class="portfolio_list da-thumbs">
            <li> <img src="/bundles/scorecms/wavex/images/portfolio/portfolio1.jpg" alt="">
              <article class="da-animate da-slideFromRight" style="display: block;"> <a class="fancybox-media video-icon" href="http://vimeo.com/36031564"><i class="fa fa-video-camera"></i>
                <h5>WaveX<br/>
                  <span>Latest Graphic Work</span></h5>
                <span>photos</span> </a></article>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="portfolio-item one-third column photos">
      <div class="freshdesignweb">
        <div class="image_grid portfolio_4col">
          <ul class="portfolio_list da-thumbs">
            <li> <img src="/bundles/scorecms/wavex/images/portfolio/portfolio2.jpg" alt="">
              <article class="da-animate da-slideFromRight" style="display: block;"> <a class="fancybox photo-icon" href="/bundles/scorecms/wavex/images/portfolio/port-large3.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"> <i class="fa fa-camera-retro"></i>
                <h5>WaveX<br/>
                  <span>Latest Graphic Work</span></h5>
                <span>photos</span> </a></article>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="portfolio-item one-third column videos">
      <div class="freshdesignweb">
        <div class="image_grid portfolio_4col">
          <ul class="portfolio_list da-thumbs">
            <li> <img src="/bundles/scorecms/wavex/images/portfolio/portfolio4.jpg" alt="">
              <article class="da-animate da-slideFromRight" style="display: block;"> <a class="fancybox-media video-icon" href="http://vimeo.com/36031564"><i class="fa fa-video-camera"></i>
                <h5>WaveX<br/>
                  <span>Latest Graphic Work</span></h5>
                <span>photos</span> </a></article>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="portfolio-item one-third column videos">
      <div class="freshdesignweb">
        <div class="image_grid portfolio_4col">
          <ul class="portfolio_list da-thumbs">
            <li> <img src="/bundles/scorecms/wavex/images/portfolio/portfolio5.jpg" alt="">
              <article class="da-animate da-slideFromRight" style="display: block;"> <a class="fancybox-media video-icon" href="http://vimeo.com/36031564"><i class="fa fa-video-camera"></i>
                <h5>WaveX<br/>
                  <span>Latest Graphic Work</span></h5>
                <span>photos</span> </a></article>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="portfolio-item one-third column photos">
      <div class="freshdesignweb">
        <div class="image_grid portfolio_4col">
          <ul class="portfolio_list da-thumbs">
            <li> <img src="/bundles/scorecms/wavex/images/portfolio/portfolio6.jpg" alt="">
              <article class="da-animate da-slideFromRight" style="display: block;"> <a class="fancybox photo-icon" href="/bundles/scorecms/wavex/images/portfolio/port-large6.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"> <i class="fa fa-camera-retro"></i>
                <h5>WaveX<br/>
                  <span>Latest Graphic Work</span></h5>
                <span>photos</span> </a></article>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="portfolio-item one-third column videos">
      <div class="freshdesignweb">
        <div class="image_grid portfolio_4col">
          <ul class="portfolio_list da-thumbs">
            <li> <img src="/bundles/scorecms/wavex/images/portfolio/portfolio7.jpg" alt="">
              <article class="da-animate da-slideFromRight" style="display: block;"> <a class="fancybox-media video-icon" href="http://vimeo.com/36031564"><i class="fa fa-video-camera"></i>
                <h5>WaveX<br/>
                  <span>Latest Graphic Work</span></h5>
                <span>photos</span> </a></article>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="portfolio-item one-third column photos">
      <div class="freshdesignweb">
        <div class="image_grid portfolio_4col">
          <ul class="portfolio_list da-thumbs">
            <li> <img src="/bundles/scorecms/wavex/images/portfolio/portfolio8.jpg" alt="">
              <article class="da-animate da-slideFromRight" style="display: block;"> <a class="fancybox photo-icon" href="/bundles/scorecms/wavex/images/portfolio/port-large8.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"> <i class="fa fa-camera-retro"></i>
                <h5>WaveX<br/>
                  <span>Latest Graphic Work</span></h5>
                <span>photos</span> </a></article>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>';
  $blocks["img_devices"] = '<section class="latest-pro">
  <div class="row">
    <div class="col-md-12">
      <div class=" animate fadeIn">
        <h2>Featured Work</h2>
        <p class="title-detail">Lorem Ipsum is simply dummy text of the printing and typesetting</p>
      </div>
      <img class="devices animate bounceInUp" src="/bundles/scorecms/wavex/images/devices.png" alt="" > </div>
  </div>
</section>';
  $blocks["prices"] = '<section class="pricing">
  <div class="container">
    <div class="row">
      <div class="title animate fadeIn">
        <h2 class="light">Our Pricing</h2>
        <h3>Check Our Latest Rates</h3>
        <p class="title-detail">Lorem Ipsum is simply dummy text of the printing and typesetting</p>
      </div>
      <div class="col-md-4">
        <div class="table animate fadeInLeftBig" data-delay="500">
          <h2>Basic</h2>
          <div class="price"> <span><span class="doller">$</span>25 <span class="month">/mon.</span></span> </div>
          <ul>
            <li>50MB Monthly Traffic</li>
            <li>3 Email Account</li>
            <li><i class="fa fa-times"></i></li>
            <li>Theme Updates</li>
            <li>Layered Photoshop Files</li>
          </ul>
          <a href="#.">Sign Up Now</a> </div>
      </div>
      <div class="col-md-4">
        <div class="table blue-table animate fadeIn" data-delay="2000">
          <h2>Standard</h2>
          <div class="price"> <span><span class="doller">$</span>45 <span class="month">/mon.</span></span> </div>
          <ul>
            <li>150MB Monthly Traffic</li>
            <li>10 Email Account</li>
            <li><i class="fa fa-check"></i></li>
            <li>Theme Updates</li>
            <li>Layered Photoshop Files</li>
          </ul>
          <a href="#.">Sign Up Now</a> </div>
      </div>
      <div class="col-md-4">
        <div class="table animate fadeInRightBig" data-delay="1000">
          <h2>Premium</h2>
          <div class="price"> <span><span class="doller">$</span>75 <span class="month">/mon.</span></span> </div>
          <ul>
            <li>300MB Monthly Traffic</li>
            <li>15 Email Account</li>
            <li><i class="fa fa-check"></i></li>
            <li>Theme Updates</li>
            <li>Layered Photoshop Files</li>
          </ul>
          <a href="#.">Sign Up Now</a> </div>
      </div>
    </div>
  </div>
</section>';
  $blocks["steps"] = '<section class="pressroom">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="title animate fadeIn">
					<h2>News & Events</h2>
					<p class="title-detail">Lorem Ipsum is simply dummy text of the printing and typesetting</p>
				</div>
				<div class="space"></div>
				<div class=" container animate fadeIn">

					<div class="row">
						<div class="press-img col-md-6 col-lg-5 order-md-3 text-center">
							<a href="blog.html"><img class="animate fadeIn" src="/bundles/scorecms/wavex/images/press-img1.jpg" alt="text of this image"></a>
						</div>
						<div class="d-none d-lg-block col-lg-2 order-md-2 text-center">
							<img src="/bundles/scorecms/wavex/images/timeline-divider.png" alt="">
						</div>
						<div class="col-md-6 col-lg-5 order-md-1">
							<h4><span class="title"><a href="blog.html">There are many variations</a></span></h4>
							<div class="clear"></div> <span>05/18/2014</span>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting by the readable content of a page when looking at its layout and calling you back. They are very responsive.</p>
							<a href="blog.html">read more</a>
						</div>
					</div>
					<div class="row">
						<div class="press-img col-md-6 col-lg-5 order-md-1 text-center">
							<a href="blog.html"><img class="animate fadeIn" src="/bundles/scorecms/wavex/images/press-img2.jpg" alt="text of this image"></a>
						</div>
						<div class="d-none d-lg-block col-lg-2 order-md-2 text-center">
							<img src="/bundles/scorecms/wavex/images/timeline-divider.png" alt="">
						</div>
						<div class="col-md-6 col-lg-5 order-md-3">
							<h4><span class="title"><a href="blog.html">There are many variations</a></span></h4>
							<div class="clear"></div> <span>05/18/2014</span>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting by the readable content of a page when looking at its layout and calling you back. They are very responsive.</p>
							<a href="blog.html">read more</a>
						</div>
					</div>
					<div class="row">
						<div class="press-img col-md-6 col-lg-5 order-md-3 text-center">
							<a href="blog.html"><img class="animate fadeIn" src="/bundles/scorecms/wavex/images/press-img1.jpg" alt="text of this image"></a>
						</div>
						<div class="d-none d-lg-block col-lg-2 order-md-2 text-center">
							<img src="/bundles/scorecms/wavex/images/timeline-divider.png" alt="">
						</div>
						<div class="col-md-6 col-lg-5 order-md-1">
							<h4><span class="title"><a href="blog.html">There are many variations</a></span></h4>
							<div class="clear"></div> <span>05/18/2014</span>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting by the readable content of a page when looking at its layout and calling you back. They are very responsive.</p>
							<a href="blog.html">read more</a>
						</div>
					</div>

			</div>
		</div>
	</div>
</section>';
  $blocks["contact"] = '
  <section class="contact">
  <div id="fifth">
      <div class="container group">
          <!-- <div class="patteren"></div> -->
          <div class="title animate fadeIn">
              <h2 class="light">Contact Us</h2>
              <h3>Get in Touch with Us</h3>
              <p class="title-detail">Lorem Ipsum is simply dummy text of the printing and typesetting</p>
          </div>
          <div class="row">
              <div class="col-md-12">
                  <div class="form animate bounceInUp">
                      <div data-cms="react-form" data-form-id="contactform"></div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</section>
';
  $blocks["slider2"] = '    <!-- Slider -->
  <section class="slider" style="background-image: url(CMS_TEXT(wavex-slider-2-bg-image-url));">
    <!-- <div class="patteren"></div> -->
    <div class="container fadeInRightBig" data-delay="5000">
      <div class="center flash animated">
        <div id="wavex_cms_slider_2" class="cms-carousel carousel slide" data-bs-ride="carousel" data-bs-interval="3000"> 
          <!-- Previous/Next controls -->
          <button class="carousel-control-prev" type="button" data-bs-target="#wavex_cms_slider_2" data-bs-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Predchádzajúca</span>
          </button>
          <button class="carousel-control-next" type="button" data-bs-target="#wavex_cms_slider_2" data-bs-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Nasledujúca</span>
          </button>
          <!-- Indicators -->
          <div class="carousel-indicators">
              <button type="button" data-bs-target="#wavex_cms_slider_2" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
              <button type="button" data-bs-target="#wavex_cms_slider_2" data-bs-slide-to="1" class="" aria-current="" aria-label="Slide 2"></button>
              <button type="button" data-bs-target="#wavex_cms_slider_2" data-bs-slide-to="2" class="" aria-current="" aria-label="Slide 3"></button>
          </div>
          <div class="carousel-inner banner-detail ">

              <div class="carousel-item  item active">
                <p>CMS_TEXT(wavex-slider-2-slide-1)</p>
              </div>

              <div class="carousel-item  item">
                <p>CMS_TEXT(wavex-slider-2-slide-2)</p>
              </div>

              <div class="carousel-item  item">
                <p>CMS_TEXT(wavex-slider-2-slide-3)</p>
              </div>
          </div>
          <!-- Pause/Play controls -->
          <button class="play-control pause" type="button">
              <span class="visually-hidden">Zastaviť alebo spustiť prehrávanie</span> 
          </button>
        </div>
      </div>
    </div>
  </section>
<!-- Slider End -->';
  $blocks["slider3"] = '    <!-- Slider -->
  <section class="testimonials">
      <!-- <div class="patteren"></div> -->
      <div id="second">

        <div class="container"> 
          <i class="fa fa-twitter"></i>
          <div class="twitter flash animated">

          <div id="wavex_cms_slider_3" class="cms-carousel carousel slide" data-bs-ride="carousel" data-bs-interval="3000"> 
            <!-- Previous/Next controls -->
            <button class="carousel-control-prev" type="button" data-bs-target="#wavex_cms_slider_3" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Predchádzajúca</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#wavex_cms_slider_3" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Nasledujúca</span>
            </button>
            <!-- Indicators -->
            <div class="carousel-indicators">
                <button type="button" data-bs-target="#wavex_cms_slider_3" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                <button type="button" data-bs-target="#wavex_cms_slider_3" data-bs-slide-to="1" class="" aria-current="" aria-label="Slide 2"></button>
                <button type="button" data-bs-target="#wavex_cms_slider_3" data-bs-slide-to="2" class="" aria-current="" aria-label="Slide 3"></button>
            </div>
            <div class="carousel-inner banner-detail ">

                <div class="carousel-item  item active">
                  <p>CMS_TEXT(wavex-slider-3-slide-1)</p>
                </div>

                <div class="carousel-item  item">
                  <p>CMS_TEXT(wavex-slider-3-slide-2)</p>
                </div>

                <div class="carousel-item  item">
                  <p>CMS_TEXT(wavex-slider-3-slide-3)</p>
                </div>
            </div>
            <!-- Pause/Play controls -->
            <button class="play-control pause" type="button">
                <span class="visually-hidden">Zastaviť alebo spustiť prehrávanie</span> 
            </button>
          </div>
        </div>
        </div>
      </div>
    </section>
  <!-- Slider End --> 
';
//   $blocks["slider4"] = '<!-- Slider -->
//   <section class="sponsors">
//       <div class="patteren"></div>
//       <div id="third">
//           <div class="container">
//               <div class="row">
//                   <div class="col-md-12">
//                       <div class="space"></div>
//                       <h1 class="light">Our&nbsp;Clients</h1>
//                       <br/>
//                       <h1 class="sponsor ">We Value The Quality First
//                       </h1>
  
//               <ol class="carousel-indicators spons">
//                               <li data-bs-target="#wavex_cms_slider_4" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"><img class="pro-logo" src="/bundles/scorecms/wavex/images/bcaus.png" alt=""></li>
//                               <li data-bs-target="#wavex_cms_slider_4" data-bs-slide-to="1" aria-current="" aria-label="Slide 2"><img class="pro-logo" src="/bundles/scorecms/wavex/images/ratings.png" alt=""></li>
//                               <li data-bs-target="#wavex_cms_slider_4" data-bs-slide-to="2" aria-current="" aria-label="Slide 3"><img class="pro-logo" src="/bundles/scorecms/wavex/images/bcaus.png" alt=""></li>
//                               <li data-bs-target="#wavex_cms_slider_4" data-bs-slide-to="3" aria-current="" aria-label="Slide 4"><img class="pro-logo" src="/bundles/scorecms/wavex/images/ratings.png" alt=""></li>
//                           </ol>
  
//                           <div class="carousel-inner spon-detail">
  
//                               <div class="carousel-item  item active">
//                                   <p>CMS_TEXT(wavex-slider-2-slide-1)</p>
//                               </div>
  
//                               <div class="carousel-item  item">
//                                   <p>CMS_TEXT(wavex-slider-2-slide-2)</p>
//                               </div>
  
//                               <div class="carousel-item  item">
//                                   <p>CMS_TEXT(wavex-slider-2-slide-3)</p>
//                               </div>
  
//                               <div class="carousel-item  item">
//                                   <p>CMS_TEXT(wavex-slider-2-slide-4)</p>
//                               </div>
//                           </div>
//                           <!-- Previous/Next controls -->
//                           <button class="carousel-control-prev" type="button" data-bs-target="#wavex_cms_slider_4" data-bs-slide="prev">
//                               <span class="carousel-control-prev-icon" aria-hidden="true"></span>
//                               <span class="visually-hidden">Predchádzajúca</span>
//                           </button>
//                           <button class="carousel-control-next" type="button" data-bs-target="#wavex_cms_slider_4" data-bs-slide="next">
//                               <span class="carousel-control-next-icon" aria-hidden="true"></span>
//                               <span class="visually-hidden">Nasledujúca</span>
//                           </button>
//                           <button class="play-control pause" type="button">
//                               <span class="visually-hidden">Zastaviť alebo spustiť prehrávanie</span>
//                           </button>
//                       </div>
  
  
//                   </div>
//               </div>
//           </div>
//       </div>
//   </section>
//   <!-- Slider End -->';


        $options = [
            'wavex-banner-image-url' => '/bundles/scorecms/wavex/images/banner.jpg', // http://www.themesindustry.com/html/wavex/images/banner2.jpg
            'wavex-slider-2-bg-image-url' => 'http://www.themesindustry.com/html/wavex/images/banner2.jpg',
            'wavex-nav-logo' => '<img src="/bundles/scorecms/wavex/images/logo.png" alt="logo">',
        ];
        $carouselText = "Lorem ipsum dolor sit elit. <strong> Pariatur %d </strong> neque consectetur fugit quidem.";

        foreach ([1,2,3,4,5] as $i) {
          $options["wavex-banner-slide-$i"] = sprintf($carouselText, $i);
        }
        foreach ([1,2,3] as $i) {
          $options["wavex-slider-2-slide-$i"] =  sprintf($carouselText, $i);
          $options["wavex-slider-3-slide-$i"] =  sprintf($carouselText, $i);
        }

        $optionGroup = new OptionGroup();
        $optionGroup->setName("Wavex – Default");
        foreach ($options as $k => $o) {
          $option = new Option();
          $option->setCode($k)
            ->setName(ucfirst(str_replace("-", " ", $k)))
            ->setValue($o);
          $manager->persist($option);
          $optionGroup->addOption($option);
        }
        $manager->persist($optionGroup);

        foreach ($blocks as $k => $b) {
          $block = new Block();
          $block->setName("Wavex " . ucfirst($k))
            ->setParamsArr(["nav" =>  ucfirst($k)])
            ->setType('html')
            ->setContent($b);
          $manager->persist($block);
      }

      $manager->flush();
      
    }
}
